/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class CartaTO {
    private int codi_cart;
    private String nume_expe;
    private int corr_hist;
    private int nude_hist;
    private String fech_cart;
    private int codi_usua;
    private String obsv_cart;

    public CartaTO() {
    }

    public int getCodi_cart() {
        return codi_cart;
    }

    public void setCodi_cart(int codi_cart) {
        this.codi_cart = codi_cart;
    }

    public String getNume_expe() {
        return nume_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }

    public int getCorr_hist() {
        return corr_hist;
    }

    public void setCorr_hist(int corr_hist) {
        this.corr_hist = corr_hist;
    }

    public int getNude_hist() {
        return nude_hist;
    }

    public void setNude_hist(int nude_hist) {
        this.nude_hist = nude_hist;
    }

    public String getFech_cart() {
        return fech_cart;
    }

    public void setFech_cart(String fech_cart) {
        this.fech_cart = fech_cart;
    }

    public int getCodi_usua() {
        return codi_usua;
    }

    public void setCodi_usua(int codi_usua) {
        this.codi_usua = codi_usua;
    }

    public String getObsv_cart() {
        return obsv_cart;
    }

    public void setObsv_cart(String obsv_cart) {
        this.obsv_cart = obsv_cart;
    }
    
    
}
