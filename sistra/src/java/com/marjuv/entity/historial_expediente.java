
package com.marjuv.entity;

import java.sql.Date;

public class historial_expediente {
    
    private int corr_hist; 
    private int nude_hist;
    private String nume_expe;
    private Date fepr_hist;
    private int nufo_hist;
    private Date fere_hist;
    private Date fesa_hist;
    private String nupr_hist;
    private String repr_hist;
    private String obsv_hist;
    private int usen_hist;
    private int esta_hist;
    private int caen_hist;
    private int coen_hist;
    private int enen_hist;
    private int care_hist;
    private int core_hist;
    private int enre_hist;
    private int enti_hist;
    private int reti_hist;
    private Date fell_hist;
    private boolean copi_hist;
    private int condicion;

    public int getCorr_hist() {
        return corr_hist;
    }

    public void setCorr_hist(int corr_hist) {
        this.corr_hist = corr_hist;
    }

    public int getNude_hist() {
        return nude_hist;
    }

    public void setNude_hist(int nude_hist) {
        this.nude_hist = nude_hist;
    }

    public String getNume_expe() {
        return nume_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }

    public Date getFepr_hist() {
        return fepr_hist;
    }

    public void setFepr_hist(Date fepr_hist) {
        this.fepr_hist = fepr_hist;
    }

    public int getNufo_hist() {
        return nufo_hist;
    }

    public void setNufo_hist(int nufo_hist) {
        this.nufo_hist = nufo_hist;
    }

    public Date getFesa_hist() {
        return fesa_hist;
    }

    public void setFesa_hist(Date fesa_hist) {
        this.fesa_hist = fesa_hist;
    }

    public String getNupr_hist() {
        return nupr_hist;
    }

    public void setNupr_hist(String nupr_hist) {
        this.nupr_hist = nupr_hist;
    }

    public String getRepr_hist() {
        return repr_hist;
    }

    public void setRepr_hist(String repr_hist) {
        this.repr_hist = repr_hist;
    }

    public String getObsv_hist() {
        return obsv_hist;
    }

    public void setObsv_hist(String obsv_hist) {
        this.obsv_hist = obsv_hist;
    }

    public int getUsen_hist() {
        return usen_hist;
    }

    public void setUsen_hist(int usen_hist) {
        this.usen_hist = usen_hist;
    }

    public int getEsta_hist() {
        return esta_hist;
    }

    public void setEsta_hist(int esta_hist) {
        this.esta_hist = esta_hist;
    }

    public int getCaen_hist() {
        return caen_hist;
    }

    public void setCaen_hist(int caen_hist) {
        this.caen_hist = caen_hist;
    }

    public int getCoen_hist() {
        return coen_hist;
    }

    public void setCoen_hist(int coen_hist) {
        this.coen_hist = coen_hist;
    }

    public int getEnen_hist() {
        return enen_hist;
    }

    public void setEnen_hist(int enen_hist) {
        this.enen_hist = enen_hist;
    }

    public int getCare_hist() {
        return care_hist;
    }

    public void setCare_hist(int care_hist) {
        this.care_hist = care_hist;
    }

    public int getCore_hist() {
        return core_hist;
    }

    public void setCore_hist(int core_hist) {
        this.core_hist = core_hist;
    }

    public int getEnre_hist() {
        return enre_hist;
    }

    public void setEnre_hist(int enre_hist) {
        this.enre_hist = enre_hist;
    }

    public Date getFere_hist() {
        return fere_hist;
    }

    public void setFere_hist(Date fere_hist) {
        this.fere_hist = fere_hist;
    }

    public int getEnti_hist() {
        return enti_hist;
    }

    public void setEnti_hist(int enti_hist) {
        this.enti_hist = enti_hist;
    }

    public int getReti_hist() {
        return reti_hist;
    }

    public void setReti_hist(int reti_hist) {
        this.reti_hist = reti_hist;
    }

    public Date getFell_hist() {
        return fell_hist;
    }

    public void setFell_hist(Date fell_hist) {
        this.fell_hist = fell_hist;
    }

    public boolean isCopi_hist() {
        return copi_hist;
    }

    public void setCopi_hist(boolean copi_hist) {
        this.copi_hist = copi_hist;
    }

    public int getCondicion() {
        return condicion;
    }

    public void setCondicion(int condicion) {
        this.condicion = condicion;
    }
    
}
