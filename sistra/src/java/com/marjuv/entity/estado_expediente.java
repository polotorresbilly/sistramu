
package com.marjuv.entity;

public class estado_expediente implements Cloneable {
    
    private int codi_esex;
    private String nomb_esex;
    private String esta_esex;

    public int getCodi_esex() {
        return codi_esex;
    }

    public void setCodi_esex(int codi_esex) {
        this.codi_esex = codi_esex;
    }

    public String getNomb_esex() {
        return nomb_esex;
    }

    public void setNomb_esex(String nomb_esex) {
        this.nomb_esex = nomb_esex;
    }

    public String getEsta_esex() {
        return esta_esex;
    }

    public void setEsta_esex(String esta_esex) {
        this.esta_esex = esta_esex;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
}
