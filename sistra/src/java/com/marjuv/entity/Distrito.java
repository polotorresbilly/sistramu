/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Distrito {
    private int codi_dist;
    private String nomb_dist;
    private Provincia provincia;

    public Distrito(int codi_dist, String nomb_dist, Provincia provincia) {
        this.codi_dist = codi_dist;
        this.nomb_dist = nomb_dist;
        this.provincia = provincia;
    }

    public int getCodi_dist() {
        return codi_dist;
    }

    public void setCodi_dist(int codi_dist) {
        this.codi_dist = codi_dist;
    }

    public String getNomb_dist() {
        return nomb_dist;
    }

    public void setNomb_dist(String nomb_dist) {
        this.nomb_dist = nomb_dist;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    
}
