/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Tipo_Expediente {
    private int codi_tiex;
    private String nomb_tiex;
    private String esta_tiex;

    public Tipo_Expediente() {
    }

    public Tipo_Expediente(int codi_tiex, String nomb_tiex, String esta_tiex) {
        this.codi_tiex = codi_tiex;
        this.nomb_tiex = nomb_tiex;
        this.esta_tiex = esta_tiex;
    }

    
    
    public int getCodi_tiex() {
        return codi_tiex;
    }

    public void setCodi_tiex(int codi_tiex) {
        this.codi_tiex = codi_tiex;
    }

    public String getNomb_tiex() {
        return nomb_tiex;
    }

    public void setNomb_tiex(String nomb_tiex) {
        this.nomb_tiex = nomb_tiex;
    }

    public String getEsta_tiex() {
        return esta_tiex;
    }

    public void setEsta_tiex(String esta_tiex) {
        this.esta_tiex = esta_tiex;
    }
    
    
}
