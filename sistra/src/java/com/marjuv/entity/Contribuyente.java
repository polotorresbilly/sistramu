/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author BILLY
 */
public class Contribuyente {
    private int codi_cont;
    private String nomb_cont;
    private String apel_cont;
    private String fena_cont;
    private Date dfen_cont;
    private int dni_cont;
    private String gene_cont;
    private String celu_cont;
    private String telf_cont;
    private String dire_cont;
    private String emai_cont;
    private String esta_cont;
    private Distrito obj_dist;
    
    private String nomb_lugar;
    private String apellidos_nombres;

    public String getApellidos_nombres() {
        return getApel_cont()+" "+getNomb_cont();
    }       

    public void setApellidos_nombres(String apellidos_nombres) {
        this.apellidos_nombres = apellidos_nombres;
    }

    public Date getDfen_cont() {
        
        return dfen_cont;
    }

    public void setDfen_cont(Date dfen_cont) {
        this.dfen_cont = dfen_cont;
    }

    public String getNomb_lugar() {
        nomb_lugar = obj_dist.getNomb_dist() + " / " + obj_dist.getProvincia().getNomb_prov()+" / "+obj_dist.getProvincia().getDepartamento().getNomb_depa();
        return nomb_lugar;
    }

    public void setNomb_lugar(String nomb_lugar) {
        this.nomb_lugar = nomb_lugar;
    }

    public Contribuyente() {
    }

    public String getEsta_cont() {
        return esta_cont;
    }

    public void setEsta_cont(String esta_cont) {
        this.esta_cont = esta_cont;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public String getNomb_cont() {
        return nomb_cont;
    }

    public void setNomb_cont(String nomb_cont) {
        this.nomb_cont = nomb_cont;
    }

    public String getApel_cont() {
        return apel_cont;
    }

    public void setApel_cont(String apel_cont) {
        this.apel_cont = apel_cont;
    }

    public String getFena_cont() {
        return fena_cont;
    }

    public void setFena_cont(String fena_cont) {
        this.fena_cont = fena_cont;
    }

    public int getDni_cont() {
        return dni_cont;
    }

    public void setDni_cont(int dni_cont) {
        this.dni_cont = dni_cont;
    }

    public String getGene_cont() {
        return gene_cont;
    }

    public void setGene_cont(String gene_cont) {
        this.gene_cont = gene_cont;
    }

    public String getCelu_cont() {
        return celu_cont;
    }

    public void setCelu_cont(String celu_cont) {
        this.celu_cont = celu_cont;
    }

    public String getTelf_cont() {
        return telf_cont;
    }

    public void setTelf_cont(String telf_cont) {
        this.telf_cont = telf_cont;
    }

    public String getDire_cont() {
        return dire_cont;
    }

    public void setDire_cont(String dire_cont) {
        this.dire_cont = dire_cont;
    }

    public String getEmai_cont() {
        return emai_cont;
    }

    public void setEmai_cont(String emai_cont) {
        this.emai_cont = emai_cont;
    }

    public Distrito getObj_dist() {
        return obj_dist;
    }

    public void setObj_dist(Distrito obj_dist) {
        this.obj_dist = obj_dist;
    }
    
    
}
