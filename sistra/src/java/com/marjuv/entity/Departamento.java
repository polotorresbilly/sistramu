/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Departamento {
    private int codi_depa;
    private String nomb_depa;

    public Departamento(int codi_depa, String nomb_depa) {
        this.codi_depa = codi_depa;
        this.nomb_depa = nomb_depa;
    }

    public int getCodi_depa() {
        return codi_depa;
    }

    public void setCodi_depa(int codi_depa) {
        this.codi_depa = codi_depa;
    }

    public String getNomb_depa() {
        return nomb_depa;
    }

    public void setNomb_depa(String nomb_depa) {
        this.nomb_depa = nomb_depa;
    }
    
    
}
