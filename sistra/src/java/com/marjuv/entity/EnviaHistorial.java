/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class EnviaHistorial {
    private int enviaEstado;
    private String nombEstado;

    public EnviaHistorial(int enviaEstado, String nombEstado) {
        this.enviaEstado = enviaEstado;
        this.nombEstado = nombEstado;
    }
    
    public int getEnviaEstado() {
        return enviaEstado;
    }

    public void setEnviaEstado(int enviaEstado) {
        this.enviaEstado = enviaEstado;
    }

    public String getNombEstado() {
        return nombEstado;
    }

    public void setNombEstado(String nombEstado) {
        this.nombEstado = nombEstado;
    }
    
    
}
