package com.marjuv.entity;

public class DependenciaTO {
    
    private int codi_arde;
    private String nomb_arde;
    private String abre_arde;
    private String esta_arde;
    private int jera_arde;
    private int codi_enti;

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public String getAbre_arde() {
        return abre_arde;
    }

    public void setAbre_arde(String abre_arde) {
        this.abre_arde = abre_arde;
    }

    public String getEsta_arde() {
        return esta_arde;
    }

    public void setEsta_arde(String esta_arde) {
        this.esta_arde = esta_arde;
    }

    

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public int getJera_arde() {
        return jera_arde;
    }

    public void setJera_arde(int jera_arde) {
        this.jera_arde = jera_arde;
    }
    
    
    
    
}
