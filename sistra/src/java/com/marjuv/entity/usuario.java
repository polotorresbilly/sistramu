
package com.marjuv.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class usuario {
    
    private int codi_usua;
    private int codi_arde;
    private String nomb_usua;
    private String apel_usua;
    private int dni_usua;
    private String nume_usua;
    private Date fere_usua;
    private String nick_usua;
    private String pass_usua;
    private String esta_usua;
    private String noms_usua;
    private int codi_rous;
    private int codi_carg;
    private List<permiso> permisos;

    public int getCodi_usua() {
        return codi_usua;
    }

    public void setCodi_usua(int codi_usua) {
        this.codi_usua = codi_usua;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public String getNomb_usua() {
        return nomb_usua;
    }

    public void setNomb_usua(String nomb_usua) {
        this.nomb_usua = nomb_usua;
    }

    public String getApel_usua() {
        return apel_usua;
    }

    public void setApel_usua(String apel_usua) {
        this.apel_usua = apel_usua;
    }

    public int getDni_usua() {
        return dni_usua;
    }

    public void setDni_usua(int dni_usua) {
        this.dni_usua = dni_usua;
    }

    public Date getFere_usua() {
        return fere_usua;
    }

    public void setFere_usua(Date fere_usua) {
        this.fere_usua = fere_usua;
    }

    public String getNick_usua() {
        return nick_usua;
    }

    public void setNick_usua(String nick_usua) {
        this.nick_usua = nick_usua;
    }

    public String getPass_usua() {
        return pass_usua;
    }

    public void setPass_usua(String pass_usua) {
        this.pass_usua = pass_usua;
    }

    public String getEsta_usua() {
        return esta_usua;
    }

    public void setEsta_usua(String esta_usua) {
        this.esta_usua = esta_usua;
    }

    public int getCodi_rous() {
        return codi_rous;
    }

    public void setCodi_rous(int codi_rous) {
        this.codi_rous = codi_rous;
    }

    public List<permiso> getPermisos() {
        return permisos;
    }

    public void setPermisos(List<permiso> permisos) {
        this.permisos = permisos;
    }

    public String getNume_usua() {
        return nume_usua;
    }

    public void setNume_usua(String nume_usua) {
        this.nume_usua = nume_usua;
    }

    public String getNoms_usua() {
        return noms_usua;
    }

    public void setNoms_usua(String noms_usua) {
        this.noms_usua = noms_usua;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }
}
