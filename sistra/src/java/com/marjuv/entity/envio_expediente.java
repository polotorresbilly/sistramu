
package com.marjuv.entity;

public class envio_expediente implements Cloneable {
    
    private int codi_enex;
    private String desc_enex;
    private String esta_enex;

    public int getCodi_enex() {
        return codi_enex;
    }

    public void setCodi_enex(int codi_enex) {
        this.codi_enex = codi_enex;
    }

    public String getDesc_enex() {
        return desc_enex;
    }

    public void setDesc_enex(String desc_enex) {
        this.desc_enex = desc_enex;
    }

    public String getEsta_enex() {
        return esta_enex;
    }

    public void setEsta_enex(String esta_enex) {
        this.esta_enex = esta_enex;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
}
