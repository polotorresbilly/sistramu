package com.marjuv.entity;

public class SubDependenciaTO {
    
    private int codi_sude;
    private String desc_sude;
    private String esta_sude;
    private int codi_arde;

    public int getCodi_sude() {
        return codi_sude;
    }

    public void setCodi_sude(int codi_sude) {
        this.codi_sude = codi_sude;
    }

    public String getDesc_sude() {
        return desc_sude;
    }

    public void setDesc_sude(String desc_sude) {
        this.desc_sude = desc_sude;
    }

    public String getEsta_sude() {
        return esta_sude;
    }

    public void setEsta_sude(String esta_sude) {
        this.esta_sude = esta_sude;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }
    
    
}
