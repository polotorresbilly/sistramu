
package com.marjuv.entity;

public class rol {
    private int codi_rous;
    private String esta_rol;
    private String nomb_rol;

    public int getCodi_rous() {
        return codi_rous;
    }

    public void setCodi_rous(int codi_rous) {
        this.codi_rous = codi_rous;
    }

    public String getEsta_rol() {
        return esta_rol;
    }

    public void setEsta_rol(String esta_rol) {
        this.esta_rol = esta_rol;
    }

    public String getNomb_rol() {
        return nomb_rol;
    }

    public void setNomb_rol(String nomb_rol) {
        this.nomb_rol = nomb_rol;
    }
}
