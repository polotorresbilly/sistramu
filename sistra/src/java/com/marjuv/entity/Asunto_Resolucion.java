/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Asunto_Resolucion {
    private int codi_asre;
    private String desc_asre;

    public Asunto_Resolucion() {
    }

    public int getCodi_asre() {
        return codi_asre;
    }

    public void setCodi_asre(int codi_asre) {
        this.codi_asre = codi_asre;
    }

    public String getDesc_asre() {
        return desc_asre;
    }

    public void setDesc_asre(String desc_asre) {
        this.desc_asre = desc_asre;
    }
    
}
