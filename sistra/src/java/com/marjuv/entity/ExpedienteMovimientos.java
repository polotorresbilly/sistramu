/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class ExpedienteMovimientos {
    private int MOVIMIENTO;
    private int NRODESTINO;
    private String ESTADO;
    private String LLEGO;
    private String RECEPCIONO;
    private String TRAMITO;
    private int NROFOLIO;
    private String REMITENTE;
    private String DESTINATARIO;
    private String PROVEIDO;
    private String DOC;
    private String OBSERVACION;
    private String nume_reso;

    public ExpedienteMovimientos() {
    }

    public String getNume_reso() {
        return nume_reso;
    }

    public void setNume_reso(String nume_reso) {
        this.nume_reso = nume_reso;
    }
    
    

    public int getMOVIMIENTO() {
        return MOVIMIENTO;
    }

    public void setMOVIMIENTO(int MOVIMIENTO) {
        this.MOVIMIENTO = MOVIMIENTO;
    }

    public int getNRODESTINO() {
        return NRODESTINO;
    }

    public void setNRODESTINO(int NRODESTINO) {
        this.NRODESTINO = NRODESTINO;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public String getLLEGO() {
        return LLEGO;
    }

    public void setLLEGO(String LLEGO) {
        this.LLEGO = LLEGO;
    }

    public String getRECEPCIONO() {
        return RECEPCIONO;
    }

    public void setRECEPCIONO(String RECEPCIONO) {
        this.RECEPCIONO = RECEPCIONO;
    }

    public String getTRAMITO() {
        return TRAMITO;
    }

    public void setTRAMITO(String TRAMITO) {
        this.TRAMITO = TRAMITO;
    }

    public int getNROFOLIO() {
        return NROFOLIO;
    }

    public void setNROFOLIO(int NROFOLIO) {
        this.NROFOLIO = NROFOLIO;
    }

    public String getREMITENTE() {
        return REMITENTE;
    }

    public void setREMITENTE(String REMITENTE) {
        this.REMITENTE = REMITENTE;
    }

    public String getDESTINATARIO() {
        return DESTINATARIO;
    }

    public void setDESTINATARIO(String DESTINATARIO) {
        this.DESTINATARIO = DESTINATARIO;
    }

    public String getPROVEIDO() {
        return PROVEIDO;
    }

    public void setPROVEIDO(String PROVEIDO) {
        this.PROVEIDO = PROVEIDO;
    }

    public String getDOC() {
        return DOC;
    }

    public void setDOC(String DOC) {
        this.DOC = DOC;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }
    
    
}
