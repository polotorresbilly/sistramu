
package com.marjuv.entity;

public class rol_usuario {
    private int codi_rous;
    private String esta_rol;
    private String nomb_rol;

    /**
     * @return the codi_rous
     */
    public int getCodi_rous() {
        return codi_rous;
    }

    /**
     * @param codi_rous the codi_rous to set
     */
    public void setCodi_rous(int codi_rous) {
        this.codi_rous = codi_rous;
    }

    /**
     * @return the esta_rol
     */
    public String getEsta_rol() {
        return esta_rol;
    }

    /**
     * @param esta_rol the esta_rol to set
     */
    public void setEsta_rol(String esta_rol) {
        this.esta_rol = esta_rol;
    }

    /**
     * @return the nomb_rol
     */
    public String getNomb_rol() {
        return nomb_rol;
    }

    /**
     * @param nomb_rol the nomb_rol to set
     */
    public void setNomb_rol(String nomb_rol) {
        this.nomb_rol = nomb_rol;
    }
}
