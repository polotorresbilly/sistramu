/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class ExpedienteReportes {
    private String EXPEDIENTE;
    private int NROTRAMITE;
    private int NRODESTINATARIO;
    private int FOLIOS;
    private String DOCUMENTO;
    private String PROCEDIMIENTO;
    private String ADMINISTRADO;
    private String REMITENTE;
    private String PROVEIDO;
    private String DESTINATARIO;
    private String DEPENDENCIA;
    private String FECHA_ENVIO;
    private String FECHA_RECEPCION;
    private String FECHA_TRAMITE;
    private String FECHA_INGRESO;
    private String OBSERVACION;
    private String FINALIZACION;
    private String FECHA_OPCION;

    public String getFINALIZACION() {
        return FINALIZACION;
    }

    public void setFINALIZACION(String FINALIZACION) {
        this.FINALIZACION = FINALIZACION;
    }
    
    

    public String getEXPEDIENTE() {
        return EXPEDIENTE;
    }

    public void setEXPEDIENTE(String EXPEDIENTE) {
        this.EXPEDIENTE = EXPEDIENTE;
    }

    public int getNROTRAMITE() {
        return NROTRAMITE;
    }

    public void setNROTRAMITE(int NROTRAMITE) {
        this.NROTRAMITE = NROTRAMITE;
    }

    public int getNRODESTINATARIO() {
        return NRODESTINATARIO;
    }

    public void setNRODESTINATARIO(int NRODESTINATARIO) {
        this.NRODESTINATARIO = NRODESTINATARIO;
    }

    public int getFOLIOS() {
        return FOLIOS;
    }

    public void setFOLIOS(int FOLIOS) {
        this.FOLIOS = FOLIOS;
    }

    public String getDOCUMENTO() {
        return DOCUMENTO;
    }

    public void setDOCUMENTO(String DOCUMENTO) {
        this.DOCUMENTO = DOCUMENTO;
    }

    public String getPROCEDIMIENTO() {
        return PROCEDIMIENTO;
    }

    public void setPROCEDIMIENTO(String PROCEDIMIENTO) {
        this.PROCEDIMIENTO = PROCEDIMIENTO;
    }

    public String getADMINISTRADO() {
        return ADMINISTRADO;
    }

    public void setADMINISTRADO(String ADMINISTRADO) {
        this.ADMINISTRADO = ADMINISTRADO;
    }

    public String getREMITENTE() {
        return REMITENTE;
    }

    public void setREMITENTE(String REMITENTE) {
        this.REMITENTE = REMITENTE;
    }

    public String getPROVEIDO() {
        return PROVEIDO;
    }

    public void setPROVEIDO(String PROVEIDO) {
        this.PROVEIDO = PROVEIDO;
    }

    public String getDESTINATARIO() {
        return DESTINATARIO;
    }

    public void setDESTINATARIO(String DESTINATARIO) {
        this.DESTINATARIO = DESTINATARIO;
    }

    public String getDEPENDENCIA() {
        return DEPENDENCIA;
    }

    public void setDEPENDENCIA(String DEPENDENCIA) {
        this.DEPENDENCIA = DEPENDENCIA;
    }

    public String getFECHA_ENVIO() {
        return FECHA_ENVIO;
    }

    public void setFECHA_ENVIO(String FECHA_ENVIO) {
        this.FECHA_ENVIO = FECHA_ENVIO;
    }

    public String getFECHA_RECEPCION() {
        return FECHA_RECEPCION;
    }

    public void setFECHA_RECEPCION(String FECHA_RECEPCION) {
        this.FECHA_RECEPCION = FECHA_RECEPCION;
    }

    public String getFECHA_TRAMITE() {
        return FECHA_TRAMITE;
    }

    public void setFECHA_TRAMITE(String FECHA_TRAMITE) {
        this.FECHA_TRAMITE = FECHA_TRAMITE;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }

    public String getFECHA_INGRESO() {
        return FECHA_INGRESO;
    }

    public void setFECHA_INGRESO(String FECHA_INGRESO) {
        this.FECHA_INGRESO = FECHA_INGRESO;
    }

    public String getFECHA_OPCION() {
        return FECHA_OPCION;
    }

    public void setFECHA_OPCION(String FECHA_OPCION) {
        this.FECHA_OPCION = FECHA_OPCION;
    }
    
    
}
