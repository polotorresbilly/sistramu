package com.marjuv.entity;

public class administracion {

    private int codi_admi;
    private int year_admi;
    private String noen_admi;
    private String dien_admi;
    private String teen_admi;
    private String enen_admi;
    private String noal_admi;
    private String noaa_admi;
    private String noge_admi;
    private String nose_admi;
    private String vaui_admi;
    private String noci_admi;
    private String url_file_admin;
    private String url_logo_admin;
    private String url_backup_admin;
    private String nombreBackup;

    public String getUrl_backup_admin() {
        return url_backup_admin;
    }

    public void setUrl_backup_admin(String url_backup_admin) {
        this.url_backup_admin = url_backup_admin;
    }

    public int getCodi_admi() {
        return codi_admi;
    }

    public void setCodi_admi(int codi_admi) {
        this.codi_admi = codi_admi;
    }

    public int getYear_admi() {
        return year_admi;
    }
    
    public void setYear_admi(int year_admi) {
        this.year_admi = year_admi;
    }

    public String getNoen_admi() {
        return noen_admi;
    }

    public void setNoen_admi(String noen_admi) {
        this.noen_admi = noen_admi;
    }

    public String getDien_admi() {
        return dien_admi;
    }

    public void setDien_admi(String dien_admi) {
        this.dien_admi = dien_admi;
    }

    public String getTeen_admi() {
        return teen_admi;
    }

    public void setTeen_admi(String teen_admi) {
        this.teen_admi = teen_admi;
    }

    public String getEnen_admi() {
        return enen_admi;
    }

    public void setEnen_admi(String enen_admi) {
        this.enen_admi = enen_admi;
    }

    public String getNoal_admi() {
        return noal_admi;
    }

    public void setNoal_admi(String noal_admi) {
        this.noal_admi = noal_admi;
    }

    public String getNoaa_admi() {
        return noaa_admi;
    }

    public void setNoaa_admi(String noaa_admi) {
        this.noaa_admi = noaa_admi;
    }

    public String getNoge_admi() {
        return noge_admi;
    }

    public void setNoge_admi(String noge_admi) {
        this.noge_admi = noge_admi;
    }

    public String getNose_admi() {
        return nose_admi;
    }

    public void setNose_admi(String nose_admi) {
        this.nose_admi = nose_admi;
    }

    public String getVaui_admi() {
        return vaui_admi;
    }

    public void setVaui_admi(String vaui_admi) {
        this.vaui_admi = vaui_admi;
    }

    public String getNoci_admi() {
        return noci_admi;
    }

    public void setNoci_admi(String noci_admi) {
        this.noci_admi = noci_admi;
    }

    public String getUrl_file_admin() {
        return url_file_admin;
    }

    public void setUrl_file_admin(String url_file_admin) {
        this.url_file_admin = url_file_admin;
    }

    public String getUrl_logo_admin() {
        return url_logo_admin;
    }

    public void setUrl_logo_admin(String url_logo_admin) {
        this.url_logo_admin = url_logo_admin;
    }

    public String getNombreBackup() {
        return nombreBackup;
    }

    public void setNombreBackup(String nombreBackup) {
        this.nombreBackup = nombreBackup;
    }

}
