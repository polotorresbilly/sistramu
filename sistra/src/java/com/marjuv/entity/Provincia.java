/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Provincia {
    private int codi_prov;
    private String nomb_prov;
    private Departamento departamento;

    public Provincia(int codi_prov, String nomb_prov, Departamento departamento) {
        this.codi_prov = codi_prov;
        this.nomb_prov = nomb_prov;
        this.departamento = departamento;
    }

    public int getCodi_prov() {
        return codi_prov;
    }

    public void setCodi_prov(int codi_prov) {
        this.codi_prov = codi_prov;
    }

    public String getNomb_prov() {
        return nomb_prov;
    }

    public void setNomb_prov(String nomb_prov) {
        this.nomb_prov = nomb_prov;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }
    
    
}
