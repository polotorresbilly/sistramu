package com.marjuv.entity;

import com.marjuv.beans_view.BeanDestinoDis;
import com.marjuv.beans_view.BeanExpediente;
import java.sql.Date;
import java.util.List;

public class resolucion {

    private int codi_reso;
    private String nume_reso;
    private int usre_reso;
    private int usno_reso;
    private int usem_reso;
    private int codi_fire;
    private int codi_aras;
    private int codi_tire;
    private int codi_enre;
    private List<BeanDestinoDis> destinos;
    private List<ExpedienteCliente> expedientes;
    private int hoja_reso;
    private Date feem_reso;
    private Date fesa_reso;
    private String asun_reso;
    private Date fetr_reso;
    private String rubr_reso;
    private String estr_reso;
    private String esfin_reso;
    private String esta_reso;
    private int codi_asre;
    private boolean check;
    private String nomb_arde;
    // Nombre notificador
    private String apno_usno;
    private int dni_usno;
    private String nuno_reso;

    public String getNuno_reso() {
        return nuno_reso;
    }

    public void setNuno_reso(String nuno_reso) {
        this.nuno_reso = nuno_reso;
    }
    
    

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public String getApno_usno() {
        return apno_usno;
    }

    public void setApno_usno(String apno_usno) {
        this.apno_usno = apno_usno;
    }

    public int getDni_usno() {
        return dni_usno;
    }

    public void setDni_usno(int dni_usno) {
        this.dni_usno = dni_usno;
    }
    
    

    public int getCodi_reso() {
        return codi_reso;
    }

    public void setCodi_reso(int codi_reso) {
        this.codi_reso = codi_reso;
    }

    public String getNume_reso() {
        return nume_reso;
    }

    public void setNume_reso(String nume_reso) {
        this.nume_reso = nume_reso;
    }

    public int getUsre_reso() {
        return usre_reso;
    }

    public void setUsre_reso(int usre_reso) {
        this.usre_reso = usre_reso;
    }

    public int getUsno_reso() {
        return usno_reso;
    }

    public void setUsno_reso(int usno_reso) {
        this.usno_reso = usno_reso;
    }

    public int getCodi_fire() {
        return codi_fire;
    }

    public void setCodi_fire(int codi_fire) {
        this.codi_fire = codi_fire;
    }

    public int getCodi_aras() {
        return codi_aras;
    }

    public void setCodi_aras(int codi_aras) {
        this.codi_aras = codi_aras;
    }

    public int getCodi_tire() {
        return codi_tire;
    }

    public void setCodi_tire(int codi_tire) {
        this.codi_tire = codi_tire;
    }

    public int getCodi_enre() {
        return codi_enre;
    }

    public void setCodi_enre(int codi_enre) {
        this.codi_enre = codi_enre;
    }

    public int getHoja_reso() {
        return hoja_reso;
    }

    public void setHoja_reso(int hoja_reso) {
        this.hoja_reso = hoja_reso;
    }

    public Date getFeem_reso() {
        return feem_reso;
    }

    public void setFeem_reso(Date feem_reso) {
        this.feem_reso = feem_reso;
    }

    public String getAsun_reso() {
        return asun_reso;
    }

    public void setAsun_reso(String asun_reso) {
        this.asun_reso = asun_reso;
    }

    public Date getFetr_reso() {
        return fetr_reso;
    }

    public void setFetr_reso(Date fetr_reso) {
        this.fetr_reso = fetr_reso;
    }

    public String getRubr_reso() {
        return rubr_reso;
    }

    public void setRubr_reso(String rubr_reso) {
        this.rubr_reso = rubr_reso;
    }

    public String getEstr_reso() {
        return estr_reso;
    }

    public void setEstr_reso(String estr_reso) {
        this.estr_reso = estr_reso;
    }

    public String getEsfin_reso() {
        return esfin_reso;
    }

    public void setEsfin_reso(String esfin_reso) {
        this.esfin_reso = esfin_reso;
    }

    public int getCodi_asre() {
        return codi_asre;
    }

    public void setCodi_asre(int codi_asre) {
        this.codi_asre = codi_asre;
    }

    public List<BeanDestinoDis> getDestinos() {
        return destinos;
    }

    public void setDestinos(List<BeanDestinoDis> destinos) {
        this.destinos = destinos;
    }

    public List<ExpedienteCliente> getExpedientes() {
        return expedientes;
    }

    public void setExpedientes(List<ExpedienteCliente> expedientes) {
        this.expedientes = expedientes;
    }

    public String getEsta_reso() {
        return esta_reso;
    }

    public void setEsta_reso(String esta_reso) {
        this.esta_reso = esta_reso;
    }

    public boolean getCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public Date getFesa_reso() {
        return fesa_reso;
    }

    public void setFesa_reso(Date fesa_reso) {
        this.fesa_reso = fesa_reso;
    }

    /**
     * @return the usem_reso
     */
    public int getUsem_reso() {
        return usem_reso;
    }

    /**
     * @param usem_reso the usem_reso to set
     */
    public void setUsem_reso(int usem_reso) {
        this.usem_reso = usem_reso;
    }

}
