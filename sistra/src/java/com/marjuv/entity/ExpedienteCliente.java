/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class ExpedienteCliente {
    private String EXPNUM;
    private int CORRELATIVO;
    private String FECHA;
    private int FOLIOS;
    private String DOCUMENTO;
    private String PROCEDIMIENTO;
    private String ADMINISTRADO;
    private String REMITENTE;
    private String DESTINATARIO;
    private String PROVEIDO;
    private int DESTINO;
    private String OBSERVACION;
    private String FINALIZACION;
    private String COPIA;
    private int codi_arde;
    private int codi_enti;
    private int codi_cont;
    private String nomb_hist;
    private String nume_reso;
    private String CORRELATIVOS;
    private int CONDICION;

    public String getCORRELATIVOS() {
        return CORRELATIVOS;
    }

    public void setCORRELATIVOS(String CORRELATIVOS) {
        this.CORRELATIVOS = CORRELATIVOS;
    }
    
    

    public String getNume_reso() {
        return nume_reso;
    }

    public void setNume_reso(String nume_reso) {
        this.nume_reso = nume_reso;
    }
    
    
    

    public ExpedienteCliente(String EXPNUM, int CORRELATIVO, int FOLIOS, String DOCUMENTO, String PROCEDIMIENTO, String ADMINISTRADO, String REMITENTE, String PROVEIDO, int DESTINO, String OBSERVACION) {
        this.EXPNUM = EXPNUM;
        this.CORRELATIVO = CORRELATIVO;
        this.FOLIOS = FOLIOS;
        this.DOCUMENTO = DOCUMENTO;
        this.PROCEDIMIENTO = PROCEDIMIENTO;
        this.ADMINISTRADO = ADMINISTRADO;
        this.REMITENTE = REMITENTE;
        this.PROVEIDO = PROVEIDO;
        this.DESTINO = DESTINO;
        this.OBSERVACION = OBSERVACION;
    }

    public String getFINALIZACION() {
        return FINALIZACION;
    }

    public void setFINALIZACION(String FINALIZACION) {
        this.FINALIZACION = FINALIZACION;
    }

    public String getCOPIA() {
        return COPIA;
    }

    public void setCOPIA(String COPIA) {
        this.COPIA = COPIA;
    }
    
    

    public ExpedienteCliente() {
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
    
    public String getEXPNUM() {
        return EXPNUM;
    }

    public void setEXPNUM(String EXPNUM) {
        this.EXPNUM = EXPNUM;
    }

    public int getCORRELATIVO() {
        return CORRELATIVO;
    }

    public void setCORRELATIVO(int CORRELATIVO) {
        this.CORRELATIVO = CORRELATIVO;
    }

    public int getFOLIOS() {
        return FOLIOS;
    }

    public void setFOLIOS(int FOLIOS) {
        this.FOLIOS = FOLIOS;
    }

    public String getDOCUMENTO() {
        return DOCUMENTO;
    }

    public void setDOCUMENTO(String DOCUMENTO) {
        this.DOCUMENTO = DOCUMENTO;
    }

    public String getPROCEDIMIENTO() {
        return PROCEDIMIENTO;
    }

    public void setPROCEDIMIENTO(String PROCEDIMIENTO) {
        this.PROCEDIMIENTO = PROCEDIMIENTO;
    }

    public String getADMINISTRADO() {
        return ADMINISTRADO;
    }

    public void setADMINISTRADO(String ADMINISTRADO) {
        this.ADMINISTRADO = ADMINISTRADO;
    }

    public String getREMITENTE() {
        return REMITENTE;
    }

    public void setREMITENTE(String REMITENTE) {
        this.REMITENTE = REMITENTE;
    }

    public String getPROVEIDO() {
        return PROVEIDO;
    }

    public void setPROVEIDO(String PROVEIDO) {
        this.PROVEIDO = PROVEIDO;
    }

    public int getDESTINO() {
        return DESTINO;
    }

    public String getDESTINATARIO() {
        return DESTINATARIO;
    }

    public void setDESTINATARIO(String DESTINATARIO) {
        this.DESTINATARIO = DESTINATARIO;
    }

    public void setDESTINO(int DESTINO) {
        this.DESTINO = DESTINO;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public String getNomb_hist() {
        return nomb_hist;
    }

    public void setNomb_hist(String nomb_hist) {
        this.nomb_hist = nomb_hist;
    }

    public int getCONDICION() {
        return CONDICION;
    }

    public void setCONDICION(int CONDICION) {
        this.CONDICION = CONDICION;
    }
    
    
}
