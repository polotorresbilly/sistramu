/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Firma_Resolucion {
    private int codi_fire;
    private Dependencia area_depe;
    private String carg_fire;
    private String pofi_fire;
    private String obsv_fire;
    private String arch_fire;
    private Boolean esta_fire;

    public Firma_Resolucion() {
    }

    public Boolean getEsta_fire() {
        return esta_fire;
    }

    public void setEsta_fire(Boolean esta_fire) {
        this.esta_fire = esta_fire;
    }
    
    

    public int getCodi_fire() {
        return codi_fire;
    }

    public void setCodi_fire(int codi_fire) {
        this.codi_fire = codi_fire;
    }

    public Dependencia getArea_depe() {
        return area_depe;
    }

    public void setArea_depe(Dependencia area_depe) {
        this.area_depe = area_depe;
    }

    public String getCarg_fire() {
        return carg_fire;
    }

    public void setCarg_fire(String carg_fire) {
        this.carg_fire = carg_fire;
    }

    public String getPofi_fire() {
        return pofi_fire;
    }

    public void setPofi_fire(String pofi_fire) {
        this.pofi_fire = pofi_fire;
    }

    public String getObsv_fire() {
        return obsv_fire;
    }

    public void setObsv_fire(String obsv_fire) {
        this.obsv_fire = obsv_fire;
    }

    public String getArch_fire() {
        return arch_fire;
    }

    public void setArch_fire(String arch_fire) {
        this.arch_fire = arch_fire;
    }
    
    
}


