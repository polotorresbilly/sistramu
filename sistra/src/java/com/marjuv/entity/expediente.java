package com.marjuv.entity;

import com.marjuv.beans_view.BeanRequisitoMan;
import java.sql.Date;
import java.util.List;

public class expediente {

    private String nume_expe;
    private int codi_cont;
    private int codi_enti;
    private int codi_carg;
    private Date fech_expe;
    private String refe_expe;
    private String valx_expe;
    private int nufo_expe;
    private String nudo_expe;
    private String reci_expe;
    private int codi_tiex;
    private int codi_proc;
    private int codi_enex;
    private int codi_usua;
    private int codi_asex;
    private int dias_proc;
    private int codi_reps;
    private String espe_expe;
    private int codi_esex;
    private List<historial_expediente> historial_expediente;
    private List<BeanRequisitoMan> requisitos;
    

    public String getNume_expe() {
        return nume_expe;
    }

    public String getReci_expe() {
        return reci_expe;
    }

    public void setReci_expe(String reci_expe) {
        this.reci_expe = reci_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public Date getFech_expe() {
        return fech_expe;
    }

    public void setFech_expe(Date fech_expe) {
        this.fech_expe = fech_expe;
    }

    public String getRefe_expe() {
        return refe_expe;
    }

    public void setRefe_expe(String refe_expe) {
        this.refe_expe = refe_expe;
    }

    public int getNufo_expe() {
        return nufo_expe;
    }

    public void setNufo_expe(int nufo_expe) {
        this.nufo_expe = nufo_expe;
    }

    public String getNudo_expe() {
        return nudo_expe;
    }

    public void setNudo_expe(String nudo_expe) {
        this.nudo_expe = nudo_expe;
    }

    public int getCodi_tiex() {
        return codi_tiex;
    }

    public void setCodi_tiex(int codi_tiex) {
        this.codi_tiex = codi_tiex;
    }

    public int getCodi_proc() {
        return codi_proc;
    }

    public void setCodi_proc(int codi_proc) {
        this.codi_proc = codi_proc;
    }

    public int getCodi_enex() {
        return codi_enex;
    }

    public void setCodi_enex(int codi_enex) {
        this.codi_enex = codi_enex;
    }

    public int getCodi_usua() {
        return codi_usua;
    }

    public void setCodi_usua(int codi_usua) {
        this.codi_usua = codi_usua;
    }

    public String getEspe_expe() {
        return espe_expe;
    }

    public void setEspe_expe(String espe_expe) {
        this.espe_expe = espe_expe;
    }

    public int getCodi_esex() {
        return codi_esex;
    }

    public void setCodi_esex(int codi_esex) {
        this.codi_esex = codi_esex;
    }

    public List<historial_expediente> getHistorial_expediente() {
        return historial_expediente;
    }

    public void setHistorial_expediente(List<historial_expediente> historial_expediente) {
        this.historial_expediente = historial_expediente;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public int getCodi_asex() {
        return codi_asex;
    }

    public void setCodi_asex(int codi_asex) {
        this.codi_asex = codi_asex;
    }

    public List<BeanRequisitoMan> getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(List<BeanRequisitoMan> requisitos) {
        this.requisitos = requisitos;
    }

    public int getDias_proc() {
        return dias_proc;
    }

    public void setDias_proc(int dias_proc) {
        this.dias_proc = dias_proc;
    }

    public int getCodi_reps() {
        return codi_reps;
    }

    public void setCodi_reps(int codi_reps) {
        this.codi_reps = codi_reps;
    }

    public String getValx_expe() {
        return valx_expe;
    }

    public void setValx_expe(String valx_expe) {
        this.valx_expe = valx_expe;
    }

    

}
