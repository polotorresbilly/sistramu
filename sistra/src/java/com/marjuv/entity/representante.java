package com.marjuv.entity;

public class representante implements Cloneable {

    private int codi_reps;
    private int tipo_reps;
    private String noti_reps;
    private String noxa_reps;
    private int rela_reps;
    private String esta_reps;
    private String noms_reps;
    private String docs_reps;
    private String dire_reps;
    private int codi_dist;
    private String nomb_dist;
    private String telf_reps;
    private String celu_reps;
    private String emai_reps;
    
    private Distrito obj_dist;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * @return the codi_reps
     */
    public int getCodi_reps() {
        return codi_reps;
    }

    /**
     * @param codi_reps the codi_reps to set
     */
    public void setCodi_reps(int codi_reps) {
        this.codi_reps = codi_reps;
    }

    /**
     * @return the tipo_reps
     */
    public int getTipo_reps() {
        return tipo_reps;
    }

    /**
     * @param tipo_reps the tipo_reps to set
     */
    public void setTipo_reps(int tipo_reps) {
        this.tipo_reps = tipo_reps;
    }

    /**
     * @return the rela_reps
     */
    public int getRela_reps() {
        return rela_reps;
    }

    /**
     * @param rela_reps the rela_reps to set
     */
    public void setRela_reps(int rela_reps) {
        this.rela_reps = rela_reps;
    }

    /**
     * @return the esta_reps
     */
    public String getEsta_reps() {
        return esta_reps;
    }

    /**
     * @param esta_reps the esta_reps to set
     */
    public void setEsta_reps(String esta_reps) {
        this.esta_reps = esta_reps;
    }

    /**
     * @return the noms_reps
     */
    public String getNoms_reps() {
        return noms_reps;
    }

    /**
     * @param noms_reps the noms_reps to set
     */
    public void setNoms_reps(String noms_reps) {
        this.noms_reps = noms_reps;
    }

    /**
     * @return the docs_reps
     */
    public String getDocs_reps() {
        return docs_reps;
    }

    /**
     * @param docs_reps the docs_reps to set
     */
    public void setDocs_reps(String docs_reps) {
        this.docs_reps = docs_reps;
    }

    /**
     * @return the dire_reps
     */
    public String getDire_reps() {
        return dire_reps;
    }

    /**
     * @param dire_reps the dire_reps to set
     */
    public void setDire_reps(String dire_reps) {
        this.dire_reps = dire_reps;
    }

    /**
     * @return the codi_dist
     */
    public int getCodi_dist() {
        return codi_dist;
    }

    /**
     * @param codi_dist the codi_dist to set
     */
    public void setCodi_dist(int codi_dist) {
        this.codi_dist = codi_dist;
    }

    /**
     * @return the telf_reps
     */
    public String getTelf_reps() {
        return telf_reps;
    }

    /**
     * @param telf_reps the telf_reps to set
     */
    public void setTelf_reps(String telf_reps) {
        this.telf_reps = telf_reps;
    }

    /**
     * @return the celu_reps
     */
    public String getCelu_reps() {
        return celu_reps;
    }

    /**
     * @param celu_reps the celu_reps to set
     */
    public void setCelu_reps(String celu_reps) {
        this.celu_reps = celu_reps;
    }

    /**
     * @return the emai_reps
     */
    public String getEmai_reps() {
        return emai_reps;
    }

    /**
     * @param emai_reps the emai_reps to set
     */
    public void setEmai_reps(String emai_reps) {
        this.emai_reps = emai_reps;
    }

    /**
     * @return the nomb_dist
     */
    public String getNomb_dist() {
        return nomb_dist;
    }

    /**
     * @param nomb_dist the nomb_dist to set
     */
    public void setNomb_dist(String nomb_dist) {
        this.nomb_dist = nomb_dist;
    }

    /**
     * @return the noti_reps
     */
    public String getNoti_reps() {
        return noti_reps;
    }

    /**
     * @param noti_reps the noti_reps to set
     */
    public void setNoti_reps(String noti_reps) {
        this.noti_reps = noti_reps;
    }

    /**
     * @return the noxa_reps
     */
    public String getNoxa_reps() {
        return noxa_reps;
    }

    /**
     * @param noxa_reps the noxa_reps to set
     */
    public void setNoxa_reps(String noxa_reps) {
        this.noxa_reps = noxa_reps;
    }

    public Distrito getObj_dist() {
        return obj_dist;
    }

    public void setObj_dist(Distrito obj_dist) {
        this.obj_dist = obj_dist;
    }

}
