
package com.marjuv.entity;

public class estado_resolucion implements Cloneable {
    
    private int codi_esre;
    private String nomb_esre;
    private String esta_esre;

    public int getCodi_esre() {
        return codi_esre;
    }

    public void setCodi_esre(int codi_esre) {
        this.codi_esre = codi_esre;
    }

    public String getNomb_esre() {
        return nomb_esre;
    }

    public void setNomb_esre(String nomb_esre) {
        this.nomb_esre = nomb_esre;
    }

    public String getEsta_esre() {
        return esta_esre;
    }

    public void setEsta_esre(String esta_esre) {
        this.esta_esre = esta_esre;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
}
