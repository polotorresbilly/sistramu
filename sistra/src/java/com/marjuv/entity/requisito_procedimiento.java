package com.marjuv.entity;

public class requisito_procedimiento {

    private int codi_repr;
    private String esta_repr;
    private String nomb_repr;
    private int codi_proc;
    private double prec_repr;
    private int jera_repr;

    public int getJera_repr() {
        return jera_repr;
    }

    public void setJera_repr(int jera_repr) {
        this.jera_repr = jera_repr;
    }

    
    public int getCodi_repr() {
        return codi_repr;
    }

    public void setCodi_repr(int codi_repr) {
        this.codi_repr = codi_repr;
    }

    public String getEsta_repr() {
        return esta_repr;
    }

    public void setEsta_repr(String esta_repr) {
        this.esta_repr = esta_repr;
    }

    public String getNomb_repr() {
        return nomb_repr;
    }

    public void setNomb_repr(String nomb_repr) {
        this.nomb_repr = nomb_repr;
    }

    public int getCodi_proc() {
        return codi_proc;
    }

    public void setCodi_proc(int codi_proc) {
        this.codi_proc = codi_proc;
    }

    public double getPrec_repr() {
        return prec_repr;
    }

    public void setPrec_repr(double prec_repr) {
        this.prec_repr = prec_repr;
    }

}
