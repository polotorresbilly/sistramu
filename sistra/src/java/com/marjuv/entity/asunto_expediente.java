
package com.marjuv.entity;

public class asunto_expediente implements Cloneable {
    
    private int codi_asex;
    private String desc_asex;
    private String esta_asex;

    public int getCodi_asex() {
        return codi_asex;
    }

    public void setCodi_asex(int codi_asex) {
        this.codi_asex = codi_asex;
    }

    public String getDesc_asex() {
        return desc_asex;
    }

    public void setDesc_asex(String desc_asex) {
        this.desc_asex = desc_asex;
    }

    public String getEsta_asex() {
        return esta_asex;
    }

    public void setEsta_asex(String esta_asex) {
        this.esta_asex = esta_asex;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
}
