
package com.marjuv.entity;

public class Entidad_Ubicacion_Expediente {
    
    private int FOLIOS;
    private String NUMERO_EXPEDIENTE;
    private int CORRELATIVO;
    private int ESTADO;
    private String FECHA_LLEGADA;
    private int CODIGO_CARGO_ENVIA;
    private int CODIGO_CONTRIBUYENTE_ENVIA;
    private int CODIGO_ENTIDAD_ENVIA;
    private int CODIGO_CARGO_RECIBE;
    private int CODIGO_CONTRIBUYENTE_RECIBE;
    private int CODIGO_ENTIDAD_RECIBE;
    private String NOMBRE_CARGO_RECIBE_DESTINATARIO;
    private String NOMBRE_CARGO_ENVIA_REMITENTE;
    private String NOMBRE_AREA_RECIBE;
    private String REMITENTE_CONTRIBUYENTE_EXPEDIENTE;
    private String REMITENTE_ENTIDAD_EXPEDIENTE;
    private String REMITENTE_CARGO_EXPEDIENTE;
    private String FECHA_INGRESO;
    private String FECHA_RECEPCION;
    private String OBSERVACION;

    public int getFOLIOS() {
        return FOLIOS;
    }

    public void setFOLIOS(int FOLIOS) {
        this.FOLIOS = FOLIOS;
    }

    public String getNUMERO_EXPEDIENTE() {
        return NUMERO_EXPEDIENTE;
    }

    public void setNUMERO_EXPEDIENTE(String NUMERO_EXPEDIENTE) {
        this.NUMERO_EXPEDIENTE = NUMERO_EXPEDIENTE;
    }

    public int getCORRELATIVO() {
        return CORRELATIVO;
    }

    public void setCORRELATIVO(int CORRELATIVO) {
        this.CORRELATIVO = CORRELATIVO;
    }

    public int getESTADO() {
        return ESTADO;
    }

    public void setESTADO(int ESTADO) {
        this.ESTADO = ESTADO;
    }

    public String getFECHA_LLEGADA() {
        return FECHA_LLEGADA;
    }

    public void setFECHA_LLEGADA(String FECHA_LLEGADA) {
        this.FECHA_LLEGADA = FECHA_LLEGADA;
    }

    public int getCODIGO_CARGO_ENVIA() {
        return CODIGO_CARGO_ENVIA;
    }

    public void setCODIGO_CARGO_ENVIA(int CODIGO_CARGO_ENVIA) {
        this.CODIGO_CARGO_ENVIA = CODIGO_CARGO_ENVIA;
    }

    public int getCODIGO_CONTRIBUYENTE_ENVIA() {
        return CODIGO_CONTRIBUYENTE_ENVIA;
    }

    public void setCODIGO_CONTRIBUYENTE_ENVIA(int CODIGO_CONTRIBUYENTE_ENVIA) {
        this.CODIGO_CONTRIBUYENTE_ENVIA = CODIGO_CONTRIBUYENTE_ENVIA;
    }

    public int getCODIGO_ENTIDAD_ENVIA() {
        return CODIGO_ENTIDAD_ENVIA;
    }

    public void setCODIGO_ENTIDAD_ENVIA(int CODIGO_ENTIDAD_ENVIA) {
        this.CODIGO_ENTIDAD_ENVIA = CODIGO_ENTIDAD_ENVIA;
    }

    public int getCODIGO_CARGO_RECIBE() {
        return CODIGO_CARGO_RECIBE;
    }

    public void setCODIGO_CARGO_RECIBE(int CODIGO_CARGO_RECIBE) {
        this.CODIGO_CARGO_RECIBE = CODIGO_CARGO_RECIBE;
    }

    public int getCODIGO_CONTRIBUYENTE_RECIBE() {
        return CODIGO_CONTRIBUYENTE_RECIBE;
    }

    public void setCODIGO_CONTRIBUYENTE_RECIBE(int CODIGO_CONTRIBUYENTE_RECIBE) {
        this.CODIGO_CONTRIBUYENTE_RECIBE = CODIGO_CONTRIBUYENTE_RECIBE;
    }

    public int getCODIGO_ENTIDAD_RECIBE() {
        return CODIGO_ENTIDAD_RECIBE;
    }

    public void setCODIGO_ENTIDAD_RECIBE(int CODIGO_ENTIDAD_RECIBE) {
        this.CODIGO_ENTIDAD_RECIBE = CODIGO_ENTIDAD_RECIBE;
    }

    public String getNOMBRE_CARGO_RECIBE_DESTINATARIO() {
        return NOMBRE_CARGO_RECIBE_DESTINATARIO;
    }

    public void setNOMBRE_CARGO_RECIBE_DESTINATARIO(String NOMBRE_CARGO_RECIBE_DESTINATARIO) {
        this.NOMBRE_CARGO_RECIBE_DESTINATARIO = NOMBRE_CARGO_RECIBE_DESTINATARIO;
    }

    public String getNOMBRE_CARGO_ENVIA_REMITENTE() {
        return NOMBRE_CARGO_ENVIA_REMITENTE;
    }

    public void setNOMBRE_CARGO_ENVIA_REMITENTE(String NOMBRE_CARGO_ENVIA_REMITENTE) {
        this.NOMBRE_CARGO_ENVIA_REMITENTE = NOMBRE_CARGO_ENVIA_REMITENTE;
    }

    public String getREMITENTE_CONTRIBUYENTE_EXPEDIENTE() {
        return REMITENTE_CONTRIBUYENTE_EXPEDIENTE;
    }

    public void setREMITENTE_CONTRIBUYENTE_EXPEDIENTE(String REMITENTE_CONTRIBUYENTE_EXPEDIENTE) {
        this.REMITENTE_CONTRIBUYENTE_EXPEDIENTE = REMITENTE_CONTRIBUYENTE_EXPEDIENTE;
    }

    public String getREMITENTE_ENTIDAD_EXPEDIENTE() {
        return REMITENTE_ENTIDAD_EXPEDIENTE;
    }

    public void setREMITENTE_ENTIDAD_EXPEDIENTE(String REMITENTE_ENTIDAD_EXPEDIENTE) {
        this.REMITENTE_ENTIDAD_EXPEDIENTE = REMITENTE_ENTIDAD_EXPEDIENTE;
    }

    public String getREMITENTE_CARGO_EXPEDIENTE() {
        return REMITENTE_CARGO_EXPEDIENTE;
    }

    public void setREMITENTE_CARGO_EXPEDIENTE(String REMITENTE_CARGO_EXPEDIENTE) {
        this.REMITENTE_CARGO_EXPEDIENTE = REMITENTE_CARGO_EXPEDIENTE;
    }

    public String getFECHA_INGRESO() {
        return FECHA_INGRESO;
    }

    public void setFECHA_INGRESO(String FECHA_INGRESO) {
        this.FECHA_INGRESO = FECHA_INGRESO;
    }

    public String getFECHA_RECEPCION() {
        return FECHA_RECEPCION;
    }

    public void setFECHA_RECEPCION(String FECHA_RECEPCION) {
        this.FECHA_RECEPCION = FECHA_RECEPCION;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }

    public String getNOMBRE_AREA_RECIBE() {
        return NOMBRE_AREA_RECIBE;
    }

    public void setNOMBRE_AREA_RECIBE(String NOMBRE_AREA_RECIBE) {
        this.NOMBRE_AREA_RECIBE = NOMBRE_AREA_RECIBE;
    }
    
    
}
