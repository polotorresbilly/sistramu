package com.marjuv.entity;

public class EntidadTO {
    
    private int codi_enti;
    private String nomb_enti;
    private String dire_enti;
    private String esta_enti;

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public String getNomb_enti() {
        return nomb_enti;
    }

    public void setNomb_enti(String nomb_enti) {
        this.nomb_enti = nomb_enti;
    }

    public String getDire_enti() {
        return dire_enti;
    }

    public void setDire_enti(String dire_enti) {
        this.dire_enti = dire_enti;
    }

    public String getEsta_enti() {
        return esta_enti;
    }

    public void setEsta_enti(String esta_enti) {
        this.esta_enti = esta_enti;
    }
    
    
    
}
