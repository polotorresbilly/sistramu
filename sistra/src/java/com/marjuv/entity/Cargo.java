/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Cargo {
    private int codi_carg;
    private String nomb_carg;
    private int codi_arde;
    private String esta_carg;

    public Cargo() {
    }

    public String getEsta_carg() {
        return esta_carg;
    }

    public void setEsta_carg(String esta_carg) {
        this.esta_carg = esta_carg;
    }

    public Cargo(int codi_carg, String nomb_carg, int codi_arde, String esta_carg) {
        this.codi_carg = codi_carg;
        this.nomb_carg = nomb_carg;
        this.codi_arde = codi_arde;
        this.esta_carg = esta_carg;
    }
    

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public String getNomb_carg() {
        return nomb_carg;
    }

    public void setNomb_carg(String nomb_carg) {
        this.nomb_carg = nomb_carg;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }
    
    
}
