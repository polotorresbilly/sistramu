
package com.marjuv.entity;

public class permiso {
    
    private int codi_perm;
    private String page_perm;
    private String urlx_perm;
    private int codi_rous;
    private String titu_perm;

    public int getCodi_perm() {
        return codi_perm;
    }

    public void setCodi_perm(int codi_perm) {
        this.codi_perm = codi_perm;
    }

    public String getPage_perm() {
        return page_perm;
    }

    public void setPage_perm(String page_perm) {
        this.page_perm = page_perm;
    }

    public String getUrlx_perm() {
        return urlx_perm;
    }

    public void setUrlx_perm(String urlx_perm) {
        this.urlx_perm = urlx_perm;
    }

    public int getCodi_rous() {
        return codi_rous;
    }

    public void setCodi_rous(int codi_rous) {
        this.codi_rous = codi_rous;
    }

    public String getTitu_perm() {
        return titu_perm;
    }

    public void setTitu_perm(String titu_perm) {
        this.titu_perm = titu_perm;
    }
    
}
