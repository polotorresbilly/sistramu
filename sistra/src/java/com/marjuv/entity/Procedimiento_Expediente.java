/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.entity;

/**
 *
 * @author BILLY
 */
public class Procedimiento_Expediente {
    private int codi_proc;
    private String nomb_proc;
    private String esta_proc;
    private int codi_asex;
    private int codi_carg;
    private int dias_proc;
    private String cost_proc;

    public Procedimiento_Expediente() {
    }

    public Procedimiento_Expediente(int codi_proc, String nomb_proc, String esta_proc, int codi_asex) {
        this.codi_proc = codi_proc;
        this.nomb_proc = nomb_proc;
        this.esta_proc = esta_proc;
        this.codi_asex = codi_asex;
    }

    
    
    public int getCodi_proc() {
        return codi_proc;
    }

    public void setCodi_proc(int codi_proc) {
        this.codi_proc = codi_proc;
    }

    public String getNomb_proc() {
        return nomb_proc;
    }

    public void setNomb_proc(String nomb_proc) {
        this.nomb_proc = nomb_proc;
    }

    public String getEsta_proc() {
        return esta_proc;
    }

    public void setEsta_proc(String esta_proc) {
        this.esta_proc = esta_proc;
    }

    public int getCodi_asex() {
        return codi_asex;
    }

    public void setCodi_asex(int codi_asex) {
        this.codi_asex = codi_asex;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public int getDias_proc() {
        return dias_proc;
    }

    public void setDias_proc(int dias_proc) {
        this.dias_proc = dias_proc;
    }

    public String getCost_proc() {
        return cost_proc;
    }

    public void setCost_proc(String cost_proc) {
        this.cost_proc = cost_proc;
    }
    
    
}
