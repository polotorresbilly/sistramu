package com.marjuv.entity;

public class Entidad implements Cloneable{
    private int codi_enti;
    private String nomb_enti;
    private String dire_enti;
    private String esta_enti;
    private int codi_dist;
    private Distrito obj_dist;
    private String nomb_lugar;

    public String getNomb_lugar() {
        nomb_lugar = obj_dist.getNomb_dist() + " / " + obj_dist.getProvincia().getNomb_prov()+" / "+obj_dist.getProvincia().getDepartamento().getNomb_depa();
        return nomb_lugar;
    }

    public void setNomb_lugar(String nomb_lugar) {
        this.nomb_lugar = nomb_lugar;
    }
    
    

    public Distrito getObj_dist() {
        return obj_dist;
    }

    public void setObj_dist(Distrito obj_dist) {
        this.obj_dist = obj_dist;
    }

    
    public int getCodi_dist() {
        return codi_dist;
    }

    public void setCodi_dist(int codi_dist) {
        this.codi_dist = codi_dist;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public String getNomb_enti() {
        return nomb_enti;
    }

    public void setNomb_enti(String nomb_enti) {
        this.nomb_enti = nomb_enti;
    }

    public String getDire_enti() {
        return dire_enti;
    }

    public void setDire_enti(String dire_enti) {
        this.dire_enti = dire_enti;
    }

    public String getEsta_enti() {
        return esta_enti;
    }

    public void setEsta_enti(String esta_enti) {
        this.esta_enti = esta_enti;
    }
    
    
}
