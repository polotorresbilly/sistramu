
package com.marjuv.beans_view;

public class BeanDependencias implements Cloneable{
    
    private int codi_arde;
    private String nomb_arde;
    private String abre_arde;
    private String esta_arde;
    private int jera_arde;
    private int codi_enti;
    private String nomb_enti;
    private String nomb_jera;
    private int nive_arde;

    public BeanDependencias() {
        this.codi_arde = 0;
        this.nomb_arde = "";
        this.abre_arde = "";
        this.esta_arde = "";
        this.jera_arde = 0;
        this.codi_enti = 0;
        this.nomb_enti = "";
        this.nomb_jera = "";
    }

    public int getNive_arde() {
        return nive_arde;
    }

    public void setNive_arde(int nive_arde) {
        this.nive_arde = nive_arde;
    }
    
    

    public BeanDependencias(int codi_arde, String nomb_arde, String abre_arde, String esta_arde, int jera_arde, int codi_enti,String nomb_enti,String nomb_jera) {
        this.codi_arde = codi_arde;
        this.nomb_arde = nomb_arde;
        this.abre_arde = abre_arde;
        this.esta_arde = esta_arde;
        this.jera_arde = jera_arde;
        this.codi_enti = codi_enti;
        this.nomb_enti = nomb_enti;
        this.nomb_jera = nomb_jera;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public String getAbre_arde() {
        return abre_arde;
    }

    public void setAbre_arde(String abre_arde) {
        this.abre_arde = abre_arde;
    }

    public String getEsta_arde() {
        return esta_arde;
    }

    public void setEsta_arde(String esta_arde) {
        this.esta_arde = esta_arde;
    }

    

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public String getNomb_enti() {
        return nomb_enti;
    }

    public void setNomb_enti(String nomb_enti) {
        this.nomb_enti = nomb_enti;
    }
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int getJera_arde() {
        return jera_arde;
    }

    public void setJera_arde(int jera_arde) {
        this.jera_arde = jera_arde;
    }

    public String getNomb_jera() {
        return nomb_jera;
    }

    public void setNomb_jera(String nomb_jera) {
        this.nomb_jera = nomb_jera;
    }
    
    
}
