package com.marjuv.beans_view;

import com.marjuv.entity.permiso;
import java.util.List;

public class BeanUsuarioMan implements Cloneable {

    private int codi_usua;
    private int codi_arde;
    private String nomb_usua;
    private String apel_usua;
    private int dni_usua;
    private int codi_carg;
    private String fere_usua;
    private String nick_usua;
    private String pass_usua;
    private String verif_pass_usua;
    private String esta_usua;
    private String codi_rous;
    private String nomb_rol;
    private String nomb_arde;
    private String nume_usua;
    private String nomb_carg;
    private List<permiso> permisos;

    public int getCodi_usua() {
        return codi_usua;
    }

    public void setCodi_usua(int codi_usua) {
        this.codi_usua = codi_usua;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public String getNomb_usua() {
        return nomb_usua;
    }

    public void setNomb_usua(String nomb_usua) {
        this.nomb_usua = nomb_usua;
    }

    public String getApel_usua() {
        return apel_usua;
    }

    public void setApel_usua(String apel_usua) {
        this.apel_usua = apel_usua;
    }

    public int getDni_usua() {
        return dni_usua;
    }

    public void setDni_usua(int dni_usua) {
        this.dni_usua = dni_usua;
    }

    public String getFere_usua() {
        return fere_usua;
    }

    public void setFere_usua(String fere_usua) {
        this.fere_usua = fere_usua;
    }

    public String getNick_usua() {
        return nick_usua;
    }

    public void setNick_usua(String nick_usua) {
        this.nick_usua = nick_usua;
    }

    public String getPass_usua() {
        return pass_usua;
    }

    public void setPass_usua(String pass_usua) {
        this.pass_usua = pass_usua;
    }

    public String getEsta_usua() {
        return esta_usua;
    }

    public void setEsta_usua(String esta_usua) {
        this.esta_usua = esta_usua;
    }

    public String getCodi_rous() {
        return codi_rous;
    }

    public void setCodi_rous(String codi_rous) {
        this.codi_rous = codi_rous;
    }

    public String getNomb_rol() {
        return nomb_rol;
    }

    public void setNomb_rol(String nomb_rol) {
        this.nomb_rol = nomb_rol;
    }

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getVerif_pass_usua() {
        return verif_pass_usua;
    }

    public void setVerif_pass_usua(String verif_pass_usua) {
        this.verif_pass_usua = verif_pass_usua;
    }

    public List<permiso> getPermisos() {
        return permisos;
    }

    public void setPermisos(List<permiso> permisos) {
        this.permisos = permisos;
    }

    public String getNume_usua() {
        return nume_usua;
    }

    public void setNume_usua(String nume_usua) {
        this.nume_usua = nume_usua;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public String getNomb_carg() {
        return nomb_carg;
    }

    public void setNomb_carg(String nomb_carg) {
        this.nomb_carg = nomb_carg;
    }

}
