package com.marjuv.beans_view;

public class BeanExpedientes_Vinculados {
    
    private String numero_expediente_tramitado;
    private String numero_expediente_adjuntado;
    private String fecha_ingreso_expediente;
    private String fecha_adjunto_expediente;
    private String nombre_usuario_adjunto;
    private int codigo_usuario_adjunto;

    public String getNumero_expediente_tramitado() {
        return numero_expediente_tramitado;
    }

    public void setNumero_expediente_tramitado(String numero_expediente_tramitado) {
        this.numero_expediente_tramitado = numero_expediente_tramitado;
    }

    public String getNumero_expediente_adjuntado() {
        return numero_expediente_adjuntado;
    }

    public void setNumero_expediente_adjuntado(String numero_expediente_adjuntado) {
        this.numero_expediente_adjuntado = numero_expediente_adjuntado;
    }

    public String getFecha_ingreso_expediente() {
        return fecha_ingreso_expediente;
    }

    public void setFecha_ingreso_expediente(String fecha_ingreso_expediente) {
        this.fecha_ingreso_expediente = fecha_ingreso_expediente;
    }

    public String getFecha_adjunto_expediente() {
        return fecha_adjunto_expediente;
    }

    public void setFecha_adjunto_expediente(String fecha_adjunto_expediente) {
        this.fecha_adjunto_expediente = fecha_adjunto_expediente;
    }

    public String getNombre_usuario_adjunto() {
        return nombre_usuario_adjunto;
    }

    public void setNombre_usuario_adjunto(String nombre_usuario_adjunto) {
        this.nombre_usuario_adjunto = nombre_usuario_adjunto;
    }

    public int getCodigo_usuario_adjunto() {
        return codigo_usuario_adjunto;
    }

    public void setCodigo_usuario_adjunto(int codigo_usuario_adjunto) {
        this.codigo_usuario_adjunto = codigo_usuario_adjunto;
    }
    
    
    
}
