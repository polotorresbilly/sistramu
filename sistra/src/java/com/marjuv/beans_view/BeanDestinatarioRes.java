package com.marjuv.beans_view;

import com.marjuv.dao.general_dao;
import com.marjuv.dao.resolucion_dao;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Timebox;

public class BeanDestinatarioRes {

    private int codi_dire;
    private int codi_reso;
    private int codi_pare;
    private int count;
    private int codi_usua;
    private int copi_dire;
    private String noms_usua;
    private String nomb_dire;
    private String nomb_pare;
    private String dni_usua;
    private long hora_dire;
    private Date feen_dire;
    private List<Pair<Integer, String>> list_pare;
    private Pair<Integer, String> cur_pare;
    private boolean sw;
    private int tipo;
    private int codi_arde;
    private int codi_cont;
    private int codi_enti;
     // DNI, Razon Social, Direccion.
    private String dni,razon,direccion;

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    
    
    public BeanDestinatarioRes() {
        this.sw = false;
    }

    public int getCodi_reso() {
        return codi_reso;
    }

    public void setCodi_reso(int codi_reso) {
        this.codi_reso = codi_reso;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCodi_usua() {
        return codi_usua;
    }

    public void setCodi_usua(int codi_usua) {
        this.codi_usua = codi_usua;
    }

    public String getNoms_usua() {
        return noms_usua;
    }

    public void setNoms_usua(String noms_usua) {
        this.noms_usua = noms_usua;
    }

    public String getNomb_dire() {
        return nomb_dire;
    }

    public void setNomb_dire(String nomb_dire) {
        this.nomb_dire = nomb_dire;
    }

    public Date getHora_dire() {
        return new Date(hora_dire);
    }

    public void setHora_dire(Date hora_dire) {
        this.hora_dire = hora_dire.getTime();
        if (sw) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            try {
                obj_resolucion_dao.update_hora_dire(this.hora_dire, this.codi_dire);
                obj_resolucion_dao.close();
            } catch (Exception ex) {
                Messagebox.show(ex.getMessage());
            }

        }
    }

    public void setHora_dire2(long hora_dire) {
        this.hora_dire = hora_dire;

    }

    public Date getFeen_dire() {
        return feen_dire;
    }

    public void setFeen_dire(Date feen_dire) {
        this.feen_dire = feen_dire;
        if (sw) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            try {
                obj_resolucion_dao.update_feen_dire(new java.sql.Date(this.feen_dire.getTime()), this.codi_dire);
                obj_resolucion_dao.close();
            } catch (Exception ex) {
                Messagebox.show(ex.getMessage());
            }

        }
    }

    public int getCopi_dire() {
        return copi_dire;
    }

    public void setCopi_dire(int copi_dire) {
        this.copi_dire = copi_dire;
    }

    public Pair<Integer, String> getCur_pare() {
        return cur_pare;
    }

    public void setCur_pare(Pair<Integer, String> cur_pare) {
        this.cur_pare = cur_pare;
        if (sw) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            try {
                obj_resolucion_dao.update_codi_pare(this.cur_pare.x, this.codi_dire, this.cur_pare.y);
                obj_resolucion_dao.close();
            } catch (Exception ex) {
                Messagebox.show(ex.getMessage());
            }

        }
    }

    public int getCodi_dire() {
        return codi_dire;
    }

    public void setCodi_dire(int codi_dire) {
        this.codi_dire = codi_dire;
        sw = true;
    }

    public String getDni_usua() {
        return dni_usua;
    }

    public void setDni_usua(String dni_usua) {
        this.dni_usua = dni_usua;
    }

    public int getCodi_pare() {
        return codi_pare;
    }

    public void setCodi_pare(int codi_pare) {
        this.codi_pare = codi_pare;
    }

    public String getNomb_pare() {
        return nomb_pare;
    }

    public void setNomb_pare(String nomb_pare) {
        this.nomb_pare = nomb_pare;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public List<Pair<Integer, String>> getList_pare() {
        try {
            general_dao obj_general_dao = new general_dao();
            list_pare = obj_general_dao.get_select_pare(this.tipo);
            obj_general_dao.close();
        } catch (SQLException e) {
        }
        return list_pare;
    }

    public void setList_pare(List<Pair<Integer, String>> list_pare) {
        this.list_pare = list_pare;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

}
