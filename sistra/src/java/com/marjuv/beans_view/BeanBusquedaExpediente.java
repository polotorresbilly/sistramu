package com.marjuv.beans_view;

public class BeanBusquedaExpediente {
    
    private String numero_expediente;
    private String estado_expediente;
    private int numero_folios_expediente;
    private String nombre_docu_expediente;
    private String nombre_proc_expediente;
    private int codigo_cont_envia_expediente;
    private int codigo_carg_envia_expedinte;
    private int codgo_enti_envia_expediente;
    private int correlativo_expediente;
    private String nombre_cont_envia_expediente;
    private String nombre_carg_envia_expediente;
    private String nombre_enti_envia_expediente;
    private String nombre_remitente_expediente;

    /**
     * @return the numero_expediente
     */
    public String getNumero_expediente() {
        return numero_expediente;
    }

    /**
     * @param numero_expediente the numero_expediente to set
     */
    public void setNumero_expediente(String numero_expediente) {
        this.numero_expediente = numero_expediente;
    }

    /**
     * @return the estado_expediente
     */
    public String getEstado_expediente() {
        return estado_expediente;
    }

    /**
     * @param estado_expediente the estado_expediente to set
     */
    public void setEstado_expediente(String estado_expediente) {
        this.estado_expediente = estado_expediente;
    }

    /**
     * @return the numero_folios_expediente
     */
    public int getNumero_folios_expediente() {
        return numero_folios_expediente;
    }

    /**
     * @param numero_folios_expediente the numero_folios_expediente to set
     */
    public void setNumero_folios_expediente(int numero_folios_expediente) {
        this.numero_folios_expediente = numero_folios_expediente;
    }

    /**
     * @return the nombre_docu_expediente
     */
    public String getNombre_docu_expediente() {
        return nombre_docu_expediente;
    }

    /**
     * @param nombre_docu_expediente the nombre_docu_expediente to set
     */
    public void setNombre_docu_expediente(String nombre_docu_expediente) {
        this.nombre_docu_expediente = nombre_docu_expediente;
    }

    /**
     * @return the nombre_proc_expediente
     */
    public String getNombre_proc_expediente() {
        return nombre_proc_expediente;
    }

    /**
     * @param nombre_proc_expediente the nombre_proc_expediente to set
     */
    public void setNombre_proc_expediente(String nombre_proc_expediente) {
        this.nombre_proc_expediente = nombre_proc_expediente;
    }

    /**
     * @return the codigo_cont_envia_expediente
     */
    public int getCodigo_cont_envia_expediente() {
        return codigo_cont_envia_expediente;
    }

    /**
     * @param codigo_cont_envia_expediente the codigo_cont_envia_expediente to set
     */
    public void setCodigo_cont_envia_expediente(int codigo_cont_envia_expediente) {
        this.codigo_cont_envia_expediente = codigo_cont_envia_expediente;
    }

    /**
     * @return the codigo_carg_envia_expedinte
     */
    public int getCodigo_carg_envia_expedinte() {
        return codigo_carg_envia_expedinte;
    }

    /**
     * @param codigo_carg_envia_expedinte the codigo_carg_envia_expedinte to set
     */
    public void setCodigo_carg_envia_expedinte(int codigo_carg_envia_expedinte) {
        this.codigo_carg_envia_expedinte = codigo_carg_envia_expedinte;
    }

    /**
     * @return the codgo_enti_envia_expediente
     */
    public int getCodgo_enti_envia_expediente() {
        return codgo_enti_envia_expediente;
    }

    /**
     * @param codgo_enti_envia_expediente the codgo_enti_envia_expediente to set
     */
    public void setCodgo_enti_envia_expediente(int codgo_enti_envia_expediente) {
        this.codgo_enti_envia_expediente = codgo_enti_envia_expediente;
    }

    /**
     * @return the correlativo_expediente
     */
    public int getCorrelativo_expediente() {
        return correlativo_expediente;
    }

    /**
     * @param correlativo_expediente the correlativo_expediente to set
     */
    public void setCorrelativo_expediente(int correlativo_expediente) {
        this.correlativo_expediente = correlativo_expediente;
    }

    /**
     * @return the nombre_cont_envia_expediente
     */
    public String getNombre_cont_envia_expediente() {
        return nombre_cont_envia_expediente;
    }

    /**
     * @param nombre_cont_envia_expediente the nombre_cont_envia_expediente to set
     */
    public void setNombre_cont_envia_expediente(String nombre_cont_envia_expediente) {
        this.nombre_cont_envia_expediente = nombre_cont_envia_expediente;
    }

    /**
     * @return the nombre_carg_envia_expediente
     */
    public String getNombre_carg_envia_expediente() {
        return nombre_carg_envia_expediente;
    }

    /**
     * @param nombre_carg_envia_expediente the nombre_carg_envia_expediente to set
     */
    public void setNombre_carg_envia_expediente(String nombre_carg_envia_expediente) {
        this.nombre_carg_envia_expediente = nombre_carg_envia_expediente;
    }

    /**
     * @return the nombre_enti_envia_expediente
     */
    public String getNombre_enti_envia_expediente() {
        return nombre_enti_envia_expediente;
    }

    /**
     * @param nombre_enti_envia_expediente the nombre_enti_envia_expediente to set
     */
    public void setNombre_enti_envia_expediente(String nombre_enti_envia_expediente) {
        this.nombre_enti_envia_expediente = nombre_enti_envia_expediente;
    }

    /**
     * @return the nombre_remitente_expediente
     */
    public String getNombre_remitente_expediente() {
        return nombre_remitente_expediente;
    }

    /**
     * @param nombre_remitente_expediente the nombre_remitente_expediente to set
     */
    public void setNombre_remitente_expediente(String nombre_remitente_expediente) {
        this.nombre_remitente_expediente = nombre_remitente_expediente;
    }
    
    
}
