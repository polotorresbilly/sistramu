package com.marjuv.beans_view;

public class BeanDestinoDis implements Cloneable{
    
    private int codi_arde;
    private int codi_cont;
    private int codi_enti;
    private String nomb_des;
    private int copias;
    private String nume_expe;
   
    
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public BeanDestinoDis() {
        codi_arde = 0;
        codi_cont = 0;
        codi_enti = 0;
        nomb_des = "";
        nume_expe = "";
        copias = 0;
    }

    public BeanDestinoDis(int codi_arde, int codi_cont, int codi_enti, String nomb_des, int copias, String nume_expe) {
        this.codi_arde = codi_arde;
        this.codi_cont = codi_cont;
        this.codi_enti = codi_enti;
        this.nomb_des = nomb_des;
        this.copias = copias;
        this.nume_expe = nume_expe;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public int getCopias() {
        return copias;
    }

    public void setCopias(int copias) {
        this.copias = copias;
    }

    public String getNomb_des() {
        return nomb_des;
    }

    public void setNomb_des(String nomb_des) {
        this.nomb_des = nomb_des;
    }

    public String getNume_expe() {
        return nume_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }


}
