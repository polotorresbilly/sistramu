
package com.marjuv.beans_view;

import java.util.Date;

public class BeanReporteEstadoExpediente {
    
    private String nume_expe;
    
    private String tipo_admi;
    private String enti_admin;
    private String arde_admin;
    private String carg_admin;
    private String nomb_admin;
    
    private String tipo_remi;
    private String enti_remi;
    private String arde_remi;
    private String carg_remi;
    private String nomb_remi;
    
    private String tipo_dest;
    private String enti_dest;
    private String arde_dest;
    private String carg_dest;
    private String nomb_dest;
    
    private String nupr_hist;
    private int nufo_hist;
    private Date fell_hist;
    private Date fere_hist;
    
    private String esta_hist;
    
    private String ubic_hist;
    
    private String obsv_hist;
    private String holl_hist;
    private String hore_hist;
    
    private Date fech_repo;
    private String noms_repo;

    public String getNume_expe() {
        return nume_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }

    public String getTipo_admi() {
        return tipo_admi;
    }

    public void setTipo_admi(String tipo_admi) {
        this.tipo_admi = tipo_admi;
    }

    public String getEnti_admin() {
        return enti_admin;
    }

    public void setEnti_admin(String enti_admin) {
        this.enti_admin = enti_admin;
    }

    public String getArde_admin() {
        return arde_admin;
    }

    public void setArde_admin(String arde_admin) {
        this.arde_admin = arde_admin;
    }

    public String getCarg_admin() {
        return carg_admin;
    }

    public void setCarg_admin(String carg_admin) {
        this.carg_admin = carg_admin;
    }

    public String getNomb_admin() {
        return nomb_admin;
    }

    public void setNomb_admin(String nomb_admin) {
        this.nomb_admin = nomb_admin;
    }

    public String getTipo_remi() {
        return tipo_remi;
    }

    public void setTipo_remi(String tipo_remi) {
        this.tipo_remi = tipo_remi;
    }

    public String getEnti_remi() {
        return enti_remi;
    }

    public void setEnti_remi(String enti_remi) {
        this.enti_remi = enti_remi;
    }

    public String getArde_remi() {
        return arde_remi;
    }

    public void setArde_remi(String arde_remi) {
        this.arde_remi = arde_remi;
    }

    public String getCarg_remi() {
        return carg_remi;
    }

    public void setCarg_remi(String carg_remi) {
        this.carg_remi = carg_remi;
    }

    public String getNomb_remi() {
        return nomb_remi;
    }

    public void setNomb_remi(String nomb_remi) {
        this.nomb_remi = nomb_remi;
    }

    public String getTipo_dest() {
        return tipo_dest;
    }

    public void setTipo_dest(String tipo_dest) {
        this.tipo_dest = tipo_dest;
    }

    public String getEnti_dest() {
        return enti_dest;
    }

    public void setEnti_dest(String enti_dest) {
        this.enti_dest = enti_dest;
    }

    public String getArde_dest() {
        return arde_dest;
    }

    public void setArde_dest(String arde_dest) {
        this.arde_dest = arde_dest;
    }

    public String getCarg_dest() {
        return carg_dest;
    }

    public void setCarg_dest(String carg_dest) {
        this.carg_dest = carg_dest;
    }

    public String getNomb_dest() {
        return nomb_dest;
    }

    public void setNomb_dest(String nomb_dest) {
        this.nomb_dest = nomb_dest;
    }

    public String getNupr_hist() {
        return nupr_hist;
    }

    public void setNupr_hist(String nupr_hist) {
        this.nupr_hist = nupr_hist;
    }

    public int getNufo_hist() {
        return nufo_hist;
    }

    public void setNufo_hist(int nufo_hist) {
        this.nufo_hist = nufo_hist;
    }

    public Date getFell_hist() {
        return fell_hist;
    }

    public void setFell_hist(Date fell_hist) {
        this.fell_hist = fell_hist;
    }

    public Date getFere_hist() {
        return fere_hist;
    }

    public void setFere_hist(Date fere_hist) {
        this.fere_hist = fere_hist;
    }

    public String getUbic_hist() {
        return ubic_hist;
    }

    public void setUbic_hist(String ubic_hist) {
        this.ubic_hist = ubic_hist;
    }

    public String getObsv_hist() {
        return obsv_hist;
    }

    public void setObsv_hist(String obsv_hist) {
        this.obsv_hist = obsv_hist;
    }

    public Date getFech_repo() {
        return fech_repo;
    }

    public void setFech_repo(Date fech_repo) {
        this.fech_repo = fech_repo;
    }

    public String getNoms_repo() {
        return noms_repo;
    }

    public void setNoms_repo(String noms_repo) {
        this.noms_repo = noms_repo;
    }

    public String getEsta_hist() {
        return esta_hist;
    }

    public void setEsta_hist(String esta_hist) {
        this.esta_hist = esta_hist;
    }

    public String getHoll_hist() {
        return holl_hist;
    }

    public void setHoll_hist(String holl_hist) {
        this.holl_hist = holl_hist;
    }

    public String getHore_hist() {
        return hore_hist;
    }

    public void setHore_hist(String hore_hist) {
        this.hore_hist = hore_hist;
    }
    
    
}
