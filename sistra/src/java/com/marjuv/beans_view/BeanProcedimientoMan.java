
package com.marjuv.beans_view;

public class BeanProcedimientoMan implements Cloneable {
    
    private int codi_proc;
    private String esta_proc;
    private String nomb_proc;
    private int codi_asex;
    private String desc_asex;
    private String cost_proc;
    private int dias_proc;
    private int codi_carg;
    private String nomb_carg;

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int getCodi_proc() {
        return codi_proc;
    }

    public void setCodi_proc(int codi_proc) {
        this.codi_proc = codi_proc;
    }

    public String getEsta_proc() {
        return esta_proc;
    }

    public void setEsta_proc(String esta_proc) {
        this.esta_proc = esta_proc;
    }

    public String getNomb_proc() {
        return nomb_proc;
    }

    public void setNomb_proc(String nomb_proc) {
        this.nomb_proc = nomb_proc;
    }

    public int getCodi_asex() {
        return codi_asex;
    }

    public void setCodi_asex(int codi_asex) {
        this.codi_asex = codi_asex;
    }

    public String getDesc_asex() {
        return desc_asex;
    }

    public void setDesc_asex(String desc_asex) {
        this.desc_asex = desc_asex;
    }

    public String getCost_proc() {
        return cost_proc;
    }

    public void setCost_proc(String cost_proc) {
        this.cost_proc = cost_proc;
    }

    public int getDias_proc() {
        return dias_proc;
    }

    public void setDias_proc(int dias_proc) {
        this.dias_proc = dias_proc;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public String getNomb_carg() {
        return nomb_carg;
    }

    public void setNomb_carg(String nomb_carg) {
        this.nomb_carg = nomb_carg;
    }

    
}
