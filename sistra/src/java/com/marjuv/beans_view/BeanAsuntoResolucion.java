package com.marjuv.beans_view;
public class BeanAsuntoResolucion implements Cloneable{

    private int codi_asre;
    private String desc_asre;
    private String esta_asre;
    
    

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCodi_asre() {
        return codi_asre;
    }

    public void setCodi_asre(int codi_asre) {
        this.codi_asre = codi_asre;
    }

    public String getDesc_asre() {
        return desc_asre;
    }

    public void setDesc_asre(String desc_asre) {
        this.desc_asre = desc_asre;
    }

    public String getEsta_asre() {
        return esta_asre;
    }

    public void setEsta_asre(String esta_asre) {
        this.esta_asre = esta_asre;
    }
    
    
    
}
