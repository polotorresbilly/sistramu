package com.marjuv.beans_view;

public class BeanDestinoExpe implements Cloneable{
    
    private int codi_carg;
    private int codi_cont;
    private int codi_enti;
    private int codigo_usuario;
    private String nomb_enti;
    private String nomb_arde;
    private String nomb_carg;
    private String nomb_cont;
    private String nombre_usuario;
    private boolean copias;
    private String copias_text;
    
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public BeanDestinoExpe() {
        codi_carg = 0;
        codi_cont = 0;
        codi_enti = 0;
        nomb_enti = "";
        nomb_arde = "";
        nomb_carg = "";
        nomb_cont = "";
        copias_text = "";
        copias = false;
    }

    public BeanDestinoExpe(int codi_carg, int codi_cont, int codi_enti, String nomb_enti, String nomb_arde, String nomb_carg, String nomb_cont, int copias) {
        this.codi_carg = codi_carg;
        this.codi_cont = codi_cont;
        this.codi_enti = codi_enti;
        this.nomb_enti = nomb_enti;
        this.nomb_arde = nomb_arde;
        this.nomb_carg = nomb_carg;
        this.nomb_cont = nomb_cont;
        this.copias = false;
    }

    public int getCodi_carg() {
        return codi_carg;
    }

    public void setCodi_carg(int codi_carg) {
        this.codi_carg = codi_carg;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

    public String getNomb_enti() {
        return nomb_enti;
    }

    public void setNomb_enti(String nomb_enti) {
        this.nomb_enti = nomb_enti;
    }

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public String getNomb_carg() {
        return nomb_carg;
    }

    public void setNomb_carg(String nomb_carg) {
        this.nomb_carg = nomb_carg;
    }

    public String getNomb_cont() {
        return nomb_cont;
    }

    public void setNomb_cont(String nomb_cont) {
        this.nomb_cont = nomb_cont;
    }

    public boolean getCopias() {
        return copias;
    }

    public void setCopias(boolean copias) {
        this.copias = copias;
    }

    public String getCopias_text() {
        return copias_text;
    }

    public void setCopias_text(String copias_text) {
        this.copias_text = copias_text;
    }

    /**
     * @return the codigo_usuario
     */
    public int getCodigo_usuario() {
        return codigo_usuario;
    }

    /**
     * @param codigo_usuario the codigo_usuario to set
     */
    public void setCodigo_usuario(int codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    /**
     * @return the nombre_usuario
     */
    public String getNombre_usuario() {
        return nombre_usuario;
    }

    /**
     * @param nombre_usuario the nombre_usuario to set
     */
    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }


}
