package com.marjuv.beans_view;

public class Bean_Reporte_Estados_Expediente {
    
    private String NUMERO_EXPEDIENTE;
    private String FECHA_INGRESO;
    private String FECHA_RECEPCION;
    private String FECHA_TRAMITE;
    private int CORRELATIVO;
    private int FOLIOS;
    private int CODIGO_PROCEDIMIENTO;
    private String NOMBRE_PROCEDIMIENTO;
    private String TIPO_DOCUMENTO;
    private int CODIGO_CARGO_ENVIA;
    private int CODIGO_CARGO_RECIBE;
    private int CODIGO_ADMINISTRADO_CONTRIBUYENTE;
    private int CODIGO_ADMINISTRADO_CARGO;
    private int CODIGO_ADMINISTRADO_ENTIDAD;
    private String NOMBRE_ADMINISTRADO_CARGO;
    private String NOMBRE_ADMINISTRADO_ENTIDAD;
    private String NOMBRE_ADMINISTRADO_CONTRIBUYENTE;
    private String NOMBRE_CARGO_ENVIA;
    private String NOMBRE_CARGO_RECIBE;
    private String PROCEDIMIENTO_UTILIZADO;
    private String FECHA_CADUCIDAD;
    private String ADMINISTRADO_CORRECTO;

    public String getNUMERO_EXPEDIENTE() {
        return NUMERO_EXPEDIENTE;
    }

    public void setNUMERO_EXPEDIENTE(String NUMERO_EXPEDIENTE) {
        this.NUMERO_EXPEDIENTE = NUMERO_EXPEDIENTE;
    }

    public int getCORRELATIVO() {
        return CORRELATIVO;
    }

    public void setCORRELATIVO(int CORRELATIVO) {
        this.CORRELATIVO = CORRELATIVO;
    }

    public int getFOLIOS() {
        return FOLIOS;
    }

    public void setFOLIOS(int FOLIOS) {
        this.FOLIOS = FOLIOS;
    }

    public int getCODIGO_PROCEDIMIENTO() {
        return CODIGO_PROCEDIMIENTO;
    }

    public void setCODIGO_PROCEDIMIENTO(int CODIGO_PROCEDIMIENTO) {
        this.CODIGO_PROCEDIMIENTO = CODIGO_PROCEDIMIENTO;
    }

    public String getNOMBRE_PROCEDIMIENTO() {
        return NOMBRE_PROCEDIMIENTO;
    }

    public void setNOMBRE_PROCEDIMIENTO(String NOMBRE_PROCEDIMIENTO) {
        this.NOMBRE_PROCEDIMIENTO = NOMBRE_PROCEDIMIENTO;
    }

    public int getCODIGO_CARGO_ENVIA() {
        return CODIGO_CARGO_ENVIA;
    }

    public void setCODIGO_CARGO_ENVIA(int CODIGO_CARGO_ENVIA) {
        this.CODIGO_CARGO_ENVIA = CODIGO_CARGO_ENVIA;
    }

    public int getCODIGO_CARGO_RECIBE() {
        return CODIGO_CARGO_RECIBE;
    }

    public void setCODIGO_CARGO_RECIBE(int CODIGO_CARGO_RECIBE) {
        this.CODIGO_CARGO_RECIBE = CODIGO_CARGO_RECIBE;
    }

    public int getCODIGO_ADMINISTRADO_CONTRIBUYENTE() {
        return CODIGO_ADMINISTRADO_CONTRIBUYENTE;
    }

    public void setCODIGO_ADMINISTRADO_CONTRIBUYENTE(int CODIGO_ADMINISTRADO_CONTRIBUYENTE) {
        this.CODIGO_ADMINISTRADO_CONTRIBUYENTE = CODIGO_ADMINISTRADO_CONTRIBUYENTE;
    }

    public int getCODIGO_ADMINISTRADO_CARGO() {
        return CODIGO_ADMINISTRADO_CARGO;
    }

    public void setCODIGO_ADMINISTRADO_CARGO(int CODIGO_ADMINISTRADO_CARGO) {
        this.CODIGO_ADMINISTRADO_CARGO = CODIGO_ADMINISTRADO_CARGO;
    }

    public int getCODIGO_ADMINISTRADO_ENTIDAD() {
        return CODIGO_ADMINISTRADO_ENTIDAD;
    }

    public void setCODIGO_ADMINISTRADO_ENTIDAD(int CODIGO_ADMINISTRADO_ENTIDAD) {
        this.CODIGO_ADMINISTRADO_ENTIDAD = CODIGO_ADMINISTRADO_ENTIDAD;
    }

    public String getNOMBRE_ADMINISTRADO_CARGO() {
        return NOMBRE_ADMINISTRADO_CARGO;
    }

    public void setNOMBRE_ADMINISTRADO_CARGO(String NOMBRE_ADMINISTRADO_CARGO) {
        this.NOMBRE_ADMINISTRADO_CARGO = NOMBRE_ADMINISTRADO_CARGO;
    }

    public String getNOMBRE_ADMINISTRADO_ENTIDAD() {
        return NOMBRE_ADMINISTRADO_ENTIDAD;
    }

    public void setNOMBRE_ADMINISTRADO_ENTIDAD(String NOMBRE_ADMINISTRADO_ENTIDAD) {
        this.NOMBRE_ADMINISTRADO_ENTIDAD = NOMBRE_ADMINISTRADO_ENTIDAD;
    }

    public String getNOMBRE_ADMINISTRADO_CONTRIBUYENTE() {
        return NOMBRE_ADMINISTRADO_CONTRIBUYENTE;
    }

    public void setNOMBRE_ADMINISTRADO_CONTRIBUYENTE(String NOMBRE_ADMINISTRADO_CONTRIBUYENTE) {
        this.NOMBRE_ADMINISTRADO_CONTRIBUYENTE = NOMBRE_ADMINISTRADO_CONTRIBUYENTE;
    }

    public String getNOMBRE_CARGO_ENVIA() {
        return NOMBRE_CARGO_ENVIA;
    }

    public void setNOMBRE_CARGO_ENVIA(String NOMBRE_CARGO_ENVIA) {
        this.NOMBRE_CARGO_ENVIA = NOMBRE_CARGO_ENVIA;
    }

    public String getNOMBRE_CARGO_RECIBE() {
        return NOMBRE_CARGO_RECIBE;
    }

    public void setNOMBRE_CARGO_RECIBE(String NOMBRE_CARGO_RECIBE) {
        this.NOMBRE_CARGO_RECIBE = NOMBRE_CARGO_RECIBE;
    }

    public String getPROCEDIMIENTO_UTILIZADO() {
        return PROCEDIMIENTO_UTILIZADO;
    }

    public void setPROCEDIMIENTO_UTILIZADO(String PROCEDIMIENTO_UTILIZADO) {
        this.PROCEDIMIENTO_UTILIZADO = PROCEDIMIENTO_UTILIZADO;
    }

    public String getFECHA_CADUCIDAD() {
        return FECHA_CADUCIDAD;
    }

    public void setFECHA_CADUCIDAD(String FECHA_CADUCIDAD) {
        this.FECHA_CADUCIDAD = FECHA_CADUCIDAD;
    }

    public String getFECHA_INGRESO() {
        return FECHA_INGRESO;
    }

    public void setFECHA_INGRESO(String FECHA_INGRESO) {
        this.FECHA_INGRESO = FECHA_INGRESO;
    }

    public String getFECHA_RECEPCION() {
        return FECHA_RECEPCION;
    }

    public void setFECHA_RECEPCION(String FECHA_RECEPCION) {
        this.FECHA_RECEPCION = FECHA_RECEPCION;
    }

    public String getFECHA_TRAMITE() {
        return FECHA_TRAMITE;
    }

    public void setFECHA_TRAMITE(String FECHA_TRAMITE) {
        this.FECHA_TRAMITE = FECHA_TRAMITE;
    }

    public String getTIPO_DOCUMENTO() {
        return TIPO_DOCUMENTO;
    }

    public void setTIPO_DOCUMENTO(String TIPO_DOCUMENTO) {
        this.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
    }

    public String getADMINISTRADO_CORRECTO() {
        return ADMINISTRADO_CORRECTO;
    }

    public void setADMINISTRADO_CORRECTO(String ADMINISTRADO_CORRECTO) {
        this.ADMINISTRADO_CORRECTO = ADMINISTRADO_CORRECTO;
    }
    
    
    
}
