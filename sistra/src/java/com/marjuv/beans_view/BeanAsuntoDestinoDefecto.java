/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.beans_view;

/**
 *
 * @author Andy
 */
public class BeanAsuntoDestinoDefecto implements Cloneable {

    private String nomb_arde;
    private String desc_asre;
    private int codi_arde;
    private int codi_asre;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNomb_arde() {
        return nomb_arde;
    }

    public void setNomb_arde(String nomb_arde) {
        this.nomb_arde = nomb_arde;
    }

    public String getDesc_asre() {
        return desc_asre;
    }

    public void setDesc_asre(String desc_asre) {
        this.desc_asre = desc_asre;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public int getCodi_asre() {
        return codi_asre;
    }

    public void setCodi_asre(int codi_asre) {
        this.codi_asre = codi_asre;
    }

}
