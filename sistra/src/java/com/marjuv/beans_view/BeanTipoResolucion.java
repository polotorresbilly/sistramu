package com.marjuv.beans_view;
public class BeanTipoResolucion implements Cloneable{

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }
    
    private int codi_tire;
    private String desc_tire;
    private String esta_tire;

    public BeanTipoResolucion(int codi_tire, String desc_tire, String esta_tire) {
        this.codi_tire = codi_tire;
        this.desc_tire = desc_tire;
        this.esta_tire = esta_tire;
    }

    public BeanTipoResolucion() {
        this.codi_tire = 0;
        this.desc_tire = "";
        this.esta_tire = "";
    }
    
    

    public int getCodi_tire() {
        return codi_tire;
    }

    public void setCodi_tire(int codi_tire) {
        this.codi_tire = codi_tire;
    }

    public String getDesc_tire() {
        return desc_tire;
    }

    public void setDesc_tire(String desc_tire) {
        this.desc_tire = desc_tire;
    }

    public String getEsta_tire() {
        return esta_tire;
    }

    public void setEsta_tire(String esta_tire) {
        this.esta_tire = esta_tire;
    }
    
    
    
    
}
