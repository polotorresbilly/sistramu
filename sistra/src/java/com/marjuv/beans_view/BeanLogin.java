package com.marjuv.beans_view;

public class BeanLogin {

    private String usuario;
    private String clave;

    public BeanLogin() {
        usuario = "";
        clave = "";
    }

    public BeanLogin(String usuario, String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

}
