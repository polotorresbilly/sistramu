
package com.marjuv.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataTransaction {
    
    private final static String username = "javacrea";
    private final static String password = "rapeanD3";
    
//    private final static String username = "postgres";
//    private final static String password = "1234";
    
    private final static String url = "jdbc:postgresql://localhost:5432/javacrea_sistramu?noAccessToProcedureBodies=true";
    public static Connection connection = null;
    public static int connectionCount = 0;
    
    public static DataTransaction INSTANCE = new DataTransaction();
    
    public static DataTransaction getInstance(){
        return INSTANCE;
    }
  
    public DataTransaction() {

    }

    public Connection getConnection() {
        try {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DataTransaction.class.getName()).log(Level.SEVERE, null, ex);
            }
            return DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            Logger.getLogger(DataTransaction.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
