package com.marjuv.controller;

import com.marjuv.beans_view.BeanTipoExpediente;
import com.marjuv.dao.Tipo_Expediente_dao;
import com.marjuv.entity.Tipo_Expediente;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class TipoExpedienteControllerMan {

    @Wire("#tipo_expediente_man")
    private Window win;
    private String recordMode;
    private List<Pair<Integer, String>> listTipo_Expediente;
    private Pair<Integer, String> curTipo_Expediente;
    private BeanTipoExpediente BeanMan;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<Pair<Integer, String>> getListTipo_Expediente() {
        return listTipo_Expediente;
    }

    public void setListTipo_Expediente(List<Pair<Integer, String>> listTipo_Expediente) {
        this.listTipo_Expediente = listTipo_Expediente;
    }

    public Pair<Integer, String> getCurTipo_Expediente() {
        return curTipo_Expediente;
    }

    public void setCurTipo_Expediente(Pair<Integer, String> curTipo_Expediente) {
        this.curTipo_Expediente = curTipo_Expediente;
    }

    public BeanTipoExpediente getBeanMan() {
        return BeanMan;
    }

    public void setBeanMan(BeanTipoExpediente BeanMan) {
        this.BeanMan = BeanMan;
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("keyTipoExpediente") BeanTipoExpediente c1,@ExecutionArgParam("accion") String recordMode)throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.BeanMan = new BeanTipoExpediente();
        }
        if (recordMode.equals("EDIT")) {
            this.BeanMan = (BeanTipoExpediente) c1.clone();
            
        }
    }
    
    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "listaTR", null);
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            Tipo_Expediente_dao objTipo_ExpedienteDAO = new Tipo_Expediente_dao();
            
            if ((getRecordMode().equals("NEW") && objTipo_ExpedienteDAO.v_nomb_tiex_new(BeanMan.getNomb_tiex())) || (getRecordMode().equals("EDIT") && objTipo_ExpedienteDAO.v_nomb_tiex_edit(BeanMan.getCodi_tiex(),BeanMan.getNomb_tiex()))) {
                    Tipo_Expediente objTipo_Expediente = new Tipo_Expediente();
                    objTipo_Expediente.setCodi_tiex(BeanMan.getCodi_tiex());
                    objTipo_Expediente.setNomb_tiex(BeanMan.getNomb_tiex());

                    Map args = new HashMap();
                    args.put("keyTipoExpediente", objTipo_Expediente);
                    args.put("accion", this.recordMode);
                    
                    BindUtils.postGlobalCommand(null, null, "updateTR", args);
                    win.detach();
                

            } else {
                Messagebox.show("El nombre del tipo de resolución ingresado ya Existe, eliga otro");
            }
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }
    
    
}
