package com.marjuv.controller.win;

import com.marjuv.controller.man.*;
import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.entity.cargos;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class hojas_des {

    @Wire("#cargos_man")
    private Window win;
    private int hoja_dest;
    private int codi_arde;
    private int codi_enti;
    private int codi_cont;

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("codi_arde") int codi_arde,
            @ExecutionArgParam("codi_enti") int codi_enti,
            @ExecutionArgParam("codi_cont") int codi_cont,
            @ExecutionArgParam("hoja_dest") int hoja_dest)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        this.codi_arde = codi_arde;
        this.codi_enti = codi_enti;
        this.codi_cont = codi_cont;
        this.hoja_dest = hoja_dest;

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            if (this.hoja_dest > 0) {
                Map args = new HashMap();
                args.put("codi_arde", this.codi_arde);
                args.put("codi_enti", this.codi_enti);
                args.put("codi_cont", this.codi_cont);
                args.put("hoja_dest", this.hoja_dest);
                BindUtils.postGlobalCommand(null, null, "updateHojasDest", args);
                win.detach();
            } else {
                Messagebox.show("Por favor, ingrese un valor igual o mayor a 1", "Advertencia", Messagebox.OK, Messagebox.ERROR);
            }

        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Ingrese valor de hojas", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateCargoList", null);
    }

    public int getHoja_dest() {
        return hoja_dest;
    }

    public void setHoja_dest(int hoja_dest) {
        this.hoja_dest = hoja_dest;
    }

    public int getCodi_arde() {
        return codi_arde;
    }

    public void setCodi_arde(int codi_arde) {
        this.codi_arde = codi_arde;
    }

    public int getCodi_cont() {
        return codi_cont;
    }

    public void setCodi_cont(int codi_cont) {
        this.codi_cont = codi_cont;
    }

    public int getCodi_enti() {
        return codi_enti;
    }

    public void setCodi_enti(int codi_enti) {
        this.codi_enti = codi_enti;
    }

}
