package com.marjuv.controller.win;

import com.marjuv.dao.estado_expediente_dao;
import com.marjuv.entity.estado_expediente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class estado_expediente_win {

    private List<estado_expediente> lst_estado_expediente;
    private estado_expediente cur_estado_expediente;
    @WireVariable
    private Session _sess;

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
    }

    public List<estado_expediente> getLst_estado_expediente() {
        try {
            estado_expediente_dao obj_estado_expediente_dao = new estado_expediente_dao();
            lst_estado_expediente = obj_estado_expediente_dao.get_estados_expediente();
            obj_estado_expediente_dao.close();
        } catch (Exception ex) {
        }
        return lst_estado_expediente;
    }

    @Command
    public void nuevo_estado_expediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sEstado_expediente", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_estado_expediente.zul", null, map);
    }

    @Command
    public void editThisEstadoExpediente() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sEstado_expediente", this.cur_estado_expediente);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_estado_expediente.zul", null, map);
        } catch (Exception ex) {
        }
    }

    @GlobalCommand
    @NotifyChange("lst_estado_expediente")
    public void updateEstadoExpedienteList(@BindingParam("sEstado_expediente") estado_expediente cur_estado_expediente,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            estado_expediente_dao obj_estado_expediente_dao = new estado_expediente_dao();
            if (recordMode.equals("NEW")) {
                obj_estado_expediente_dao.insert(cur_estado_expediente);
                obj_estado_expediente_dao.close();
            } else if (recordMode.equals("EDIT")) {
                obj_estado_expediente_dao.update(cur_estado_expediente);
                obj_estado_expediente_dao.close();
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisEstadoExpediente() {
        String str = "¿Está seguro de que desea eliminar el estado de expediente \""
                + cur_estado_expediente.getNomb_esex()+ "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        estado_expediente_dao obj_estado_expediente_dao = new estado_expediente_dao();
                        obj_estado_expediente_dao.delete(cur_estado_expediente.getCodi_esex());
                        BindUtils.postNotifyChange(null, null, estado_expediente_win.this, "lst_estado_expediente");
                    } catch (Exception ex) {
                        Logger.getLogger(estado_expediente_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_estado_expediente(List<estado_expediente> lst_estado_expediente) {
        this.lst_estado_expediente = lst_estado_expediente;
    }

    public estado_expediente getCur_estado_expediente() {
        return cur_estado_expediente;
    }

    public void setCur_estado_expediente(estado_expediente cur_estado_expediente) {
        this.cur_estado_expediente = cur_estado_expediente;
    }

}
