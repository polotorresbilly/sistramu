package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanBusquedaExpediente;
import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.dao.Expediente_dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_expediente_win {

    @Wire("#search_expe")
    private Window win;
    private List<BeanBusquedaExpediente> lst_expediente;
    private BeanBusquedaExpediente cur_expediente;
    private List<Pair<Integer, String>> listEstadoExpediente;
    private Pair<Integer, String> curEstadoExpediente;
//    private List<BeanExpediente> lst_expediente;
//    private BeanExpediente cur_expediente;
    @WireVariable
    private Session _sess;

    @Wire
    private Textbox label_busq;
    private String Accion_Enviada;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("accion") String Accion) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        setAccion_Enviada(Accion);
//        Messagebox.show(getAccion_Enviada());
        Selectors.wireComponents(view, this, false);
        loadDataControls();
    }

    public List<BeanBusquedaExpediente> getLst_expediente() {
        try {
            Expediente_dao obj_expediente_dao = new Expediente_dao();
            lst_expediente = obj_expediente_dao.Busqueda_Expedientes(curEstadoExpediente.x,label_busq.getValue());
            obj_expediente_dao.close();
        } catch (Exception ex) {
        }
        return lst_expediente;
    }
    
    private void loadDataControls() {
        listEstadoExpediente = new ArrayList<Pair<Integer, String>>();
        listEstadoExpediente.add(new Pair<Integer, String>(1, "Pendientes"));
        listEstadoExpediente.add(new Pair<Integer, String>(2, "Recepcionados"));
        listEstadoExpediente.add(new Pair<Integer, String>(3, "Tramitados"));
        listEstadoExpediente.add(new Pair<Integer, String>(4, "Finalizados"));
        //listEstadoExpediente.add(new Pair<Integer, String>(6, "Adjuntos"));
        listEstadoExpediente.add(new Pair<Integer, String>(5, "Observados"));
        //listEstadoExpediente.add(new Pair<Integer, String>(7, "Por Tramitar"));
        curEstadoExpediente = listEstadoExpediente.get(0);
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("numero_expediente", this.cur_expediente.getNumero_expediente());
        if (getAccion_Enviada().equals("Buscar")) {
            BindUtils.postGlobalCommand(null, null, "setExpediente", args);
        } else {
            if (getAccion_Enviada().equals("Adjuntar")) {
                BindUtils.postGlobalCommand(null, null, "setExpedienteAdjunto", args);
            }
        }
        win.detach();
    }
    
    @NotifyChange("lst_expediente")
    @Command
    public void cambiarListaExpedientes(){
        getLst_expediente();
    }

    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_expediente(List<BeanBusquedaExpediente> lst_expediente) {
        this.lst_expediente = lst_expediente;
    }

    public BeanBusquedaExpediente getCur_expediente() {
        return cur_expediente;
    }

    public void setCur_expediente(BeanBusquedaExpediente cur_expediente) {
        this.cur_expediente = cur_expediente;
    }

    public String getAccion_Enviada() {
        return Accion_Enviada;
    }

    public void setAccion_Enviada(String Accion_Enviada) {
        this.Accion_Enviada = Accion_Enviada;
    }

    /**
     * @return the listEstadoExpediente
     */
    public List<Pair<Integer, String>> getListEstadoExpediente() {
        return listEstadoExpediente;
    }

    /**
     * @param listEstadoExpediente the listEstadoExpediente to set
     */
    public void setListEstadoExpediente(List<Pair<Integer, String>> listEstadoExpediente) {
        this.listEstadoExpediente = listEstadoExpediente;
    }

    /**
     * @return the curEstadoExpediente
     */
    public Pair<Integer, String> getCurEstadoExpediente() {
        return curEstadoExpediente;
    }

    /**
     * @param curEstadoExpediente the curEstadoExpediente to set
     */
    public void setCurEstadoExpediente(Pair<Integer, String> curEstadoExpediente) {
        this.curEstadoExpediente = curEstadoExpediente;
    }

}
