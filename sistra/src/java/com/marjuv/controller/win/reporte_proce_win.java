/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import static com.marjuv.controller.win.main.generarReporte;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.Procedimiento_dao;
import com.marjuv.dao.Tipo_Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;
import com.marjuv.entity.ExpedienteReportes;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.Tipo_Expediente;
import com.marjuv.entity.administracion;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class reporte_proce_win {

    private Date curFechaDesde;
    private Date curFechaHasta;

    @WireVariable
    private Session _sess;

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("reporteproce.zul")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Init
    public void initSetup() {
        check_permisos();
        curFechaDesde = new Date();
        curFechaHasta = new Date();

    }

    @Command
    public void generar() throws Exception {
        // LLAMAR REPORTE
        usuario obj_usua = (usuario) _sess.getAttribute("usuario");
        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();

        Map parametros = new HashMap();
        parametros.put("esta_con", 12);

        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        java.sql.Date fecha = new java.sql.Date(currentDate.getTime());
        int year = fecha.getYear() + 1900;
        
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(curFechaHasta);
        calendar2.add(Calendar.DAY_OF_MONTH, 1);
        
        String fecha01 = new SimpleDateFormat("yyyy-MM-dd").format(curFechaDesde);
        String fecha02 = new SimpleDateFormat("yyyy-MM-dd").format(calendar2.getTime());

        administracion_dao obj_admin_dao = new administracion_dao();
        administracion obj_admin = obj_admin_dao.get_administracion(year);

        parametros.put("noen_admi", obj_admin.getNoen_admi());
        Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
        parametros.put("noar_admi", obj_Dependencia_dao.get_nomb2(obj_usua.getCodi_arde()));
        parametros.put("codi_arde", obj_usua.getCodi_arde());
        parametros.put("realPath", sv.getRealPath("/reportes/"));
        parametros.put("fecha01", fecha01);
        parametros.put("fecha02", fecha02);
        procedimiento_expediente_dao obj_proce = new procedimiento_expediente_dao();
        parametros.put("total", String.valueOf(obj_proce.get_total(obj_usua.getCodi_arde(), fecha01, fecha02)));

        main.generarReporte("/reportes/procedimiento.jasper", parametros, "Reporte de ingresos por procedimiento.pdf", sv);

    }

    public Date getCurFechaDesde() {
        return curFechaDesde;
    }

    public void setCurFechaDesde(Date curFechaDesde) {
        this.curFechaDesde = curFechaDesde;
    }

    public Date getCurFechaHasta() {
        return curFechaHasta;
    }

    public void setCurFechaHasta(Date curFechaHasta) {
        this.curFechaHasta = curFechaHasta;
    }

}
