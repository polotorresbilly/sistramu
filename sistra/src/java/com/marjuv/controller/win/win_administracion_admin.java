package com.marjuv.controller.win;

import com.marjuv.controller.man.*;
import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.permisos_usuario_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.administracion;
import com.marjuv.entity.permiso;
import com.marjuv.entity.usuario;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.swing.JFileChooser;
import javax.swing.plaf.FileChooserUI;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class win_administracion_admin {

    @Wire("#admin_win")
    private Window win;
    private administracion selectedAdmin;
    private String recordMode;
    @WireVariable
    private Session _sess;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sUsuario") BeanUsuarioMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException, Exception {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);

        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        Date fecha = new Date(currentDate.getTime());
        int year = fecha.getYear() + 1900;

        administracion_dao obj_administracion_dao = new administracion_dao();
        if (obj_administracion_dao.verificar_admin(year)) {
            setRecordMode("EDIT");
            this.selectedAdmin = obj_administracion_dao.get_administracion(year);
        } else {
            setRecordMode("NEW");
            this.selectedAdmin = new administracion();
            this.selectedAdmin.setYear_admi(year);
        }
        obj_administracion_dao.close();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        
//        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
//        Messagebox.show("URL: " + sv.getRealPath("/archivos/"));
        
        administracion_dao obj_administracion_dao = new administracion_dao();
        try {
//            Calendar fecha2 = new GregorianCalendar();
//            int año = fecha2.get(Calendar.YEAR);
//            administracion ObjAdministracionPrueba = new administracion();
//            ObjAdministracionPrueba = obj_administracion_dao.get_administracion(año);
            if (getRecordMode().equals("NEW")) {
                obj_administracion_dao.insert_admin(selectedAdmin);
                Messagebox.show("La administración ha sido registrada con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                Executions.sendRedirect("page.zul");
            } else if (getRecordMode().equals("EDIT")) {
                obj_administracion_dao.update_admin(selectedAdmin);
                Messagebox.show("La administración ha sido actualizada con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
            obj_administracion_dao.close();
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public administracion getSelectedAdmin() {
        return selectedAdmin;
    }

    public void setSelectedAdmin(administracion selectedAdmin) {
        this.selectedAdmin = selectedAdmin;
    }

}
