package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanExpediente;
import static com.marjuv.controller.win.main.generarReporte;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.Procedimiento_dao;
import com.marjuv.dao.Tipo_Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;
import com.marjuv.entity.ExpedienteReportes;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.Tipo_Expediente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author BILLY
 */
public class Cexpedientereportesareas_win {

    private List<Procedimiento_Expediente> list_procedimiento;
    private List<Tipo_Expediente> list_tipoexpediente;
    private List<Contribuyente> list_contribuyente;
    private List<BeanDependencias> list_dependencia;
    private List<Entidad> list_entidad;
    private List<ExpedienteReportes> list_expediente;
    private List<BeanExpediente> lista_expediente_reporte;

    private String curNumExp;
    private Procedimiento_Expediente curProcedimiento;
    private Tipo_Expediente curTipoExp;
    private Contribuyente curContribuyente;
    private BeanDependencias curDependencia;
    private Entidad curEntidad;
    private Contribuyente curContribuyenteDest;
    private BeanDependencias curDependenciaDest;
    private Entidad curEntidadDest;
    private ExpedienteReportes curExpediente;
    // ELECCION DE CHECKBOX
    private String curEleccion;
    private List<Pair<Integer, String>> listEstadoExpediente;
    private Pair<Integer, String> curEstadoExpediente;
    private int curAdministrado;
    private int curDestinatario;

    private Date curFechaDesde;
    private Date curFechaHasta;

    private Contribuyente_dao dao_contribuyente;
    private Dependencia_dao dao_dependencia;
    private Entidad_dao dao_entidad;
    private Procedimiento_dao dao_procedimiento;
    private Tipo_Expediente_dao dao_tipoexpediente;
    private Expediente_dao dao_expediente;

    private List<BeanDependencias> list_dependencias;
    private BeanDependencias curDependenciamaster;
    private BeanDependencias curDependencia2master;

    private String mensaje;
    @WireVariable
    private Session _sess;
    
    @Wire
    private Textbox txtNExpediente;
    @Wire
    private Label lblNumero_Expediente;
    @Wire
    private Label lblTipoDoc;
    @Wire
    private Combobox cboTipoDoc;
    @Wire
    private Button btnAddProc;
    @Wire
    private Label lblCodiProc;
    @Wire
    private Label lblNombProc;
    @Wire
    private Textbox txtCodiProc;
    @Wire
    private Textbox txtNombProc;
    @Wire
    private Button btnAddDestinatario;
    @Wire
    private Label lblCodiDestinatario;
    @Wire
    private Label lblNombDestinatario;
    @Wire
    private Textbox txtCodiDestinatario;
    @Wire
    private Textbox txtNombDestinatario;
    @Wire
    private Button btnAddAdministrado;
    @Wire
    private Label lblCodiAdministrado;
    @Wire
    private Label lblNombAdministrado;
    @Wire
    private Textbox txtCodiAdministrado;
    @Wire
    private Textbox txtNombAdministrado;
    
    

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
        }
        return fin;
    }
    
    @Command
    public void ver_expedientes_vinculados(){
        Map parametros = new HashMap();
        parametros.put("nume_expe", curExpediente.getEXPEDIENTE());
        Executions.createComponents("proceso/win/win_ver_expediente_vinculados.zul", null, parametros);
    }

    @NotifyChange("*")
    @Command
    public void openProc() {
        Executions.createComponents("ZonaCompartida/add_procedimiento.zul", null, null);
    }

    @NotifyChange("*")
    @Command
    public void openDestinatario() {
        Executions.createComponents("ZonaCompartida/add_destinatario_report_expediente.zul", null, null);
    }

    @NotifyChange("*")
    @Command
    public void openAdministrado() {
        Executions.createComponents("ZonaCompartida/add_administrado.zul", null, null);
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<BeanDependencias> getList_dependencias() {
        return list_dependencias;
    }

    public void setList_dependencias(List<BeanDependencias> list_dependencias) {
        this.list_dependencias = list_dependencias;
    }

    public BeanDependencias getCurDependenciamaster() {
        return curDependenciamaster;
    }

    public void setCurDependenciamaster(BeanDependencias curDependenciamaster) {
        this.curDependenciamaster = curDependenciamaster;
    }

    public BeanDependencias getCurDependencia2master() {
        return curDependencia2master;
    }

    public void setCurDependencia2master(BeanDependencias curDependencia2master) {
        this.curDependencia2master = curDependencia2master;
    }

    private void loadDataControls() {
        listEstadoExpediente = new ArrayList<Pair<Integer, String>>();
        listEstadoExpediente.add(new Pair<Integer, String>(1, "Pendientes"));
        listEstadoExpediente.add(new Pair<Integer, String>(2, "Recepcionados"));
        listEstadoExpediente.add(new Pair<Integer, String>(3, "Tramitados"));
        listEstadoExpediente.add(new Pair<Integer, String>(4, "Finalizados"));
        //listEstadoExpediente.add(new Pair<Integer, String>(6, "Adjuntos"));
        listEstadoExpediente.add(new Pair<Integer, String>(5, "Observados"));
        //listEstadoExpediente.add(new Pair<Integer, String>(7, "Por Tramitar"));
        curEstadoExpediente = listEstadoExpediente.get(0);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("reporteexpediente_win.zul")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Init
    public void initSetup() {
        check_permisos();
        loadDataControls();

        curFechaDesde = new Date();
        curFechaHasta = new Date();
        setMensaje("Seleccione una dependencia");
        try {
            dao_contribuyente = new Contribuyente_dao();
            dao_dependencia = new Dependencia_dao();
            dao_entidad = new Entidad_dao();
            dao_procedimiento = new Procedimiento_dao();
            dao_tipoexpediente = new Tipo_Expediente_dao();
            setList_tipoexpediente(dao_tipoexpediente.get_tipo_expedientes());
            dao_tipoexpediente.close();
            setList_procedimiento(dao_procedimiento.get_procedimientos());
            dao_procedimiento.close();
            setList_contribuyente(dao_contribuyente.get_contribuyentes());
            dao_contribuyente.close();
            setList_dependencia(dao_dependencia.getDependencias());

            setList_entidad(dao_entidad.get_entidades());
            dao_entidad.close();

            setCurEleccion("01");
            setCurAdministrado(1);
            setCurDestinatario(1);
            setCurNumExp("");

            List<BeanDependencias> list = new ArrayList<BeanDependencias>();
            for (BeanDependencias beanDependencias : dao_dependencia.get_dependencias_reporte_estado_areas()) {
//                if (beanDependencias.getNive_arde() == 1) {
                list.add(beanDependencias);
//                }
            }
            dao_dependencia.close();
            setList_dependencias(list);
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }

        BindUtils.postNotifyChange(null, null, Cexpedientereportesareas_win.this, "list_tipoexpediente");
        BindUtils.postNotifyChange(null, null, Cexpedientereportesareas_win.this, "list_procedimiento");
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("*")
    public void updateCargo() {
        try {
            dao_dependencia = new Dependencia_dao();
            List<BeanDependencias> obj = dao_dependencia.getDependenciasJera(curDependencia2master.getCodi_arde());
            dao_dependencia.close();
            if (obj.size() > 0) {
                setList_dependencias(obj);
            }
            curDependenciamaster = curDependencia2master;
            setMensaje("Nivel " + curDependenciamaster.getNive_arde() + ": " + curDependenciamaster.getNomb_arde());
        } catch (Exception ex) {
            //Logger.getLogger(CtramitarexpedienteT_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Command
    @NotifyChange("*")
    public void atrasDependencia() {
        try {
            dao_dependencia = new Dependencia_dao();
            BeanDependencias beforexpe = dao_dependencia.getDependencia(curDependenciamaster.getJera_arde());
            if (beforexpe != null) {
                if (beforexpe.getNive_arde() > 1) {
                    List<BeanDependencias> obj = dao_dependencia.getDependenciasJera(beforexpe.getJera_arde());
                    setList_dependencias(obj);
                    setCurDependencia2master(null);
                    setCurDependenciamaster(getList_dependencias().get(get_dependencia(beforexpe)));
                } else {
                    List<BeanDependencias> list = new ArrayList<BeanDependencias>();
                    for (BeanDependencias beanDependencias : dao_dependencia.getDependencias()) {
                        if (beanDependencias.getNive_arde() == 1) {
                            list.add(beanDependencias);
                        }
                    }
                    setList_dependencias(list);
                    setCurDependencia2master(null);
                    setCurDependenciamaster(getList_dependencias().get(get_dependencia(beforexpe)));
                }
                setMensaje("Nivel " + curDependenciamaster.getNive_arde() + ": " + curDependenciamaster.getNomb_arde());
            }
            dao_dependencia.close();
        } catch (Exception ex) {
            Logger.getLogger(Cexpedientereportesareas_win.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int get_dependencia(BeanDependencias obj) {
        for (int i = 0; i < list_dependencias.size(); i++) {
            if (list_dependencias.get(i).getCodi_arde() == obj.getCodi_arde()) {
                return i;
            }
        }
        return 0;
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setProcedimiento(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc) {
        this.txtCodiProc.setValue(String.valueOf(codi_proc));
        this.txtNombProc.setValue(nomb_proc);

    }

    @GlobalCommand
    @NotifyChange("*")
    public void setDestinatario(@BindingParam("tipo") int tipo, @BindingParam("codigo") int codi_cont, @BindingParam("nombre") String nomb_cont) {
        if (tipo == 1) {
            this.txtCodiDestinatario.setValue(String.valueOf(codi_cont));
            this.txtNombDestinatario.setValue(nomb_cont);
        } else if (tipo == 2) {
            this.txtCodiDestinatario.setValue(String.valueOf(codi_cont));
            this.txtNombDestinatario.setValue(nomb_cont);
        } else if (tipo == 3) {
            this.txtCodiDestinatario.setValue(String.valueOf(codi_cont));
            this.txtNombDestinatario.setValue(nomb_cont);
        } else {
            Messagebox.show("No se ah enviado ningún destinatario");
        }

    }
    @Command
    public void Busqueda_General(){
        try {
            txtNExpediente.setValue("");
            txtCodiAdministrado.setValue("");
            txtCodiDestinatario.setValue("");
            txtCodiProc.setValue("");
            txtNombAdministrado.setValue("");
            txtNombDestinatario.setValue("");
            txtNombProc.setValue("");
//            txtNExpediente.setVisible(false);
//            lblNumero_Expediente.setVisible(false);
//            lblCodiAdministrado.setVisible(false);
//            lblCodiDestinatario.setVisible(false);
//            lblCodiProc.setVisible(false);
//            lblNombAdministrado.setVisible(false);
//            lblNombDestinatario.setVisible(false);
//            lblNombProc.setVisible(false);
//            lblNumero_Expediente.setVisible(false);
//            lblTipoDoc.setVisible(false);
//            btnAddAdministrado.setVisible(false);
//            btnAddDestinatario.setVisible(false);;
//            btnAddProc.setVisible(false);
        } catch (Exception e) {
        }
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setAdministrado(@BindingParam("tipo") int tipo, @BindingParam("codigo") int codi_cont, @BindingParam("nombre") String nomb_cont) {
        if (tipo == 1) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
        } else if (tipo == 2) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
        } else if (tipo == 3) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
        } else {
            Messagebox.show("No se ah enviado ningún Administrado");
        }

    }

    @Command
    public void CargarDatos() {
        setList_expediente(null);
        if (getCurEleccion().equals("06")) // POR NUMERO DE EXPEDIENTE
        {
            if (this.txtNExpediente.getValue().equals("")) {
                Messagebox.show("Escriba un numero de expediente");
                return;
            } else {
                //Messagebox.show("ok");
            }

        }
        if (getCurEleccion().equals("02")) {
            if (getCurTipoExp() == null) {
                Messagebox.show("Seleccione un tipo de Expediente");
                return;
            }
        }
        if (getCurEleccion().equals("03")) // POR PROCEDIMIENTO
        {

            if (txtCodiProc.getValue().equals("")) {
                Messagebox.show("Seleccione un procedimiento");
                return;
            } else {
                //Messagebox.show("ok");
            }
        }
        if (getCurEleccion().equals("04")) // POR DESTINATARIO
        {
            if (this.txtCodiDestinatario.getValue().equals("")) {
                Messagebox.show("Seleccione un destinatario");
            } else {
                //Messagebox.show("ok");
            }

        }
        if (getCurEleccion().equals("05")) // POR ADMINISTRADO
        {
            if (this.txtCodiAdministrado.getValue().equals("")) {
                Messagebox.show("Seleccione un destinatario");
            } else {
                //Messagebox.show("ok");
            }

        }
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            int Param4 = 0;
            int Param5 = 0;
            if (curEleccion.equals("02")) {
                Param4 = curTipoExp.getCodi_tiex();
            } else if (curEleccion.equals("03")) {
                //Param4 = curProcedimiento.getCodi_proc();
                Param4 = Integer.parseInt(txtCodiProc.getValue());
            } else if (curEleccion.equals("04")) {
                Param4 = curDestinatario;
                Param5 = Integer.parseInt(this.txtCodiDestinatario.getValue());
                /*if (Param4 == 1) {
                 Param5 = curDependenciaDest.getCodi_arde();
                 } else if (Param4 == 2) {
                 Param5 = curContribuyenteDest.getCodi_cont();
                 } else if (Param4 == 3) {
                 Param5 = curEntidadDest.getCodi_enti();
                 }*/
            } else if (curEleccion.equals("05")) {
                Param4 = curAdministrado;
                Param5 = Integer.parseInt(this.txtCodiAdministrado.getValue());
                /*if (Param4 == 1) {
                 Param5 = curDependencia.getCodi_arde();
                 } else if (Param4 == 2) {
                 Param5 = curContribuyente.getCodi_cont();
                 } else if (Param4 == 3) {
                 Param5 = curEntidad.getCodi_enti();
                 }*/
            } else if (curEleccion.equals("06")) {
                try {
                    Param4 = Integer.parseInt(curNumExp);
                    //Messagebox.show(curNumExp);
                    Param5 = Integer.parseInt(this.txtNExpediente.getValue());
                } catch (Exception e) {
                    Messagebox.show("Ingrese solo numeros a Expediente");
                    return;
                }
            }
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            //Messagebox.show(formatter.format(curFechaDesde)+ ","+formatter.format(curFechaHasta)+","+ curEleccion+","+ Param4+","+Param5+","+obj_usuario.getCodi_arde()+","+curEstadoExpediente.x);
            curDependenciamaster = curDependencia2master;
            if (curDependenciamaster == null) {
                Messagebox.show("Seleccione una dependencia");
                return;
            }
            dao_expediente = new Expediente_dao();
            setList_expediente(dao_expediente.get_expedientes_reportes(formatter.format(curFechaDesde), formatter.format(curFechaHasta), curEleccion, Param4, Param5, curDependenciamaster.getCodi_arde(), curEstadoExpediente.x));
//            for(int x=0; x <= list_expediente.size(); x++){
//               Messagebox.show("Numero de Expediente Buscado:" + list_expediente.get(x).getEXPEDIENTE()); 
//            }
            dao_expediente.close();

            setCurExpediente(null);
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
        BindUtils.postNotifyChange(null, null, Cexpedientereportesareas_win.this, "list_expediente");
    }

    @Command
    public void actualizar_eleccion2() {
        setCurEleccion("02");
    }

    @Command
    public void actualizar_eleccion3() {
        setCurEleccion("03");
    }

    @Command
    public void actualizar_eleccion4() {
        setCurEleccion("04");
    }

    @Command
    public void actualizar_eleccion5() {
        setCurEleccion("05");
    }

    @Command
    public void actualizar_eleccion6() {
        setCurEleccion("06");
    }

    @Command
    public void ReporteExpedientes() {

        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            int Param4 = 0;
            int Param5 = 0;
            String cons_con = "";
            if (curEleccion.equals("06")) // POR NUMERO DE EXPEDIENTE
            {
                cons_con = "NÚMERO DE EXPEDIENTE";
                if (getCurNumExp().isEmpty()) {
                    Messagebox.show("Ingrese un numero de expediente");
                    return;
                }
            }
            if (curEleccion.equals("02")) // POR PROCEDIMIENTO
            {
                cons_con = "TIPO DE DOCUMENTO";
                if (getCurTipoExp() == null) {
                    Messagebox.show("Seleccione un tipo de Expediente");
                    return;
                }
            }
            if (curEleccion.equals("03")) // POR PROCEDIMIENTO
            {
                cons_con = "PROCEDIMIENTO";
                if (txtCodiProc.getValue().equals("")) {
                    Messagebox.show("Seleccione un procedimiento");
                    return;
                } else {
                    //Messagebox.show("ok");
                }
            }
            if (curEleccion.equals("04")) // POR DESTINATARIO
            {
                cons_con = "DESTINATARIO";
                if (this.txtCodiDestinatario.getValue().equals("")) {
                    Messagebox.show("Seleccione un destinatario");
                } else {
                    //Messagebox.show("ok");
                }

            }
            if (curEleccion.equals("05")) // POR ADMINISTRADO
            {
                cons_con = "ADMINISTRADO";
                if (this.txtCodiAdministrado.getValue().equals("")) {
                    Messagebox.show("Seleccione un destinatario");
                } else {
                    //Messagebox.show("ok");
                }

            }
            if (curEleccion.equals("02")) {
                Param4 = curTipoExp.getCodi_tiex();
            } else if (curEleccion.equals("03")) {
                Param4 = Integer.parseInt(txtCodiProc.getValue());
            } else if (curEleccion.equals("04")) {
                Param4 = curDestinatario;
                Param5 = Integer.parseInt(this.txtCodiDestinatario.getValue());

            } else if (curEleccion.equals("05")) {
                Param4 = curAdministrado;
                Param5 = Integer.parseInt(this.txtCodiAdministrado.getValue());

            } else if (curEleccion.equals("06")) {
                Param4 = Integer.parseInt(txtNExpediente.getValue());
                Messagebox.show("El numero ingresado es: " + Param4);
            }
            curDependenciamaster = curDependencia2master;
            if (curDependenciamaster == null) {
                Messagebox.show("Seleccione una dependencia");
                return;
            }
            
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            dao_expediente = new Expediente_dao();
            setList_expediente(dao_expediente.get_expedientes_reportes(formatter.format(curFechaDesde), formatter.format(curFechaHasta), curEleccion, Param4, Param5, curDependenciamaster.getCodi_arde(), curEstadoExpediente.x));

            String[] params = {formatter.format(curFechaDesde), formatter.format(curFechaHasta), curEleccion + "", Param4 + "", Param5 + "", curDependenciamaster.getCodi_arde() + "", curEstadoExpediente.x + ""};
            dao_expediente.close();
            setCurExpediente(null);

            // LLAMAR REPORTE
            Map parametros = new HashMap();
            Calendar fecha = Calendar.getInstance();
            int año = fecha.get(Calendar.YEAR);
            //Messagebox.show("El año actual: " + año);
            administracion_dao ObjAdministracionDAO = new administracion_dao();

            parametros.put("esta_con", ("ESTADO " + curEstadoExpediente.y).toUpperCase());
            parametros.put("cons_con", cons_con);
            parametros.put("nombre_ano", ObjAdministracionDAO.get_administracion(año).getNoaa_admi());
//            parametros.put("fecha_ingreso", Fecha_Ingreso_Expediente);

            ObjAdministracionDAO.close();
            //parametros.put("cons_cont", getList_expediente().size()+"");
            //parametros.put("nom_area", curDependenciamaster.getNomb_arde());
            _sess.setAttribute("lista", getList_expediente());
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            main.generarReporteEstadosExp("/reportes/R_Expedientes_Estados.jasper", parametros, "Reporte de estado expediente.pdf", sv, curDependenciamaster.getNomb_arde(), curDependenciamaster.getCodi_arde(), params);

            // AQUI
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Command
    public void ReporteExpedientesFirma() {

        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            int Param4 = 0;
            int Param5 = 0;

            String cons_con = "";
            if (curEleccion.equals("06")) // POR NUMERO DE EXPEDIENTE
            {
                cons_con = "NÚMERO DE EXPEDIENTE";
                if (getCurNumExp().isEmpty()) {
                    Messagebox.show("Ingrese un numero de expediente");
                    return;
                }
            }
            if (curEleccion.equals("02")) // POR PROCEDIMIENTO
            {
                cons_con = "TIPO DE DOCUMENTO";
                if (getCurTipoExp() == null) {
                    Messagebox.show("Seleccione un tipo de Expediente");
                    return;
                }
            }
            if (curEleccion.equals("03")) // POR PROCEDIMIENTO
            {
                cons_con = "PROCEDIMIENTO";
                if (txtCodiProc.getValue().equals("")) {
                    Messagebox.show("Seleccione un procedimiento");
                    return;
                } else {
                    //Messagebox.show("ok");
                }
            }
            if (curEleccion.equals("04")) // POR DESTINATARIO
            {
                cons_con = "DESTINATARIO";
                if (this.txtCodiDestinatario.getValue().equals("")) {
                    Messagebox.show("Seleccione un destinatario");
                } else {
                    //Messagebox.show("ok");
                }

            }
            if (curEleccion.equals("05")) // POR ADMINISTRADO
            {
                cons_con = "ADMINISTRADO";
                if (this.txtCodiAdministrado.getValue().equals("")) {
                    Messagebox.show("Seleccione un destinatario");
                } else {
                    //Messagebox.show("ok");
                }

            }
            if (curEleccion.equals("02")) {
                Param4 = curTipoExp.getCodi_tiex();
            } else if (curEleccion.equals("03")) {
                Param4 = Integer.parseInt(txtCodiProc.getValue());
            } else if (curEleccion.equals("04")) {
                Param4 = curDestinatario;
                Param5 = Integer.parseInt(this.txtCodiDestinatario.getValue());
               
            } else if (curEleccion.equals("05")) {
                Param4 = curAdministrado;
                Param5 = Integer.parseInt(this.txtCodiAdministrado.getValue());
               
            }else if(curEleccion.equals("06")){
                Param4 = Integer.parseInt(txtNExpediente.getValue());
                Messagebox.show("El numero ingresado es: " + Param4);
            }
            if (curDependenciamaster == null) {
                Messagebox.show("Seleccione una dependencia");
                return;
            }
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            dao_expediente = new Expediente_dao();
            setList_expediente(dao_expediente.get_expedientes_reportes(formatter.format(curFechaDesde), formatter.format(curFechaHasta), curEleccion, Param4, Param5, curDependenciamaster.getCodi_arde(), curEstadoExpediente.x));
            String[] params = {formatter.format(curFechaDesde), formatter.format(curFechaHasta), curEleccion + "", Param4 + "", Param5 + "", curDependenciamaster.getCodi_arde() + "", curEstadoExpediente.x + ""};
            dao_expediente.close();
            setCurExpediente(null);
            // LLAMAR REPORTE
            Map parametros = new HashMap();

            parametros.put("esta_con", ("ESTADO " + curEstadoExpediente.y).toUpperCase());
            parametros.put("cons_con", cons_con);
            //parametros.put("cons_cont", getList_expediente().size()+"");
            //parametros.put("nom_area", curDependenciamaster.getNomb_arde());
            _sess.setAttribute("lista", getList_expediente());
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            main.generarReporteEstadosExp("/reportes/R_Expedientes_Estados2.jasper", parametros, "Reporte de estado expediente.pdf", sv, curDependenciamaster.getNomb_arde(), curDependenciamaster.getCodi_arde(), params);

            // AQUI
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Procedimiento_Expediente getCurProcedimiento() {
        return curProcedimiento;
    }

    public void setCurProcedimiento(Procedimiento_Expediente curProcedimiento) {
        this.curProcedimiento = curProcedimiento;
    }

    public Tipo_Expediente getCurTipoExp() {
        return curTipoExp;
    }

    public void setCurTipoExp(Tipo_Expediente curTipoExp) {
        this.curTipoExp = curTipoExp;
    }

    public String getCurNumExp() {
        return curNumExp;
    }

    public void setCurNumExp(String curNumExp) {
        this.curNumExp = curNumExp;
    }

    public List<Procedimiento_Expediente> getList_procedimiento() {
        return list_procedimiento;
    }

    public void setList_procedimiento(List<Procedimiento_Expediente> list_procedimiento) {
        this.list_procedimiento = list_procedimiento;
    }

    public List<Tipo_Expediente> getList_tipoexpediente() {
        return list_tipoexpediente;
    }

    public void setList_tipoexpediente(List<Tipo_Expediente> list_tipoexpediente) {
        this.list_tipoexpediente = list_tipoexpediente;
    }

    public List<Contribuyente> getList_contribuyente() {
        return list_contribuyente;
    }

    public void setList_contribuyente(List<Contribuyente> list_contribuyente) {
        this.list_contribuyente = list_contribuyente;
    }

    public List<BeanDependencias> getList_dependencia() {
        return list_dependencia;
    }

    public void setList_dependencia(List<BeanDependencias> list_dependencia) {
        this.list_dependencia = list_dependencia;
    }

    public List<Entidad> getList_entidad() {
        return list_entidad;
    }

    public void setList_entidad(List<Entidad> list_entidad) {
        this.list_entidad = list_entidad;
    }

    public Contribuyente getCurContribuyente() {
        return curContribuyente;
    }

    public void setCurContribuyente(Contribuyente curContribuyente) {
        this.curContribuyente = curContribuyente;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    public Entidad getCurEntidad() {
        return curEntidad;
    }

    public void setCurEntidad(Entidad curEntidad) {
        this.curEntidad = curEntidad;
    }

    public List<ExpedienteReportes> getList_expediente() {
        try {
            if (list_expediente != null && !list_expediente.isEmpty()) {
                for (ExpedienteReportes exp : list_expediente) {
                    if (exp.getNRODESTINATARIO() == 0) {
                        list_expediente.remove(exp);
                    }
                }
            }
        } catch (Exception e) {
        }
        return list_expediente;
    }

    public void setList_expediente(List<ExpedienteReportes> list_expediente) {
        this.list_expediente = list_expediente;
    }

    public ExpedienteReportes getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteReportes curExpediente) {
        this.curExpediente = curExpediente;
    }

    public String getCurEleccion() {
        return curEleccion;
    }

    public void setCurEleccion(String curEleccion) {
        this.curEleccion = curEleccion;
    }

    public int getCurAdministrado() {
        return curAdministrado;
    }

    public void setCurAdministrado(int curAdministrado) {
        this.curAdministrado = curAdministrado;
    }

    public Date getCurFechaDesde() {
        return curFechaDesde;
    }

    public void setCurFechaDesde(Date curFechaDesde) {
        this.curFechaDesde = curFechaDesde;
    }

    public Date getCurFechaHasta() {
        return curFechaHasta;
    }

    public void setCurFechaHasta(Date curFechaHasta) {
        this.curFechaHasta = curFechaHasta;
    }

    public Pair<Integer, String> getCurEstadoExpediente() {
        return curEstadoExpediente;
    }

    public void setCurEstadoExpediente(Pair<Integer, String> curEstadoExpediente) {
        this.curEstadoExpediente = curEstadoExpediente;
    }

    public List<Pair<Integer, String>> getListEstadoExpediente() {
        return listEstadoExpediente;
    }

    public void setListEstadoExpediente(List<Pair<Integer, String>> listEstadoExpediente) {
        this.listEstadoExpediente = listEstadoExpediente;
    }

    public int getCurDestinatario() {
        return curDestinatario;
    }

    public void setCurDestinatario(int curDestinatario) {
        this.curDestinatario = curDestinatario;
    }

    public Contribuyente getCurContribuyenteDest() {
        return curContribuyenteDest;
    }

    public void setCurContribuyenteDest(Contribuyente curContribuyenteDest) {
        this.curContribuyenteDest = curContribuyenteDest;
    }

    public BeanDependencias getCurDependenciaDest() {
        return curDependenciaDest;
    }

    public void setCurDependenciaDest(BeanDependencias curDependenciaDest) {
        this.curDependenciaDest = curDependenciaDest;
    }

    public Entidad getCurEntidadDest() {
        return curEntidadDest;
    }

    public void setCurEntidadDest(Entidad curEntidadDest) {
        this.curEntidadDest = curEntidadDest;
    }

    public List<BeanExpediente> getLista_expediente_reporte() {
        return lista_expediente_reporte;
    }

    public void setLista_expediente_reporte(List<BeanExpediente> lista_expediente_reporte) {
        this.lista_expediente_reporte = lista_expediente_reporte;
    }
   
}
