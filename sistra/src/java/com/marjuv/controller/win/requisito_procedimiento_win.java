package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.entity.requisito_procedimiento;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class requisito_procedimiento_win {

    @Wire("#requisito_procedimiento_man")
    private Window win;
    private List<BeanRequisitoMan> lst_requisito_procedimiento;
    private BeanRequisitoMan cur_requisito_procedimiento;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
    
    @Command
    private void closeThis()
    {
        win.detach();
    }
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Requisito"));
        listBusqueda.add(new Pair<Integer, String>(3, "Procedimiento"));
        listBusqueda.add(new Pair<Integer, String>(4, "Precio"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lst_requisito_procedimiento")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_requisito_procedimiento")
    public void updateGrid() {
        try {
            requisito_procedimiento_dao dao = new requisito_procedimiento_dao();
            lst_requisito_procedimiento = dao.get_requisito_procedimiento();
            List<BeanRequisitoMan> new_list = new ArrayList<BeanRequisitoMan>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanRequisitoMan contribuyente : lst_requisito_procedimiento) {
                            if ((contribuyente.getNomb_repr().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (BeanRequisitoMan contribuyente : lst_requisito_procedimiento) {
                            if ((contribuyente.getNomb_proc()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 4:
                        for (BeanRequisitoMan contribuyente : lst_requisito_procedimiento) {
                            if ((contribuyente.getPrec_repr()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lst_requisito_procedimiento = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
            lst_requisito_procedimiento = obj_requisito_procedimiento_dao.get_requisito_procedimiento();
            obj_requisito_procedimiento_dao.close();
        } catch (Exception ex) {
            Messagebox.show("Error: " + ex.getMessage());
        }
    }

    public List<BeanRequisitoMan> getLst_requisito_procedimiento() {
        return lst_requisito_procedimiento;
    }
    
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @Command
    public void nuevo_requisito_procedimiento() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_requisito_procedimiento.zul", null, map);
    }

    @Command
    public void editThisRequisito() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sRequisito", this.cur_requisito_procedimiento);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_requisito_procedimiento.zul", null, map);
        } catch (Exception ex) {
        }
    }
    
    @GlobalCommand
    @NotifyChange("lst_requisito_procedimiento")
    public void updateRequisitoList(@BindingParam("sRequisito") requisito_procedimiento cur_requisito_procedimiento,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
            if (recordMode.equals("NEW")) {
                obj_requisito_procedimiento_dao.insert(cur_requisito_procedimiento);
                obj_requisito_procedimiento_dao.close();
                insert_auditoria("I", "REQUISITO_PROCEDIMIENTO");
                Messagebox.show("El requisito fue registrado");
            } else if (recordMode.equals("EDIT")) {
                obj_requisito_procedimiento_dao.update(cur_requisito_procedimiento);
                obj_requisito_procedimiento_dao.close();
                insert_auditoria("U", "REQUISITO_PROCEDIMIENTO");
                Messagebox.show("El requisito fue actualizado");
            }
        }
        updateGrid();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisRequisito() {
        String str = "¿Está seguro de que desea eliminar el requisito de procedimiento \""
                + cur_requisito_procedimiento.getNomb_repr() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
                        obj_requisito_procedimiento_dao.delete(cur_requisito_procedimiento.getCodi_repr());
                        BindUtils.postNotifyChange(null, null, requisito_procedimiento_win.this, "lst_requisito_procedimiento");
                        insert_auditoria("D", "REQUISITO_PROCEDIMIENTO");
                        updateGrid();
                    } catch (Exception ex) {
                        Logger.getLogger(requisito_procedimiento_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_requisito_procedimiento(List<BeanRequisitoMan> lst_requisito_procedimiento) {
        this.lst_requisito_procedimiento = lst_requisito_procedimiento;
    }

    public BeanRequisitoMan getCur_requisito_procedimiento() {
        return cur_requisito_procedimiento;
    }

    public void setCur_requisito_procedimiento(BeanRequisitoMan cur_requisito_procedimiento) {
        this.cur_requisito_procedimiento = cur_requisito_procedimiento;
    }

}