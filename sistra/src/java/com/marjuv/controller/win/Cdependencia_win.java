package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.Dependencia;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class Cdependencia_win {

    @Wire("#dependencia_man")
    private Window win;
    private BeanDependencias beandatos;
    private String recordMode;
    private List<Pair<Integer, String>> listEntidad;
    private List<Pair<Integer, String>> listSubdep;
    private Pair<Integer, String> curEntidad;
    private Pair<Integer, String> curSubDep;
    
    
    @WireVariable
    private Session _sess;
    
    @Init
    @NotifyChange("listSubdep")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("objDependencia") BeanDependencias c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        loadDataDependencia();
        loadDataEntidad();
        
        if (recordMode.equals("NEW")) {
            this.beandatos = new BeanDependencias();
        }

        if (recordMode.equals("EDIT")) {
            this.beandatos = (BeanDependencias) c1.clone();
            for (int i = 0; i < listEntidad.size(); i++) {
                if (listEntidad.get(i).getX() == this.beandatos.getCodi_enti()) {
                    this.curEntidad = listEntidad.get(i);
                    break;
                }

            }
            for(int j = 0; j<listSubdep.size(); j++){
                if(listSubdep.get(j).getX() == this.beandatos.getJera_arde()){
                    this.curSubDep = listSubdep.get(j);
                    break;
                }
            }
        }
    }

    public BeanDependencias getBeandatos() {
        return beandatos;
    }

    public void setBeandatos(BeanDependencias beandatos) {
        this.beandatos = beandatos;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public void loadDataEntidad() {
        try {
            
            Dependencia_dao objDependenciaDAO = new Dependencia_dao();
            setListEntidad(objDependenciaDAO.getEntidad());
            objDependenciaDAO.close();
            if (listEntidad.size() > 0) {
                this.curEntidad = listEntidad.get(0);
            }
        } catch (Exception e) {
        }
    }
    
    public void loadDataDependencia() {
        try {
            
            Dependencia_dao objDependenciaDAO = new Dependencia_dao();
            setListSubdep(objDependenciaDAO.getDependencia());
            listSubdep.add(new Pair<Integer, String>(0, "Ninguno"));
            objDependenciaDAO.close();
            if (getListSubdep().size() > 0){
                this.setCurSubDep(getListSubdep().get(getListSubdep().size()-1));
            }
        } catch (Exception e) {
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            Dependencia_dao objDependenciaDAO = new Dependencia_dao();
            if ((getRecordMode().equals("NEW") && objDependenciaDAO.validate_nameDependencia_nuevo(beandatos.getNomb_arde())) || (getRecordMode().equals("EDIT") && objDependenciaDAO.validate_nameDependencia_editar(beandatos.getCodi_arde(), beandatos.getNomb_arde()))) {
                if ((getRecordMode().equals("NEW") && objDependenciaDAO.validate_abrevDependencia_nuevo(beandatos.getAbre_arde())) || (getRecordMode().equals("EDIT") && objDependenciaDAO.validate_abrevDependencia_editar(beandatos.getCodi_arde(), beandatos.getAbre_arde()))) {
                    Dependencia objDependenciaTO = new Dependencia();
                    objDependenciaTO.setCodi_arde(beandatos.getCodi_arde());
                    objDependenciaTO.setNomb_arde(beandatos.getNomb_arde());
                    if(curSubDep.y.equals("Ninguno")){
                        objDependenciaTO.setJera_arde(0);
                        objDependenciaTO.setNive_arde(1);
                    }else{
                        objDependenciaTO.setJera_arde(curSubDep.x);
                        objDependenciaTO.setNive_arde(objDependenciaDAO.getDependencia(curSubDep.x).getNive_arde()+1);
                    }
                    objDependenciaTO.setAbre_arde(beandatos.getAbre_arde());
                    objDependenciaTO.setCodi_enti(curEntidad.x);

                    Map args = new HashMap();
                    args.put("objDependencia", objDependenciaTO);
                    args.put("accion", this.recordMode);
                    
                    BindUtils.postGlobalCommand(null, null, "updateUsuarioList", args);
                    win.detach();
                } else {
                    Messagebox.show("El nombre de la abreviatura ingresado ya Existe, eliga otro", "Mantenimiento de Dependencias", Messagebox.OK, Messagebox.ERROR);
                }

            } else {
                Messagebox.show("El nombre de la dependecia ingresado ya Existe, eliga otro", "Mantenimiento de Dependencias", Messagebox.OK, Messagebox.ERROR);
            }
            objDependenciaDAO.close();
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public List<Pair<Integer, String>> getListEntidad() {
        return listEntidad;
    }

    public void setListEntidad(List<Pair<Integer, String>> listEntidad) {
        this.listEntidad = listEntidad;
    }

    public Pair<Integer, String> getCurEntidad() {
        return curEntidad;
    }

    public void setCurEntidad(Pair<Integer, String> curEntidad) {
        this.curEntidad = curEntidad;
    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "allDependencias", null);
    }

    public List<Pair<Integer, String>> getListSubdep() {
        return listSubdep;
    }

    public void setListSubdep(List<Pair<Integer, String>> listSubdep) {
        this.listSubdep = listSubdep;
    }

    public Pair<Integer, String> getCurSubDep() {
        return curSubDep;
    }

    public void setCurSubDep(Pair<Integer, String> curSubDep) {
        this.curSubDep = curSubDep;
    }

}
