/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.CartaDAO;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.Cargo;
import com.marjuv.entity.CartaTO;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Ctramitarexpediente_win {

    @WireVariable
    private Session _sess;
    private Expediente_dao dao;
    private int tipoCNS;
    private String curNumExp;
    private List<ExpedienteCliente> lista_expediente;
    private ExpedienteCliente curExpediente;

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("tramitarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
        }
        return fin;
    }

    @Init
    public void initSetup() {

        check_permisos();
        dao = new Expediente_dao();
        curNumExp = "";
        tipoCNS = 2;
        //actualizar_lista_expediente();
    }

    @Command
    public void BuscarExpediente() {
        actualizar_lista_expediente();
        BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
    }

    @Command
    public void TodosExpediente() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            try {
                String parametro = curNumExp;
                setLista_expediente(dao.get_expedientes_tramitar(obj_usuario.getCodi_arde(), 1, parametro));
                //System.out.println("Tamaño : "+getLista_expediente().size());
            } catch (Exception ex) {
                Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
    }

    private void actualizar_lista_expediente() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            try {
                String parametro = curNumExp;
                setLista_expediente(dao.get_expedientes_tramitar(obj_usuario.getCodi_arde(), tipoCNS, parametro));
                //System.out.println(curNumExp + "/" + tipoCNS);
                //System.out.println("Tamaño : "+getLista_expediente().size());
            } catch (Exception ex) {
                Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @GlobalCommand
    @NotifyChange("lista_expediente")
    public void updateTramitarExpediente(@BindingParam("curCargo") Cargo curCargo, @BindingParam("foliosAdd") int folios, @BindingParam("curExpediente") ExpedienteCliente Expediente, @BindingParam("curNProveido") String NProveido, @BindingParam("curRProveido") String RProveido, @BindingParam("curDocumento") boolean curDocumento, @BindingParam("curCosto") Double curCosto, @BindingParam("listVinculados") List<ExpedienteCliente> listVinculados) {
        com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
        try {
            if (listVinculados == null || listVinculados.isEmpty()) {
                dao.expediente_tramitar(Expediente.getEXPNUM(), Expediente.getCORRELATIVO(), Expediente.getDESTINO(), obj_usuario.getCodi_usua(), Expediente.getFOLIOS(), curCargo.getCodi_carg(), NProveido, RProveido, curDocumento, curCosto);
                delete_element_list(Expediente.getEXPNUM());
                BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                Messagebox.show("Expediente Tramitado",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            } else {
                List<ExpedienteCliente> lista_total = dao.get_expedientes_tramitar(obj_usuario.getCodi_arde(), 1, curNumExp);
                List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
                boolean isPosible = true;
                for (ExpedienteCliente expedienteCliente : listVinculados) {
                    boolean sw = false;
                    for (ExpedienteCliente expedienteCliente1 : lista_total) {
                        if (expedienteCliente.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                            sw = true;
                            lista_agregar.add(expedienteCliente1);
                            break;
                        }
                    }
                    if (!sw) {
                        Messagebox.show("Uno de los expedientes vinculados no se encuentra recepcionado en la dependencia no es posible proceder.");
                        isPosible = false;
                        break;
                    }
                }
                if (isPosible) {
                    for (ExpedienteCliente expedienteCliente : lista_agregar) {
                        dao.expediente_tramitar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua(), expedienteCliente.getFOLIOS() + folios, curCargo.getCodi_carg(), NProveido, RProveido, curDocumento, curCosto);
                        delete_element_list(expedienteCliente.getEXPNUM());
                        BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                    }
                    Messagebox.show( lista_agregar.size() +" Expediente(s) Tramitado(s)",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                }
            }
        } catch (Exception ex) {
            Messagebox.show("Ocurrio un error al Tramitar",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
            Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @GlobalCommand
    @NotifyChange("lista_expediente")
    public void updateObservarExpediente(@BindingParam("curExpediente") ExpedienteCliente Expediente, @BindingParam("curNObservacion") String NObservacion, @BindingParam("listVinculados") List<ExpedienteCliente> listVinculados) {
        com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
        try {
            if (listVinculados == null || listVinculados.isEmpty()) {
                dao.expediente_observar(Expediente.getEXPNUM(), Expediente.getCORRELATIVO(), Expediente.getDESTINO(), obj_usuario.getCodi_usua(), NObservacion);
                delete_element_list(Expediente.getEXPNUM());
                
                CartaDAO carta = new CartaDAO();
                CartaTO objcarta = new CartaTO();
                objcarta.setNume_expe(Expediente.getEXPNUM());
                objcarta.setCorr_hist(Expediente.getCORRELATIVO());
                objcarta.setNude_hist(Expediente.getDESTINO());
                objcarta.setCodi_usua(obj_usuario.getCodi_usua());
                objcarta.setObsv_cart(NObservacion);
                carta.insert_carta(objcarta);
                carta.close();
                BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                Messagebox.show("Expediente Observado",
                        "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
            } else {
                List<ExpedienteCliente> lista_total = dao.get_expedientes_tramitar(obj_usuario.getCodi_arde(), 1, curNumExp);
                List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
                boolean isPosible = true;
                for (ExpedienteCliente expedienteCliente : listVinculados) {
                    boolean sw = false;
                    for (ExpedienteCliente expedienteCliente1 : lista_total) {
                        if (expedienteCliente.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                            sw = true;
                            lista_agregar.add(expedienteCliente1);
                            break;
                        }
                    }
                    if (!sw) {
                        Messagebox.show("Uno de los expedientes vinculados no se encuentra en la dependencia no es posible proceder.");
                        isPosible = false;
                        break;
                    }
                }
                if (isPosible) {
                    for (ExpedienteCliente expedienteCliente : lista_agregar) {
                        dao.expediente_observar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua(), NObservacion);
                        delete_element_list(expedienteCliente.getEXPNUM());
                        CartaDAO carta = new CartaDAO();
                        CartaTO objcarta = new CartaTO();
                        objcarta.setNume_expe(expedienteCliente.getEXPNUM());
                        objcarta.setCorr_hist(expedienteCliente.getCORRELATIVO());
                        objcarta.setNude_hist(expedienteCliente.getDESTINO());
                        objcarta.setCodi_usua(obj_usuario.getCodi_usua());
                        objcarta.setObsv_cart(NObservacion);
                        carta.insert_carta(objcarta);
                        carta.close();
                        BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                    }
                    Messagebox.show( lista_agregar.size() +" Expediente(s) Observado(s)",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                }
            }
        } catch (Exception ex) {
            Messagebox.show("Ocurrio un error al Observar",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @GlobalCommand
    @NotifyChange("lista_expediente")
    public void updateFinalizarExpediente(@BindingParam("curExpediente") ExpedienteCliente Expediente, @BindingParam("curNObservacion") String NObservacion, @BindingParam("listVinculados") List<ExpedienteCliente> listVinculados) {
        com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
        try {
            if (listVinculados == null || listVinculados.isEmpty()) {
                dao.expediente_finalizar(Expediente.getEXPNUM(), Expediente.getCORRELATIVO(), Expediente.getDESTINO(), obj_usuario.getCodi_usua(), NObservacion);
                delete_element_list(Expediente.getEXPNUM());
                BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                Messagebox.show("Expediente Finalizado",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            }
            else {
                List<ExpedienteCliente> lista_total = dao.get_expedientes_tramitar(obj_usuario.getCodi_arde(), 1, curNumExp);
                List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
                boolean isPosible = true;
                for (ExpedienteCliente expedienteCliente : listVinculados) {
                    boolean sw = false;
                    for (ExpedienteCliente expedienteCliente1 : lista_total) {
                        if (expedienteCliente.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                            sw = true;
                            lista_agregar.add(expedienteCliente1);
                            break;
                        }
                    }
                    if (!sw) {
                        Messagebox.show("Uno de los expedientes vinculados no se encuentra en la dependencia no es posible proceder.");
                        isPosible = false;
                        break;
                    }
                }
                if (isPosible) {
                    for (ExpedienteCliente expedienteCliente : lista_agregar) {
                        dao.expediente_finalizar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua(), NObservacion);
                        delete_element_list(expedienteCliente.getEXPNUM());
                        BindUtils.postNotifyChange(null, null, Ctramitarexpediente_win.this, "lista_expediente");
                    }
                    Messagebox.show( lista_agregar.size() +" Expediente(s) Finalizado(s)",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                }
            }

        } catch (Exception ex) {
            Messagebox.show("Ocurrio un error al Finalizar",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete_element_list(String expnum) {
        int cont = 0;
        for (ExpedienteCliente expedienteCliente : lista_expediente) {
            if (expedienteCliente.getEXPNUM().equals(expnum)) {
                break;
            }
            cont++;
        }
        if (cont < lista_expediente.size()) {
            lista_expediente.remove(cont);
        }
    }

    @Command
    public void TramitarExpediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        //Messagebox.show("Destino: " + curExpediente.getDESTINO());
        map.put("curExpediente", getCurExpediente());
        Executions.createComponents("/ZonaCompartida/tramitarexpedienteT_man.zul", null, map);
    }

    @Command
    public void ObservarExpediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curExpediente", getCurExpediente());
        Executions.createComponents("/ZonaCompartida/tramitarexpedienteO_man.zul", null, map);
    }

    @Command
    public void FinalizarExpediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curExpediente", getCurExpediente());
        Executions.createComponents("/ZonaCompartida/tramitarexpedienteF_man.zul", null, map);
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }

    public Expediente_dao getDao() {
        return dao;
    }

    public void setDao(Expediente_dao dao) {
        this.dao = dao;
    }

    public int getTipoCNS() {
        return tipoCNS;
    }

    public void setTipoCNS(int tipoCNS) {
        this.tipoCNS = tipoCNS;
    }

    public String getCurNumExp() {
        return curNumExp;
    }

    public void setCurNumExp(String curNumExp) {
        this.curNumExp = curNumExp;
    }

    public List<ExpedienteCliente> getLista_expediente() {
        return lista_expediente;
    }

    public void setLista_expediente(List<ExpedienteCliente> lista_expediente) {
        this.lista_expediente = lista_expediente;
    }

}
