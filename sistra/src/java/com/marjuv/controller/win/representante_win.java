package com.marjuv.controller.win;

import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.representante_dao;
import com.marjuv.entity.representante;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class representante_win {

    private List<representante> lst_repre;
    private representante cur_repre;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lst_repre")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @Command
    @NotifyChange("lst_repre")
    public void updateGrid() {
        try {
            representante_dao dao = new representante_dao();
            lst_repre = dao.get_representantes();
            List<representante> new_list = new ArrayList<representante>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (representante repre : lst_repre) {
                            if ((repre.getNoms_reps().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(repre);
                            }
                        }
                        break;
                }
                lst_repre = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            representante_dao obj_representante_dao = new representante_dao();
            lst_repre = obj_representante_dao.get_representantes();
            obj_representante_dao.close();
        } catch (Exception ex) {
        }
    }

    @Command
    public void nuevo_representante() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRepre", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_representante.zul", null, map);
    }

    @Command
    public void editThisRepre() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sRepre", this.cur_repre);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_representante.zul", null, map);
        } catch (Exception ex) {
        }
    }
    
    @GlobalCommand
    @NotifyChange("lst_repre")
    public void updateRepreList(@BindingParam("sRepre") representante cur_representante,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            representante_dao obj_representante_dao = new representante_dao();
            if (recordMode.equals("NEW")) {
                obj_representante_dao.insert(cur_representante);
                obj_representante_dao.close();
                insert_auditoria("I", "REPRESENTANTE");
                Messagebox.show("El representante fue registrado");
            } else if (recordMode.equals("EDIT")) {
                obj_representante_dao.update(cur_representante);
                obj_representante_dao.close();
                insert_auditoria("U", "REPRESENTANTE");
                Messagebox.show("El representante fue actualizado");
            }
        }
        updateGrid();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisRepre() {
        String str = "¿Está seguro de que desea eliminar el representante \""
                + cur_repre.getNoms_reps() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        representante_dao obj_representante_dao = new representante_dao();
                        obj_representante_dao.delete(cur_repre.getCodi_reps());
                        insert_auditoria("D", "REPRESENTANTE");
                        updateGrid();
                        BindUtils.postNotifyChange(null, null, representante_win.this, "lst_repre");
                    } catch (Exception ex) {
                        Logger.getLogger(representante_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    /**
     * @return the lst_repre
     */
    public List<representante> getLst_repre() {
        return lst_repre;
    }

    /**
     * @param lst_repre the lst_repre to set
     */
    public void setLst_repre(List<representante> lst_repre) {
        this.lst_repre = lst_repre;
    }

    /**
     * @return the cur_repre
     */
    public representante getCur_repre() {
        return cur_repre;
    }

    /**
     * @param cur_repre the cur_repre to set
     */
    public void setCur_repre(representante cur_repre) {
        this.cur_repre = cur_repre;
    }


}
