/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.EnviaHistorial;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Crestaurarexpediente_win {

    @WireVariable
    private Session _sess;
    private Expediente_dao dao;

    private ExpedienteCliente curExpediente;
    private List<ExpedienteCliente> list_expediente;
    private List<EnviaHistorial> list_envia;
    private EnviaHistorial curSEnvia;
    private EnviaHistorial curAEnvia;
    private String curAEstado;
    private boolean curFechas;
    private Date curFechaDesde;
    private Date curFechaHasta;
    private int tipoCNS;
    private String curNumExp;

    @Init
    @NotifyChange
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        dao = new Expediente_dao();
        setList_envia(dao.get_envia_estado_restaurar());
        curSEnvia = getList_envia().get(0);
        curFechaDesde = new Date();
        curFechaHasta = new Date();
        curNumExp = "";
        tipoCNS = 2;
        curFechas = false;
        setCurAEstado("SELECCIONE UN TIPO DE BUSQUEDA");
    }

    @Command
    public void updateTipo2() {
        this.tipoCNS = 2;
    }

    @Command
    public void updateTipo3() {
        this.tipoCNS = 3;
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
        }
        return fin;
    }

    @Command
    @NotifyChange
    public void VerTodos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String fechas = formatter.format(curFechaDesde) + formatter.format(curFechaHasta);
            if (!curFechas) {
                fechas = null;
            }
            try {
                setList_expediente(dao.get_expedientes_restaurar(obj_usuario.getCodi_arde(), 1, curNumExp, fechas, curSEnvia.getEnviaEstado()));
                setCurAEnvia(getCurSEnvia());
                setCurAEstado("EXPEDIENTES " + getCurAEnvia().getNombEstado().toUpperCase());
            } catch (Exception ex) {
                Logger.getLogger(Crestaurarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
            BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "list_expediente");
            BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "curAEstado");
        }
    }

    @Command
    @NotifyChange
    public void BuscarExpedientes() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String fechas = formatter.format(curFechaDesde) + formatter.format(curFechaHasta);
            if (!curFechas) {
                fechas = null;
            }
            try {
                
                setList_expediente(dao.get_expedientes_restaurar(obj_usuario.getCodi_arde(), tipoCNS, curNumExp, fechas, curSEnvia.getEnviaEstado()));
                Messagebox.show("Condición del Expediente: " + list_expediente.get(0).getCONDICION());
                setCurAEnvia(getCurSEnvia());
                setCurAEstado("EXPEDIENTES " + getCurAEnvia().getNombEstado().toUpperCase());
            } catch (Exception ex) {
                Logger.getLogger(Crestaurarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
            BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "list_expediente");
            BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "curAEstado");
        }
    }

    @Command
    public void RestaurarExpediente() {
        if (getCurExpediente() != null) {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("curExpediente", getCurExpediente());
            map.put("curAEnvia", getCurAEnvia());
            Executions.createComponents("/ZonaCompartida/restaurarexpediente_man.zul", null, map);
        } else {
            Messagebox.show("Debe de Seleccionar un expediente");
        }
    }

    ExpedienteCliente obj_expediente;
    usuario obj_usuario;
    EnviaHistorial curEnvia_temp;
    String obsv;

    @GlobalCommand
    @NotifyChange("list_expediente")
    public void updateRestaurarExpediente(@BindingParam("curExpediente") ExpedienteCliente Expediente, @BindingParam("curNObservacion") String NObservacion, @BindingParam("curAEnvia") EnviaHistorial Envia, @BindingParam("listVinculados") List<ExpedienteCliente> listVinculados) {
        obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
        obj_expediente = Expediente;
        curEnvia_temp = getCurAEnvia();
        obsv = NObservacion;
        try {
            if (listVinculados == null || listVinculados.isEmpty()) {
                dao.expediente_restaurar(obj_expediente.getEXPNUM(), obj_expediente.getCORRELATIVO(), obj_expediente.getDESTINO(), obj_usuario.getCodi_usua(), obsv, curEnvia_temp.getEnviaEstado());
                delete_element_list(obj_expediente.getEXPNUM());
                BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "list_expediente");
                Messagebox.show("Expediente Restaurado",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                setCurExpediente(null);
            } else {
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String fechas = formatter.format(curFechaDesde) + formatter.format(curFechaHasta);
                if (!curFechas) {
                    fechas = null;
                }
                List<ExpedienteCliente> lista_total = dao.get_expedientes_restaurar(obj_usuario.getCodi_arde(), 1, curNumExp, fechas, curSEnvia.getEnviaEstado());
                List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
                boolean isPosible = true;
                for (ExpedienteCliente expedienteCliente : listVinculados) {
                    boolean sw = false;
                    for (ExpedienteCliente expedienteCliente1 : lista_total) {
                        if (expedienteCliente.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                            sw = true;
                            lista_agregar.add(expedienteCliente1);
                            break;
                        }
                    }
                    if (!sw) {
                        Messagebox.show("Uno de los expedientes vinculados no se encuentra en la dependencia no es posible proceder.");
                        isPosible = false;
                        break;
                    }
                }
                if (isPosible) {
                    for (ExpedienteCliente expedienteCliente : lista_agregar) {
                        dao.expediente_restaurar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua(), obsv, curEnvia_temp.getEnviaEstado());
                        delete_element_list(expedienteCliente.getEXPNUM());
                        BindUtils.postNotifyChange(null, null, Crestaurarexpediente_win.this, "lista_expediente");
                    }
                    Messagebox.show("Expediente Restaurado",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                    setCurExpediente(null);
                }
            }

        } catch (Exception ex) {
            Messagebox.show("Ocurrio un error al Restaurar",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete_element_list(String expnum) {
        int cont = 0;
        for (ExpedienteCliente expedienteCliente : list_expediente) {
            if (expedienteCliente.getEXPNUM().equals(expnum)) {
                break;
            }
            cont++;
        }
        list_expediente.remove(cont);
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }

    public Expediente_dao getDao() {
        return dao;
    }

    public void setDao(Expediente_dao dao) {
        this.dao = dao;
    }

    public boolean isCurFechas() {
        return curFechas;
    }

    public void setCurFechas(boolean curFechas) {
        this.curFechas = curFechas;
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public List<ExpedienteCliente> getList_expediente() {
        return list_expediente;
    }

    public List<EnviaHistorial> getList_envia() {
        return list_envia;
    }

    public void setList_envia(List<EnviaHistorial> list_envia) {
        this.list_envia = list_envia;
    }

    public EnviaHistorial getCurSEnvia() {
        return curSEnvia;
    }

    public void setCurSEnvia(EnviaHistorial curSEnvia) {
        this.curSEnvia = curSEnvia;
    }

    public EnviaHistorial getCurAEnvia() {
        return curAEnvia;
    }

    public void setCurAEnvia(EnviaHistorial curAEnvia) {
        this.curAEnvia = curAEnvia;
    }

    public void setList_expediente(List<ExpedienteCliente> list_expediente) {
        this.list_expediente = list_expediente;
    }

    public Date getCurFechaDesde() {
        return curFechaDesde;
    }

    public void setCurFechaDesde(Date curFechaDesde) {
        this.curFechaDesde = curFechaDesde;
    }

    public Date getCurFechaHasta() {
        return curFechaHasta;
    }

    public void setCurFechaHasta(Date curFechaHasta) {
        this.curFechaHasta = curFechaHasta;
    }

    public int getTipoCNS() {
        return tipoCNS;
    }

    public void setTipoCNS(int tipoCNS) {
        this.tipoCNS = tipoCNS;
    }

    public String getCurNumExp() {
        return curNumExp;
    }

    public void setCurNumExp(String curNumExp) {
        this.curNumExp = curNumExp;
    }

    public String getCurAEstado() {
        return curAEstado;
    }

    public void setCurAEstado(String curAEstado) {
        this.curAEstado = curAEstado;
    }
}
