/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Div;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;

import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;

/**
 *
 * @author BILLY
 */
public class Cmodulocliente_win {

    List<String> anios;
    private String curAnio;
    private String curExpediente;
    private List<ExpedienteCliente> list_expedientes;
    private ExpedienteCliente curExpedienteObj;
    private List<ExpedienteMovimientos> list_movimientos;
    private Expediente_dao dao_expediente;
    private String curDNI;

    @Wire
    private Listbox test;

    public List<ExpedienteCliente> getList_expedientes() {
        
        return list_expedientes;
    }

    public void setList_expedientes(List<ExpedienteCliente> list_expedientes) {
        this.list_expedientes = list_expedientes;
    }

    @Command
    public void CerrarExpediente() {
        Executions.sendRedirect("/index.zul");
    }

    @WireVariable
    private Session _sess;

    @Wire
    private Div expediente;
    @Wire
    private Div dni;
    @Wire
    private Div expedientesxdni;
    @Wire
    private Radio busqueda_exp;
    @Wire
    private Radio busqueda_dni;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        expediente.setVisible(true);
        dni.setVisible(false);
        expedientesxdni.setVisible(false);

    }

    public String getCurDNI() {
        return curDNI;
    }

    public void setCurDNI(String curDNI) {
        this.curDNI = curDNI;
    }

    @Command
    @NotifyChange
    public void selectExpediente() {
        expediente.setVisible(true);
        dni.setVisible(false);
        expedientesxdni.setVisible(false);
        limpiarCampos();
    }

    @Command
    @NotifyChange
    public void selectDNI() {
        dni.setVisible(true);
        expediente.setVisible(false);
        expedientesxdni.setVisible(true);
        limpiarCampos();

    }

    private void limpiarCampos() {
        setCurDNI("");
        setCurExpediente("");
        curExpedienteObj = new ExpedienteCliente();
        list_movimientos = new ArrayList<ExpedienteMovimientos>();
        list_expedientes = new ArrayList<ExpedienteCliente>();
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpediente");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curDNI");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpedienteObj");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_expedientes");
    }

    @Init
    @NotifyChange
    public void initSetup() {
        dao_expediente = new Expediente_dao();
        anios = new ArrayList<String>();
        Date fecha = new Date();
        DateFormat formatter = new SimpleDateFormat("yyyy");
        for (int i = 2016; i <= Integer.parseInt(formatter.format(fecha).substring(0, 4)); i++) {
            anios.add(i + "");
        }
        curAnio = anios.get(0);
        curExpediente = "";
        //win_exp.detach();
    }

    @Command
    @NotifyChange
    public void numeroexpediente(@BindingParam("num") String num) {

        if (busqueda_exp.isChecked()) {
            if (getCurExpediente().length() < 6) {
                setCurExpediente(getCurExpediente() + num);
            }
            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpediente");
        } else {
            if (getCurDNI().length() < 8) {
                setCurDNI(getCurDNI() + num);
            }
            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curDNI");
        }
    }

    @Command
    @NotifyChange
    public void numeroexpedientecut() {
        if (busqueda_exp.isChecked()) {
            if (getCurExpediente().length() - 1 >= 0) {
                setCurExpediente(getCurExpediente().substring(0, getCurExpediente().length() - 1));
            }
            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpediente");
        } else {
            if (getCurDNI().length() - 1 >= 0) {
                setCurDNI(getCurDNI().substring(0, getCurDNI().length() - 1));
            }
            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curDNI");
        }
    }

    @Command
    @NotifyChange
    public void numeroexpedienteclear() {
        setCurExpediente("");
        setCurDNI("");
        curExpedienteObj = new ExpedienteCliente();
        list_movimientos = new ArrayList<ExpedienteMovimientos>();
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpediente");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpedienteObj");
        BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
    }

    @Command
    @NotifyChange
    public void cargarExpediente() {
        if (busqueda_exp.isChecked()) {
            if (curAnio != null && !curAnio.isEmpty()) {
                if (curExpediente != null & !curExpediente.isEmpty()) {
                    try {
                        List<ExpedienteCliente> lista = dao_expediente.get_expedientes_consultar_unico(getCurExpediente() + "-" + getCurAnio());
                        if (lista.size() > 0) {
                            setCurExpedienteObj(lista.get(0));
                            setList_movimientos(dao_expediente.get_expedientes_movimientos(getCurExpediente() + "-" + getCurAnio()));
                            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpedienteObj");
                            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
                        } else {
                            setCurExpedienteObj(null);
                            setList_movimientos(null);
                            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpedienteObj");
                            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
                            Messagebox.show("No se encontro al expediente");
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(Cmodulocliente_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    Messagebox.show("Escriba un expediente");
                }
            } else {
                Messagebox.show("Seleccione un año");
            }
        } else if (!curDNI.isEmpty() && curDNI.length() == 8) {
            try {
                setList_expedientes(dao_expediente.get_expedientes_consultar_by_dni(getCurDNI()));
                setCurExpedienteObj(null);
                setList_movimientos(null);
                BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "curExpedienteObj");
                BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
                BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_expedientes");
                if (getList_expedientes().isEmpty()) {
                    Messagebox.show("No se encontro registros con este numero de DNI");
                }
            } catch (Exception ex) {
                Logger.getLogger(Cmodulocliente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Messagebox.show("Escriba un numero de DNI valido");
        }
    }

    @Listen("onSelect = listbox")
    public void updateMessage() throws Exception {

    }

    public List<String> getAnios() {
        return anios;
    }

    public void setAnios(List<String> anios) {
        this.anios = anios;
    }

    public String getCurAnio() {
        return curAnio;
    }

    public void setCurAnio(String curAnio) {
        this.curAnio = curAnio;
    }

    public List<ExpedienteMovimientos> getList_movimientos(){
        
        return list_movimientos;
    }

    public void setList_movimientos(List<ExpedienteMovimientos> list_movimientos) {
        
        this.list_movimientos = list_movimientos;
    }

    public String getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(String curExpediente) {
        
        this.curExpediente = curExpediente;
    }

    public ExpedienteCliente getCurExpedienteObj() {
        return curExpedienteObj;
    }

    public void setCurExpedienteObj(ExpedienteCliente curExpedienteObj) {
        if (curExpedienteObj != null) {
            try {
                setList_movimientos(dao_expediente.get_expedientes_movimientos(curExpedienteObj.getEXPNUM()));
                //Messagebox.show("Okaz");
            } catch (Exception ex) {
                Logger.getLogger(Cmodulocliente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
            BindUtils.postNotifyChange(null, null, Cmodulocliente_win.this, "list_movimientos");
            //Messagebox.show("XD");
        }
        this.curExpedienteObj = curExpedienteObj;
    }

}
