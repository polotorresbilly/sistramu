package com.marjuv.controller.win;

import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.entity.asunto_expediente;
import com.marjuv.entity.asunto_expediente;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class asunto_expediente_win {

    private List<asunto_expediente> lst_asunto_expediente;
    private asunto_expediente cur_asunto_expediente;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Asunto"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lst_asunto_expediente")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_asunto_expediente")
    public void updateGrid() {
        try {
            asunto_expediente_dao dao = new asunto_expediente_dao();
            lst_asunto_expediente = dao.get_asuntos_expediente();
            List<asunto_expediente> new_list = new ArrayList<asunto_expediente>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (asunto_expediente contribuyente : lst_asunto_expediente) {
                            if ((contribuyente.getDesc_asex().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
            }
            lst_asunto_expediente = new_list;
        } catch (Exception ex) {
            Messagebox.show("error"+ex.getMessage());
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
            lst_asunto_expediente = obj_asunto_expediente_dao.get_asuntos_expediente();
            obj_asunto_expediente_dao.close();
        } catch (Exception ex) {
        }
    }

    public List<asunto_expediente> getLst_asunto_expediente() {
        return lst_asunto_expediente;
    }

    @Command
    public void nuevo_asunto_expediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sAsunto_expediente", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_asunto_expediente.zul", null, map);
    }

    @Command
    public void editThisAsuntoExpediente() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sAsunto_expediente", this.cur_asunto_expediente);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_asunto_expediente.zul", null, map);
        } catch (Exception ex) {
        }
    }

    @GlobalCommand
    @NotifyChange("lst_asunto_expediente")
    public void updateAsuntoExpedienteList(@BindingParam("sAsunto_expediente") asunto_expediente cur_asunto_expediente,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
            if (recordMode.equals("NEW")) {
                obj_asunto_expediente_dao.insert(cur_asunto_expediente);
                obj_asunto_expediente_dao.close();
                Messagebox.show("El Asunto fue registrado");
            } else if (recordMode.equals("EDIT")) {
                obj_asunto_expediente_dao.update(cur_asunto_expediente);
                obj_asunto_expediente_dao.close();
                Messagebox.show("El Asunto fue Actualizado");
            }
           
        }
         updateGrid();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisAsuntoExpediente() {
        String str = "¿Está seguro de que desea eliminar el asunto de expediente \""
                + cur_asunto_expediente.getDesc_asex()+ "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
                        obj_asunto_expediente_dao.delete(cur_asunto_expediente.getCodi_asex());
                        BindUtils.postNotifyChange(null, null, asunto_expediente_win.this, "lst_asunto_expediente");
                        updateGrid();
                    } catch (Exception ex) {
                        Logger.getLogger(asunto_expediente_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_asunto_expediente(List<asunto_expediente> lst_asunto_expediente) {
        this.lst_asunto_expediente = lst_asunto_expediente;
    }

    public asunto_expediente getCur_asunto_expediente() {
        return cur_asunto_expediente;
    }

    public void setCur_asunto_expediente(asunto_expediente cur_asunto_expediente) {
        this.cur_asunto_expediente = cur_asunto_expediente;
    }

}
