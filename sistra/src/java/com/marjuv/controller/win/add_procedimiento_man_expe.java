package com.marjuv.controller.win;

import com.marjuv.controller.man.*;
import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.entity.Contribuyente;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class add_procedimiento_man_expe {

    @Wire("#winadd")
    private Window win;
    @WireVariable
    private Session _sess;
    private List<BeanProcedimientoMan> lst_procedimiento_expediente;
    private BeanProcedimientoMan cur_procedimiento_expediente;
     private int codi_asex;
     private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
 
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        curBusqueda = listBusqueda.get(0);
    }
    
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("codi_asex") int codigo) {
        
        if (getSess().getAttribute("acceso") == null && Integer.parseInt(getSess().getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            procedimiento_expediente_dao ObjProcedimientoExp = new procedimiento_expediente_dao();
            setLst_procedimiento_expediente(ObjProcedimientoExp.get_procedimiento_expediente2(codigo));
            ObjProcedimientoExp.close();
            setCodi_asex(codigo);
            loadDataControls();
        } catch (Exception ex) {
        }
    }
    
    @Command
    @NotifyChange("lst_procedimiento_expediente")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_procedimiento_expediente")
    public void updateGrid() {
        try {
            procedimiento_expediente_dao dao = new procedimiento_expediente_dao();
            lst_procedimiento_expediente = dao.get_procedimiento_expediente2(codi_asex);
            List<BeanProcedimientoMan> new_list = new ArrayList<BeanProcedimientoMan>();
            dao.close();

                        for (BeanProcedimientoMan procedimiento : lst_procedimiento_expediente) {
                            if ((procedimiento.getNomb_proc().toUpperCase() + " " + procedimiento.getNomb_proc().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(procedimiento);
                            }
                        }
                lst_procedimiento_expediente = new_list;
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Command
    public void addprocedimiento() {
        try {
            Map map = new HashMap();
            map.put("codi_proc", cur_procedimiento_expediente.getCodi_proc());
            map.put("nomb_proc", cur_procedimiento_expediente.getNomb_proc());
            map.put("dias_proc", cur_procedimiento_expediente.getDias_proc());
            map.put("estado", 2);
            BindUtils.postGlobalCommand(null, null, "setProcedimiento_expe", map);
            win.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void closeAddProcedimiento() {
        win.detach();
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }

    public List<BeanProcedimientoMan> getLst_procedimiento_expediente() {
        return lst_procedimiento_expediente;
    }

    public void setLst_procedimiento_expediente(List<BeanProcedimientoMan> lst_procedimiento_expediente) {
        this.lst_procedimiento_expediente = lst_procedimiento_expediente;
    }

    public BeanProcedimientoMan getCur_procedimiento_expediente() {
        return cur_procedimiento_expediente;
    }

    public void setCur_procedimiento_expediente(BeanProcedimientoMan cur_procedimiento_expediente) {
        this.cur_procedimiento_expediente = cur_procedimiento_expediente;
    }

    public int getCodi_asex() {
        return codi_asex;
    }

    public void setCodi_asex(int codi_asex) {
        this.codi_asex = codi_asex;
    }

}
