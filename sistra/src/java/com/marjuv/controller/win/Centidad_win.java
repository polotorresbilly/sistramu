package com.marjuv.controller.win;

import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class Centidad_win {

    //private BeanEntidad bean;
    private List<Entidad> lista_entidades;
    private Entidad_dao dao;
    @WireVariable
    //private Session _sess;
    private Entidad curSelectedEntidad;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
    
    
    @WireVariable
    private Session _sess;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        listBusqueda.add(new Pair<Integer, String>(3, "Direccion"));
        listBusqueda.add(new Pair<Integer, String>(4, "Lugar"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lista_entidades")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lista_entidades")
    public void updateGrid() {
        try {
            lista_entidades = dao.get_entidades();
            dao.close();
            List<Entidad> new_list = new ArrayList<Entidad>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (Entidad contribuyente : lista_entidades) {
                            if ((contribuyente.getNomb_enti()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (Entidad contribuyente : lista_entidades) {
                            if ((contribuyente.getDire_enti().toUpperCase()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 4:
                        for (Entidad contribuyente : lista_entidades) {
                            if (((contribuyente.getObj_dist().getNomb_dist()+contribuyente.getObj_dist().getProvincia().getNomb_prov()+contribuyente.getObj_dist().getProvincia().getDepartamento().getNomb_depa()).toUpperCase()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lista_entidades = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Entidad getCurSelectedEntidad() {
        return curSelectedEntidad;
    }

    public void setCurSelectedEntidad(Entidad curSelectedEntidad) {
        this.curSelectedEntidad = curSelectedEntidad;
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        loadDataControls();
        try {
            dao = new Entidad_dao();
            lista_entidades = dao.get_entidades();
            dao.close();
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public List<Entidad> getLista_entidades() {
        return lista_entidades;
    }

    public void setLista_entidades(List<Entidad> lista_entidades) {
        this.lista_entidades = lista_entidades;
    }

    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteEntidad() {
        Messagebox.show("Esta Seguro que desea Eliminar la Entidad \"" + curSelectedEntidad.getNomb_enti()
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            try {
                                dao = new Entidad_dao();
                                dao.delete_entidad(curSelectedEntidad.getCodi_enti());
                                dao.close();
                                insert_auditoria("D", "ENTIDAD");
                                updateGrid();
                                BindUtils.postNotifyChange(null, null, Centidad_win.this, "lista_entidades");
                            } catch (Exception ex) {
                                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                        "Lista de Entidades", Messagebox.CANCEL, Messagebox.ERROR);
                                Logger.getLogger(Centidad_win.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                });

    }
    
    @Command
    public void addEntidad() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curEntidad", null);
        map.put("accion", "NEW");
        Executions.createComponents("/mantenimiento/man/entidad_man.zul", null, map);
    }

    @Command
    public void editEntidad() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        
        map.put("curEntidad", this.curSelectedEntidad);
        map.put("accion", "EDIT");
        Executions.createComponents("/mantenimiento/man/entidad_man.zul", null, map);
    }
    
    @GlobalCommand
    @NotifyChange("lista_entidades")
    public void updateListEntidad(@BindingParam("curEntidad") Entidad entidad, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                dao = new Entidad_dao();
                dao.insert_entidad(entidad);
                dao.close();
                insert_auditoria("I", "ENTIDAD");
                BindUtils.postNotifyChange(null, null, Centidad_win.this, "allEntidades");
                Messagebox.show("La entidad fue registrada");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Entidades", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Centidad_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (recordMode.equals("EDIT")) {
            try {
                dao = new Entidad_dao();
                dao.update_entidad(entidad);
                dao.close();
                insert_auditoria("U", "ENTIDAD");
                BindUtils.postNotifyChange(null, null, Centidad_win.this, "allEntidades");
                 Messagebox.show("La entidad fue actualizada");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Entidades", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Centidad_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        updateGrid();
    }
}
