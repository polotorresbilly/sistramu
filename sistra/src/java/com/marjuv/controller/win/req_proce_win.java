package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.administracion;
import com.marjuv.entity.requisito_procedimiento;
import com.marjuv.entity.usuario;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class req_proce_win {

    @Wire("#win_req")
    private Window win2;

    private List<BeanRequisitoMan> lst_req;
    private BeanRequisitoMan cur_req;
    @WireVariable
    private Session _sess;
    private int codi_proc;
    private String sw;
    private List<BeanRequisitoMan> lst_req_temp;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sw") String sw,
            @ExecutionArgParam("lst_req") List<BeanRequisitoMan> lst_req_temp,
            @ExecutionArgParam("codi_proc") int codi_proc) {
        this.codi_proc = codi_proc;
        this.sw = sw;
        if (this.sw.equals("TRUE")) {
            this.lst_req_temp = lst_req_temp;
        }
    }
    

    @Command
    public void agregar_destino_requisitos_incompletos() {
        try {
            Map map = new HashMap();
            map.put("codi_arde",1);
            map.put("nomb_arde", "OFI. DE TRÁMITE DOCUMENTARIO Y ARCHIVO");
            map.put("nomb_carg", "JEFE DE OFICINA");
            map.put("codi_enti", 0);
            map.put("codi_carg", 20);
            map.put("tipo", 2);
            map.put("dias", 2);
            
            administracion_dao ObjAdministracion = new administracion_dao();
            administracion ObjAdministracionTO = new administracion();
            Calendar fecha = Calendar.getInstance();
            int año = fecha.get(Calendar.YEAR);
            ObjAdministracionTO = ObjAdministracion.get_administracion(año);
            map.put("nomb_enti", ObjAdministracionTO.getNoen_admi());
            ObjAdministracion.close();
            
            BindUtils.postGlobalCommand(null, null, "setDestinatarioTramite", map);
            win2.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una excepción en Agregar Destino Trámite: " + ex.getMessage());
        }
    }
    
    @Command
    public void agregar_destino_conforme() {
        try {
            procedimiento_expediente_dao ObjProcedimientoDAO = new procedimiento_expediente_dao();
            List<BeanProcedimientoMan> list = new ArrayList<BeanProcedimientoMan>();
            list = ObjProcedimientoDAO.get_procedimiento_requisito_conforme(codi_proc);
            Map map = new HashMap();
            map.put("codi_proc",list.get(0).getCodi_proc());
            map.put("nomb_proc",list.get(0).getNomb_proc());
            map.put("estado",1);
            map.put("tipo", 1);
            ObjProcedimientoDAO.close();
            BindUtils.postGlobalCommand(null, null, "setProcedimiento_expe2", map);
            win2.detach();
            
            
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una excepción en Agregar Destino Trámite: " + ex.getMessage());
        }
    }

    @Command
    public void change() {
        for (int i = 0; i < lst_req.size(); i++) {
            if (lst_req.get(i).getCodi_repr() == cur_req.getCodi_repr()) {
                if (cur_req.getCheck()) {
                    lst_req.get(i).setCheck(false);
                } else {
                    lst_req.get(i).setCheck(true);
                }
                break;
            }
        }
    }

    @Command
    public void save() {
        List<BeanRequisitoMan> lst_tem = new ArrayList<BeanRequisitoMan>();
        for (int i = 0; i < lst_req.size(); i++) {
            if (lst_req.get(i).getCheck()) {
                lst_tem.add(lst_req.get(i));
            }
        }
        Map args = new HashMap();
        args.put("requisito", lst_tem);
        BindUtils.postGlobalCommand(null, null, "setRequisitos", args);
        win2.detach();
    }
    
    @Command
    public void view_subreq() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curRequerimiento", cur_req.getCodi_repr());
        Executions.createComponents("proceso/win/view_req_proc.zul", null, map);
    }
    
    public List<BeanRequisitoMan> getLst_req() {
        try {
            requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
            lst_req = obj_requisito_procedimiento_dao.get_requisito_procedimiento(codi_proc);
            obj_requisito_procedimiento_dao.close();
            if (this.sw.equals("TRUE")) {
                boolean sw2;
                for (int i = 0; i < lst_req.size(); i++) {
                    sw2 = true;
                    for (int j = 0; j < lst_req_temp.size(); j++) {
                        if (lst_req.get(i).getCodi_repr() == lst_req_temp.get(j).getCodi_repr()) {
                            lst_req.get(i).setCheck(true);
                            sw2 = false;
                            break;
                        }
                    }
                    if (sw2) {
                        lst_req.get(i).setCheck(false);
                    }
                }
                this.sw = "FALSE";
            }
        } catch (Exception ex) {
            System.out.println("ERROR " + ex.getMessage());
        }
        return lst_req;
    }

    public void setLst_req(List<BeanRequisitoMan> lst_req) {
        this.lst_req = lst_req;
    }

    public BeanRequisitoMan getCur_req() {
        return cur_req;
    }

    public void setCur_req(BeanRequisitoMan cur_req) {
        this.cur_req = cur_req;
    }

}
