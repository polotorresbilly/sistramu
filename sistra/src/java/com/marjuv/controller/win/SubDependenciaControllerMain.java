package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanSubDependencias;
import com.marjuv.dao.DependenciaDAO;
import com.marjuv.dao.SubDependenciaDAO;
import com.marjuv.entity.DependenciaTO;
import com.marjuv.entity.SubDependenciaTO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

public class SubDependenciaControllerMain {
    
    private BeanSubDependencias BeanSub;
    private List<BeanSubDependencias> ListaSubDependencia;
    
    @Init
    public void initSetup() {

    }

    public BeanSubDependencias getBeanSub() {
        return BeanSub;
    }

    public void setBeanSub(BeanSubDependencias BeanSub) {
        this.BeanSub = BeanSub;
    }

    public List<BeanSubDependencias> getListaSubDependencia() {
        return ListaSubDependencia;
    }

    public void setListaSubDependencia(List<BeanSubDependencias> ListaSubDependencia) {
        this.ListaSubDependencia = ListaSubDependencia;
    }
    
    @GlobalCommand
    @NotifyChange("allSubDependencias")
    public void updateSubDepndenciaList(@BindingParam("objSubDependencia") SubDependenciaTO objSubDependenciaTO,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            SubDependenciaDAO objSubDependenciaDAO = new SubDependenciaDAO();
            if (recordMode.equals("NEW")) {
                objSubDependenciaDAO.insertSubDependencia(objSubDependenciaTO);
                objSubDependenciaDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                     //Messagebox.show("Antes del Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                     objSubDependenciaDAO.updateSubDependencia(objSubDependenciaTO);
                     //Messagebox.show("Paso el Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                    objSubDependenciaDAO.close();
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateSubDepndenciaList: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }
               
            }
        }
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteSubDependencia() {
        String str = "¿Está seguro de que desea eliminar la subdepedencia \""
                + BeanSub.getDesc_sude() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        SubDependenciaDAO objSubDependenciaDAO = new SubDependenciaDAO();
                        objSubDependenciaDAO.deleteSubDependencia(BeanSub.getCodi_sude());
                        BindUtils.postNotifyChange(null, null,SubDependenciaControllerMain.this, "allSubDependencias");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

   
    
    public List<BeanSubDependencias> getAllSubDependencias() {
        try {
            SubDependenciaDAO objSubDependencia = new SubDependenciaDAO();
            ListaSubDependencia = objSubDependencia.getSubDependencias();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        return ListaSubDependencia;
    }
    
    @Command
    public void editarSubDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objSubDependencia", this.BeanSub);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_subdependencia.zul", null, map);
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void nuevaSubDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objSubDependencia", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/man/man_subdependencia.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception 2: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }
}
