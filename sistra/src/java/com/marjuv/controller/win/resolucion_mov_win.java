package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.entity.ExpedienteMovimientos;
import com.marjuv.entity.administracion;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.ServletContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class resolucion_mov_win {

    @Wire("#win_vis_expe")
    private Window win;

    private List<ExpedienteMovimientos> lst_expe;
    private ExpedienteMovimientos cur_expe;
    @WireVariable
    private Session _sess;

    private String nume_expe;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("nume_expe") String nume_expe,
            @ExecutionArgParam("nude_hist") int nude_hist,
            @ExecutionArgParam("correlativo") int corre_hist,
            @ExecutionArgParam("nume_reso") String nume_reso
    ) throws Exception {
        Expediente_dao obj_expe_dao = new Expediente_dao();
        lst_expe = obj_expe_dao.get_expedientes_movimientos_des(nume_expe, nude_hist);
        for (int i = 0; i < lst_expe.size(); i++) {
            if (lst_expe.get(i).getMOVIMIENTO() == corre_hist) {
                lst_expe.get(i).setNume_reso(nume_reso);
                break;
            }
        }
        this.setNume_expe(nume_expe);
    }

    @Command
    public void ver_expe() throws FileNotFoundException {
        try {
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin() + "\\archivos\\";
            obj_administracion_dao.close();
            String ruta = URL_FILE + "/" + this.getNume_expe() + "_" + cur_expe.getNRODESTINO() + "_" + cur_expe.getMOVIMIENTO();
            File archivo = new File(ruta + ".pdf");
            if (!archivo.exists()) {
                archivo = new File(ruta + ".docx");
                if (!archivo.exists()) {
                    archivo = new File(ruta + ".xlsx");
                    if (!archivo.exists()) {
                        archivo = new File(ruta + ".doc");
                        if (!archivo.exists()) {
                            archivo = new File(ruta + ".xls");
                            if (!archivo.exists()) {
                                archivo = new File(ruta + ".zip");
                                if (!archivo.exists()) {
                                    archivo = new File(ruta + ".rar");
                                    if (!archivo.exists()) {
                                        Messagebox.show("No se encontró documento " + this.getNume_expe() + "_" + cur_expe.getNRODESTINO() + "_" + cur_expe.getMOVIMIENTO());
                                        return;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            Filedownload.save(archivo, null);
        } catch (Exception e) {
        }
    }

    @Command
    public void ver_carta() throws FileNotFoundException {

        try {
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin() + "\\cartas\\";
            obj_administracion_dao.close();

            String ruta = URL_FILE + "/" + this.getNume_expe() + "_" + cur_expe.getNRODESTINO() + "_" + cur_expe.getMOVIMIENTO();
            File archivo = new File(ruta + ".pdf");
            if (!archivo.exists()) {
                archivo = new File(ruta + ".docx");
                if (!archivo.exists()) {
                    archivo = new File(ruta + ".xlsx");
                    if (!archivo.exists()) {
                        archivo = new File(ruta + ".doc");
                        if (!archivo.exists()) {
                            archivo = new File(ruta + ".xls");
                            if (!archivo.exists()) {
                                archivo = new File(ruta + ".zip");
                                if (!archivo.exists()) {
                                    archivo = new File(ruta + ".rar");
                                    if (!archivo.exists()) {
                                        Messagebox.show("No se encontró documento " + this.getNume_expe() + "_" + cur_expe.getNRODESTINO() + "_" + cur_expe.getMOVIMIENTO());
                                        return;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            Filedownload.save(archivo, null);
        } catch (Exception e) {
        }
    }

    @Command
    public void ver_reso() throws FileNotFoundException {

        try {
            if (cur_expe.getNume_reso() != null && !cur_expe.equals("")) {
                ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
                Calendar fecha2 = new GregorianCalendar();
                int año = fecha2.get(Calendar.YEAR);
                administracion_dao obj_administracion_dao = new administracion_dao();
                administracion ObjAdministracion = new administracion();
                ObjAdministracion = obj_administracion_dao.get_administracion(año);
                String URL_FILE = ObjAdministracion.getUrl_file_admin() + "\\resoluciones\\";
                obj_administracion_dao.close();

                String ruta = URL_FILE + "/" + cur_expe.getNume_reso().replace(' ', '_').replace('\\', '_').replace('/', '_').replace(':', '_').replace('*', '_').replace('?', '_').replace('"', '_').replace('<', '_').replace('>', '_').replace('|', '_');
                File archivo = new File(ruta + ".pdf");
                if (!archivo.exists()) {
                    archivo = new File(ruta + ".docx");
                    if (!archivo.exists()) {
                        archivo = new File(ruta + ".xlsx");
                        if (!archivo.exists()) {
                            archivo = new File(ruta + ".doc");
                            if (!archivo.exists()) {
                                archivo = new File(ruta + ".xls");
                                if (!archivo.exists()) {
                                    archivo = new File(ruta + ".zip");
                                    if (!archivo.exists()) {
                                        archivo = new File(ruta + ".rar");
                                        if (!archivo.exists()) {
                                            Messagebox.show("No se encontró documento " + cur_expe.getNume_reso().replace(' ', '_').replace('\\', '_').replace('/', '_').replace(':', '_').replace('*', '_').replace('?', '_').replace('"', '_').replace('<', '_').replace('>', '_').replace('|', '_'));
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                Filedownload.save(archivo, null);
            } else {
                Messagebox.show("El expediente y movimiento seleccionado no se encuentra adjuntado a una resolución");
            }
        } catch (Exception e) {
        }
    }

    @Command
    public void closeThis() {
        win.detach();
    }

    public List<ExpedienteMovimientos> getLst_expe() {
        return lst_expe;
    }

    public void setLst_expe(List<ExpedienteMovimientos> lst_expe) {
        this.lst_expe = lst_expe;
    }

    public ExpedienteMovimientos getCur_expe() {
        return cur_expe;
    }

    public void setCur_expe(ExpedienteMovimientos cur_expe) {
        this.cur_expe = cur_expe;
    }

    public String getNume_expe() {
        return nume_expe;
    }

    public void setNume_expe(String nume_expe) {
        this.nume_expe = nume_expe;
    }

}
