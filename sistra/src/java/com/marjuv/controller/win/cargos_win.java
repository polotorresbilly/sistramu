package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.entity.cargos;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class cargos_win {

    private List<BeanCargoMan> lst_cargos;
    private BeanCargoMan cur_cargos;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        listBusqueda.add(new Pair<Integer, String>(3, "Area"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lst_cargos")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_cargos")
    public void updateGrid() {
        try {
            cargos_dao dao = new cargos_dao();
            lst_cargos = dao.get_cargos();
            List<BeanCargoMan> new_list = new ArrayList<BeanCargoMan>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanCargoMan contribuyente : lst_cargos) {
                            if ((contribuyente.getNomb_carg().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (BeanCargoMan contribuyente : lst_cargos) {
                            if ((contribuyente.getNomb_arde().toUpperCase()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lst_cargos = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            cargos_dao obj_cargos_dao = new cargos_dao();
            lst_cargos = obj_cargos_dao.get_cargos();
            obj_cargos_dao.close();
        } catch (Exception ex) {
        }
    }

    public List<BeanCargoMan> getLst_cargos() {
        return lst_cargos;
    }

    @Command
    public void nuevo_cargos() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sCargo", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_cargos.zul", null, map);
    }

    @Command
    public void editThisCargo() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sCargo", this.cur_cargos);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_cargos.zul", null, map);
        } catch (Exception ex) {
        }
    }
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }
    
    @GlobalCommand
    @NotifyChange("lst_cargos")
    public void updateCargoList(@BindingParam("sCargo") cargos cur_cargos,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            if (recordMode.equals("NEW")) {
                obj_cargos_dao.insert(cur_cargos);
                obj_cargos_dao.close();
                insert_auditoria("I", "CARGOS");
                Messagebox.show("El cargo fue Registrado");
            } else if (recordMode.equals("EDIT")) {
                obj_cargos_dao.update(cur_cargos);
                obj_cargos_dao.close();
                insert_auditoria("U", "CARGOS");
                Messagebox.show("El cargo fue Actualizado");
            }
        }
        updateGrid();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisCargo() {
        String str = "¿Está seguro de que desea eliminar el cargos \""
                + cur_cargos.getNomb_carg() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        cargos_dao obj_cargos_dao = new cargos_dao();
                        obj_cargos_dao.delete(cur_cargos.getCodi_carg());
                        insert_auditoria("D", "CARGOS");
                        updateGrid();
                        BindUtils.postNotifyChange(null, null, cargos_win.this, "lst_cargos");
                    } catch (Exception ex) {
                        Logger.getLogger(cargos_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_cargos(List<BeanCargoMan> lst_cargos) {
        this.lst_cargos = lst_cargos;
    }

    public BeanCargoMan getCur_cargos() {
        return cur_cargos;
    }

    public void setCur_cargos(BeanCargoMan cur_cargos) {
        this.cur_cargos = cur_cargos;
    }

}
