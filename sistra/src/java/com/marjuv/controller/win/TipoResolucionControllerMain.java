package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.TipoResolucionDAO;
import com.marjuv.entity.TipoResolucionTO;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class TipoResolucionControllerMain {

    private BeanTipoResolucion BeanTipoResolucion;
    private List<BeanTipoResolucion> ListTipoResolucion;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Tipo"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("listaTR")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

        @Command
    @NotifyChange("listaTR")
    public void updateGrid() {
        try {
            TipoResolucionDAO dao = new TipoResolucionDAO();
            ListTipoResolucion = dao.getListTipoResolucion();
            dao.close();
            List<BeanTipoResolucion> new_list = new ArrayList<BeanTipoResolucion>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanTipoResolucion contribuyente : ListTipoResolucion) {
                            if ((contribuyente.getDesc_tire().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                ListTipoResolucion = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        try {
            loadDataControls();
            TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
            ListTipoResolucion = objTipoResolucionDAO.getListTipoResolucion();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en ListaTR " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public BeanTipoResolucion getBeanTipoResolucion() {
        return BeanTipoResolucion;
    }

    public void setBeanTipoResolucion(BeanTipoResolucion BeanTipoResolucion) {
        this.BeanTipoResolucion = BeanTipoResolucion;
    }

    public List<BeanTipoResolucion> getListTipoResolucion() {
        return ListTipoResolucion;
    }

    public void setListTipoResolucion(List<BeanTipoResolucion> ListTipoResolucion) {
        this.ListTipoResolucion = ListTipoResolucion;
    }

    @Command
    public void editTipoResolucion() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyTipoResolucion", this.BeanTipoResolucion);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_tipo_resolucion.zul", null, map);

        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception en Edit(Tipo Resolucion): " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Command
    public void newTipoResolucion() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyTipoResolucion", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/man/man_tipo_resolucion.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en New(Tipo Resolucion): " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @GlobalCommand
    @NotifyChange("listaTR")
    public void updateTR(@BindingParam("keyTipoResolucion") TipoResolucionTO objTipoResolucionTO, @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
            if (recordMode.equals("NEW")) {
                objTipoResolucionDAO.insert(objTipoResolucionTO);
                objTipoResolucionDAO.close();
                insert_auditoria("I", "TIPO_RESOLUCION");
                Messagebox.show("El tipo de resolución fue registrado");
            } else if (recordMode.equals("EDIT")) {
                try {
                    objTipoResolucionDAO.update(objTipoResolucionTO);
                    objTipoResolucionDAO.close();
                    insert_auditoria("U", "TIPO_RESOLUCION");
                    Messagebox.show("El tipo de resolución fue actualizado");
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateTR: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }

            }
        }
        updateGrid();
    }

    public List<BeanTipoResolucion> getListaTR() {
        return ListTipoResolucion;
    }
    
    @WireVariable
    private Session _sess;
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteTipoResolucion() {
        String str = "¿Está seguro de que desea eliminar el tipo de Resolución \"" + BeanTipoResolucion.getDesc_tire() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {

            @Override
            public void onEvent(Event t) throws Exception {
                if (Messagebox.ON_YES.equals(t.getName())) {
                    try {
                        TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
                        objTipoResolucionDAO.delete(BeanTipoResolucion.getCodi_tire());
                        objTipoResolucionDAO.close();
                        insert_auditoria("D", "TIPO_RESOLUCION");
                        updateGrid();
                        BindUtils.postNotifyChange(null, null, TipoResolucionControllerMain.this, "listaTR");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        });
    }

}
