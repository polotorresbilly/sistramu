/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanTipoExpediente;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Tipo_Expediente_dao;
import com.marjuv.entity.Tipo_Expediente;
import com.marjuv.entity.usuario;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author YO
 */
public class TipoExpedienteControllerMain {
    private BeanTipoExpediente BeanTipoExpediente;
    private List<BeanTipoExpediente> ListTipoExpediente;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Tipo"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("listaTR")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

        @Command
    @NotifyChange("listaTR")
    public void updateGrid() {
        try {
            Tipo_Expediente_dao dao = new Tipo_Expediente_dao();
            ListTipoExpediente = dao.getListTipoExpediente();
            dao.close();
            List<BeanTipoExpediente> new_list = new ArrayList<BeanTipoExpediente>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanTipoExpediente contribuyente : ListTipoExpediente) {
                            if ((contribuyente.getNomb_tiex().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                ListTipoExpediente = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        try {
            loadDataControls();
            Tipo_Expediente_dao objTipo_Expediente_dao = new Tipo_Expediente_dao();
            ListTipoExpediente = objTipo_Expediente_dao.getListTipoExpediente();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en ListaTR " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public BeanTipoExpediente getBeanTipoExpediente() {
        return BeanTipoExpediente;
    }

    public void setBeanTipoExpediente(BeanTipoExpediente BeanTipoExpediente) {
        this.BeanTipoExpediente = BeanTipoExpediente;
    }

    public List<BeanTipoExpediente> getListTipoExpediente() {
        return ListTipoExpediente;
    }

    public void setListTipoExpediente(List<BeanTipoExpediente> ListTipoExpediente) {
        this.ListTipoExpediente = ListTipoExpediente;
    }

    @Command
    public void editTipoExpediente() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyTipoExpediente", this.BeanTipoExpediente);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/win/man_tipo_expediente.zul", null, map);

        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception en Edit(Tipo Expediente): " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Command
    public void newTipoExpediente() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyTipoExpediente", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/win/man_tipo_expediente.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en New(Tipo Expediente): " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @WireVariable
    private Session _sess;
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @GlobalCommand
    @NotifyChange("listaTR")
    public void updateTR(@BindingParam("keyTipoExpediente") Tipo_Expediente objTipoExpediente, @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            Tipo_Expediente_dao objTipo_Expediente_dao = new Tipo_Expediente_dao();
            if (recordMode.equals("NEW")) {
                objTipo_Expediente_dao.insert(objTipoExpediente);
                objTipo_Expediente_dao.close();
                insert_auditoria("I", "TIPO_EXPEDIENTE");
                Messagebox.show("El tipo de expediente fue registrado");
            } else if (recordMode.equals("EDIT")) {
                try {
                    objTipo_Expediente_dao.update(objTipoExpediente);
                    objTipo_Expediente_dao.close();
                    insert_auditoria("U", "TIPO_EXPEDIENTE");
                    Messagebox.show("El tipo de expediente fue actualizado");
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateTR: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }

            }
        }
        updateGrid();
    }

    public List<BeanTipoExpediente> getListaTR() {
        return ListTipoExpediente;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteTipoExpediente() {
        String str = "¿Está seguro de que desea eliminar el tipo de Expediente \"" + BeanTipoExpediente.getNomb_tiex()+ "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {

            @Override
            public void onEvent(Event t) throws Exception {
                if (Messagebox.ON_YES.equals(t.getName())) {
                    try {
                        Tipo_Expediente_dao objTipo_Expediente_dao = new Tipo_Expediente_dao();
                        objTipo_Expediente_dao.delete(BeanTipoExpediente.getCodi_tiex());
                        objTipo_Expediente_dao.close();
                        insert_auditoria("D", "TIPO_EXPEDIENTE");
                        updateGrid();
                        BindUtils.postNotifyChange(null, null, TipoExpedienteControllerMain.this, "listaTR");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        });
    }
}
