package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_expediente_reso_win {

    @Wire("#search_reso_expe")
    private Window win;
    private List<ExpedienteCliente> lst_expediente_reso;
    private ExpedienteCliente cur_expediente_reso;
    @WireVariable
    private Session _sess;

    @Wire
    private Textbox label_busq;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
    }

    public List<ExpedienteCliente> getLst_expediente_reso() {
        try {
            Expediente_dao obj_expediente_dao = new Expediente_dao();
            usuario obj = (usuario) _sess.getAttribute("usuario");
            lst_expediente_reso = obj_expediente_dao.search_expediente_reso(label_busq.getValue(), obj.getCodi_arde());
            obj_expediente_dao.close();
        } catch (Exception ex) {
        }
        return lst_expediente_reso;
    }

    @Command
    public void seleccionar() throws SQLException, Exception {
        Expediente_dao expdao = new Expediente_dao();
        List<ExpedienteCliente> list = new ArrayList<ExpedienteCliente>();
        try {
            list = expdao.get_expediente_vinculados(cur_expediente_reso.getEXPNUM(), cur_expediente_reso.getCORRELATIVO(), cur_expediente_reso.getDESTINO());
            //Messagebox.show(cur_expediente_reso.getEXPNUM()+"/"+cur_expediente_reso.getCORRELATIVO()+"/"+cur_expediente_reso.getDESTINO());
        } catch (Exception ex) {
            Logger.getLogger(search_expediente_reso_win.class.getName()).log(Level.SEVERE, null, ex);
            //Messagebox.show(cur_expediente_reso.getEXPNUM()+"/"+cur_expediente_reso.getCORRELATIVO()+"/"+cur_expediente_reso.getDESTINO()+ex.getMessage());
        } finally {
            expdao.close();
        }
        /*if (list.size() > 0) {
            Messagebox.show("Este expediente ya fue vinculado en este tramite.");
        } else */if (!cur_expediente_reso.getDOCUMENTO().contains("-COPIA")) {
            List<ExpedienteCliente> lista_vinc = expdao.get_expediente_vinculados_total(cur_expediente_reso.getEXPNUM(), cur_expediente_reso.getDESTINO());
            Map args = new HashMap();
            args.put("ExpedienteCliente", this.cur_expediente_reso);
            args.put("listaVinculados", lista_vinc);
            BindUtils.postGlobalCommand(null, null, "setExpedienteReso", args);
            win.detach();
        } else {
            Messagebox.show("Solo se puede vincular expedientes de tipo ORIGINAL");
        }

    }

    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_expediente_reso(List<ExpedienteCliente> lst_expediente_reso) {
        this.lst_expediente_reso = lst_expediente_reso;
    }

    public ExpedienteCliente getCur_expediente_reso() {
        return cur_expediente_reso;
    }

    public void setCur_expediente_reso(ExpedienteCliente cur_expediente_reso) {
        this.cur_expediente_reso = cur_expediente_reso;
    }

}
