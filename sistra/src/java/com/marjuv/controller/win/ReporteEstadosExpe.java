package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.entity.ExpedienteReportes;
import com.marjuv.entity.administracion;
import com.marjuv.entity.usuario;
import java.io.*;
import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author Joel
 */
public class ReporteEstadosExpe extends GenericForwardComposer implements Serializable {

    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;
    private String nume_expe;
    private List<ExpedienteReportes> lista;
    private String nomb_arde;
    private String[] codices;
    private int codi_arde;

    protected DataTransaction dt;
    protected Connection cn;

    public ReporteEstadosExpe() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    public void onCreate$winReport() throws IOException {
        params = (params == null) ? null : (HashMap) winReport.getAttribute("params");
        name = winReport.getAttribute("name").toString();
        url = winReport.getAttribute("url").toString();
        nomb_arde = winReport.getAttribute("nomb_arde").toString();
        codi_arde = Integer.parseInt(winReport.getAttribute("codi_arde").toString());
        codices = (winReport.getAttribute("codices") == null) ? null : (String[]) winReport.getAttribute("codices");
        lista = (List<ExpedienteReportes>) session.getAttribute("lista");
        cargarDatos();
//        System.out.println(lista.size());
        reportar();
    }

    public void reportar() throws IOException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();

            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_logo_admin();
            obj_administracion_dao.close();
            params.put("realPath",URL_FILE);
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, new JRBeanCollectionDataSource(lista, true));

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (is != null) {
                is.close();
            }
        }
    }

    private void cargarDatos() {
        try {

            usuario obj_usua = (usuario) session.getAttribute("usuario");

            Calendar calendar = Calendar.getInstance();
            java.util.Date currentDate = calendar.getTime();
            Date fecha = new Date(currentDate.getTime());
            int year = fecha.getYear() + 1900;

            administracion_dao obj_admin_dao = new administracion_dao();
            administracion obj_admin = obj_admin_dao.get_administracion(year);
            obj_admin_dao.close();
            params.put("noan_admi", obj_admin.getNoaa_admi());
            params.put("noen_admi", obj_admin.getNoen_admi());
            params.put("noms_con", obj_usua.getNoms_usua());
            usuario obj_login = (usuario) session.getAttribute("usuario");
            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            if (nomb_arde.equals("")) {
                nomb_arde = obj_Dependencia_dao.get_nomb2(obj_login.getCodi_arde());
            }
            int cant = lista.size();
            params.put("noar_admi", nomb_arde);
            obj_Dependencia_dao.close();
//            if (codi_arde > 0) { // BUSCAR SUBDEPENDENCIAS
//                obj_Dependencia_dao = new Dependencia_dao();
//                Expediente_dao expe_dao = new Expediente_dao();
//                List<BeanDependencias> list = obj_Dependencia_dao.getDependenciasJera(codi_arde);
//                if (list != null && !list.isEmpty()) {
//                    // AGREGAR SUS DEPENDENCIAS
//                    for (BeanDependencias level2 : list) {
//                        ExpedienteReportes obj = new ExpedienteReportes();
//                        obj.setFOLIOS(0);
//                        obj.setNROTRAMITE(0);
//                        obj.setNRODESTINATARIO(0);
//                        obj.setDESTINATARIO("");
//                        obj.setOBSERVACION("TRAMITES DE " + nomb_arde + " - " + level2.getNomb_arde());
//                        lista.add(obj);
//                        List<ExpedienteReportes> listlevel2 = expe_dao.get_expedientes_reportes(codices[0], codices[1], codices[2], Integer.parseInt(codices[3]), Integer.parseInt(codices[4]), level2.getCodi_arde(), Integer.parseInt(codices[6]));
//                        
//                        lista.addAll(listlevel2);
//                        cant+=listlevel2.size();
//                        List<BeanDependencias> lista3 = obj_Dependencia_dao.getDependenciasJera(level2.getCodi_arde());
//                        for (BeanDependencias level3 : lista3) {
//                            obj = new ExpedienteReportes();
//                            obj.setFOLIOS(0);
//                            obj.setNROTRAMITE(0);
//                            obj.setNRODESTINATARIO(0);
//                            obj.setDESTINATARIO("");
//                            obj.setOBSERVACION("TRAMITES DE " + nomb_arde + " - " + level2.getNomb_arde() + " - " + level3.getNomb_arde());
//                            lista.add(obj);
//                            List<ExpedienteReportes> listlevel3 = expe_dao.get_expedientes_reportes(codices[0], codices[1], codices[2], Integer.parseInt(codices[3]), Integer.parseInt(codices[4]), level3.getCodi_arde(), Integer.parseInt(codices[6]));
//                            lista.addAll(listlevel3);
//                            cant+=listlevel3.size();
//                            List<BeanDependencias> lista4 = obj_Dependencia_dao.getDependenciasJera(level3.getCodi_arde());
//                            for (BeanDependencias level4 : lista4) {
//                                obj = new ExpedienteReportes();
//                                obj.setFOLIOS(0);
//                                obj.setNROTRAMITE(0);
//                                obj.setNRODESTINATARIO(0);
//                                obj.setDESTINATARIO("");
//                                obj.setOBSERVACION("TRAMITES DE " + nomb_arde + " - " + level2.getNomb_arde() + " - " + level3.getNomb_arde() + " - " + level4.getNomb_arde());
//                                lista.add(obj);
//                                List<ExpedienteReportes> listlevel4 = expe_dao.get_expedientes_reportes(codices[0], codices[1], codices[2], Integer.parseInt(codices[3]), Integer.parseInt(codices[4]), level4.getCodi_arde(), Integer.parseInt(codices[6]));
//                                lista.addAll(listlevel4);
//                                cant+=listlevel4.size();
//                                List<BeanDependencias> lista5 = obj_Dependencia_dao.getDependenciasJera(level4.getCodi_arde());
//                                for (BeanDependencias level5 : lista5) {
//                                    obj = new ExpedienteReportes();
//                                    obj.setFOLIOS(0);
//                                    obj.setNROTRAMITE(0);
//                                    obj.setNRODESTINATARIO(0);
//                                    obj.setDESTINATARIO("");
//                                    obj.setOBSERVACION("TRAMITES DE " + nomb_arde + " - " + level2.getNomb_arde() + " - " + level3.getNomb_arde() + " - " + level4.getNomb_arde() +" - "+level5.getNomb_arde());
//                                    lista.add(obj);
//                                    Messagebox.show("Comprobacion 2");
//                                    Messagebox.show("Contenido del Parametro 1: " + codices[0]);
//                                    Messagebox.show("Contenido del Parametro 2: " + codices[1]);
//                                    Messagebox.show("Contenido del Parametro 3: " + codices[2]);
//                                    Messagebox.show("Contenido del Parametro 4: " + Integer.parseInt(codices[3]));
//                                    Messagebox.show("Contenido del Parametro 5: " + Integer.parseInt(codices[4]));
//                                    Messagebox.show("Contenido del Parametro 6: " + level5.getCodi_arde());
//                                    Messagebox.show("Contenido del Parametro 7: " + Integer.parseInt(codices[6]));
//                                    List<ExpedienteReportes> listlevel5 = expe_dao.get_expedientes_reportes(codices[0], codices[1], codices[2], Integer.parseInt(codices[3]), Integer.parseInt(codices[4]), level5.getCodi_arde(), Integer.parseInt(codices[6]));
//                                    lista.addAll(listlevel5);
//                                    cant+=listlevel5.size();
//                                    //List<BeanDependencias> lista6 = obj_Dependencia_dao.getDependenciasJera(level5.getCodi_arde());
//                                }
//                            }
//                        }
//                    }
//                } else {
//                    ExpedienteReportes obj = new ExpedienteReportes();
//                    obj.setFOLIOS(0);
//                    obj.setNROTRAMITE(0);
//                    obj.setNRODESTINATARIO(0);
//                    obj.setDESTINATARIO("");
//                    obj.setOBSERVACION("NO EXISTEN SUB-DEPENDENCIAS");
//                    lista.add(obj);
//                }
//                obj_Dependencia_dao.close();
//                expe_dao.close();
//            } else {
//                ExpedienteReportes obj = new ExpedienteReportes();
//                obj.setFOLIOS(0);
//                obj.setNROTRAMITE(0);
//                obj.setNRODESTINATARIO(0);
//                obj.setDESTINATARIO("");
//                obj.setOBSERVACION("NO EXISTEN SUB-DEPENDENCIAS");
//                lista.add(obj);
//            }
            params.put("cons_cont", cant+"");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
