/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.controller.win;

import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Firma_Resolucion_dao;
import com.marjuv.entity.Firma_Resolucion;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Cfirmaresolucion_win {
    private List<Firma_Resolucion> list_firmas;
    private Firma_Resolucion curFirma;
    private Firma_Resolucion_dao dao;
    
    @WireVariable
    private Session _sess;
    
    @Command
    public void addFirma() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curFirma", null);
        map.put("accion", "NEW");
        Executions.createComponents("/mantenimiento/man/firmaresolucion_man.zul", null, map);
    }

    @Command
    public void editFirma() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        
        map.put("curFirma", this.curFirma);
        map.put("accion", "EDIT");
        Executions.createComponents("/mantenimiento/man/firmaresolucion_man.zul", null, map);
    }
    @Init
    @NotifyChange
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            dao = new Firma_Resolucion_dao();
            setList_firmas(dao.get_all_firma_resolucion());
            
        } catch (Exception ex) {
            Logger.getLogger(Cfirmaresolucion_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteFirma() {
        Messagebox.show("Esta Seguro que desea Deshabilitar la Firma \"" + curFirma.getPofi_fire()
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            try {
                                dao.delete_firma(curFirma.getCodi_fire());
                                insert_auditoria("D", "FIRMA_RESOLUCION");
                                setList_firmas(dao.get_all_firma_resolucion());
                                BindUtils.postNotifyChange(null, null, Cfirmaresolucion_win.this, "list_firmas");
                                
                            } catch (Exception ex) {
                                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                        "Lista de Firmas", Messagebox.CANCEL, Messagebox.ERROR);
                                Logger.getLogger(Cfirmaresolucion_win.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                });

    }
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void rebornFirma() {
        Messagebox.show("Esta Seguro que desea Habilitar la Firma \"" + curFirma.getPofi_fire()
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            try {
                                dao.reborn_firma(curFirma.getCodi_fire());
                                insert_auditoria("U", "FIRMA_RESOLUCION");
                                setList_firmas(dao.get_all_firma_resolucion());
                                BindUtils.postNotifyChange(null, null, Cfirmaresolucion_win.this, "list_firmas");
                            } catch (Exception ex) {
                                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                        "Lista de Firmas", Messagebox.CANCEL, Messagebox.ERROR);
                                Logger.getLogger(Cfirmaresolucion_win.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                });

    }
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }
    
    
    @GlobalCommand
    @NotifyChange("list_firmas")
    public void updateFirmaResolucion(@BindingParam("curFirma") Firma_Resolucion firma, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                dao.insert_firma(firma);
                setList_firmas(dao.get_all_firma_resolucion());
                insert_auditoria("I", "FIRMA_RESOLUCION");
                BindUtils.postNotifyChange(null, null, Cfirmaresolucion_win.this, "list_firmas");
                Messagebox.show("El firma fue registrada");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Firmas", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Cfirmaresolucion_win.class.getName()).log(Level.SEVERE, null, ex);
               
            }
        }
        if (recordMode.equals("EDIT")) {
            try {
                dao.update_firma(firma);
                setList_firmas(dao.get_all_firma_resolucion());
                insert_auditoria("U", "FIRMA_RESOLUCION");
                BindUtils.postNotifyChange(null, null, Cfirmaresolucion_win.this, "list_firmas");
                 Messagebox.show("El firma fue Actualizada");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Firmas", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Cfirmaresolucion_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<Firma_Resolucion> getList_firmas() {
        return list_firmas;
    }

    public void setList_firmas(List<Firma_Resolucion> list_firmas) {
        this.list_firmas = list_firmas;
    }

    public Firma_Resolucion getCurFirma() {
        return curFirma;
    }

    public void setCurFirma(Firma_Resolucion curFirma) {
        this.curFirma = curFirma;
    }
    
    
}
