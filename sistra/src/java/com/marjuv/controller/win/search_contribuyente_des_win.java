package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.entity.Contribuyente;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_contribuyente_des_win {

    @Wire("#search_cont_des")
    private Window win;
    private List<Contribuyente> lst_contribuyente;
    private Contribuyente cur_contribuyente;
    @WireVariable
    private Session _sess;
    
    @Wire
    private Textbox label_busq;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
    }
    

    public List<Contribuyente> getLst_contribuyente() {
        try {
            Contribuyente_dao obj_contribuyente_dao = new Contribuyente_dao();
            lst_contribuyente = obj_contribuyente_dao.search_contribuyente(label_busq.getValue());
            obj_contribuyente_dao.close();
        } catch (Exception ex) {
        }
        return lst_contribuyente;
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("codi_cont", this.cur_contribuyente.getCodi_cont());
        args.put("nomb_cont", this.cur_contribuyente.getNomb_cont());
        //args.put("apel_cont", this.cur_contribuyente.getApel_cont());
        args.put("dni_cont", this.cur_contribuyente.getDni_cont());
        args.put("dire_cont", this.cur_contribuyente.getDire_cont());
        args.put("emai_cont", this.cur_contribuyente.getEmai_cont());
        args.put("nomb_dist", this.cur_contribuyente.getObj_dist().getNomb_dist());
        args.put("nomb_prov", this.cur_contribuyente.getObj_dist().getProvincia().getNomb_prov());
        args.put("nomb_depa", this.cur_contribuyente.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
        BindUtils.postGlobalCommand(null, null, "setContribuyenteDes", args);
        win.detach();
    }
    
    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_contribuyente(List<Contribuyente> lst_contribuyente) {
        this.lst_contribuyente = lst_contribuyente;
    }

    public Contribuyente getCur_contribuyente() {
        return cur_contribuyente;
    }

    public void setCur_contribuyente(Contribuyente cur_contribuyente) {
        this.cur_contribuyente = cur_contribuyente;
    }

}
