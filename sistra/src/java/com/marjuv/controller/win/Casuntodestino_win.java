/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.entity.Asunto_Resolucion;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Casuntodestino_win {

    private List<Asunto_Resolucion> lista_asuntos;
    private List<BeanDependencias> lista_dependencias;
    private Asunto_Resolucion_dao dao_asur;
    private Dependencia_dao dao_depe;
    @WireVariable
    private Asunto_Resolucion curAsuntoRes;
    private BeanDependencias curDependencia;
    private int curcoddependencia;
    
    @WireVariable
    private Session _sess;


    public List<Asunto_Resolucion> getLista_asuntos() {
        return lista_asuntos;
    }

    public void setLista_asuntos(List<Asunto_Resolucion> lista_asuntos) {
        this.lista_asuntos = lista_asuntos;
    }

    public Asunto_Resolucion getCurAsuntoRes() {
        return curAsuntoRes;
    }

    public void setCurAsuntoRes(Asunto_Resolucion curAsuntoRes) {
        this.curAsuntoRes = curAsuntoRes;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        dao_asur = new Asunto_Resolucion_dao();
        dao_depe = new Dependencia_dao();
    }

    @Command
    public void updateGridAsunto() {
        try {
            lista_asuntos = dao_asur.get_asunto_dependencia(curDependencia.getCodi_arde());
            curcoddependencia = curDependencia.getCodi_arde();
            dao_asur.close();
        } catch (Exception ex) {
            Logger.getLogger(Casuntodestino_win.class.getName()).log(Level.SEVERE, null, ex);
        }
        BindUtils.postNotifyChange(null, null, Casuntodestino_win.this, "lista_asuntos");
    }

    public List<BeanDependencias> getLista_dependencias() {
        try {
            lista_dependencias = dao_depe.getDependencias();
            dao_depe.close();
        } catch (Exception ex) {
            
        }
        return lista_dependencias;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteAsuntoResDependencia() {
        Messagebox.show("Esta Seguro que desea Eliminar la Contribuyente \"" + curAsuntoRes.getDesc_asre()
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        dao_asur.delete_asunto_dependencia(curcoddependencia, curAsuntoRes.getCodi_asre());
                        lista_asuntos = dao_asur.get_asunto_dependencia(curcoddependencia);
                        BindUtils.postNotifyChange(null, null, Casuntodestino_win.this, "lista_asuntos");
                        dao_asur.close();
                    } catch (Exception ex) {
                        Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                "Lista de Asuntos", Messagebox.CANCEL, Messagebox.ERROR);
                        Logger.getLogger(Casuntodestino_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        });

    }

    @Command
    public void addAsuntoResolucion() {
        if (curDependencia != null) {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("curDependencia", curDependencia);
            map.put("accion", "NEW");
            Executions.createComponents("/mantenimiento/man/asuntodestinodefecto_man.zul", null, map);
        } else {
            Messagebox.show("Debe de Seleccionar una Dependencia para poder agregar un nuevo asunto.",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
        }

    }

    @GlobalCommand
    @NotifyChange("lista_asuntos")
    public void updateListAsunto(@BindingParam("curAsunto") Asunto_Resolucion asunto,@BindingParam("curDependencia") BeanDependencias dependencia, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                dao_asur.insert_asunto_dependencia(dependencia.getCodi_arde(), asunto.getCodi_asre());
                
                BindUtils.postNotifyChange(null, null, Casuntodestino_win.class, "lista_asuntos");
                Messagebox.show("El asunto fue agregado");
                updateGridAsunto();
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Entidades", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Centidad_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        updateGridAsunto();
    }
}
