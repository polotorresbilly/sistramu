package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.DependenciaDAO;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.DependenciaTO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class DependenciaControllerEdit {

    @Wire("#dependencia_man")
    private Window win;
    private BeanDependencias beandatos;
    private String recordMode;
    private List<Pair<Integer, String>> listEntidad;
    private Pair<Integer, String> curEntidad;

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("objDependencia") BeanDependencias c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        loadDataEntidad();
        if (recordMode.equals("NEW")) {
            this.beandatos = new BeanDependencias();
        }

        if (recordMode.equals("EDIT")) {
            this.beandatos = (BeanDependencias) c1.clone();
            for (int i = 0; i < listEntidad.size(); i++) {
                if (listEntidad.get(i).getX() == this.beandatos.getCodi_enti()) {
                    this.curEntidad = listEntidad.get(i);
                    break;
                }

            }
        }
    }

    public BeanDependencias getBeandatos() {
        return beandatos;
    }

    public void setBeandatos(BeanDependencias beandatos) {
        this.beandatos = beandatos;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public void loadDataEntidad() {
        try {
            DependenciaDAO objDependenciaDAO = new DependenciaDAO();
            setListEntidad(objDependenciaDAO.getEntidad());
            objDependenciaDAO.close();
            if (listEntidad.size() > 0) {
                this.curEntidad = listEntidad.get(0);
            }
        } catch (Exception e) {
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            DependenciaDAO objDependenciaDAO = new DependenciaDAO();
            Messagebox.show("Despues del DAO ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
            if ((getRecordMode().equals("NEW") && objDependenciaDAO.validate_nameDependencia_nuevo(beandatos.getNomb_arde())) || (getRecordMode().equals("EDIT") && objDependenciaDAO.validate_nameDependencia_editar(beandatos.getCodi_arde(), beandatos.getNomb_arde()))) {
                Messagebox.show("Dentro del IF", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                if ((getRecordMode().equals("NEW") && objDependenciaDAO.validate_abrevDependencia_nuevo(beandatos.getAbre_arde())) || (getRecordMode().equals("EDIT") && objDependenciaDAO.validate_abrevDependencia_editar(beandatos.getCodi_arde(), beandatos.getAbre_arde()))) {
                    DependenciaTO objDependenciaTO = new DependenciaTO();
                    objDependenciaTO.setCodi_arde(beandatos.getCodi_arde());
                    objDependenciaTO.setNomb_arde(beandatos.getNomb_arde());
                    objDependenciaTO.setJera_arde(beandatos.getJera_arde());
                    objDependenciaTO.setAbre_arde(beandatos.getAbre_arde());
                    objDependenciaTO.setCodi_enti(curEntidad.x);

                    Map args = new HashMap();
                    args.put("objDependencia", objDependenciaTO);
                    args.put("accion", this.recordMode);
                    if (this.recordMode.equals("EDIT")) {
                        Messagebox.show("La dependencia fue actualizada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                    } else {
                        if (this.recordMode.equals("NEW")) {
                            Messagebox.show("La dependencia fue registrada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                        }
                    }
                    BindUtils.postGlobalCommand(null, null, "updateUsuarioList", args);
                    win.detach();
                } else {
                    Messagebox.show("El nombre de la abreviatura ingresado ya Existe, eliga otro", "Mantenimiento de Dependencias", Messagebox.OK, Messagebox.ERROR);
                }

            } else {
                Messagebox.show("El nombre de la dependecia ingresado ya Existe, eliga otro", "Mantenimiento de Dependencias", Messagebox.OK, Messagebox.ERROR);
            }
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public List<Pair<Integer, String>> getListEntidad() {
        return listEntidad;
    }

    public void setListEntidad(List<Pair<Integer, String>> listEntidad) {
        this.listEntidad = listEntidad;
    }

    public Pair<Integer, String> getCurEntidad() {
        return curEntidad;
    }

    public void setCurEntidad(Pair<Integer, String> curEntidad) {
        this.curEntidad = curEntidad;
    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "allDependencias", null);
    }

}
