/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.EnviaHistorial;
import com.marjuv.entity.ExpedienteCliente;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Crestaurarexpediente_man {

    @Wire("#restaurarexpediente_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private EnviaHistorial curEnvia;
    private String curAObservacion;
    private String curNObservacion;
    private List<ExpedienteCliente> listVinculados;
    
    public List<ExpedienteCliente> getListVinculados() {
        return listVinculados;
    }

    public void setListVinculados(List<ExpedienteCliente> listVinculados) {
        this.listVinculados = listVinculados;
    }

    public int get_total_vinculados() {
        return listVinculados.size();
    }
    
    @Command
    public void ver_expe() {
        Map parametros = new HashMap();
        parametros.put("list", listVinculados);
        Executions.createComponents("ZonaCompartida/win_visualizar_cargo.zul", null, parametros);
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1,
            @ExecutionArgParam("curAEnvia") EnviaHistorial c2)
            throws CloneNotSupportedException, Exception {
        Expediente_dao dao_exp = new Expediente_dao();
        setListVinculados(dao_exp.get_expediente_vinculados_total(c1.getEXPNUM(), c1.getDESTINO()));
        dao_exp.close();
        Selectors.wireComponents(view, this, false);
        curExpediente = c1;
        curEnvia = c2;
        setCurNObservacion("");
        setCurAObservacion(curExpediente.getOBSERVACION());
        win.setTitle("Restaurando Expediente " + curEnvia.getNombEstado() + " N° : " + curExpediente.getEXPNUM());
        setCurAObservacion(curExpediente.getOBSERVACION());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (!curNObservacion.isEmpty()) {
            Messagebox.show("Se restaurara el expediente  " + curExpediente.getEXPNUM()+ ((listVinculados!=null && !listVinculados.isEmpty())?" y sus vinculaciones":"")
                    + "\n ¿Desea continuar?", "Restaurar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                public void onEvent(Event e) throws SQLException {
                    if (Messagebox.ON_YES.equals(e.getName())) {
                        Map args = new HashMap();
                        args.put("curNObservacion", curNObservacion);
                        args.put("curExpediente", curExpediente);
                        args.put("curAEnvia", curEnvia);
                        args.put("listVinculados", listVinculados);
                        BindUtils.postGlobalCommand(null, null, "updateRestaurarExpediente", args);
                        win.detach();
                    }
                }
            });

        } else {
            Messagebox.show("Debe de escribir un motivo de Restauracion",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    @Command
    public void cancelar() {
        win.detach();
    }

    public String getCurNObservacion() {
        return curNObservacion;
    }

    public void setCurNObservacion(String curNObservacion) {
        this.curNObservacion = curNObservacion;
    }

    public String getCurAObservacion() {
        return curAObservacion;
    }

    public void setCurAObservacion(String curAObservacion) {
        this.curAObservacion = curAObservacion;
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public EnviaHistorial getCurEnvia() {
        return curEnvia;
    }

    public void setCurEnvia(EnviaHistorial curEnvia) {
        this.curEnvia = curEnvia;
    }

}
