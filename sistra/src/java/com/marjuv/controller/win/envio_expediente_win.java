package com.marjuv.controller.win;

import com.marjuv.dao.envio_expediente_dao;
import com.marjuv.entity.envio_expediente;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class envio_expediente_win {

    private List<envio_expediente> lst_envio_expediente;
    private envio_expediente cur_envio_expediente;
    @WireVariable
    private Session _sess;

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
    }

    public List<envio_expediente> getLst_envio_expediente() {
        try {
            envio_expediente_dao obj_envio_expediente_dao = new envio_expediente_dao();
            lst_envio_expediente = obj_envio_expediente_dao.get_envios_expediente();
            obj_envio_expediente_dao.close();
        } catch (Exception ex) {
        }
        return lst_envio_expediente;
    }

    @Command
    public void nuevo_envio_expediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sEnvio_expediente", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_envio_expediente.zul", null, map);
    }

    @Command
    public void editThisEnvioExpediente() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sEnvio_expediente", this.cur_envio_expediente);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_envio_expediente.zul", null, map);
        } catch (Exception ex) {
        }
    }

    @GlobalCommand
    @NotifyChange("lst_envio_expediente")
    public void updateEnvioExpedienteList(@BindingParam("sEnvio_expediente") envio_expediente cur_envio_expediente,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            envio_expediente_dao obj_envio_expediente_dao = new envio_expediente_dao();
            if (recordMode.equals("NEW")) {
                obj_envio_expediente_dao.insert(cur_envio_expediente);
                obj_envio_expediente_dao.close();
            } else if (recordMode.equals("EDIT")) {
                obj_envio_expediente_dao.update(cur_envio_expediente);
                obj_envio_expediente_dao.close();
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisEnvioExpediente() {
        String str = "¿Está seguro de que desea eliminar el envio de expediente \""
                + cur_envio_expediente.getDesc_enex()+ "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        envio_expediente_dao obj_envio_expediente_dao = new envio_expediente_dao();
                        obj_envio_expediente_dao.delete(cur_envio_expediente.getCodi_enex());
                        BindUtils.postNotifyChange(null, null, envio_expediente_win.this, "lst_envio_expediente");
                    } catch (Exception ex) {
                        Logger.getLogger(envio_expediente_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_envio_expediente(List<envio_expediente> lst_envio_expediente) {
        this.lst_envio_expediente = lst_envio_expediente;
    }

    public envio_expediente getCur_envio_expediente() {
        return cur_envio_expediente;
    }

    public void setCur_envio_expediente(envio_expediente cur_envio_expediente) {
        this.cur_envio_expediente = cur_envio_expediente;
    }

}
