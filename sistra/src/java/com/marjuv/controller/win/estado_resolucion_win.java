package com.marjuv.controller.win;

import com.marjuv.dao.estado_resolucion_dao;
import com.marjuv.entity.estado_resolucion;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class estado_resolucion_win {

    private List<estado_resolucion> lst_estado_resolucion;
    private estado_resolucion cur_estado_resolucion;
    @WireVariable
    private Session _sess;

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
    }

    public List<estado_resolucion> getLst_estado_resolucion() {
        try {
            estado_resolucion_dao obj_estado_resolucion_dao = new estado_resolucion_dao();
            lst_estado_resolucion = obj_estado_resolucion_dao.get_estados_resolucion();
            obj_estado_resolucion_dao.close();
        } catch (Exception ex) {
        }
        return lst_estado_resolucion;
    }

    @Command
    public void nuevo_estado_resolucion() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sEstado_resolucion", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_estado_resolucion.zul", null, map);
    }

    @Command
    public void editThisEstadoResolucion() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sEstado_resolucion", this.cur_estado_resolucion);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_estado_resolucion.zul", null, map);
        } catch (Exception ex) {
        }
    }

    @GlobalCommand
    @NotifyChange("lst_estado_resolucion")
    public void updateEstadoResolucionList(@BindingParam("sEstado_resolucion") estado_resolucion cur_estado_resolucion,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            estado_resolucion_dao obj_estado_resolucion_dao = new estado_resolucion_dao();
            if (recordMode.equals("NEW")) {
                obj_estado_resolucion_dao.insert(cur_estado_resolucion);
                obj_estado_resolucion_dao.close();
            } else if (recordMode.equals("EDIT")) {
                obj_estado_resolucion_dao.update(cur_estado_resolucion);
                obj_estado_resolucion_dao.close();
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisEstadoResolucion() {
        String str = "¿Está seguro de que desea eliminar el estado de resolucion \""
                + cur_estado_resolucion.getNomb_esre()+ "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        estado_resolucion_dao obj_estado_resolucion_dao = new estado_resolucion_dao();
                        obj_estado_resolucion_dao.delete(cur_estado_resolucion.getCodi_esre());
                        BindUtils.postNotifyChange(null, null, estado_resolucion_win.this, "lst_estado_resolucion");
                    } catch (Exception ex) {
                        Logger.getLogger(estado_resolucion_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_estado_resolucion(List<estado_resolucion> lst_estado_resolucion) {
        this.lst_estado_resolucion = lst_estado_resolucion;
    }

    public estado_resolucion getCur_estado_resolucion() {
        return cur_estado_resolucion;
    }

    public void setCur_estado_resolucion(estado_resolucion cur_estado_resolucion) {
        this.cur_estado_resolucion = cur_estado_resolucion;
    }

}
