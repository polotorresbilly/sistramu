package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanDestinoExpe;
import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.beans_view.BeanRequisitoMan;
import static com.marjuv.controller.win.main.generarReporte;
import com.marjuv.dao.Cargo_dao;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.representante_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.administracion;
import com.marjuv.entity.expediente;
import com.marjuv.entity.historial_expediente;
import com.marjuv.entity.representante;
import com.marjuv.entity.requisito_procedimiento;
import com.marjuv.entity.usuario;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class expediente_win {

    @Wire("#expediente_win")
    private Window win;
    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> list_repre, list_repre2, listTipoDocumento, listAsuntoExpediente, listDependencia, listCargo;
    private Pair<Integer, String> cur_repre, cur_repre2, curTipoDocumento, curAsuntoExpediente, curDependencia, curCargo;

    private List<Pair<Integer, String>> listUsuarios;
    private Pair<Integer, String> curlistUsuarios;

    private List<BeanRequisitoMan> lst_req;

    private List<BeanDestinoExpe> lst_destino_exp;
    private BeanDestinoExpe cur_destino_exp, destino_block;

    private String recordMode;

    @Wire
    private Button btnNew;
    @Wire
    private Button btnEdit;
    @Wire
    private Button btnSave;
    @Wire
    private Button btnSearch;
    @Wire
    private Button btnCancel;
    @Wire
    private Button btn_proc;
    @Wire
    private Button btnFUT;
    @Wire
    private Button btn_cont_new;
    @Wire
    private Button btn_enti_new;
    @Wire
    private Button btn_repr_new;
    @Wire
    private Button btn_repr_new2;
    @Wire
    private Button btnAddProc;
    @Wire
    private Button btnVerExp;

    // DATOS GENERALES
    @Wire()
    private Textbox nume_expe;
    @Wire()
    private Textbox valx_expe;
    @Wire()
    private Textbox fech_expe;
    @Wire()
    private Textbox feca_expe;
    @Wire()
    private Textbox codi_usua;
    @Wire()
    private Combobox codi_tiex;
    @Wire()
    private Textbox refe_expe;
    @Wire()
    private Textbox codi_proc;
    @Wire()
    private Textbox nomb_proc;
    @Wire()
    private Textbox reci_expe;
    @Wire()
    private Combobox codi_asex;
    @Wire()
    private Textbox espe_expe;
    @Wire()
    private Textbox nufo_expe;
    @Wire()
    private Textbox nudo_expe;
    @Wire()
    private Textbox txtExpAdj;

    // REMITENTE
    @Wire()
    private Radio re0;
    @Wire()
    private Radio re1;
    @Wire()
    private Radio re2;
    @Wire()
    private Grid muni_grid;
    @Wire()
    private Grid cont_grid;
    @Wire()
    private Grid enti_grid;

    // CONTRIBUYENTE
    @Wire()
    private Textbox cost_proc;
    @Wire()
    private Textbox codi_cont;
    @Wire()
    private Textbox nomb_cont;
    @Wire()
    private Textbox dire_cont;
    @Wire()
    private Textbox regio_cont;
    @Wire()
    private Textbox prov_cont;
    @Wire()
    private Textbox dist_cont;
    @Wire()
    private Textbox dni_cont;
    @Wire()
    private Textbox emai_cont;
    @Wire()
    private Button btn_cont;

    // ENTIDAD    
    @Wire()
    private Textbox codi_enti;
    @Wire()
    private Textbox nomb_enti;
    @Wire()
    private Textbox dire_enti;
    @Wire()
    private Button btn_enti;

    // DEPENDENCIA
    @Wire()
    private Combobox codi_depe_muni;
    @Wire()
    private Combobox codi_carg_muni;
    @Wire()
    private Combobox codi_reps_muni;
    @Wire()
    private Combobox codi_reps_enti;

    @Wire
    private Button btnAgregarDes;

    @Wire
    private Tabbox tb;
    @Wire
    private Tabpanel tbl_dat;
    @Wire
    private Tabpanel tbl_rem;
    @Wire
    private Tabpanel tbl_des;
    @Wire
    private Textbox txtDias;

    @Wire
    private Listbox lstbox_des;

    private boolean sw_quitar = false;

    int tipo_registro;

    int dias_caducidad_inconforme;

    String numero_expediente_adjunto;
    String numero_expediente;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);

    }

    @NotifyChange("*")
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        loadDataControls();
        lst_destino_exp = new ArrayList<BeanDestinoExpe>();
        destino_block = new BeanDestinoExpe();
        //codi_tiex.setFocus(true);
    }

    @Command
    public void addContribuyente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curContribuyente", null);
        map.put("accion", "NEW");
        Executions.createComponents("/mantenimiento/man/contribuyente_man.zul", null, map);
    }

    @Command
    public void addEntidad() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curEntidad", null);
        map.put("accion", "NEW");
        Executions.createComponents("/mantenimiento/man/entidad_man.zul", null, map);
    }

    @Command
    public void nuevo_representante() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRepre", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_representante.zul", null, map);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void updateRepreList(@BindingParam("sRepre") representante cur_representante,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            representante_dao obj_representante_dao = new representante_dao();
            if (recordMode.equals("NEW")) {
                obj_representante_dao.insert(cur_representante);
                Messagebox.show("El representante fue registrado");

                cur_representante = obj_representante_dao.get_representante(cur_representante.getNoms_reps());

                if (re0.isChecked()) {
                    if (curCargo != null) {
                        list_repre = obj_representante_dao.get_select(curCargo.x, 2);
                        if (list_repre.size() > 0) {
                            cur_repre = list_repre.get(0);
                        } else {
                            cur_repre = null;
                        }
                    } else {
                        cur_repre = null;
                    }
                } else if (re2.isChecked()) {
                    if (!codi_enti.getValue().isEmpty()) {
                        list_repre2 = obj_representante_dao.get_select(Integer.parseInt(codi_enti.getValue()), 1);
                        if (list_repre2.size() > 0) {
                            cur_repre2 = list_repre2.get(0);
                        } else {
                            cur_repre2 = null;
                        }
                    } else {
                        cur_repre2 = null;
                    }
                }
                obj_representante_dao.close();
            }
        }
    }

    @GlobalCommand
    public void updateListContribuyente(@BindingParam("curContribuyente") Contribuyente contribuyente, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                Contribuyente_dao dao = new Contribuyente_dao();
                dao.insert_contribuyente(contribuyente);
                Messagebox.show("El contribuyente fue registrado");

                contribuyente = dao.get_contribuyente(contribuyente);
                dao.close();

                this.codi_cont.setValue(String.valueOf(contribuyente.getCodi_cont()));
                this.nomb_cont.setValue(contribuyente.getNomb_cont() + " " + contribuyente.getApel_cont());
                this.dni_cont.setValue(String.valueOf(contribuyente.getDni_cont()));
                this.dire_cont.setValue(contribuyente.getDire_cont());
                this.emai_cont.setValue(contribuyente.getEmai_cont());
                this.regio_cont.setValue(contribuyente.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
                this.prov_cont.setValue(contribuyente.getObj_dist().getProvincia().getNomb_prov());
                this.dist_cont.setValue(contribuyente.getObj_dist().getNomb_dist());
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Contribuyentes", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @GlobalCommand
    public void updateListEntidad(@BindingParam("curEntidad") Entidad entidad, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                Entidad_dao dao = new Entidad_dao();
                dao.insert_entidad(entidad);
                Messagebox.show("La entidad fue registrada");

                entidad = dao.get_entidad(entidad);
                dao.close();

                this.codi_enti.setValue(String.valueOf(entidad.getCodi_enti()));
                this.nomb_enti.setValue(entidad.getNomb_enti());
                this.dire_enti.setValue(entidad.getDire_enti());
                representante_dao obj_representante_dao = new representante_dao();
                list_repre2 = obj_representante_dao.get_select(entidad.getCodi_enti(), 1);
                obj_representante_dao.close();
                if (list_repre2.size() > 0) {
                    cur_repre2 = list_repre2.get(0);
                } else {
                    cur_repre2 = null;
                }
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Entidades", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Centidad_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @NotifyChange("*")
    @Command

    public void quitar() {
        if (sw_quitar) {
            /*Se quito la validación para que en el caso que el contribuyente no cumpla con los requisitos el Jefe de 
             Tramite Documentario pueda direccionarlo hacia su misma dependencia hasta que el contribuyente
             cumpla con todos los requisitos*/
//            if (cur_destino_exp.getCodi_carg() == 0 || (cur_destino_exp.getCodi_carg() != destino_block.getCodi_carg())) {
//            if (cur_destino_exp.getCodi_carg() == 0) {
            for (int i = 0; i < lst_destino_exp.size(); i++) {
                if (lst_destino_exp.get(i).getCodi_carg() == cur_destino_exp.getCodi_carg()
                        && lst_destino_exp.get(i).getCodi_cont() == cur_destino_exp.getCodi_cont()
                        && lst_destino_exp.get(i).getCodi_enti() == cur_destino_exp.getCodi_enti()) {
                    lst_destino_exp.remove(lst_destino_exp.get(i));
                    break;
                }
            }
//            } else {
//                Messagebox.show("El destinatario con cargo " + cur_destino_exp.getNomb_carg() + " está referenciado por la selección de procedimiento " + nomb_proc.getValue());
//            }
        }
    }

    @Command
    public void add_destino() {

        Executions.createComponents("proceso/win/add_destinatario_expediente.zul", null, null);
    }

    @Command
    public void searchContribuyente() {
        Executions.createComponents("proceso/win/search_contribuyente_rem.zul", null, null);
    }

    @Command
    public void searchEntidad() {
        Executions.createComponents("proceso/win/search_entidad_rem.zul", null, null);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setContribuyenteRem(
            @BindingParam("codi_cont") String codi_cont,
            @BindingParam("nomb_cont") String nomb_cont,
            @BindingParam("dni_cont") String dni_cont,
            @BindingParam("dire_cont") String dire_cont,
            @BindingParam("emai_cont") String emai_cont,
            @BindingParam("nomb_dist") String nomb_dist,
            @BindingParam("nomb_prov") String nomb_prov,
            @BindingParam("nomb_depa") String nomb_depa
    ) {
        this.codi_cont.setValue(codi_cont);
        this.nomb_cont.setValue(nomb_cont);
        this.dni_cont.setValue(dni_cont);
        this.dire_cont.setValue(dire_cont);
        this.emai_cont.setValue(emai_cont);
        this.regio_cont.setValue(nomb_depa);
        this.prov_cont.setValue(nomb_prov);
        this.dist_cont.setValue(nomb_dist);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setEntidadRem(
            @BindingParam("codi_enti") String codi_enti,
            @BindingParam("nomb_enti") String nomb_enti,
            @BindingParam("dire_enti") String dire_enti
    ) throws SQLException {
        this.codi_enti.setValue(codi_enti);
        this.nomb_enti.setValue(nomb_enti);
        this.dire_enti.setValue(dire_enti);
        representante_dao obj_representante_dao = new representante_dao();
        list_repre2 = obj_representante_dao.get_select(Integer.parseInt(codi_enti), 1);
        obj_representante_dao.close();
        if (list_repre2.size() > 0) {
            cur_repre2 = list_repre2.get(0);
        } else {
            cur_repre2 = null;
        }
    }

    @GlobalCommand
    public void setRequisitos(
            @BindingParam("requisito") List<BeanRequisitoMan> requisito
    ) {
        this.lst_req = requisito;

    }

    public void cargar_expediente(String nume_expe2) {
        try {
//            Messagebox.show(nume_expe2);
            Expediente_dao objExpediente_dao = new Expediente_dao();
            BeanExpediente obj_expediente = objExpediente_dao.cargar_expediente(nume_expe2);
            Expediente_dao objExpedienteDAO_01 = new Expediente_dao();
            String Expediente_Adjunto = objExpedienteDAO_01.getExpediente_Adjunto(nume_expe2);
            objExpediente_dao.close();
            objExpedienteDAO_01.close();

            limpiar_campos();
//            Messagebox.show(Expediente_Adjunto);
            txtExpAdj.setValue(Expediente_Adjunto);
//            Messagebox.show(Expediente_Adjunto);
            lst_req = obj_expediente.getRequisitos();
            nume_expe.setValue(obj_expediente.getNume_expe());
            reci_expe.setValue(obj_expediente.getReci_expe());
            usuario_dao obj_usuario_dao = new usuario_dao();
            codi_usua.setValue(obj_usuario_dao.get_nume(obj_expediente.getCodi_usua()));
            obj_usuario_dao.close();
            fech_expe.setValue(obj_expediente.getFech_expe().toString());

            feca_expe.setValue(obj_expediente.getFeca_expe().toString());
            for (int i = 0; i < getListTipoDocumento().size(); i++) {
                if (getListTipoDocumento().get(i).getX() == obj_expediente.getCodi_tiex()) {
                    curTipoDocumento = getListTipoDocumento().get(i);
                    break;
                }
            }
            nudo_expe.setValue(obj_expediente.getNudo_expe());

            valx_expe.setValue(obj_expediente.getValx_expe());
            for (int i = 0; i < getListAsuntoExpediente().size(); i++) {
                if (getListAsuntoExpediente().get(i).getX() == obj_expediente.getCodi_asex()) {
                    curAsuntoExpediente = getListAsuntoExpediente().get(i);
                    break;
                }
            }
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            codi_proc.setValue(String.valueOf(obj_expediente.getCodi_proc()));
            nomb_proc.setValue(obj_procedimiento_expediente_dao.get_nomb_proc(obj_expediente.getCodi_proc()));
            cost_proc.setValue(obj_procedimiento_expediente_dao.get_cost_proc(obj_expediente.getCodi_proc()));

            obj_procedimiento_expediente_dao.close();

            nufo_expe.setValue(String.valueOf(obj_expediente.getNufo_expe()));
            espe_expe.setValue(obj_expediente.getEspe_expe());
            if (obj_expediente.getCodi_cont() != 0) {

                re1.setChecked(true);

                cont_grid.setVisible(true);
                enti_grid.setVisible(false);
                muni_grid.setVisible(false);

                Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
                Contribuyente obj_Contribuyente = obj_Contribuyente_dao.get_contribuyente(obj_expediente.getCodi_cont());
                codi_cont.setValue(String.valueOf(obj_expediente.getCodi_cont()));
                nomb_cont.setValue(obj_Contribuyente.getNomb_cont() + " " + obj_Contribuyente.getApel_cont());
                dire_cont.setValue(obj_Contribuyente.getDire_cont());
                regio_cont.setValue(obj_Contribuyente.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
                prov_cont.setValue(obj_Contribuyente.getObj_dist().getProvincia().getNomb_prov());
                dist_cont.setValue(obj_Contribuyente.getObj_dist().getNomb_dist());
                dni_cont.setValue(String.valueOf(obj_Contribuyente.getDni_cont()));
                emai_cont.setValue(obj_Contribuyente.getEmai_cont());
                obj_Contribuyente_dao.close();

            } else if (obj_expediente.getCodi_enti() != 0) {
                re2.setChecked(true);

                cont_grid.setVisible(false);
                enti_grid.setVisible(true);
                muni_grid.setVisible(false);

                Entidad_dao obj_Entidad_dao = new Entidad_dao();
                Entidad obj_Entidad = obj_Entidad_dao.get_entidad(obj_expediente.getCodi_enti());
                codi_enti.setValue(String.valueOf(obj_expediente.getCodi_enti()));
                nomb_enti.setValue(obj_Entidad.getNomb_enti());
                dire_enti.setValue(obj_Entidad.getDire_enti());
                obj_Entidad_dao.close();

                representante_dao obj_representante_dao = new representante_dao();
                list_repre2 = obj_representante_dao.get_select(obj_expediente.getCodi_enti(), 1);
                obj_representante_dao.close();
                if (list_repre2.size() > 0) {
                    cur_repre2 = list_repre2.get(0);
                } else {
                    cur_repre2 = null;
                }
            } else if (obj_expediente.getCodi_carg() != 0) {
                re0.setChecked(true);

                cont_grid.setVisible(false);
                enti_grid.setVisible(false);
                muni_grid.setVisible(true);

                cargos_dao obj_cargos_dao = new cargos_dao();
                BeanCargoMan obj_cargos = obj_cargos_dao.get_cargo(obj_expediente.getCodi_carg());
                Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
                setListDependencia(obj_Dependencia_dao.get_select(3));
                obj_Dependencia_dao.close();
                if (getListDependencia().size() > 0) {
                    curDependencia = getListDependencia().get(0);
                } else {
                    curDependencia = null;
                    //codi_depe_muni.setSelectedIndex(-1);
                }

                boolean sw_depe = false;
                for (int i = 0; i < getListDependencia().size(); i++) {
                    if (getListDependencia().get(i).getX() == obj_cargos.getCodi_arde()) {
                        curDependencia = getListDependencia().get(i);
                        sw_depe = true;
                        break;
                    }
                }
                if (sw_depe) {

                    setListCargo(obj_cargos_dao.get_select(curDependencia.x));
                    if (getListCargo().size() > 0) {
                        curCargo = getListCargo().get(0);
                    } else {
                        curCargo = new Pair<Integer, String>(-1, "");
                        //codi_carg_muni.setSelectedIndex(-1);
                    }

                    boolean sw_carg = false;
                    for (int i = 0; i < getListCargo().size(); i++) {
                        if (getListCargo().get(i).getX() == obj_cargos.getCodi_carg()) {
                            curCargo = getListCargo().get(i);
                            representante_dao obj_representante_dao = new representante_dao();
                            list_repre = obj_representante_dao.get_select(obj_cargos.getCodi_carg(), 2);
                            obj_representante_dao.close();
                            if (list_repre.size() > 0) {
                                cur_repre = list_repre.get(0);
                            } else {
                                cur_repre = null;
                            }

                            sw_carg = true;
                            break;
                        } else {
                            cur_repre = null;
                        }
                    }
                    if (!sw_carg) {
                        curCargo = null;
                        cur_repre = null;
                    }
                } else {
                    curDependencia = null;
                    cur_repre = null;
                }
                obj_cargos_dao.close();
            }

            BeanDestinoExpe obj_bean;

            for (int i = 0; i < obj_expediente.getHistorial_expediente().size(); i++) {
                obj_bean = new BeanDestinoExpe();
                obj_bean.setCodi_carg(obj_expediente.getHistorial_expediente().get(i).getCare_hist());
                obj_bean.setCodi_cont(obj_expediente.getHistorial_expediente().get(i).getCore_hist());
                obj_bean.setCodi_enti(obj_expediente.getHistorial_expediente().get(i).getEnre_hist());
                if (obj_bean.getCodi_enti() != 0) {
                    Entidad_dao obj_Entidad_dao3 = new Entidad_dao();
                    obj_bean.setNomb_carg("");
                    obj_bean.setNomb_arde("");
                    obj_bean.setNomb_enti(obj_Entidad_dao3.get_nomb(obj_bean.getCodi_enti()));
                    obj_bean.setNomb_cont("");
                    obj_Entidad_dao3.close();
                } else if (obj_bean.getCodi_cont() != 0) {
                    Contribuyente_dao obj_Contribuyente_dao2 = new Contribuyente_dao();
                    obj_bean.setNomb_carg("");
                    obj_bean.setNomb_arde("");
                    obj_bean.setNomb_enti("");
                    obj_bean.setNomb_cont(obj_Contribuyente_dao2.get_nomb(obj_bean.getCodi_cont()));
                    obj_Contribuyente_dao2.close();
                } else if (obj_bean.getCodi_carg() != 0) {
                    cargos_dao obj_cargos_dao3 = new cargos_dao();
                    Dependencia_dao obj_Dependencia_dao2 = new Dependencia_dao();
                    Entidad_dao obj_Entidad_dao2 = new Entidad_dao();
                    obj_bean.setNomb_carg(obj_cargos_dao3.get_nomb(obj_bean.getCodi_carg()));
                    obj_bean.setNomb_arde(obj_Dependencia_dao2.get_nomb(obj_bean.getCodi_carg()));
                    obj_bean.setNomb_enti(obj_Entidad_dao2.get_nomb_for_carg(obj_bean.getCodi_carg()));
                    obj_bean.setNomb_cont("");
                    obj_cargos_dao3.close();
                    obj_Dependencia_dao2.close();
                    obj_Entidad_dao2.close();
                }

                obj_bean.setCopias(obj_expediente.getHistorial_expediente().get(i).isCopi_hist());
                if (obj_bean.getCopias()) {
                    obj_bean.setCopias_text("Copia");
                } else {
                    obj_bean.setCopias_text("Original");
                }
                this.lst_destino_exp.add(obj_bean);
            }
            btnEdit.setDisabled(false);
        } catch (Exception e) {
            Messagebox.show(e.getMessage());
        }
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setExpediente(@BindingParam("numero_expediente") String Numero_Expediente) throws Exception {
        cargar_expediente(Numero_Expediente);
        btnEdit.setDisabled(false);
        btnFUT.setDisabled(false);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setExpedienteAdjunto(@BindingParam("numero_expediente") String Numero_Expediente) throws Exception {
        txtExpAdj.setText(Numero_Expediente);
    }

    String doc;

    @NotifyChange("*")
    @Command
    public void changeCheck() {
        doc = "";
        if (cur_destino_exp.getCopias()) {
            doc = "Original";
        } else {
            doc = "Copia";
        }
        Messagebox.show("El documento del destinatario seleccionado se cambiará a \"" + doc + "\""
                + "\n ¿Desea continuar?", "Documento", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            for (int i = 0; i < lst_destino_exp.size(); i++) {
                                if (lst_destino_exp.get(i).getCodi_carg() == cur_destino_exp.getCodi_carg()
                                && lst_destino_exp.get(i).getCodi_cont() == cur_destino_exp.getCodi_cont()
                                && lst_destino_exp.get(i).getCodi_enti() == cur_destino_exp.getCodi_enti()) {
                                    if (doc.equals("Copia")) {
                                        lst_destino_exp.get(i).setCopias(true);
                                        lst_destino_exp.get(i).setCopias_text("Copia");
                                    } else if (doc.equals("Original")) {
                                        for (int f = 0; f < lst_destino_exp.size(); f++) {
                                            if (lst_destino_exp.get(f).getCopias()) {
                                                lst_destino_exp.get(f).setCopias(true);
                                                lst_destino_exp.get(f).setCopias_text("Copia");
                                                break;
                                            }
                                        }
                                        lst_destino_exp.get(i).setCopias(false);
                                        lst_destino_exp.get(i).setCopias_text("Original");
                                    }
                                    BindUtils.postNotifyChange(null, null, expediente_win.this, "lst_destino_exp");
                                    break;
                                }
                            }
                        }
                    }
                });
    }

    private boolean sw;

    @GlobalCommand
    @NotifyChange("*")
    public void setDestinatario(
            @BindingParam("codi_carg") Integer codi_carg,
            @BindingParam("codi_cont") Integer codi_cont,
            @BindingParam("codi_enti") Integer codi_enti,
            @BindingParam("codigo_usuario") Integer codigo_usuario,
            @BindingParam("nomb_enti") String nomb_enti,
            @BindingParam("nombre_usuario") String nombre_usuario,
            @BindingParam("nomb_arde") String nomb_arde,
            @BindingParam("nomb_carg") String nomb_carg,
            @BindingParam("nomb_cont") String nomb_cont
    ) throws SQLException {

        this.sw = true;
        Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
        for (int i = 0; i < lst_destino_exp.size(); i++) {
            if (lst_destino_exp.get(i).getCodi_carg() == codi_carg
                    && lst_destino_exp.get(i).getCodi_cont() == codi_cont
                    && lst_destino_exp.get(i).getCodi_enti() == codi_enti) {
                Messagebox.show("El destinatario ya existe");
                this.sw = false;
                break;
            } else if (lst_destino_exp.get(i).getCodi_carg() != 0
                    && obj_Dependencia_dao.get_nomb(lst_destino_exp.get(i).getCodi_carg()).equals(
                            obj_Dependencia_dao.get_nomb(codi_carg))) {
                Messagebox.show("El área de destinatario ya existe");
                this.sw = false;
                break;
            }
        }

        if (this.sw) {
            BeanDestinoExpe obj_bean = new BeanDestinoExpe();
            obj_bean.setCodi_carg(codi_carg);
            obj_bean.setCodi_cont(codi_cont);
            obj_bean.setCodi_enti(codi_enti);
            obj_bean.setNomb_carg(nomb_carg);
            obj_bean.setNomb_arde(nomb_arde);
            obj_bean.setNomb_enti(nomb_enti);
            obj_bean.setNomb_cont(nomb_cont);
            obj_bean.setCodigo_usuario(codigo_usuario);
            obj_bean.setNombre_usuario(nombre_usuario);
            obj_bean.setCopias(false);
            obj_bean.setCopias_text("Original");
            for (int f = 0; f < lst_destino_exp.size(); f++) {
                if (lst_destino_exp.get(f).getCopias()) {
                    lst_destino_exp.get(f).setCopias(true);
                    lst_destino_exp.get(f).setCopias_text("Copia");
                    break;
                }
            }
            this.lst_destino_exp.add(obj_bean);
        }

    }

    @NotifyChange("*")
    @Command
    public void changeRemitente() {
        try {

            if (re1.isChecked()) {
                cont_grid.setVisible(true);
                enti_grid.setVisible(false);
                muni_grid.setVisible(false);

                limpiar_muni();
                limpiar_enti();
            } else if (re2.isChecked()) {
                cont_grid.setVisible(false);
                muni_grid.setVisible(false);
                enti_grid.setVisible(true);
                limpiar_cont();
                limpiar_muni();
            } else if (re0.isChecked()) {
                cont_grid.setVisible(false);
                enti_grid.setVisible(false);
                muni_grid.setVisible(true);
                limpiar_cont();
                limpiar_enti();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @NotifyChange("*")
    @Command
    public void openProc() {
        try {

            if (codi_proc.getValue().isEmpty()) {
                Messagebox.show("Debe agregar un procedimiento.");
            } else {
                final HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("sw", "TRUE");
                map.put("lst_req", lst_req);
                map.put("codi_proc", Integer.parseInt(codi_proc.getValue()));
                Executions.createComponents("proceso/win/win_req_proce.zul", null, map);
            }

        } catch (Exception e) {
            Messagebox.show("Error al visualizar Requisitos: " + e.getMessage());
        }

    }

    @NotifyChange("*")
    @Command
    public void openProc2() throws Exception {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("codi_asex", curAsuntoExpediente.x);
        Executions.createComponents("proceso/win/add_procedimiento_expe.zul", null, map);

    }

    @NotifyChange("*")
    @GlobalCommand
    public void setProcedimiento_expe(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc, @BindingParam("dias_proc") String dias_proc) throws Exception {

        /*Mostrar el codigo del Procedimiento Agregado*/
        this.codi_proc.setValue(String.valueOf(codi_proc));

        /*Mostrar el nombre del Procedimiento Agregado*/
        this.nomb_proc.setValue(nomb_proc);

        /*Mostrar los días hábiles del procedimientos*/
        this.txtDias.setValue(dias_proc);
        /*Se crea un Map para enviar como parametro el codigo del Procedimiento a la Interfaz de Requerimientos*/
        final HashMap<String, Object> map = new HashMap<String, Object>();

        /*Enviamos los parametros*/
        map.put("sw", "FALSE");
        map.put("codi_proc", codi_proc);

        /*Se llama a la interfaz y envia parametros*/
        Executions.createComponents("proceso/win/win_req_proce.zul", null, map);

        /*Se crea el Objeto Procedimiento DAO*/
        procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();

        /*Mostrar el costo del Procedimiento según el codigo de Procedimiento que se le envie*/
        cost_proc.setValue(obj_procedimiento_expediente_dao.get_cost_proc(codi_proc));
        valx_expe.setValue(obj_procedimiento_expediente_dao.get_cost_proc(codi_proc));

        /*Se crea el Objeto Bean Destino Expediente*/
        BeanDestinoExpe obj_bean = new BeanDestinoExpe();

        /*Se crea una variable para almacenar el Codigo del Cargo*/
        int codi_carg = obj_procedimiento_expediente_dao.get_carg_proc(codi_proc);
        obj_procedimiento_expediente_dao.close();

        /*Se almacena los datos al Bean Destino Expediente - (Codigo Cargo,Codigo Contribuyente, Codigo Entidad)*/
        obj_bean.setCodi_carg(codi_carg);
        obj_bean.setCodi_cont(0);
        obj_bean.setCodi_enti(0);

        /*Crear el Objeto de Cargos DAO*/
        cargos_dao obj_cargos_dao = new cargos_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre del Cargo*/
        obj_bean.setNomb_carg(obj_cargos_dao.get_nomb(codi_carg));
        obj_cargos_dao.close();

        /*Crear el Objeto Dependencia DAO*/
        Dependencia_dao objDependencia_dao = new Dependencia_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre del Area*/
        obj_bean.setNomb_arde(objDependencia_dao.get_nomb(codi_carg));
        objDependencia_dao.close();

        /*Crear el Objeto Entidad DAO*/
        Entidad_dao obj_Entidad_dao = new Entidad_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre Entidad*/
        obj_bean.setNomb_enti(obj_Entidad_dao.get_nomb_for_carg(codi_carg));
        obj_bean.setNomb_cont("");
        obj_Entidad_dao.close();

        /*Recorrer la Lista de destino Expediente en el caso que exista un destinatario agregado*/
        for (int f = 0; f < lst_destino_exp.size(); f++) {

            /*Verifica si es falso y le asigna TRUE para que le establesca que sea Copia*/
            if (lst_destino_exp.get(f).getCopias()) {
                lst_destino_exp.get(f).setCopias(true);
                lst_destino_exp.get(f).setCopias_text("Copia");
                break;
            }
        }

        /*Se almacena los datos al Bean Destino Expediente - Estado del Expediente (Original) si no existe ningun Destinatario*/
        obj_bean.setCopias(false);
        obj_bean.setCopias_text("Original");

        /*Recorre la lista Destino Expediente para verificar que no se repita el destinatario*/
        for (int i = 0; i < lst_destino_exp.size(); i++) {

            /*Si los codigos de los cargos coinciden los remueve antes de agregarlo*/
            if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()) {
                lst_destino_exp.remove(lst_destino_exp.get(i));
                break;
            }
        }

        /*Almacena el destinatario a un contenedor temporal para que se pueda verificar que no se repita*/
        destino_block = obj_bean;

        /*Se vuelve a verificar si existe el mismo destinatario que se desea agregar para removerlo antes*/
        /*En este caso verifica entre los 3 tipos de destinatarios - ENTIDAD,CONTRIBUYENTE Y CARGO*/
        for (int i = 0; i < lst_destino_exp.size(); i++) {
            if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()
                    && lst_destino_exp.get(i).getCodi_enti() == destino_block.getCodi_enti()
                    && lst_destino_exp.get(i).getCodi_cont() == destino_block.getCodi_cont()) {
                lst_destino_exp.remove(lst_destino_exp.get(i));
                break;
            }
        }

        /*Se agrega el destinatario a la lista*/
        lst_destino_exp.add(destino_block);
    }

    @NotifyChange("*")
    @GlobalCommand
    public void setProcedimiento_expe2(
            @BindingParam("codi_proc") int codi_proc,
            @BindingParam("nomb_proc") String nomb_proc,
            @BindingParam("estado") int estado,
            @BindingParam("tipo") int tipo) throws Exception {

        this.tipo_registro = tipo;

        /*Mostrar el codigo del Procedimiento Agregado*/
        this.codi_proc.setValue(String.valueOf(codi_proc));

        /*Mostrar el nombre del Procedimiento Agregado*/
        this.nomb_proc.setValue(nomb_proc);

        /*Se crea un Map para enviar como parametro el codigo del Procedimiento a la Interfaz de Requerimientos*/
        final HashMap<String, Object> map = new HashMap<String, Object>();

        /*Enviamos los parametros*/
        map.put("sw", "FALSE");
        map.put("codi_proc", codi_proc);

        /*Se llama a la interfaz y envia parametros*/
        if (estado == 1) {
            //Messagebox.show("El expediente cumple con los requisitos.");
        } else {
            Executions.createComponents("proceso/win/win_req_proce.zul", null, map);
        }


        /*Se crea el Objeto Procedimiento DAO*/
        procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();

        /*Mostrar el costo del Procedimiento según el codigo de Procedimiento que se le envie*/
        cost_proc.setValue(obj_procedimiento_expediente_dao.get_cost_proc(codi_proc));
        valx_expe.setValue(obj_procedimiento_expediente_dao.get_cost_proc(codi_proc));

        /*Se crea el Objeto Bean Destino Expediente*/
        BeanDestinoExpe obj_bean = new BeanDestinoExpe();

        /*Se crea una variable para almacenar el Codigo del Cargo*/
        int codi_carg = obj_procedimiento_expediente_dao.get_carg_proc(codi_proc);
        obj_procedimiento_expediente_dao.close();

        /*Se almacena los datos al Bean Destino Expediente - (Codigo Cargo,Codigo Contribuyente, Codigo Entidad)*/
        obj_bean.setCodi_carg(codi_carg);
        obj_bean.setCodi_cont(0);
        obj_bean.setCodi_enti(0);

        /*Crear el Objeto de Cargos DAO*/
        cargos_dao obj_cargos_dao = new cargos_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre del Cargo*/
        obj_bean.setNomb_carg(obj_cargos_dao.get_nomb(codi_carg));
        obj_cargos_dao.close();

        /*Crear el Objeto Dependencia DAO*/
        Dependencia_dao objDependencia_dao = new Dependencia_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre del Area*/
        obj_bean.setNomb_arde(objDependencia_dao.get_nomb(codi_carg));
        objDependencia_dao.close();

        /*Crear el Objeto Entidad DAO*/
        Entidad_dao obj_Entidad_dao = new Entidad_dao();

        /*Se almacena los datos al Bean Destino Expediente - Nombre Entidad*/
        obj_bean.setNomb_enti(obj_Entidad_dao.get_nomb_for_carg(codi_carg));
        obj_bean.setNomb_cont("");
        obj_Entidad_dao.close();

        /*Recorrer la Lista de destino Expediente en el caso que exista un destinatario agregado*/
        for (int f = 0; f < lst_destino_exp.size(); f++) {

            /*Verifica si es falso y le asigna TRUE para que le establesca que sea Copia*/
            if (lst_destino_exp.get(f).getCopias()) {
                lst_destino_exp.get(f).setCopias(true);
                lst_destino_exp.get(f).setCopias_text("Copia");
                break;
            }
        }

        /*Se almacena los datos al Bean Destino Expediente - Estado del Expediente (Original) si no existe ningun Destinatario*/
        obj_bean.setCopias(false);
        obj_bean.setCopias_text("Original");

        /*Recorre la lista Destino Expediente para verificar que no se repita el destinatario*/
        for (int i = 0; i < lst_destino_exp.size(); i++) {

            /*Si los codigos de los cargos coinciden los remueve antes de agregarlo*/
            if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()) {
                lst_destino_exp.remove(lst_destino_exp.get(i));
                break;
            }
        }

        /*Almacena el destinatario a un contenedor temporal para que se pueda verificar que no se repita*/
        destino_block = obj_bean;

        /*Se vuelve a verificar si existe el mismo destinatario que se desea agregar para removerlo antes*/
        /*En este caso verifica entre los 3 tipos de destinatarios - ENTIDAD,CONTRIBUYENTE Y CARGO*/
        for (int i = 0; i < lst_destino_exp.size(); i++) {
            if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()
                    && lst_destino_exp.get(i).getCodi_enti() == destino_block.getCodi_enti()
                    && lst_destino_exp.get(i).getCodi_cont() == destino_block.getCodi_cont()) {
                lst_destino_exp.remove(lst_destino_exp.get(i));
                break;
            }
        }

        /*Se agrega el destinatario a la lista*/
        lst_destino_exp.add(destino_block);
    }

    @NotifyChange("*")
    @GlobalCommand
    public void setDestinatarioTramite(
            @BindingParam("codi_arde") int codi_arde,
            @BindingParam("nomb_arde") String nomb_arde,
            @BindingParam("nomb_carg") String nomb_carg,
            @BindingParam("nomb_enti") String nomb_enti,
            @BindingParam("codi_carg") int codi_carg,
            @BindingParam("codi_enti") int codi_enti,
            @BindingParam("tipo") int tipo,
            @BindingParam("dias") int dias) throws Exception {
        try {

            this.tipo_registro = tipo;
            this.dias_caducidad_inconforme = dias;
            /*Se crea el Objeto Bean Destino Expediente*/
            BeanDestinoExpe obj_bean = new BeanDestinoExpe();
            obj_bean.setCodi_carg(codi_carg);
            obj_bean.setCodi_cont(0);
            obj_bean.setCodi_enti(codi_enti);
            obj_bean.setCopias(false);
            obj_bean.setCopias_text("Original");
            obj_bean.setNomb_arde(nomb_arde);
            obj_bean.setNomb_carg(nomb_carg);
            obj_bean.setNomb_cont("");
            obj_bean.setNomb_enti(nomb_enti);

            /*Recorrer la Lista de destino Expediente en el caso que exista un destinatario agregado*/
            for (int f = 0; f < lst_destino_exp.size(); f++) {

                /*Verifica si es falso y le asigna TRUE para que le establesca que sea Copia*/
                if (lst_destino_exp.get(f).getCopias()) {
                    lst_destino_exp.get(f).setCopias(true);
                    lst_destino_exp.get(f).setCopias_text("Copia");
                    break;
                }
            }

            /*Recorre la lista Destino Expediente para verificar que no se repita el destinatario*/
            for (int i = 0; i < lst_destino_exp.size(); i++) {

                /*Si los codigos de los cargos coinciden los remueve antes de agregarlo*/
                if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()) {
                    lst_destino_exp.remove(lst_destino_exp.get(i));
                    break;
                }
            }

            /*Almacena el destinatario a un contenedor temporal para que se pueda verificar que no se repita*/
            destino_block = obj_bean;

            /*Se vuelve a verificar si existe el mismo destinatario que se desea agregar para removerlo antes*/
            /*En este caso verifica entre los 3 tipos de destinatarios - ENTIDAD,CONTRIBUYENTE Y CARGO*/
            for (int i = 0; i < lst_destino_exp.size(); i++) {
                if (lst_destino_exp.get(i).getCodi_carg() == destino_block.getCodi_carg()
                        && lst_destino_exp.get(i).getCodi_enti() == destino_block.getCodi_enti()
                        && lst_destino_exp.get(i).getCodi_cont() == destino_block.getCodi_cont()) {
                    lst_destino_exp.remove(lst_destino_exp.get(i));
                    break;
                }
            }

            /*Se agrega el destinatario a la lista*/
            lst_destino_exp.add(destino_block);

        } catch (Exception e) {
            Messagebox.show("Error al asignar el destinatario por defecto:" + e.getMessage());
        }
    }

    @NotifyChange("*")
    @Command
    public void updateInputNumDoc() throws SQLException {

        if (curTipoDocumento.x > 0) {
            if (curTipoDocumento.y.equals("SOLICITUD")) {

                nudo_expe.setDisabled(true);
            } else {
                nudo_expe.setDisabled(false);
                nudo_expe.setText("");
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void updateDependencia() throws SQLException {

//        Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
//        setListDependencia(obj_Dependencia_dao.get_select(3));
//        obj_Dependencia_dao.close();
//        if (getListDependencia().size() > 0) {
//            curDependencia = getListDependencia().get(0);
//            
//        } else {
//            curDependencia = null;
//        }
        if (curDependencia.x != -1) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            setListCargo(obj_cargos_dao.get_select(curDependencia.x));
            obj_cargos_dao.close();
            if (getListCargo().size() > 0) {
                curCargo = getListCargo().get(0);
                representante_dao obj_representante_dao = new representante_dao();
                setList_repre(obj_representante_dao.get_select(curCargo.x, 2));
                obj_representante_dao.close();
                if (getList_repre().size() > 0) {
                    setCur_repre(getList_repre().get(0));
                    //Messagebox.show("getList_repre: " + getList_repre().get(0) + "setCur_repre: " + getCur_repre().getY());
                } else {
                    cur_repre = null;
                }
            } else {
                curCargo = null;
                cur_repre = null;
            }
        }

    }

    @NotifyChange("*")
    @Command
    public void updateCargo() throws SQLException {
//        if(curDependencia.x >0){
//            cargos_dao obj_cargos_dao = new cargos_dao();
//            
//            listCargo = obj_cargos_dao.get_select(curDependencia.x);
//            obj_cargos_dao.close();
//            if(listCargo.size() > 0){
//                curCargo = listCargo.get(0);
//            }else{
//                curCargo = null;
//                
//            }
//        }
        if (curDependencia.x != -1) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            setListCargo(obj_cargos_dao.get_select(curDependencia.x));
            obj_cargos_dao.close();
            if (getListCargo().size() > 0) {
                curCargo = getListCargo().get(0);
                representante_dao obj_representante_dao = new representante_dao();
                setList_repre(obj_representante_dao.get_select(curCargo.x, 2));
                obj_representante_dao.close();
                if (getList_repre().size() > 0) {
                    setCur_repre(getList_repre().get(0));
                } else {
                    cur_repre = null;
                }
            } else {
                curCargo = null;
                cur_repre = null;
            }
        } else {
            curCargo = null;
            cur_repre = null;
        }
    }

    @NotifyChange("*")
    @Command
    public void updateRepre() throws SQLException {

        if (curCargo.x > 0) {
            
//            usuario_dao ObjUsuarioDAO = new usuario_dao();
//            setListUsuarios(ObjUsuarioDAO.llenarComboUsuarios_Expedientes(curCargo.x));
//            setCurlistUsuarios(listUsuarios.get(0));
//            if(listUsuarios.size() > 0){
//                curlistUsuarios = listUsuarios.get(0);
//            }else{
//                curlistUsuarios = null;
//            }
            //Messagebox.show("Codigo Cargo: " + curCargo.x +"Nombre Cargo: " + curCargo.y);
            representante_dao obj_representante_dao = new representante_dao();
            list_repre = obj_representante_dao.get_select(curCargo.x, 2);
            obj_representante_dao.close();
            if (list_repre.size() > 0) {
                cur_repre = list_repre.get(0);
            } else {
                cur_repre = null;
            }
        }
//        if (curCargo.x != -1) {
//            representante_dao obj_representante_dao = new representante_dao();
//            setList_repre(obj_representante_dao.get_select(curCargo.x, 2));
//            obj_representante_dao.close();
//            if (getList_repre().size() > 0) {
//                cur_repre = getList_repre().get(0);
//            } else {
//                cur_repre = null;
//            }
//        } else {
//            cur_repre = null;
//        }
    }

    @Command
    public void nuevo_expediente() throws SQLException {
        limpiar_campos();
        habilitar_botones(true);
        btnNew.setDisabled(true);
        btnSave.setDisabled(false);
        btnEdit.setDisabled(true);
        btnSearch.setDisabled(true);
        btnVerExp.setDisabled(false);
        btnCancel.setDisabled(false);
        btnFUT.setDisabled(true);
        codi_tiex.setFocus(true);
        sw_quitar = true;
        setRecordMode("NEW");
    }

    @Command
    public void modificar_expediente() throws SQLException {
        Expediente_dao obj_expediente_dao = new Expediente_dao();
        if (obj_expediente_dao.validar_modificacion(nume_expe.getValue())) {
            habilitar_botones(true);
            btnNew.setDisabled(true);
            btnSave.setDisabled(false);
            btnEdit.setDisabled(true);
            btnSearch.setDisabled(true);
            btnCancel.setDisabled(false);
            btnFUT.setDisabled(true);
            sw_quitar = false;
            btnAgregarDes.setDisabled(true);
            btnVerExp.setDisabled(false);
            setRecordMode("EDIT");
        } else {
            Messagebox.show("El Expediente " + nume_expe.getValue() + " ya ha sido recepcionado.", "Mensaje", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }

    @Command
    public void cancelar_expediente() throws SQLException {
        limpiar_campos();
        habilitar_botones(false);
        btnNew.setDisabled(false);
        btnSave.setDisabled(true);
        btnEdit.setDisabled(true);
        btnSearch.setDisabled(false);
        btnCancel.setDisabled(true);
        btnFUT.setDisabled(true);
        valx_expe.setReadonly(true);
        btnVerExp.setDisabled(true);
        sw_quitar = false;
    }

    @Command
    public void buscar_expediente() {
        final HashMap<String, String> map = new HashMap<String, String>();
        map.put("accion", "Buscar");
        Executions.createComponents("proceso/win/search_expediente.zul", null, map);
    }

    @Command
    public void adjuntar_expediente() {
        final HashMap<String, String> map = new HashMap<String, String>();
        map.put("accion", "Adjuntar");
        Executions.createComponents("proceso/win/search_expediente.zul", null, map);
    }

    @Command
    public void ver_expediente() throws Exception {

        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
//        Messagebox.show("ASDASD");
        int year = Integer.parseInt(fech_expe.getValue().substring(0, 4));
//        Messagebox.show(""+year);
        administracion_dao obj_administracion_dao = new administracion_dao();
        administracion admin = obj_administracion_dao.get_administracion(year);
        obj_administracion_dao.close();
        usuario user = (usuario) _sess.getAttribute("usuario");

        Dependencia_dao depe = new Dependencia_dao();

        Map parametros = new HashMap();
        parametros.put("noen_admi", admin.getNoen_admi());
        parametros.put("noar_admi", depe.get_nomb2(user.getCodi_arde()));
        parametros.put("noal_admi", admin.getNoal_admi());
        depe.close();
        parametros.put("procedimiento", nomb_proc.getValue());
        String destinatario = "";
        String enti_reps = "123";
        if (!nomb_enti.getValue().isEmpty()) {
            enti_reps = nomb_enti.getValue();
        } else {
            if (re0.isChecked()) {
                enti_reps = "Municipalidad de Hualmay - " + curDependencia.y.toString();
            }
        }
        if (lst_destino_exp.get(0).getCodi_enti() != 0) {
            destinatario = lst_destino_exp.get(0).getNomb_enti();

        } else if (lst_destino_exp.get(0).getCodi_cont() != 0) {
            destinatario = lst_destino_exp.get(0).getNomb_cont();

        } else if (lst_destino_exp.get(0).getCodi_carg() != 0) {
            destinatario = lst_destino_exp.get(0).getNomb_carg() + " - " + lst_destino_exp.get(0).getNomb_arde();

        }
        parametros.put("destinatario", destinatario);
        parametros.put("enti_reps", enti_reps);

        if (!codi_cont.getValue().isEmpty()) {
            Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
            Contribuyente obj_contrib = obj_Contribuyente_dao.get_contribuyente(Integer.parseInt(codi_cont.getValue()));
            obj_Contribuyente_dao.close();
            parametros.put("noms_reps", obj_contrib.getNomb_cont());
            parametros.put("docs_reps", String.valueOf(obj_contrib.getDni_cont()));
            parametros.put("dire_reps", obj_contrib.getDire_cont());
            parametros.put("nomb_dist", obj_contrib.getObj_dist().getNomb_dist());
            parametros.put("nomb_prov", obj_contrib.getObj_dist().getProvincia().getNomb_prov());
            parametros.put("nomb_depa", obj_contrib.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
            parametros.put("telf_reps", obj_contrib.getTelf_cont());
            parametros.put("celu_reps", obj_contrib.getCelu_cont());
            parametros.put("emai_reps", obj_contrib.getEmai_cont());

        } else if (cur_repre2 != null) {

            representante_dao obj_repre_dao2 = new representante_dao();
            representante obj_repre2 = obj_repre_dao2.get_representante(cur_repre2.x);
            obj_repre_dao2.close();
            parametros.put("noms_reps", obj_repre2.getNoms_reps());
            parametros.put("docs_reps", obj_repre2.getDocs_reps());
            parametros.put("dire_reps", obj_repre2.getDire_reps());
            parametros.put("nomb_dist", obj_repre2.getObj_dist().getNomb_dist());
            parametros.put("nomb_prov", obj_repre2.getObj_dist().getProvincia().getNomb_prov());
            parametros.put("nomb_depa", obj_repre2.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
            parametros.put("telf_reps", obj_repre2.getTelf_reps());
            parametros.put("celu_reps", obj_repre2.getCelu_reps());
            parametros.put("emai_reps", obj_repre2.getEmai_reps());

        } else if (cur_repre != null) {

            representante_dao obj_repre_dao = new representante_dao();
            representante obj_repre = obj_repre_dao.get_representante(cur_repre.x);
            obj_repre_dao.close();
            parametros.put("noms_reps", obj_repre.getNoms_reps());
            parametros.put("docs_reps", obj_repre.getDocs_reps());
            parametros.put("dire_reps", obj_repre.getDire_reps());
            parametros.put("nomb_dist", obj_repre.getObj_dist().getNomb_dist());
            parametros.put("nomb_prov", obj_repre.getObj_dist().getProvincia().getNomb_prov());
            parametros.put("nomb_depa", obj_repre.getObj_dist().getProvincia().getDepartamento().getNomb_depa());
            parametros.put("telf_reps", obj_repre.getTelf_reps());
            parametros.put("celu_reps", obj_repre.getCelu_reps());
            parametros.put("emai_reps", obj_repre.getEmai_reps());

        }

        parametros.put("espe_expe", espe_expe.getValue());
        parametros.put("reci_expe", reci_expe.getValue());
        parametros.put("fech_expe", fech_expe.getValue());
        parametros.put("nufo_expe", nufo_expe.getValue());
        String req = "";
        requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
        List<BeanRequisitoMan> list_Requisitos = new ArrayList<BeanRequisitoMan>();
        list_Requisitos = obj_requisito_procedimiento_dao.get_requisito_procedimiento(Integer.parseInt(this.codi_proc.getValue()));
        //BeanRequisitoMan obj_requisito;
        for (int c = 0; c < list_Requisitos.size(); c++) {
            req = req + ", " + (c + 1) + ") " + list_Requisitos.get(c).getNomb_repr();
        }
        //Messagebox.show("Requisitos:" + req);
//        for (int i = 0; i < lst_req.size(); i++) {
//            obj_requisito = obj_requisito_procedimiento_dao.get_requisito_procedimiento2(lst_req.get(i).getCodi_repr());
//            if (i == 0) {
//                req = (i + 1) + ") " + obj_requisito.getNomb_repr();
//            } else {
//                req = req + ", " + (i + 1) + ") " + obj_requisito.getNomb_repr();
//            }
//        }
        obj_requisito_procedimiento_dao.close();
        parametros.put("requ_expe", req);
        parametros.put("nume_expe", nume_expe.getValue());
        String cc;
        String[] cc_split;
        Dependencia_dao obj_depe2;
        for (int v = 0; v < lst_destino_exp.size(); v++) {
            cc = "CC. ";
            if (lst_destino_exp.get(v).getCopias() == true) {
                area_dependencia_dao ObjDependenciaDAO = new area_dependencia_dao();
                cc = cc + ObjDependenciaDAO.get_abreviatura_dependencia(lst_destino_exp.get(v).getNomb_arde());
                parametros.put("cc" + (v + 1), cc);
            }
        }

//        for (int i = 0; i < lst_destino_exp.size(); i++) {
//            if (!lst_destino_exp.get(i).getNomb_carg().equals("")) {
//                obj_depe2 = new Dependencia_dao();
//                cc_split = obj_depe2.get_nomb(lst_destino_exp.get(i).getCodi_carg()).split(" ");
//                cc = "CC. ";
//                for (int j = 0; j < cc_split.length; j++) {
//                    cc = cc + cc_split[j].substring(0, 1) + ".";
//                }
//                parametros.put("cc" + (i + 1), cc);
//                obj_depe2.close();
//            } else if (!lst_destino_exp.get(i).getNomb_enti().equals("")) {
//                cc_split = lst_destino_exp.get(i).getNomb_enti().split(" ");
//                cc = "CC. ";
//                for (int j = 0; j < cc_split.length; j++) {
//                    cc = cc + cc_split[j].substring(0, 1) + ".";
//                }
//                parametros.put("cc" + (i + 1), cc);
//            } else if (!lst_destino_exp.get(i).getNomb_cont().equals("")) {
//                cc_split = lst_destino_exp.get(i).getNomb_cont().split(" ");
//                cc = "CC. ";
//                for (int j = 0; j < cc_split.length; j++) {
//                    cc = cc + cc_split[j].substring(0, 1) + ".";
//                }
//                parametros.put("cc" + (i + 1), cc);
//            }
//
//        }
        parametros.put("realPath", sv.getRealPath("/reportes/"));

        generarReporte("/reportes/fut.jasper", parametros, "Formulario único de trámite " + nume_expe.getValue() + ".pdf", sv);
    }

    @NotifyChange("*")
    @Command
    public void guardar_expediente() throws SQLException, Exception {

        String mensaje = "";
        if (cost_proc.getText().equals("0")) {
            cost_proc.setText("0");
        } else {
            if (reci_expe.getText().trim().equals("")) {
                mensaje = "El campo recibo no puede estar vacio.";
                Messagebox.show(mensaje, "Error", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        }
        Expediente_dao exp = new Expediente_dao();
        if (getRecordMode().equals("NEW")) {
            String reciboexiste = exp.get_recibo_existe(reci_expe.getValue().trim());
            if (cost_proc.getText().equals("0")) {

            } else {
                if (!reciboexiste.equals("")) {
                    mensaje = "El recibo ingresado ya existe en el expediente n° " + reciboexiste;
                    Messagebox.show(mensaje, "Error", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
            }
        } else {
            String reciboexiste = exp.get_recibo_existe(reci_expe.getValue().trim());
            if (!reciboexiste.equals(nume_expe.getValue()) && !reciboexiste.equals("")) {
                mensaje = "El recibo ingresado ya existe en el expediente n° " + reciboexiste;
                Messagebox.show(mensaje, "Error", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        }
        mensaje = "";
        if (curTipoDocumento != null) {
            if (curAsuntoExpediente != null) {
                if (!codi_proc.getValue().isEmpty() && !codi_proc.getValue().equals("0")) {
                    if (!nufo_expe.getValue().isEmpty()) {
                        if (valx_expe.getValue().matches("^\\d*\\.?\\d+$")) {
                            if (espe_expe.getValue().isEmpty()) {
                                espe_expe.setValue("");
                            }
                            boolean sw_remitente = false;
                            if (re0.isChecked()) {
                                if (cur_repre != null && cur_repre.x != null) {
                                    sw_remitente = true;
                                } else {
                                    mensaje = "Por favor, seleccione representante en remitente";
                                    tb.setSelectedPanel(tbl_rem);
                                }
                            } else if (re1.isChecked()) {
                                if (!codi_cont.getValue().isEmpty()) {
                                    sw_remitente = true;
                                } else {
                                    mensaje = "Por favor, seleccione un contribuyente remitente";
                                    tb.setSelectedPanel(tbl_rem);
                                }
                            } else if (re2.isChecked()) {
                                if (!codi_enti.getValue().isEmpty()) {
                                    if (cur_repre2 != null && cur_repre2.x != null) {
                                        sw_remitente = true;
                                    } else {
                                        mensaje = "Por favor, seleccione representante en remitente";
                                        tb.setSelectedPanel(tbl_rem);
                                    }
                                } else {
                                    mensaje = "Por favor, seleccione Entidad en remitente";
                                    tb.setSelectedPanel(tbl_rem);
                                }
                            }
                            if (sw_remitente) {
                                if (lst_destino_exp.size() > 0) {

                                    expediente obj_expediente = new expediente();
                                    Expediente_dao obj_expediente_dao = new Expediente_dao();
                                    String codi_expe = "";
                                    int codi_usua2 = -1;
                                    if (getRecordMode().equals("EDIT")) {
                                        codi_expe = this.nume_expe.getValue();
                                        usuario_dao obj_usuadao = new usuario_dao();
                                        codi_usua2 = obj_usuadao.get_codi(this.codi_usua.getValue());
                                    } else if (getRecordMode().equals("NEW")) {

                                        codi_expe = obj_expediente_dao.nuevo_codi_expe();
                                        usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
                                        codi_usua2 = obj_usuario.getCodi_usua();
                                        // VERIFICAR SESSION
                                        obj_expediente.setCodi_esex(1);
                                    }
                                    obj_expediente.setCodi_usua(codi_usua2);
                                    obj_expediente.setNume_expe(codi_expe);
                                    // REMITENTE
                                    int codi_enex = 0;
                                    if (re0.isChecked()) {
                                        codi_enex = 1;
                                        obj_expediente.setCodi_carg(curCargo.x);
                                        obj_expediente.setCodi_cont(0);
                                        obj_expediente.setCodi_enti(0);
                                        obj_expediente.setCodi_reps(cur_repre.x);
                                    } else if (re1.isChecked()) {
                                        codi_enex = 2;
                                        obj_expediente.setCodi_carg(0);
                                        obj_expediente.setCodi_cont(Integer.parseInt(codi_cont.getValue()));
                                        obj_expediente.setCodi_enti(0);
                                        obj_expediente.setCodi_reps(0);
                                    } else if (re2.isChecked()) {
                                        codi_enex = 3;
                                        obj_expediente.setCodi_carg(0);
                                        obj_expediente.setCodi_cont(0);
                                        obj_expediente.setCodi_reps(cur_repre2.x);
                                        obj_expediente.setCodi_enti(Integer.parseInt(codi_enti.getValue()));
                                    }
                                    obj_expediente.setValx_expe(valx_expe.getValue());
                                    obj_expediente.setRefe_expe("");
                                    obj_expediente.setNufo_expe(Integer.parseInt(nufo_expe.getValue()));
                                    if (!curTipoDocumento.y.equals("SOLICITUD")) {
                                        if (!nudo_expe.getValue().isEmpty()) {
                                            obj_expediente.setNudo_expe(nudo_expe.getValue());
                                        }
                                    } else {
                                        obj_expediente.setNudo_expe("");
                                    }
                                    obj_expediente.setCodi_tiex(curTipoDocumento.x);
                                    obj_expediente.setCodi_proc(Integer.parseInt(codi_proc.getValue()));
                                    obj_expediente.setCodi_enex(codi_enex);  // NULO
                                    obj_expediente.setCodi_asex(curAsuntoExpediente.x);
                                    obj_expediente.setEspe_expe(espe_expe.getValue());
                                    List<historial_expediente> destinatarios = new ArrayList<historial_expediente>();
                                    historial_expediente obj_historial_expediente;
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy '-' hh:mm:ss a");
                                    usuario usua = (usuario) _sess.getAttribute("usuario");
                                    String obsv_envia = "- ENVIA : " + usua.getNume_usua() + " " + sdf.format(new java.util.Date()).toLowerCase();
                                    for (int i = 0; i < lst_destino_exp.size(); i++) {
                                        obj_historial_expediente = new historial_expediente();
                                        obj_historial_expediente.setCorr_hist(1);
                                        obj_historial_expediente.setNude_hist(i + 1);
                                        obj_historial_expediente.setNume_expe(codi_expe);
                                        obj_historial_expediente.setNufo_hist(Integer.parseInt(nufo_expe.getValue()));
                                        obj_historial_expediente.setUsen_hist(codi_usua2);
                                        obj_historial_expediente.setEsta_hist(1);
                                        obj_historial_expediente.setCare_hist(lst_destino_exp.get(i).getCodi_carg());
                                        obj_historial_expediente.setCore_hist(lst_destino_exp.get(i).getCodi_cont());
                                        obj_historial_expediente.setEnre_hist(lst_destino_exp.get(i).getCodi_enti());
                                        usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
                                        if (lst_destino_exp.get(i).getCodi_carg() != 0) {
                                            obj_historial_expediente.setReti_hist(1);
                                        } else if (lst_destino_exp.get(i).getCodi_cont() != 0) {
                                            obj_historial_expediente.setReti_hist(2);
                                            obj_historial_expediente.setCare_hist(obj_usuario.getCodi_carg());
                                        } else if (lst_destino_exp.get(i).getCodi_enti() != 0) {
                                            obj_historial_expediente.setReti_hist(3);
                                            obj_historial_expediente.setCare_hist(obj_usuario.getCodi_carg());
                                        }
                                        obj_historial_expediente.setCaen_hist(obj_usuario.getCodi_carg());
                                        obj_historial_expediente.setEnti_hist(1);
                                        obj_historial_expediente.setObsv_hist(obsv_envia);
                                        obj_historial_expediente.setCopi_hist(lst_destino_exp.get(i).getCopias());
                                        destinatarios.add(obj_historial_expediente);
                                    }
                                    obj_expediente.setHistorial_expediente(destinatarios);
                                    obj_expediente.setRequisitos(lst_req);
                                    obj_expediente.setReci_expe(reci_expe.getValue());

                                    procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
                                    obj_expediente.setDias_proc(obj_procedimiento_expediente_dao.get_dias_proc(Integer.parseInt(codi_proc.getValue())));
                                    obj_procedimiento_expediente_dao.close();
                                    numero_expediente_adjunto = txtExpAdj.getValue();
                                    numero_expediente = nume_expe.getValue();
                                    if (getRecordMode().equals("NEW")) {
                                        /**
                                         * Tipo Registro = 1 es cuando esta
                                         * conforme con los requisitos
                                         */

                                        if (this.tipo_registro == 1) {
                                            /*Insertar el expediente a adjuntar*/
                                            obj_expediente_dao.insert(obj_expediente, Integer.parseInt(txtDias.getValue()));
                                            Expediente_dao ObjExpedienteDAO = new Expediente_dao();
                                            int Codigo_usuario_actual = Integer.parseInt(_sess.getAttribute("acceso").toString());
                                            ObjExpedienteDAO.Insert_Expediente_Adjunto_New(numero_expediente_adjunto, Codigo_usuario_actual);
                                            ObjExpedienteDAO.close();

                                        } else {
                                            if (this.tipo_registro == 2) {
                                                obj_expediente_dao.insert2(obj_expediente, tipo_registro, dias_caducidad_inconforme);
                                            }
                                        }
                                    } else if (getRecordMode().equals("EDIT")) {
                                        obj_expediente_dao.update(obj_expediente);
                                        int Codigo_usuario_actual = Integer.parseInt(_sess.getAttribute("acceso").toString());
                                        Expediente_dao ObjExpedienteDAO = new Expediente_dao();
                                        if (txtExpAdj.getValue().isEmpty()) {
                                            Messagebox.show("Vacio");
                                            ObjExpedienteDAO.Insert_Expediente_Adjunto_Edit(numero_expediente, numero_expediente_adjunto, Codigo_usuario_actual);
                                            ObjExpedienteDAO.close();
                                        } else {
                                            if (!txtExpAdj.getValue().isEmpty()) {
                                                Messagebox.show("No Vacio");
                                                Messagebox.show(numero_expediente_adjunto);
                                                Messagebox.show(numero_expediente);

                                                ObjExpedienteDAO.Update_Expediente_Adjunto_Edit(numero_expediente_adjunto, numero_expediente);
                                                ObjExpedienteDAO.close();
                                            }
                                        }

                                    }

                                    limpiar_campos();
                                    btnSave.setDisabled(true);
                                    btnCancel.setDisabled(true);
                                    btnNew.setDisabled(false);
                                    btnSearch.setDisabled(false);
                                    btnEdit.setDisabled(true);
                                    btnFUT.setDisabled(true);
                                    sw_quitar = false;
                                    habilitar_botones(false);
                                    numero_expediente = "";
                                    numero_expediente_adjunto = "";
                                    cargar_expediente(codi_expe);
                                    ver_expediente();
                                    Messagebox.show("Expediente guardado con éxito", "Registro", Messagebox.OK, Messagebox.INFORMATION);
                                } else {
                                    mensaje = "Por favor, añada un destinatario";
                                    tb.setSelectedPanel(tbl_des);
                                }
                            }
                        } else {
                            mensaje = "Formato de ingreso inválido. Ejemplo: 100.00";
                            tb.setSelectedPanel(tbl_dat);
                            tb.setSelectedPanel(tbl_dat);
                        }
                    } else {
                        mensaje = "Por favor, ingrese número de folios";
                        tb.setSelectedPanel(tbl_dat);
                        tb.setSelectedPanel(tbl_dat);
                    }
                } else {
                    mensaje = "Por favor, seleccione procedimiento";
                    tb.setSelectedPanel(tbl_dat);
                }
            } else {
                mensaje = "Por favor, seleccione tipo";
                tb.setSelectedPanel(tbl_dat);
            }

        } else {
            mensaje = "Por favor, seleccione un tipo de documento";
            tb.setSelectedPanel(tbl_dat);
        }
        if (!mensaje.equals("")) {
            Messagebox.show(mensaje, "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @NotifyChange("*")
    public void limpiar_campos() throws SQLException {
        nume_expe.setValue("");
        fech_expe.setValue("");
        feca_expe.setValue("");
        txtExpAdj.setValue("");
        cost_proc.setValue("");
        codi_usua.setValue("");
        if (getListTipoDocumento().size() > 0) {
            curTipoDocumento = getListTipoDocumento().get(0);
            if (curTipoDocumento.x > 0) {
                if (curTipoDocumento.y.equals("SOLICITUD")) {

                    nudo_expe.setDisabled(true);
                } else {
                    nudo_expe.setDisabled(false);
                }
            }
        } else {
            curTipoDocumento = null;
        }
        nudo_expe.setValue("");
        //refe_expe.setValue("");
        valx_expe.setValue("");
        reci_expe.setValue("");
        if (getListAsuntoExpediente().size() > 0) {
            curAsuntoExpediente = getListAsuntoExpediente().get(0);
        } else {
            curAsuntoExpediente = null;
        }

        codi_proc.setValue("");
        nomb_proc.setValue("");

        nufo_expe.setValue("");
        espe_expe.setValue("");
        re0.setChecked(true);
        cont_grid.setVisible(false);
        enti_grid.setVisible(false);
        muni_grid.setVisible(true);
        limpiar_muni();
        limpiar_cont();
        limpiar_enti();
        lst_destino_exp = new ArrayList<BeanDestinoExpe>();
        while (lstbox_des.getItems().size() > 0) {
            lstbox_des.removeItemAt(0);
        }
        tb.setSelectedPanel(tbl_dat);
    }

    @NotifyChange("*")
    public void limpiar_enti() throws SQLException {
        codi_enti.setValue("");
        nomb_enti.setValue("");
        dire_enti.setValue("");
        list_repre2 = null;
        cur_repre2 = null;
    }

    public void limpiar_cont() throws SQLException {
        codi_cont.setValue("");
        nomb_cont.setValue("");
        dire_cont.setValue("");
        regio_cont.setValue("");
        prov_cont.setValue("");
        dist_cont.setValue("");
        dni_cont.setValue("");
        emai_cont.setValue("");
    }

    public void limpiar_muni() throws SQLException {
        try {
            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            setListDependencia(obj_Dependencia_dao.get_select(3));
            obj_Dependencia_dao.close();
            if (getListDependencia().size() > 0) {
                curDependencia = getListDependencia().get(0);
            } else {
                curDependencia = null;
            }
            if (curDependencia.x != -1) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                setListCargo(obj_cargos_dao.get_select(curDependencia.x));
                obj_cargos_dao.close();
                if (getListCargo().size() > 0) {
                    curCargo = getListCargo().get(0);

                    representante_dao obj_repre = new representante_dao();
                    obj_repre.get_select(curCargo.x, 2);
                    obj_repre.close();
                    if (getList_repre().size() > 0) {
                        setCur_repre(getList_repre().get(0));
                    } else {
                        setCur_repre(null);
                    }
                } else {
                    curCargo = null;
                    setCur_repre(null);
                }
            } else {
                curCargo = null;
                setCur_repre(null);
            }
        } catch (Exception e) {
        }

    }

    public void habilitar_botones(boolean bw) {
        codi_tiex.setDisabled(!bw);
        nudo_expe.setReadonly(!bw);
        //refe_expe.setReadonly(!bw);
        valx_expe.setReadonly(bw);
        codi_asex.setDisabled(!bw);
        codi_proc.setDisabled(!bw);
        reci_expe.setReadonly(!bw);
        nufo_expe.setReadonly(!bw);
        espe_expe.setReadonly(!bw);
        btn_enti.setDisabled(!bw);
        btn_cont.setDisabled(!bw);
        btn_cont_new.setDisabled(!bw);
        btn_enti_new.setDisabled(!bw);
        btn_repr_new.setDisabled(!bw);
        btn_repr_new2.setDisabled(!bw);
        re0.setDisabled(!bw);
        re1.setDisabled(!bw);
        re2.setDisabled(!bw);
        codi_depe_muni.setDisabled(!bw);
        codi_carg_muni.setDisabled(!bw);
        codi_reps_enti.setDisabled(!bw);
        codi_reps_muni.setDisabled(!bw);
        btnAgregarDes.setDisabled(!bw);
        btn_proc.setDisabled(!bw);
        btnAddProc.setDisabled(!bw);
    }

    @Command
    public void nuevo_procedimiento_expediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sProcedimiento", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_procedimiento_expediente.zul", null, map);
    }

    public List<Pair<Integer, String>> getListTipoDocumento() {
        return listTipoDocumento;
    }

    public void setListTipoDocumento(List<Pair<Integer, String>> listTipoDocumento) {
        this.listTipoDocumento = listTipoDocumento;
    }

    public Pair<Integer, String> getCurTipoDocumento() {
        return curTipoDocumento;
    }

    public void setCurTipoDocumento(Pair<Integer, String> curTipoDocumento) {
        this.curTipoDocumento = curTipoDocumento;
    }

    public void loadUsuarios() {
        try {
            usuario_dao ObjUsuarioDAO = new usuario_dao();
            setListUsuarios(ObjUsuarioDAO.llenarComboUsuarios_Expedientes(curCargo.x));
            setCurlistUsuarios(listUsuarios.get(0));
        } catch (Exception e) {
            Messagebox.show("Error : " + e.getMessage() + "Método LoadUsuarios");
        }
    }

    private void loadDataControls() {
        try {

            general_dao obj_general_dao = new general_dao();
            setListTipoDocumento(obj_general_dao.get_select_tipo_expediente());
            obj_general_dao.close();
            if (getListTipoDocumento().size() > 0) {
                curTipoDocumento = getListTipoDocumento().get(0);
            }

            asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
            setListAsuntoExpediente(obj_asunto_expediente_dao.get_select());
            obj_asunto_expediente_dao.close();
            if (getListAsuntoExpediente().size() > 0) {
                curAsuntoExpediente = getListAsuntoExpediente().get(0);
            }

            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            setListDependencia(obj_Dependencia_dao.get_select(3));
            obj_Dependencia_dao.close();
            if (getListDependencia().size() > 0) {
                curDependencia = listDependencia.get(0);
            } else {
                curDependencia = null;
                //codi_depe_muni.setSelectedIndex(-1);
            }

            if (curDependencia.x != -1) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                setListCargo(obj_cargos_dao.get_select(curDependencia.x));
                obj_cargos_dao.close();
                if (getListCargo().size() > 0) {
                    curCargo = getListCargo().get(0);

//                    usuario_dao ObjUsuarioDAO = new usuario_dao();
//                    setListUsuarios(ObjUsuarioDAO.llenarComboUsuarios_Expedientes(curCargo.x));
//                    setCurlistUsuarios(listUsuarios.get(0));

                    representante_dao obj_repre = new representante_dao();
                    list_repre = obj_repre.get_select(curCargo.x, 2);
                    obj_repre.close();

                    if (getList_repre().size() > 0) {

                        cur_repre = list_repre.get(0);

                    } else {
                        cur_repre = null;
                    }
                } else {
                    curCargo = null;
                    setCur_repre(null);
                }
            } else {
                curCargo = null;
                setCur_repre(null);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * @return the listAsuntoExpediente
     */
    public List<Pair<Integer, String>> getListAsuntoExpediente() {
        return listAsuntoExpediente;
    }

    /**
     * @param listAsuntoExpediente the listAsuntoExpediente to set
     */
    public void setListAsuntoExpediente(List<Pair<Integer, String>> listAsuntoExpediente) {
        this.listAsuntoExpediente = listAsuntoExpediente;
    }

    public Pair<Integer, String> getCurAsuntoExpediente() {
        return curAsuntoExpediente;
    }

    /**
     * @param curAsuntoExpediente the curAsuntoExpediente to set
     */
    public void setCurAsuntoExpediente(Pair<Integer, String> curAsuntoExpediente) {
        this.curAsuntoExpediente = curAsuntoExpediente;
    }

    public List<Pair<Integer, String>> getListDependencia() {
        return listDependencia;
    }

    /**
     * @param listDependencia the listDependencia to set
     */
    public void setListDependencia(List<Pair<Integer, String>> listDependencia) {
        this.listDependencia = listDependencia;
    }

    /**
     * @return the listCargo
     */
    public List<Pair<Integer, String>> getListCargo() {
        return listCargo;
    }

    /**
     * @param listCargo the listCargo to set
     */
    public void setListCargo(List<Pair<Integer, String>> listCargo) {
        this.listCargo = listCargo;
    }

    /**
     * @return the curDependencia
     */
    public Pair<Integer, String> getCurDependencia() {
        return curDependencia;
    }

    /**
     * @param curDependencia the curDependencia to set
     */
    public void setCurDependencia(Pair<Integer, String> curDependencia) {
        this.curDependencia = curDependencia;
    }

    /**
     * @return the curCargo
     */
    public Pair<Integer, String> getCurCargo() {
        return curCargo;
    }

    /**
     * @param curCargo the curCargo to set
     */
    public void setCurCargo(Pair<Integer, String> curCargo) {
        this.curCargo = curCargo;
    }

    public List<BeanDestinoExpe> getLst_destino_exp() {
        return lst_destino_exp;
    }

    public void setLst_destino_exp(List<BeanDestinoExpe> lst_destino_exp) {
        this.lst_destino_exp = lst_destino_exp;
    }

    public BeanDestinoExpe getCur_destino_exp() {
        return cur_destino_exp;
    }

    public void setCur_destino_exp(BeanDestinoExpe cur_destino_exp) {
        this.cur_destino_exp = cur_destino_exp;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<BeanRequisitoMan> getLst_req() {
        return lst_req;
    }

    public void setLst_req(List<BeanRequisitoMan> lst_req) {
        this.lst_req = lst_req;
    }

    public List<Pair<Integer, String>> getList_repre() {
        return list_repre;
    }

    public void setList_repre(List<Pair<Integer, String>> list_repre) {
        this.list_repre = list_repre;
    }

    public Pair<Integer, String> getCur_repre() {
        return cur_repre;
    }

    public void setCur_repre(Pair<Integer, String> cur_repre) {
        this.cur_repre = cur_repre;
    }

    public List<Pair<Integer, String>> getList_repre2() {
        return list_repre2;
    }

    public void setList_repre2(List<Pair<Integer, String>> list_repre2) {
        this.list_repre2 = list_repre2;
    }

    public Pair<Integer, String> getCur_repre2() {
        return cur_repre2;
    }

    public void setCur_repre2(Pair<Integer, String> cur_repre2) {
        this.cur_repre2 = cur_repre2;
    }

    /**
     * @return the listUsuarios
     */
    public List<Pair<Integer, String>> getListUsuarios() {
        return listUsuarios;
    }

    /**
     * @param listUsuarios the listUsuarios to set
     */
    public void setListUsuarios(List<Pair<Integer, String>> listUsuarios) {
        this.listUsuarios = listUsuarios;
    }

    /**
     * @return the curlistUsuarios
     */
    public Pair<Integer, String> getCurlistUsuarios() {
        return curlistUsuarios;
    }

    /**
     * @param curlistUsuarios the curlistUsuarios to set
     */
    public void setCurlistUsuarios(Pair<Integer, String> curlistUsuarios) {
        this.curlistUsuarios = curlistUsuarios;
    }

}
