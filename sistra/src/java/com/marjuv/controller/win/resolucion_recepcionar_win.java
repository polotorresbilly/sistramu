package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDestinoDis;
import com.marjuv.beans_view.BeanDestinoExpe;
import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.area_dependencia;
import com.marjuv.entity.expediente;
import com.marjuv.entity.historial_expediente;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.sql.Date;
import java.sql.SQLException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class resolucion_recepcionar_win {

    @Wire("#resolucion_win")
    private Window win;
    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> lst_tire, lst_asre;
    private Pair<Integer, String> cur_tire, cur_asre;

    private List<resolucion> lst_resolucion;
    private resolucion cur_resolucion;

    @Wire
    private Checkbox chk_gen;
    @Wire
    private Checkbox chk_res;
    @Wire
    private Checkbox chk_asu;
    @Wire
    private Checkbox chk_exp;
    @Wire
    private Checkbox chk_nre;
    @Wire
    private Checkbox chk_tas;

    private String tipo;

    private usuario obj_usua;

    @Wire
    private Datebox fech_reso1;
    @Wire
    private Datebox fech_reso2;

    @Wire
    private Textbox nume_expe;
    @Wire
    private Textbox nume_reso;
    @Wire
    private Textbox asun_reso;

    private boolean select_all_st;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) throws Exception {
        Selectors.wireComponents(view, this, false);

        fech_reso1.setValue(new java.util.Date());
        fech_reso2.setValue(new java.util.Date());
        search_reso();
    }

    @NotifyChange("*")
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        loadDataControls();
        lst_resolucion = new ArrayList<resolucion>();
        tipo = "gen";
        select_all_st = false;
    }

    @NotifyChange("*")
    @Command
    public void marcar()  {
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCodi_reso()==cur_resolucion.getCodi_reso()) {
                if (lst_resolucion.get(i).getCheck()) {
                    lst_resolucion.get(i).setCheck(false);
                } else {
                    lst_resolucion.get(i).setCheck(true);
                }
                break;
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void search_reso() throws SQLException, Exception {
        usuario obj_usua2 = (usuario) _sess.getAttribute("usuario");
        if (tipo.equals("gen")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_gen("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), "usem_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("res")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_res("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_tire.x, "usem_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("asu")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_asu("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_asre.x, "usem_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("exp")) {
            if (!nume_expe.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_exp("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_expe.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el número de expediente", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (tipo.equals("tas")) {
            if (!asun_reso.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_tas("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), asun_reso.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el asunto de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (tipo.equals("nre")) {
            if (!nume_reso.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_nre("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_reso.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el número de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void recepcionar() throws SQLException, Exception {
        boolean sw = false;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                sw = true;
                break;
            }
        }
        if (sw) {
            if (obj_usua != null) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                obj_resolucion_dao.notificar(lst_resolucion, obj_usua.getCodi_usua());
                obj_resolucion_dao.close();
                usuario obj_usua2 = (usuario) _sess.getAttribute("usuario");
                if (tipo.equals("gen")) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_gen("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), "usem_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                } else if (tipo.equals("res")) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_res("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_tire.x, "usem_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                } else if (tipo.equals("asu")) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_asu("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_asre.x, "usem_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                } else if (tipo.equals("exp")) {
                    if (!nume_expe.getValue().isEmpty()) {
                        obj_resolucion_dao = new resolucion_dao();
                        lst_resolucion = obj_resolucion_dao.search_resolucion_exp("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_expe.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                        obj_resolucion_dao.close();
                        if (lst_resolucion.size() == 0) {
                            Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                        }
                    } else {
                        Messagebox.show("Por favor, ingrese el número de expediente", "Error", Messagebox.OK, Messagebox.ERROR);
                    }
                } else if (tipo.equals("tas")) {
                    if (!asun_reso.getValue().isEmpty()) {
                        obj_resolucion_dao = new resolucion_dao();
                        lst_resolucion = obj_resolucion_dao.search_resolucion_tas("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), asun_reso.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                        obj_resolucion_dao.close();
                        if (lst_resolucion.size() == 0) {
                            Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                        }
                    } else {
                        Messagebox.show("Por favor, ingrese el asunto de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
                    }
                } else if (tipo.equals("nre")) {
                    if (!nume_reso.getValue().isEmpty()) {
                        obj_resolucion_dao = new resolucion_dao();
                        lst_resolucion = obj_resolucion_dao.search_resolucion_nre("1", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_reso.getValue(), "usem_reso", obj_usua2.getCodi_arde());
                        obj_resolucion_dao.close();
                        if (lst_resolucion.size() == 0) {
                            Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                        }
                    } else {
                        Messagebox.show("Por favor, ingrese el número de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
                    }
                }

                Messagebox.show("Las resoluciones han sido recepcionados con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            } else {
                Messagebox.show("Por favor, seleccione un distribuidor", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else {
            Messagebox.show("Por favor, marque al menos una resolución", "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @NotifyChange("*")
    @Command
    public void select_all() throws SQLException, Exception {
        select_all_st = !select_all_st;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            lst_resolucion.get(i).setCheck(select_all_st);
        }
    }

    @NotifyChange("*")
    @Command
    public void select() throws SQLException, Exception {
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCodi_reso() == cur_resolucion.getCodi_reso()) {
                if (cur_resolucion.getCheck()) {
                    lst_resolucion.get(i).setCheck(false);
                } else {
                    lst_resolucion.get(i).setCheck(true);
                }
                break;
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void changeGen() {
        if (chk_gen.isChecked()) {
            //chk_asu.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "gen";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeRes() {
        if (chk_res.isChecked()) {
            chk_gen.setChecked(false);
            //chk_asu.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "res";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeAsu() {
        if (chk_asu.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "asu";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeExp() {
        if (chk_exp.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            //chk_asu.setChecked(false);
            tipo = "exp";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeNre() {
        if (chk_nre.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            //chk_asu.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "nre";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeTas() {
        if (chk_tas.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_asu.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "tas";
        } else {
            tipo = "";
        }
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setNotificador(
            @BindingParam("usuario") usuario obj_usuario
    ) throws Exception {
        this.obj_usua = obj_usuario;
    }

    @Command
    public void search_des() {
        Executions.createComponents("proceso/win/search_usuario.zul", null, null);
    }

    @NotifyChange("*")
    private void loadDataControls() {
        try {

            general_dao obj_general_dao = new general_dao();
            lst_tire = obj_general_dao.get_select_tipo_resolucion();
            obj_general_dao.close();
            if (lst_tire.size() > 0) {
                cur_tire = lst_tire.get(0);
            } else {
                cur_tire = null;
            }

            Asunto_Resolucion_dao obj_Asunto_Resolucion_dao = new Asunto_Resolucion_dao();
            lst_asre = obj_Asunto_Resolucion_dao.get_select();
            if (lst_asre.size() > 0) {
                cur_asre = lst_asre.get(0);
            } else {
                cur_asre = null;
            }
            obj_Asunto_Resolucion_dao.close();
        } catch (Exception ex) {
            Logger.getLogger(resolucion_recepcionar_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Pair<Integer, String>> getLst_tire() {
        return lst_tire;
    }

    public void setLst_tire(List<Pair<Integer, String>> lst_tire) {
        this.lst_tire = lst_tire;
    }

    public Pair<Integer, String> getCur_tire() {
        return cur_tire;
    }

    public void setCur_tire(Pair<Integer, String> cur_tire) {
        this.cur_tire = cur_tire;
    }

    public List<Pair<Integer, String>> getLst_asre() {
        return lst_asre;
    }

    public void setLst_asre(List<Pair<Integer, String>> lst_asre) {
        this.lst_asre = lst_asre;
    }

    public Pair<Integer, String> getCur_asre() {
        return cur_asre;
    }

    public void setCur_asre(Pair<Integer, String> cur_asre) {
        this.cur_asre = cur_asre;
    }

    public List<resolucion> getLst_resolucion() {
        return lst_resolucion;
    }

    public void setLst_resolucion(List<resolucion> lst_resolucion) {
        this.lst_resolucion = lst_resolucion;
    }

    public resolucion getCur_resolucion() {
        return cur_resolucion;
    }

    public void setCur_resolucion(resolucion cur_resolucion) {
        this.cur_resolucion = cur_resolucion;
    }

    public usuario getObj_usua() {
        return obj_usua;
    }

    public void setObj_usua(usuario obj_usua) {
        this.obj_usua = obj_usua;
    }

}
