package com.marjuv.controller.win;

import com.marjuv.dao.usuario_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.usuario;
import com.marjuv.entity.usuario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_usuario_win {

    @Wire("#search_usuario")
    private Window win;
    private List<usuario> lst_usuario;
    private usuario cur_usuario;
    @WireVariable
    private Session _sess;
    
    @Wire
    private Textbox label_busq;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
    }
    

    public List<usuario> getLst_usuario() {
        try {
            usuario_dao obj_usuario_dao = new usuario_dao();
            lst_usuario = obj_usuario_dao.search_usuario(label_busq.getValue().toUpperCase());
            obj_usuario_dao.close();
        } catch (Exception ex) {
        }
        return lst_usuario;
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("usuario", this.cur_usuario);
        BindUtils.postGlobalCommand(null, null, "setNotificador", args);
        win.detach();
    }
    
    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_usuario(List<usuario> lst_usuario) {
        this.lst_usuario = lst_usuario;
    }

    public usuario getCur_usuario() {
        return cur_usuario;
    }

    public void setCur_usuario(usuario cur_usuario) {
        this.cur_usuario = cur_usuario;
    }

}
