package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDestinoExpe;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class add_dest_reso_win {

    @Wire("#search_reso")
    private Window win;
    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> listEntidad2, listDependencia2,listCargo2;
    private Pair<Integer, String> curEntidad2, curDependencia2,curCargo2;

    // REMITENTE
    @Wire()
    private Radio re01;
    @Wire()
    private Radio re11;
    @Wire()
    private Radio re21;
    @Wire()
    private Grid muni_grid;
    @Wire()
    private Grid cont_grid;
    @Wire()
    private Grid enti_grid;

    // CONTRIBUYENTE
    @Wire()
    private Textbox codi_cont;
    @Wire()
    private Textbox nomb_cont;
    @Wire()
    private Textbox dire_cont;
    @Wire()
    private Textbox regio_cont;
    @Wire()
    private Textbox prov_cont;
    @Wire()
    private Textbox dist_cont;
    @Wire()
    private Textbox dni_cont;
    @Wire()
    private Textbox emai_cont;
    @Wire()
    private Textbox copia_des;
    @Wire()
    private Button btn_cont;

    // ENTIDAD    
    @Wire()
    private Textbox codi_enti;
    @Wire()
    private Textbox nomb_enti;
    @Wire()
    private Textbox dire_enti;
    @Wire()
    private Button btn_enti;

    // DEPENDENCIA
    @Wire()
    private Combobox codi_carg_muni2;
    @Wire()
    private Combobox codi_depe_muni2;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @NotifyChange("*")
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        loadDataControls();
    }

    @Command
    public void searchContribuyente2() {
        Executions.createComponents("proceso/win/search_contribuyente_des.zul", null, null);
    }

    @Command
    public void searchEntidad2() {
        Executions.createComponents("proceso/win/search_entidad_des.zul", null, null);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setContribuyenteDes(
            @BindingParam("codi_cont") String codi_cont,
            @BindingParam("nomb_cont") String nomb_cont,
            //@BindingParam("apel_cont") String apel_cont,
            @BindingParam("dni_cont") String dni_cont,
            @BindingParam("dire_cont") String dire_cont,
            @BindingParam("emai_cont") String emai_cont,
            @BindingParam("nomb_dist") String nomb_dist,
            @BindingParam("nomb_prov") String nomb_prov,
            @BindingParam("nomb_depa") String nomb_depa
    ) {
        this.codi_cont.setValue(codi_cont);
        this.nomb_cont.setValue(nomb_cont);
        this.dni_cont.setValue(dni_cont);
        this.dire_cont.setValue(dire_cont);
        this.emai_cont.setValue(emai_cont);
        this.regio_cont.setValue(nomb_depa);
        this.prov_cont.setValue(nomb_prov);
        this.dist_cont.setValue(nomb_dist);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setEntidadDes(
            @BindingParam("codi_enti") String codi_enti,
            @BindingParam("nomb_enti") String nomb_enti,
            @BindingParam("dire_enti") String dire_enti
    ) {
        this.codi_enti.setValue(codi_enti);
        this.nomb_enti.setValue(nomb_enti);
        this.dire_enti.setValue(dire_enti);
    }

    @Command
    public void add_destinatario() {
        Map args = new HashMap();
        if (re11.isChecked()) {
            // CONTRIBUYENTE
            if (!this.codi_cont.getValue().isEmpty()/* && !this.codi_cont.equals("")*/) {
                args.put("codi_arde", 0);
                args.put("codi_cont", Integer.parseInt(this.codi_cont.getValue()));
                args.put("codi_enti", 0);
                args.put("nomb_enti", "");
                args.put("nomb_arde", "");
                args.put("nomb_cont", this.nomb_cont.getValue());
                //args.put("copia_des", this.copia_des.getValue());
                BindUtils.postGlobalCommand(null, null, "setDestinatarioReso", args);
                win.detach();
            } else {
                Messagebox.show("Por favor, seleccione un contribuyente", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (re21.isChecked()) {
            // ENTIDAD
            if (!this.codi_enti.getValue().isEmpty()/* && !this.codi_enti.equals("")*/) {
                args.put("codi_arde", 0);
                args.put("codi_cont", 0);
                args.put("codi_enti", Integer.parseInt(this.codi_enti.getValue()));
                args.put("nomb_enti", this.nomb_enti.getValue());
                args.put("nomb_arde", "");
                args.put("nomb_cont", "");
                //args.put("copia_des", this.copia_des.getValue());
                BindUtils.postGlobalCommand(null, null, "setDestinatarioReso", args);
                win.detach();
            } else {
                Messagebox.show("Por favor, seleccione una entidad", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (re01.isChecked()) {
            // MUNICIPALIDAD
            if (this.curDependencia2 != null) {
                args.put("codi_arde", this.curDependencia2.x);
                args.put("codi_cont", 0);
                args.put("codi_enti", 0);
                args.put("nomb_enti", "");
                args.put("nomb_arde", this.curDependencia2.y + " - " + curCargo2.y);
                args.put("nomb_carg", this.curCargo2.y);
                args.put("nomb_cont", "");
                //args.put("copia_des", this.copia_des.getValue());
                BindUtils.postGlobalCommand(null, null, "setDestinatarioReso", args);
                win.detach();
            } else {
                Messagebox.show("Por favor, seleccione una área de dependencia", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @Command
    public void close_win() {
        win.detach();
    }

    @Command
    public void changeDestinatario() {
        if (re11.isChecked()) {
            cont_grid.setVisible(true);
            enti_grid.setVisible(false);
            muni_grid.setVisible(false);
        } else if (re21.isChecked()) {
            cont_grid.setVisible(false);
            muni_grid.setVisible(false);
            enti_grid.setVisible(true);
        } else if (re01.isChecked()) {
            cont_grid.setVisible(false);
            enti_grid.setVisible(false);
            muni_grid.setVisible(true);
        }
    }
    
    @NotifyChange("*")
    @Command
    public void updateCargo2() throws SQLException {
        if (curDependencia2 != null) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            setListCargo2(obj_cargos_dao.get_select(curDependencia2.x));
            obj_cargos_dao.close();
            if (getListCargo2().size() > 0) {
                setCurCargo2(getListCargo2().get(0));
            } else {
                setCurCargo2(new Pair<Integer, String>(-1, ""));
                codi_carg_muni2.setSelectedIndex(-1);
            }
        }
    }
    
    @NotifyChange("*")
    @Command
    public void updateDependencia2() throws SQLException {
        curDependencia2 = null;
        Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
        listDependencia2 = obj_Dependencia_dao.get_select(curEntidad2.x);
        obj_Dependencia_dao.close();
        if (listDependencia2.size() > 0) {
            curDependencia2 = listDependencia2.get(0);
        } else {
            curDependencia2 = null;
            //codi_depe_muni2.setSelectedIndex(-1);
        }

    }


    private void loadDataControls() {
        try {
            
            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            listDependencia2 = obj_Dependencia_dao.get_select(3);
            obj_Dependencia_dao.close();
            if (listDependencia2.size() > 0) {
                curDependencia2 = listDependencia2.get(0);
            } else {
                curDependencia2 = null;
            }

            if (curDependencia2 != null) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                listCargo2 = obj_cargos_dao.get_select(curDependencia2.x);
                obj_cargos_dao.close();
                if (listCargo2.size() > 0) {
                    curCargo2 = listCargo2.get(0);
                } else {
                    curCargo2 = null;
                }
            }
            /*Entidad_dao obj_Entidad_dao = new Entidad_dao();
            listEntidad2 = obj_Entidad_dao.get_select();
            if (listEntidad2.size() > 0) {
                curEntidad2 = listEntidad2.get(0);
            }

            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            listDependencia2 = obj_Dependencia_dao.get_select(curEntidad2.x);
            obj_Dependencia_dao.close();
            if (listDependencia2.size() > 0) {
                curDependencia2 = listDependencia2.get(0);
            } else {
                curDependencia2 = null;
                //codi_depe_muni2.setSelectedIndex(-1);
            }*/


        } catch (Exception ex) {
            Logger.getLogger(expediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Pair<Integer, String>> getListEntidad2() {
        return listEntidad2;
    }

    public void setListEntidad2(List<Pair<Integer, String>> listEntidad2) {
        this.listEntidad2 = listEntidad2;
    }

    public List<Pair<Integer, String>> getListDependencia2() {
        return listDependencia2;
    }

    public void setListDependencia2(List<Pair<Integer, String>> listDependencia2) {
        this.listDependencia2 = listDependencia2;
    }

    public Pair<Integer, String> getCurEntidad2() {
        return curEntidad2;
    }

    public void setCurEntidad2(Pair<Integer, String> curEntidad2) {
        this.curEntidad2 = curEntidad2;
    }

    public Pair<Integer, String> getCurDependencia2() {
        return curDependencia2;
    }

    public void setCurDependencia2(Pair<Integer, String> curDependencia2) {
        this.curDependencia2 = curDependencia2;
    }

    public List<Pair<Integer, String>> getListCargo2() {
        return listCargo2;
    }

    public void setListCargo2(List<Pair<Integer, String>> listCargo2) {
        this.listCargo2 = listCargo2;
    }

    public Pair<Integer, String> getCurCargo2() {
        return curCargo2;
    }

    public void setCurCargo2(Pair<Integer, String> curCargo2) {
        this.curCargo2 = curCargo2;
    }

}
