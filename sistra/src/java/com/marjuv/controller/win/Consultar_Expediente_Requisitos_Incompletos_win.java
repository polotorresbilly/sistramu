/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanUbicacionExpediente;
import static com.marjuv.controller.win.Crear_ReporteUbicacion_Expediente.lista;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.Procedimiento_dao;
import com.marjuv.dao.Tipo_Expediente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.Cargo;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.Entidad_Ubicacion_Expediente;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.Tipo_Expediente;
import com.marjuv.entity.usuario;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

public class Consultar_Expediente_Requisitos_Incompletos_win {

    private List<Procedimiento_Expediente> list_procedimiento;
    private List<Tipo_Expediente> list_tipoexpediente;
    private List<Contribuyente> list_contribuyente;
    private List<BeanDependencias> list_dependencia;
    private List<Entidad> list_entidad;
    private List<ExpedienteCliente> list_expediente;

    private String curNumExp;
    private Procedimiento_Expediente curProcedimiento;
    private Tipo_Expediente curTipoExp;
    private Contribuyente curContribuyente;
    private BeanDependencias curDependencia;
    private Entidad curEntidad;
    private ExpedienteCliente curExpediente;
    // ELECCION DE CHECKBOX
    private int curEleccion;
    private int curAdministrado;

    private Contribuyente_dao dao_contribuyente;
    private Dependencia_dao dao_dependencia;
    private Entidad_dao dao_entidad;
    private Procedimiento_dao dao_procedimiento;
    private Tipo_Expediente_dao dao_tipoexpediente;
    private Expediente_dao dao_expediente;

    @Wire
    private Textbox txtCodiProc;
    @Wire
    private Textbox txtNombProc;

    @WireVariable
    private Session _sess;

    @Wire
    private Textbox txtCodiDestinatario;

    @Wire
    private Textbox txtNombDestinatario;

    @Wire
    private Textbox txtCodiAdministrado;

    @Wire
    private Textbox txtNombAdministrado;

    @Wire
    private Textbox txtNExpediente;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("consultarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Command
    public void cambiar_eleccion(@BindingParam("eleccion") int eleccion) {
        setCurEleccion(eleccion);
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }
    
    @Command
    public void TramitarExpediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        Messagebox.show("Destino:" + curExpediente.getDESTINO());
        map.put("curExpediente", getCurExpediente());
        Executions.createComponents("/ZonaCompartida/tramitarexpedienteT_man.zul", null, map);
    }
    
    @GlobalCommand
    @NotifyChange("list_expediente")
    public void updateTramitarExpediente(@BindingParam("curCargo") Cargo curCargo, @BindingParam("foliosAdd") int folios, @BindingParam("curExpediente") ExpedienteCliente Expediente, @BindingParam("curNProveido") String NProveido, @BindingParam("curRProveido") String RProveido, @BindingParam("curDocumento") boolean curDocumento, @BindingParam("curCosto") Double curCosto, @BindingParam("listVinculados") List<ExpedienteCliente> listVinculados) {
        com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
        try {
            if (listVinculados == null || listVinculados.isEmpty()) {
                if(Expediente.getDESTINO() != 0){
                    dao_expediente.expediente_tramitar(Expediente.getEXPNUM(), Expediente.getCORRELATIVO(), Expediente.getDESTINO(), obj_usuario.getCodi_usua(), Expediente.getFOLIOS(), curCargo.getCodi_carg(), NProveido, RProveido, curDocumento, curCosto);
                }else{
                    Messagebox.show("No existe el destino");
                }
                
                delete_element_list(Expediente.getEXPNUM());
                BindUtils.postNotifyChange(null, null, Consultar_Expediente_Requisitos_Incompletos_win.this, "list_expediente");
                Messagebox.show("Expediente Tramitado",
                        "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
            } else {
                List<ExpedienteCliente> lista_total = dao_expediente.get_expedientes_tramitar(obj_usuario.getCodi_arde(), 1, curNumExp);
                List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
                boolean isPosible = true;
                for (ExpedienteCliente expedienteCliente : listVinculados) {
                    boolean sw = false;
                    for (ExpedienteCliente expedienteCliente1 : lista_total) {
                        if (expedienteCliente.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                            sw = true;
                            lista_agregar.add(expedienteCliente1);
                            break;
                        }
                    }
                    if (!sw) {
                        Messagebox.show("Uno de los expedientes vinculados no se encuentra recepcionado en la dependencia no es posible proceder.");
                        isPosible = false;
                        break;
                    }
                }
                if (isPosible) {
                    for (ExpedienteCliente expedienteCliente : lista_agregar) {
                        dao_expediente.expediente_tramitar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua(), expedienteCliente.getFOLIOS() + folios, curCargo.getCodi_carg(), NProveido, RProveido, curDocumento, curCosto);
                        delete_element_list(expedienteCliente.getEXPNUM());
                        BindUtils.postNotifyChange(null, null, Consultar_Expediente_Requisitos_Incompletos_win.this, "list_expediente");
                    }
                    Messagebox.show( lista_agregar.size() +" Expediente(s) Tramitado(s)",
                            "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
                }
            }
        } catch (Exception ex) {
            Messagebox.show("Ocurrio un error al Tramitar",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
            Logger.getLogger(Ctramitarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete_element_list(String expnum) {
        int cont = 0;
        for (ExpedienteCliente expedienteCliente : list_expediente) {
            if (expedienteCliente.getEXPNUM().equals(expnum)) {
                break;
            }
            cont++;
        }
        if (cont < list_expediente.size()) {
            list_expediente.remove(cont);
        }
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            String fechafin = finalizo.substring(0, 10);
//            // ALTERAR ESTE PROCEDIMIENTO U.U
//            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
//            List<Date> list = getDaysBetweenDates(new java.util.Date(), format.parse(fechafin));
//            if (list.size() <= 0) {
//                fin = "0";
//            }else if (list.size() == 2) {
//                fin = "2";
//            }else{
//                fin = "1";
//            }
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
            Messagebox.show("Error:" + e.getMessage());
        }
        
        return fin;
    }

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate)) {
            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                Date result = calendar.getTime();
                dates.add(result);
            }
            calendar.add(Calendar.DATE, 1);
        }
        return dates;
    }

    @Init
    public void initSetup() {
        check_permisos();
        dao_contribuyente = new Contribuyente_dao();
        dao_dependencia = new Dependencia_dao();
        dao_entidad = new Entidad_dao();
        dao_procedimiento = new Procedimiento_dao();
        dao_tipoexpediente = new Tipo_Expediente_dao();
        dao_expediente = new Expediente_dao();
        try {
            setList_tipoexpediente(dao_tipoexpediente.get_tipo_expedientes());
            setList_procedimiento(dao_procedimiento.get_procedimientos());
            setList_contribuyente(dao_contribuyente.get_contribuyentes());
            setList_dependencia(dao_dependencia.getDependencias());
            setList_entidad(dao_entidad.get_entidades());
            dao_contribuyente.close();
            dao_dependencia.close();
            dao_entidad.close();
            dao_procedimiento.close();
            dao_tipoexpediente.close();
            dao_expediente.close();
            setCurEleccion(1);
            setCurAdministrado(1);
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }

        BindUtils.postNotifyChange(null, null, Consultar_Expediente_Requisitos_Incompletos_win.this, "list_tipoexpediente");
        BindUtils.postNotifyChange(null, null, Consultar_Expediente_Requisitos_Incompletos_win.this, "list_procedimiento");
    }

    @GlobalCommand
    @NotifyChange("")
    public void setProcedimiento_cns(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc) {
        this.txtCodiProc.setValue(String.valueOf(codi_proc));
        this.txtNombProc.setValue(nomb_proc);

    }

    @Command
    public void openProc() {

        Executions.createComponents("proceso/win/add_procedimiento_cns.zul", null, null);
    }

    @NotifyChange("*")
    @Command
    public void openAdministrado() {
        Executions.createComponents("ZonaCompartida/add_administrado.zul", null, null);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setProcedimiento(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc) {
        this.txtCodiProc.setValue(String.valueOf(codi_proc));
        this.txtNombProc.setValue(nomb_proc);
        Procedimiento_Expediente proc = new Procedimiento_Expediente();
        proc.setCodi_proc((codi_proc));
        proc.setNomb_proc(nomb_proc);
        setCurProcedimiento(proc);
        setCurContribuyente(null);
        setCurDependencia(null);
        setCurEntidad(null);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setAdministrado(@BindingParam("tipo") int tipo, @BindingParam("codigo") int codi_cont, @BindingParam("nombre") String nomb_cont) {
        if (tipo == 1) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
            Contribuyente obj = new Contribuyente();
            obj.setCodi_cont(codi_cont);
            setCurProcedimiento(null);
            setCurContribuyente(obj);
            setCurDependencia(null);
            setCurEntidad(null);
            setCurAdministrado(3);
        } else if (tipo == 2) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
            Entidad obj = new Entidad();
            obj.setCodi_enti(codi_cont);
            setCurProcedimiento(null);
            setCurContribuyente(null);
            setCurDependencia(null);
            setCurEntidad(obj);
            setCurAdministrado(2);
        } else if (tipo == 3) {
            this.txtCodiAdministrado.setValue(String.valueOf(codi_cont));
            this.txtNombAdministrado.setValue(nomb_cont);
            BeanDependencias obj = new BeanDependencias();
            obj.setCodi_arde(codi_cont);
            setCurProcedimiento(null);
            setCurContribuyente(null);
            setCurDependencia(obj);
            setCurEntidad(null);
            setCurAdministrado(1);
        } else {
            Messagebox.show("No se ah enviado ningún Administrado");
        }

    }

    @Command
    public void CargarDatos() {
        String parametro = "";
        switch (getCurEleccion()) {
            case 1:
                parametro = getCurNumExp();
                if (parametro == null || parametro.isEmpty()) {
                    Messagebox.show("El Valor no puede estar vacio");
                    return;
                }
                break;
            case 2:
                if (getCurTipoExp() == null) {
                    Messagebox.show("Seleccione un Tipo de Expediente");
                    return;
                }
                parametro = getCurTipoExp().getCodi_tiex() + "";
                break;
            case 3:
                if (txtCodiProc.getValue().isEmpty()) {
                    Messagebox.show("Seleccione un Procedimiento");
                    return;
                }
                parametro = txtCodiProc.getValue() + "";
                break;
            case 4:
                switch (getCurAdministrado()) {
                    case 1:
                        if (getCurDependencia() == null) {
                            Messagebox.show("Seleccione una Dependencia");
                            return;
                        }
                        parametro = getCurDependencia().getCodi_arde() + "";
                        break;
                    case 2:
                        if (getCurEntidad() == null) {
                            Messagebox.show("Seleccione una Entidad");
                            return;
                        }
                        parametro = getCurEntidad().getCodi_enti() + "";
                        break;
                    case 3:
                        if (getCurContribuyente() == null) {
                            Messagebox.show("Seleccione un Contribuyente");
                            return;
                        }
                        parametro = getCurContribuyente().getCodi_cont() + "";
                        break;
                }
                break;
        }
        try {
            dao_contribuyente = new Contribuyente_dao();
            setList_expediente(dao_expediente.get_expedientes_consultar_in(getCurEleccion(), getCurAdministrado(), parametro));
            setCurExpediente(null);
            dao_contribuyente.close();
        } catch (Exception ex) {
            Logger.getLogger(Cconsultarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
        BindUtils.postNotifyChange(null, null, Consultar_Expediente_Requisitos_Incompletos_win.this, "list_expediente");
    }

    @Command
    public void ReporteEstadoActual() {
        try {
            if (getCurExpediente() != null) {

                List<BeanUbicacionExpediente> list = new ArrayList<BeanUbicacionExpediente>();
                Expediente_dao objExpedienteDAO = new Expediente_dao();
                list = objExpedienteDAO.Ubicacion_Actual_Expediente(curExpediente.getEXPNUM());
                objExpedienteDAO.close();

                Map parametros = new HashMap();

                /*Enviar el Administrado y el tipo de Administrado*/
                String Administrado = "";
                String Tipo = "";

                if (list.get(0).getREMITENTE_CARGO_EXPEDIENTE() != null) {
                    Administrado = list.get(0).getREMITENTE_CARGO_EXPEDIENTE();
                    Tipo = "CARGO - INTERNO";

                } else {
                    if (list.get(0).getREMITENTE_CONTRIBUYENTE_EXPEDIENTE() != null) {
                        Administrado = list.get(0).getREMITENTE_CONTRIBUYENTE_EXPEDIENTE();
                        Tipo = "CONTRIBUYENTE";
                    } else {
                        if (list.get(0).getREMITENTE_ENTIDAD_EXPEDIENTE() != null) {
                            Administrado = list.get(0).getREMITENTE_ENTIDAD_EXPEDIENTE();
                            Tipo = "ENTIDAD";
                        }
                    }
                }

                /*Enviar el Nombre del Estado en el cual se encuentra el expedientes*/
                String Estado = "";
                if (list.get(0).getESTADO() == 1) {
                    Estado = "ENVIADO";
                } else {
                    if (list.get(0).getESTADO() == 2) {
                        Estado = "RECEPCIONADO";
                    } else {
                        if (list.get(0).getESTADO() == 3) {
                            Estado = "OBSERVADO";
                        } else {
                            if (list.get(0).getESTADO() == 4) {
                                Estado = "FINALIZADO";
                            } else {
                                if (list.get(0).getESTADO() == 6) {
                                    Estado = "TRAMITADO";
                                }
                            }
                        }
                    }
                }
                parametros.put("prueba", "sdasdsad");
                parametros.put("ADMINISTRADO", Administrado);
                parametros.put("TIPO", Tipo);
                parametros.put("ESTADO", Estado);
                parametros.put("NUMERO_EXPEDIENTE", list.get(0).getNUMERO_EXPEDIENTE());

//                Messagebox.show("nombre area recibe: " + lista.get(0).getNOMBRE_AREA_RECIBE());
                ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
//                Messagebox.show("Número de Expediente: " + getCurExpediente().getEXPNUM());
//
                main.generarReporteEstadoExpediente2("/reportes/report3.jasper", parametros, "Estado Actual Expediente " + getCurExpediente().getEXPNUM() + ".pdf", sv, getCurExpediente().getEXPNUM());
            } else {
                Messagebox.show("Seleccione un expediente");
            }
        } catch (Exception e) {
            Messagebox.show("Error Reporte Estado Actual2: " + e.getMessage());
        }

    }
    /*Reporte Expediente Estado Actual Versión : 2.0 Autor: Andy*/

    @Command
    public void Reporte_Estado_Actual_Expediente() {
        try {
            Map Parametros = new HashMap();
            Parametros.put("numero_expediente", this);
            Messagebox.show("Expediente: " + curExpediente.getEXPNUM().toString());
        } catch (Exception e) {
            Messagebox.show("Error Reporte Estado Actual: " + e.getMessage());
        }
    }

    @Command
    public void open_expe() {
        Map parametros = new HashMap();
        parametros.put("nume_expe", curExpediente.getEXPNUM());
        parametros.put("nude_hist", 1);
        parametros.put("correlativo", curExpediente.getCORRELATIVO());
        Executions.createComponents("proceso/win/win_visualizar_expe.zul", null, parametros);
    }

    @Command
    public void actualizar_eleccion1() {
        setCurEleccion(1);
    }

    @Command
    public void actualizar_eleccion2() {
        setCurEleccion(2);
    }

    @Command
    public void actualizar_eleccion3() {
        setCurEleccion(3);
    }

    @Command
    public void actualizar_eleccion4() {
        setCurEleccion(4);
    }

    @Command
    public void ReporteMovimientos() {
        try {
            if (getCurExpediente() != null) {

                List<BeanUbicacionExpediente> list = new ArrayList<BeanUbicacionExpediente>();
                Expediente_dao objExpedienteDAO = new Expediente_dao();
                list = objExpedienteDAO.Movimientos_Expediente(curExpediente.getEXPNUM());
                objExpedienteDAO.close();

                String Administrado = "";
                String Tipo = "";

                if (list.get(0).getREMITENTE_CARGO_EXPEDIENTE() != null) {
                    Administrado = list.get(0).getREMITENTE_CARGO_EXPEDIENTE();
                    Tipo = "CARGO - INTERNO";

                } else {
                    if (list.get(0).getREMITENTE_CONTRIBUYENTE_EXPEDIENTE() != null) {
                        Administrado = list.get(0).getREMITENTE_CONTRIBUYENTE_EXPEDIENTE();
                        Tipo = "CONTRIBUYENTE";
                    } else {
                        if (list.get(0).getREMITENTE_ENTIDAD_EXPEDIENTE() != null) {
                            Administrado = list.get(0).getREMITENTE_ENTIDAD_EXPEDIENTE();
                            Tipo = "ENTIDAD";
                        }
                    }
                }
                Map parametros = new HashMap();
                parametros.put("ADMINISTRADO", Administrado);
                parametros.put("TIPO", Tipo);
                parametros.put("NUMERO_EXPEDIENTE", list.get(0).getNUMERO_EXPEDIENTE());
                parametros.put("FECHA_INGRESO", list.get(0).getFECHA_INGRESO());
                parametros.put("PROCEDIMIENTO", list.get(0).getPROCEDIMIENTO());
                parametros.put("TIPO_DOCUMENTO", list.get(0).getTIPO_DOCUMENTO());
                parametros.put("NUMERO_DOCUMENTO", list.get(0).getNUMERO_DOCUMENTO());

                ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
                main.generarReporteMovimientoExpediente("/reportes/report4.jasper", parametros, "Movimientos de Expediente " + getCurExpediente().getEXPNUM() + ".pdf", sv, getCurExpediente().getEXPNUM());
            } else {
                Messagebox.show("Seleccione un expediente");
            }
        } catch (Exception e) {
        }

    }

    public Procedimiento_Expediente getCurProcedimiento() {
        return curProcedimiento;
    }

    public void setCurProcedimiento(Procedimiento_Expediente curProcedimiento) {
        this.curProcedimiento = curProcedimiento;
    }

    public Tipo_Expediente getCurTipoExp() {
        return curTipoExp;
    }

    public void setCurTipoExp(Tipo_Expediente curTipoExp) {
        this.curTipoExp = curTipoExp;
    }

    public String getCurNumExp() {
        return curNumExp;
    }

    public void setCurNumExp(String curNumExp) {
        this.curNumExp = curNumExp;
    }

    public List<Procedimiento_Expediente> getList_procedimiento() {
        return list_procedimiento;
    }

    public void setList_procedimiento(List<Procedimiento_Expediente> list_procedimiento) {
        this.list_procedimiento = list_procedimiento;
    }

    public List<Tipo_Expediente> getList_tipoexpediente() {
        return list_tipoexpediente;
    }

    public void setList_tipoexpediente(List<Tipo_Expediente> list_tipoexpediente) {
        this.list_tipoexpediente = list_tipoexpediente;
    }

    public List<Contribuyente> getList_contribuyente() {
        return list_contribuyente;
    }

    public void setList_contribuyente(List<Contribuyente> list_contribuyente) {
        this.list_contribuyente = list_contribuyente;
    }

    public List<BeanDependencias> getList_dependencia() {
        return list_dependencia;
    }

    public void setList_dependencia(List<BeanDependencias> list_dependencia) {
        this.list_dependencia = list_dependencia;
    }

    public List<Entidad> getList_entidad() {
        return list_entidad;
    }

    public void setList_entidad(List<Entidad> list_entidad) {
        this.list_entidad = list_entidad;
    }

    public Contribuyente getCurContribuyente() {
        return curContribuyente;
    }

    public void setCurContribuyente(Contribuyente curContribuyente) {
        this.curContribuyente = curContribuyente;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    public Entidad getCurEntidad() {
        return curEntidad;
    }

    public void setCurEntidad(Entidad curEntidad) {
        this.curEntidad = curEntidad;
    }

    public List<ExpedienteCliente> getList_expediente() {
        return list_expediente;
    }

    public void setList_expediente(List<ExpedienteCliente> list_expediente) {
        this.list_expediente = list_expediente;
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public int getCurEleccion() {
        return curEleccion;
    }

    public void setCurEleccion(int curEleccion) {
        this.curEleccion = curEleccion;
    }

    public int getCurAdministrado() {
        return curAdministrado;
    }

    public void setCurAdministrado(int curAdministrado) {
        this.curAdministrado = curAdministrado;
    }

}
