package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.beans_view.BeanReporteEstadoExpediente;
import com.marjuv.connect.DataTransaction;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.administracion;
import com.marjuv.entity.historial_expediente;
import com.marjuv.entity.usuario;
import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.zkoss.util.CollectionsX;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author Joel
 */
public class ReporteEstadoExpediente extends GenericForwardComposer implements Serializable {

    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;
    private String nume_expe;
    public static List<BeanReporteEstadoExpediente> lista;

    protected DataTransaction dt;
    protected Connection cn;

    public ReporteEstadoExpediente() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    public void onCreate$winReport() throws IOException {
        params = (params == null) ? null : (HashMap) winReport.getAttribute("params");
        name = winReport.getAttribute("name").toString();
        url = winReport.getAttribute("url").toString();
        nume_expe = winReport.getAttribute("nume_expe").toString();
        cargarDatos();
        reportar();
    }

    public void reportar() throws IOException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();

            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            params.put("realPath", sv.getRealPath("/reportes/"));
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, new JRBeanCollectionDataSource(lista, true));

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (is != null) {
                is.close();
            }
        }
    }

    private void cargarDatos() {
        try {
            lista = new ArrayList<BeanReporteEstadoExpediente>();
            List<BeanExpediente> lista_temp;

            Expediente_dao obj_Expediente_dao = new Expediente_dao();
            lista_temp = obj_Expediente_dao.get_expediente(nume_expe);
            obj_Expediente_dao.close();
            BeanReporteEstadoExpediente obj;
            historial_expediente obj_hist;

            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            Contribuyente_dao obj_Contribuyente_dao;
            Entidad_dao obj_Entidad_dao;
            cargos_dao obj_cargos_dao = new cargos_dao();
            usuario_dao obj_usuario_dao = new usuario_dao();
            usuario obj_usua = obj_usuario_dao.get_usuario(lista_temp.get(0).getCodi_usua());

            administracion_dao obj_administracion_dao = new administracion_dao();
            Calendar cal = Calendar.getInstance();
            cal.setTime(lista_temp.get(0).getFech_expe());
            int year = cal.get(Calendar.YEAR);
            administracion obj_admin = obj_administracion_dao.get_administracion(year);
            obj_administracion_dao.close();
            params = new HashMap();
            params.put("noan_admi", obj_admin.getNoaa_admi());
            params.put("noen_admi", obj_admin.getNoen_admi());
            usuario obj_login = (usuario) session.getAttribute("usuario");
            params.put("noar_admi", obj_Dependencia_dao.get_nomb2(obj_login.getCodi_arde()));
            obj_Dependencia_dao.close();
            String tipo_admi = "";
            String enti_admin = "";
            String arde_admin = "";
            String carg_admin = "";
            String nomb_admin = "";
            /*Posible Error*/
            Expediente_dao obj_expe = new Expediente_dao();
            BeanExpediente obj_bean_expe = obj_expe.cargar_expediente(nume_expe);
            obj_expe.close();
            switch (obj_bean_expe.getCodi_enex()) {
                case 1:
                    tipo_admi = "MUNICIPALIDAD DISTRITAL DE HUALMAY";
                    obj_Entidad_dao = new Entidad_dao();
                    enti_admin = obj_Entidad_dao.get_nomb_for_carg(obj_bean_expe.getCodi_carg());
                    obj_Entidad_dao.close();
                    obj_Dependencia_dao = new Dependencia_dao();
                    obj_cargos_dao = new cargos_dao();
                    arde_admin = obj_Dependencia_dao.get_nomb(obj_bean_expe.getCodi_carg());
                    carg_admin = obj_cargos_dao.get_nomb(obj_bean_expe.getCodi_carg());
                    obj_Dependencia_dao.close();
                    obj_cargos_dao.close();
                    break;
                case 2:
                    tipo_admi = "PERSONA NATURAL";
                    obj_Contribuyente_dao = new Contribuyente_dao();
                    nomb_admin = obj_Contribuyente_dao.get_nomb(obj_bean_expe.getCodi_cont());
                    obj_Contribuyente_dao.close();
                    break;
                case 3:
                    tipo_admi = "PERSONA JURÍDICA";
                    obj_Entidad_dao = new Entidad_dao();
                    enti_admin = obj_Entidad_dao.get_nomb_for_carg(obj_bean_expe.getCodi_carg());
                    obj_Entidad_dao.close();
                    break;
                default:
                    break;
            }
            /*Fin*/
            String tipo_remi = "";
            String enti_remi = "";
            String arde_remi = "";
            String carg_remi = "";
            String nomb_remi = "";

            usuario obj_current = (usuario) session.getAttribute("usuario");

            String noms_repo = obj_current.getNoms_usua();

            if (lista_temp.get(0).getCodi_cont() != 0) {
                obj_Contribuyente_dao = new Contribuyente_dao();
                tipo_remi = "CONTRIBUYENTE";
                nomb_remi = obj_Contribuyente_dao.get_nomb(lista_temp.get(0).getCodi_cont());
                obj_Contribuyente_dao.close();
            } else if (lista_temp.get(0).getCodi_enti() != 0) {
                obj_Entidad_dao = new Entidad_dao();
                tipo_remi = "ENTIDAD";
                enti_remi = obj_Entidad_dao.get_nomb(lista_temp.get(0).getCodi_enti());
                obj_Entidad_dao.close();
            } else if (lista_temp.get(0).getCodi_carg() != 0) {
                tipo_remi = "ÁREA DE DEPENDENCIA";
                obj_cargos_dao = new cargos_dao();
                obj_Entidad_dao = new Entidad_dao();
                obj_Dependencia_dao = new Dependencia_dao();
                enti_remi = obj_Entidad_dao.get_nomb_for_carg(lista_temp.get(0).getCodi_carg());
                arde_remi = obj_Dependencia_dao.get_nomb(lista_temp.get(0).getCodi_carg());
                carg_remi = obj_cargos_dao.get_nomb(lista_temp.get(0).getCodi_carg());
                obj_Entidad_dao.close();
                obj_cargos_dao.close();
                obj_Dependencia_dao.close();
            }

            for (int i = 0; i < lista_temp.get(0).getHistorial_expediente().size(); i++) {
                obj_Expediente_dao = new Expediente_dao();
                obj_hist = obj_Expediente_dao.last_desti_hist(nume_expe, lista_temp.get(0).getHistorial_expediente().get(i).getNude_hist());
                obj_Expediente_dao.close();
                obj = new BeanReporteEstadoExpediente();

                obj.setNume_expe(nume_expe);

                obj.setTipo_admi(tipo_admi);
                obj.setEnti_admin(enti_admin);
                obj.setArde_admin(arde_admin);
                obj.setCarg_admin(carg_admin);
                obj.setNomb_admin(nomb_admin);

                obj.setTipo_remi(tipo_remi);
                obj.setEnti_remi(enti_remi);
                obj.setArde_remi(arde_remi);
                obj.setCarg_remi(carg_remi);
                obj.setNomb_remi(nomb_remi);

                if (obj_hist.getCore_hist() != 0) {
                    obj_Contribuyente_dao = new Contribuyente_dao();
                    obj.setTipo_dest("CONTRIBUYENTE");
                    obj.setNomb_dest(obj_Contribuyente_dao.get_nomb(obj_hist.getCore_hist()));
                    obj_Contribuyente_dao.close();
                } else if (obj_hist.getEnre_hist() != 0) {
                    obj_Entidad_dao = new Entidad_dao();
                    obj.setTipo_dest("ENTIDAD");
                    obj.setEnti_dest(obj_Entidad_dao.get_nomb(obj_hist.getEnre_hist()));
                    obj_Entidad_dao.close();
                } else if (obj_hist.getCare_hist() != 0) {
                    obj_cargos_dao = new cargos_dao();
                    obj_Entidad_dao = new Entidad_dao();
                    obj_Dependencia_dao = new Dependencia_dao();
                    obj.setTipo_dest("ÁREA DE DEPENDENCIA");
                    obj.setEnti_dest(obj_Entidad_dao.get_nomb_for_carg(obj_hist.getCare_hist()));
                    obj.setArde_dest(obj_Dependencia_dao.get_nomb(obj_hist.getCare_hist()));
                    obj.setCarg_dest(obj_cargos_dao.get_nomb(obj_hist.getCare_hist()));
                    obj_Entidad_dao.close();
                    obj_cargos_dao.close();
                }

                switch (obj_hist.getEsta_hist()) {
                    case 0:
                        obj.setEsta_hist("RECEPCIONADO");
                        break;
                    case 1:
                        obj.setEsta_hist("ENVIADO");
                        break;
                    case 2:
                        obj.setEsta_hist("PENDIENTE");
                        break;
                    case 3:
                        obj.setEsta_hist("OBSERVADO");
                        break;
                    case 4:
                        obj.setEsta_hist("FINALIZADO");
                        break;
                    case 5:
                        obj.setEsta_hist("REENVIADO");
                        break;
                    case 6:
                        obj.setEsta_hist("TRAMITADO");
                        break;
                    default:
                        break;
                }

                obj.setNupr_hist(obj_hist.getNupr_hist());
                obj.setNufo_hist(obj_hist.getNufo_hist());
                obj.setFell_hist(obj_hist.getFell_hist());
                obj.setHoll_hist(new java.text.SimpleDateFormat("HH:mm").format(obj.getFell_hist()));

                if (obj_hist.getFere_hist() != null) {
                    obj.setFere_hist(obj_hist.getFere_hist());
                    obj.setHore_hist(new java.text.SimpleDateFormat("HH:mm").format(obj.getFere_hist()));
                }
                obj.setObsv_hist(obj_hist.getObsv_hist());
                obj.setFech_repo(new java.util.Date());
                obj.setNoms_repo(noms_repo);
                obj.setUbic_hist(obj_Dependencia_dao.get_nomb(obj_hist.getCare_hist()));
                obj_Dependencia_dao.close();
                lista.add(obj);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            
        }
    }
}
