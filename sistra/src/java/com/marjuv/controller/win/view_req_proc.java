package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.dao.requisito_procedimiento_dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.Message;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class view_req_proc {

    @Wire("#view_win_req")
    private Window win;
    private List<BeanRequisitoMan> lst_sub_requisitos;
    private BeanRequisitoMan BeanReq;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("curRequerimiento") int codi_repr) throws Exception {
        requisito_procedimiento_dao ObjRequisitoDAO = new requisito_procedimiento_dao();
        if (codi_repr == 0) {
            Messagebox.show("No cuenta con Sub - Requisitos");
        } else {
            lst_sub_requisitos = new ArrayList<BeanRequisitoMan>();
            lst_sub_requisitos = ObjRequisitoDAO.get_requisito_Jerarquia(codi_repr);
        }

    }

    @Command
    public void close() {
        win.detach();
    }

    public List<BeanRequisitoMan> getLst_sub_requisitos() {
        return lst_sub_requisitos;
    }

    public void setLst_sub_requisitos(List<BeanRequisitoMan> lst_sub_requisitos) {
        this.lst_sub_requisitos = lst_sub_requisitos;
    }

    public BeanRequisitoMan getBeanReq() {
        return BeanReq;
    }

    public void setBeanReq(BeanRequisitoMan BeanReq) {
        this.BeanReq = BeanReq;
    }
}
