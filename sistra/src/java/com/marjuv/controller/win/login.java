package com.marjuv.controller.win;

import com.marjuv.dao.usuario_dao;
import com.marjuv.beans_view.BeanLogin;
import com.marjuv.entity.usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import javax.servlet.ServletContext;
import org.apache.log4j.Level;
import org.docx4j.Docx4jProperties;
import org.docx4j.convert.out.pdf.PdfConversion;
import org.docx4j.convert.out.pdf.viaXSLFO.PdfSettings;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.utils.Log4jConfigurator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class login {

    private BeanLogin bean;
    @WireVariable
    private Session _sess;
    private static Session session_temp;

    @Init
    public void initSetup() {
        bean = new BeanLogin();
        session_temp = _sess;
        //createPDF();
    }

    private static void createPDF() {
        try {
            Docx4jProperties.getProperties().setProperty("docx4j.Log4j.Configurator.disabled", "true");
            Log4jConfigurator.configure();
            org.docx4j.convert.out.pdf.viaXSLFO.Conversion.log.setLevel(Level.OFF);

            long start = System.currentTimeMillis();

            ServletContext sv = (ServletContext) session_temp.getWebApp().getNativeContext();
            // 1) Load DOCX into WordprocessingMLPackage
            InputStream is = new FileInputStream(new File(sv.getRealPath("/docx/")
                    + "/HelloWorld.docx"));
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage
                    .load(is);

            // 2) Prepare Pdf settings
            PdfSettings pdfSettings = new PdfSettings();

            // 3) Convert WordprocessingMLPackage to Pdf
            OutputStream out = new FileOutputStream(new File(sv.getRealPath("/pdf/") + "/HelloWorld.pdf"));
            PdfConversion converter = new org.docx4j.convert.out.pdf.viaXSLFO.Conversion(
                    wordMLPackage);
            converter.output(out, pdfSettings);

            System.err.println("Generate pdf/HelloWorld.pdf with "
                    + (System.currentTimeMillis() - start) + "ms");

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Command
    public void save() {
        try {
            usuario_dao obj_usuario_dao = new usuario_dao();
            usuario obj_usuario = new usuario();
            obj_usuario.setNick_usua(bean.getUsuario());
            obj_usuario.setPass_usua(bean.getClave());
            obj_usuario.setCodi_usua(obj_usuario_dao.validar_usuario(obj_usuario));
            if (obj_usuario.getCodi_usua() != -1) {
                obj_usuario.setCodi_rous(obj_usuario_dao.get_usuario(obj_usuario.getCodi_usua()).getCodi_rous());
                _sess.setAttribute("usuario", obj_usuario_dao.get_usuario(obj_usuario.getCodi_usua()));
                _sess.setAttribute("acceso", 1);
                obj_usuario_dao.close();
                Executions.sendRedirect("page.zul");
            } else {
                Messagebox.show("Usuario y/o contraseña incorrectos",
                        "Login", Messagebox.OK, Messagebox.ERROR);
            }
            obj_usuario_dao.close();
        } catch (Exception ex) {
            Messagebox.show("A Ocurrido un error Inesperado al Consultar sus Datos! \n" + ex.getMessage(),
                    "Login", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public BeanLogin getBean() {
        return bean;
    }

    public void setBean(BeanLogin bean) {
        this.bean = bean;
    }
}
