/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.administracion;
import com.marjuv.entity.usuario;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class CtramitarexpedienteO_man {

    @Wire("#tramitarexpedienteO_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private String curAObservacion;
    private String curNObservacion;

    @WireVariable
    private Session _sess;
    private List<ExpedienteCliente> listVinculados;

    private Media media;
    private String file_name;
    private String ext;

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public List<ExpedienteCliente> getListVinculados() {
        return listVinculados;
    }

    public void setListVinculados(List<ExpedienteCliente> listVinculados) {
        this.listVinculados = listVinculados;
    }

    public int get_total_vinculados() {
        return listVinculados.size();
    }

    boolean exists = true;

    @NotifyChange({"media", "file_name"})
    @Command
    public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
        if (event.getMedia() != null) {
            //Messagebox.show(event.getMedia().getContentType());
            if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                    || event.getMedia().getContentType().equals("application/msword")
                    || event.getMedia().getContentType().equals("application/vnd.ms-excel")
                    || event.getMedia().getContentType().equals("application/pdf")
                    || event.getMedia().getContentType().equals("application/x-zip-compressed")
                    || event.getMedia().getContentType().equals("application/x-rar-compressed")
                    || event.getMedia().getContentType().equals("application/zip")
                    || event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                String extension = "";
                if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                    extension = "docx";
                } else if (event.getMedia().getContentType().equals("application/msword")) {
                    extension = "doc";
                } else if (event.getMedia().getContentType().equals("application/vnd.ms-excel")) {
                    extension = "xls";
                } else if (event.getMedia().getContentType().equals("application/pdf")) {
                    extension = "pdf";
                } else if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                    extension = "xlsx";
                } else if (event.getMedia().getContentType().equals("application/zip")) {
                    extension = "zip";
                } else if (event.getMedia().getContentType().equals("application/x-zip-compressed")) {
                    extension = "zip";
                } else if (event.getMedia().getContentType().equals("application/x-rar-compressed")) {
                    extension = "rar";
                }
                media = event.getMedia();
                if (listVinculados != null && !listVinculados.isEmpty()) {

                }
                file_name = curExpediente.getEXPNUM() + "_" + curExpediente.getDESTINO()
                        + "_" + (curExpediente.getCORRELATIVO()) + "." + extension;
                ext = extension;
            } else {
                Messagebox.show("Tipo de archivo no válido. Sólo se permite *.zip,*.rar ,*.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"media", "file_name"})
    public void clearupload() {
        media = null;
        file_name = "";
    }

    public void guardar_file() {
        try {
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin()+"\\cartas\\";
            obj_administracion_dao.close();
            ArrayList<File> files = new ArrayList<File>();
            for (ExpedienteCliente exp : listVinculados) {
                String[] correlativos = {};
                int correlativo = 0;
                if (exp.getCORRELATIVOS().length() > 0 && exp.getCORRELATIVOS().contains(",")) {
                    correlativos = exp.getCORRELATIVOS().trim().split(",");
                    correlativo = Integer.parseInt(correlativos[correlativos.length - 1]);
                } else if (exp.getCORRELATIVOS().length() > 0) {
                    correlativo = Integer.parseInt(exp.getCORRELATIVOS().trim());
                }
                File fi = new File(URL_FILE + "/" + exp.getEXPNUM() + "_" + exp.getDESTINO()
//                File fi = new File(sv.getRealPath("/archivos/") + "/" + exp.getEXPNUM() + "_" + exp.getDESTINO()
                        + "_" + (correlativo) + "." + ext);
                files.add(fi);
            }

            if (files.isEmpty()) {
                File destino = new File(URL_FILE + "/" + file_name);

                if (destino.exists()) {
                    Messagebox.show("El archivo " + file_name + " existe, ¿Está seguro de que desea reemplazar?", "Confirmarción", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                        @Override
                        public void onEvent(Event e) {
                            if (Messagebox.ON_YES.equals(e.getName())) {
                                exists = true;
                            } else {
                                exists = false;
                            }
                        }
                    });
                }

                if (exists) {
                    if (media != null) {
                        if (media.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                                || media.getContentType().equals("application/msword")
                                || media.getContentType().equals("application/vnd.ms-excel")
                                || media.getContentType().equals("application/x-zip-compressed")
                                || media.getContentType().equals("application/x-rar-compressed")
                                || media.getContentType().equals("application/zip")
                                || media.getContentType().equals("application/pdf")
                                || media.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            try {
                                Files.copy(destino, media.getStreamData());
                            } catch (Exception e) {
                            }
                        } else {
                            Messagebox.show("Tipo de archivo no válido. Sólo se permite *.zip,*.rar ,*.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                        }
                    }

                }
            } else {
                for (File file : files) {
                    if (media != null) {
                        if (media.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                                || media.getContentType().equals("application/msword")
                                || media.getContentType().equals("application/vnd.ms-excel")
                                || media.getContentType().equals("application/pdf")
                                || media.getContentType().equals("application/x-zip-compressed")
                                || media.getContentType().equals("application/x-rar-compressed")
                                || media.getContentType().equals("application/zip")
                                || media.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            try {
                                Files.copy(file, media.getStreamData());
                            } catch (Exception e) {
                            }
                        } else {
                            //Messagebox.show("Tipo de archivo no válido. Sólo se permite *.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Command
    public void ver_expe() {
        Map parametros = new HashMap();
        parametros.put("list", listVinculados);
        Executions.createComponents("ZonaCompartida/win_visualizar_cargo.zul", null, parametros);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("tramitarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1)
            throws CloneNotSupportedException, Exception {
        check_permisos();
        Expediente_dao dao_exp = new Expediente_dao();
        setListVinculados(dao_exp.get_expediente_vinculados_total(c1.getEXPNUM(), c1.getDESTINO()));
        dao_exp.close();
        Selectors.wireComponents(view, this, false);
        curExpediente = c1;
        setCurNObservacion("");
        win.setTitle("Observando Expediente N° : " + curExpediente.getEXPNUM());
        setCurAObservacion(curExpediente.getOBSERVACION());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (!curNObservacion.isEmpty()) {
            Messagebox.show("Se observará el expediente  " + curExpediente.getEXPNUM() + ((listVinculados != null && !listVinculados.isEmpty()) ? " y sus vinculaciones" : "")
                    + "\n ¿Desea continuar?", "Observar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                        public void onEvent(Event e) throws SQLException {
                            if (Messagebox.ON_YES.equals(e.getName())) {
                                Map args = new HashMap();
                                args.put("curNObservacion", curNObservacion);
                                args.put("curExpediente", curExpediente);
                                args.put("listVinculados", listVinculados);
                                guardar_file();
                                BindUtils.postGlobalCommand(null, null, "updateObservarExpediente", args);
                                win.detach();
                            }
                        }
                    });

        } else {
            Messagebox.show("Debe de escribir un motivo de Observacion",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    @Command
    public void cancelar() {
        win.detach();
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public String getCurAObservacion() {
        return curAObservacion;
    }

    public void setCurAObservacion(String curAObservacion) {
        this.curAObservacion = curAObservacion;
    }

    public String getCurNObservacion() {
        return curNObservacion;
    }

    public void setCurNObservacion(String curNObservacion) {
        this.curNObservacion = curNObservacion;
    }

}
