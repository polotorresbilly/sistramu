package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDestinatarioRes;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;
import org.zkoss.zul.Window;

public class search_cargo_win {

    @Wire("#search_cargo")
    private Window win;
    private List<BeanDestinatarioRes> lst_des;
    private BeanDestinatarioRes cur_des;
    @WireVariable
    private Session _sess;
    private int codi_reso;

    

    Timebox asd;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("codi_reso") int codi_reso) throws SQLException, Exception {
        Selectors.wireComponents(view, this, false);
        this.codi_reso = codi_reso;
        
    }

    public List<BeanDestinatarioRes> getLst_des() throws Exception {
        resolucion_dao obj_des_dao = new resolucion_dao();
        lst_des = obj_des_dao.search_destinatarios(codi_reso);
        obj_des_dao.close();

        for (int i = 0; i < lst_des.size(); i++) {
            for (int j = 0; j < lst_des.get(i).getList_pare().size(); j++) {
                if (lst_des.get(i).getCodi_pare() == lst_des.get(i).getList_pare().get(j).getX()) {
                    lst_des.get(i).setCur_pare(lst_des.get(i).getList_pare().get(j));
                    break;
                }
            }
        }
        return lst_des;
    }

    @Command
    public void search_usua() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("codi_dire", cur_des.getCodi_dire());
        if (cur_des.getTipo()==3) {
            map.put("codi_arde", cur_des.getCodi_arde());
        }
        Executions.createComponents("proceso/win/search_usua_des.zul", null, map);
    }
    
    @NotifyChange("*")
    @Command
    public void delete_usua() throws Exception {
        resolucion_dao obj_resolucion_dao = new resolucion_dao();
        obj_resolucion_dao.clear_usuario(cur_des.getCodi_dire());
        obj_resolucion_dao.close();
    }
    
    @GlobalCommand
    @NotifyChange("*")
    public void setRecepciona(@BindingParam("usuario") usuario cur_usuario,
            @BindingParam("codi_dire") int codi_dire) throws SQLException, Exception {
        for (int i = 0; i < lst_des.size(); i++) {
            if (lst_des.get(i).getCodi_dire() == codi_dire) {
                lst_des.get(i).setNoms_usua(cur_usuario.getNoms_usua());
                lst_des.get(i).setDni_usua(String.valueOf(cur_usuario.getDni_usua()));
                break;
            }
        }
        resolucion_dao obj_resolucion_dao = new resolucion_dao();
        obj_resolucion_dao.update_usuario(cur_usuario.getCodi_usua(), cur_usuario.getNoms_usua(), String.valueOf(cur_usuario.getDni_usua()), codi_dire);
        obj_resolucion_dao.close();
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("des", this.cur_des);
        BindUtils.postGlobalCommand(null, null, "setResolucion", args);
        win.detach();
    }

    public void setLst_des(List<BeanDestinatarioRes> lst_des) {
        this.lst_des = lst_des;
    }

    public BeanDestinatarioRes getCur_des() {
        return cur_des;
    }

    public void setCur_des(BeanDestinatarioRes cur_des) {
        this.cur_des = cur_des;
    }


}
