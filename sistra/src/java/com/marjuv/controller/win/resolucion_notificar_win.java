package com.marjuv.controller.win;

import static com.marjuv.controller.win.main.generarReporte;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.administracion;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class resolucion_notificar_win {

    @Wire("#notificacion_res")
    private Window win;
    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> lst_tire, lst_asre;
    private Pair<Integer, String> cur_tire, cur_asre;

    private List<resolucion> lst_resolucion;
    private resolucion cur_resolucion;

    private resolucion resolucion_selected;

    @Wire
    private Checkbox chk_gen;
    @Wire
    private Checkbox chk_res;
    @Wire
    private Checkbox chk_asu;
    @Wire
    private Checkbox chk_exp;
    @Wire
    private Checkbox chk_nre;
    @Wire
    private Checkbox chk_tas;

    private String tipo;

    private usuario obj_usua;

    @Wire
    private Datebox fech_reso1;
    @Wire
    private Datebox fech_reso2;

    private boolean select_all_st;

    @Wire
    private Textbox nume_expe;
    @Wire
    private Textbox nume_reso;
    @Wire
    private Textbox asun_reso;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) throws Exception {
        Selectors.wireComponents(view, this, false);

        fech_reso1.setValue(new java.util.Date());
        fech_reso2.setValue(new java.util.Date());
        search_reso();
    }

    @NotifyChange("*")
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        loadDataControls();
        lst_resolucion = new ArrayList<resolucion>();
        tipo = "gen";
        select_all_st = false;
    }

    @NotifyChange("*")
    @Command
    public void marcar() {
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCodi_reso() == cur_resolucion.getCodi_reso()) {
                if (lst_resolucion.get(i).getCheck()) {
                    lst_resolucion.get(i).setCheck(false);
                } else {
                    lst_resolucion.get(i).setCheck(true);
                }
                break;
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void search_reso() throws SQLException, Exception {
        usuario obj_usua2 = (usuario) _sess.getAttribute("usuario");
        if (tipo.equals("gen")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_gen("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), "usre_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("res")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_res("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_tire.x, "usre_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("asu")) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            lst_resolucion = obj_resolucion_dao.search_resolucion_asu("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_asre.x, "usre_reso", obj_usua2.getCodi_arde());
            obj_resolucion_dao.close();
            if (lst_resolucion.size() == 0) {
                Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
        } else if (tipo.equals("exp")) {
            if (!nume_expe.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_exp("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_expe.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el número de expediente", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (tipo.equals("tas")) {
            if (!asun_reso.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_tas("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), asun_reso.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el asunto de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (tipo.equals("nre")) {
            if (!nume_reso.getValue().isEmpty()) {
                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_nre("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_reso.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
                if (lst_resolucion.size() == 0) {
                    Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Por favor, ingrese el número de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void restaurar() throws SQLException, Exception {
        boolean sw = false;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                sw = true;
                break;
            }
        }
        if (sw) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            obj_resolucion_dao.restaurar(lst_resolucion);
            obj_resolucion_dao.close();
            usuario obj_usua2 = (usuario) _sess.getAttribute("usuario");
            if (tipo.equals("gen")) {
                obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_gen("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
            } else if (tipo.equals("res")) {
                obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_res("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_tire.x, "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
            } else if (tipo.equals("asu")) {
                obj_resolucion_dao = new resolucion_dao();
                lst_resolucion = obj_resolucion_dao.search_resolucion_asu("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), cur_asre.x, "usre_reso", obj_usua2.getCodi_arde());
                obj_resolucion_dao.close();
            } else if (tipo.equals("exp")) {
                if (!nume_expe.getValue().isEmpty()) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_exp("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_expe.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                    if (lst_resolucion.size() == 0) {
                        Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                    }
                } else {
                    Messagebox.show("Por favor, ingrese el número de expediente", "Error", Messagebox.OK, Messagebox.ERROR);
                }
            } else if (tipo.equals("tas")) {
                if (!asun_reso.getValue().isEmpty()) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_tas("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), asun_reso.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                    if (lst_resolucion.size() == 0) {
                        Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                    }
                } else {
                    Messagebox.show("Por favor, ingrese el asunto de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
                }
            } else if (tipo.equals("nre")) {
                if (!nume_reso.getValue().isEmpty()) {
                    obj_resolucion_dao = new resolucion_dao();
                    lst_resolucion = obj_resolucion_dao.search_resolucion_nre("2", new java.sql.Date(fech_reso1.getValue().getTime()), new java.sql.Date(fech_reso2.getValue().getTime()), nume_reso.getValue(), "usre_reso", obj_usua2.getCodi_arde());
                    obj_resolucion_dao.close();
                    if (lst_resolucion.size() == 0) {
                        Messagebox.show("No se encontró registros", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                    }
                } else {
                    Messagebox.show("Por favor, ingrese el número de resolución", "Error", Messagebox.OK, Messagebox.ERROR);
                }
            }

            Messagebox.show("Las resoluciones han sido restaurados con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
        } else {
            Messagebox.show("Por favor, marque al menos una resolución", "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @NotifyChange("*")
    @Command
    public void select() throws SQLException, Exception {
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCodi_reso() == cur_resolucion.getCodi_reso()) {
                if (cur_resolucion.getCheck()) {
                    lst_resolucion.get(i).setCheck(false);
                } else {
                    lst_resolucion.get(i).setCheck(true);
                }
            } else {
                lst_resolucion.get(i).setCheck(false);
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void changeGen() {
        if (chk_gen.isChecked()) {
            //chk_asu.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "gen";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeRes() {
        if (chk_res.isChecked()) {
            chk_gen.setChecked(false);
            //chk_asu.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "res";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeAsu() {
        if (chk_asu.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "asu";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeExp() {
        if (chk_exp.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            chk_nre.setChecked(false);
            //chk_asu.setChecked(false);
            tipo = "exp";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeNre() {
        if (chk_nre.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_tas.setChecked(false);
            //chk_asu.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "nre";
        } else {
            tipo = "";
        }
    }

    @NotifyChange("*")
    @Command
    public void changeTas() {
        if (chk_tas.isChecked()) {
            chk_gen.setChecked(false);
            chk_res.setChecked(false);
            //chk_asu.setChecked(false);
            chk_nre.setChecked(false);
            chk_exp.setChecked(false);
            tipo = "tas";
        } else {
            tipo = "";
        }
    }

    @Command
    public void visualizar() throws Exception {
        boolean sw = false;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                sw = true;
                resolucion_selected = lst_resolucion.get(i);
                break;
            }
        }
        if (sw) {
//            Map parametros = new HashMap();
//            parametros.put("codi_reso", resolucion_selected.getCodi_reso());
//            for (int i = 0; i < lst_tire.size(); i++) {
//                if (lst_tire.get(i).x == resolucion_selected.getCodi_tire()) {
//                    parametros.put("nomb_tire", lst_tire.get(i).y);
//                    break;
//                }
//            }
//            administracion_dao obj_administracion_dao = new administracion_dao();
//            Calendar cal = Calendar.getInstance();
//            cal.setTime(resolucion_selected.getFeem_reso());
//            int year = cal.get(Calendar.YEAR);
//            administracion obj_admin = obj_administracion_dao.get_administracion(year);
//
//            parametros.put("noan_admi", obj_admin.getNoaa_admi());
//            parametros.put("noen_admi", obj_admin.getNoen_admi());
//            usuario obj_login = (usuario) _sess.getAttribute("usuario");
//            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
//            parametros.put("noar_admi", obj_Dependencia_dao.get_nomb2(obj_login.getCodi_arde()));
//            
//            parametros.put("nume_reso", resolucion_selected.getNume_reso());
//            parametros.put("feem_reso", resolucion_selected.getFeem_reso());
//            parametros.put("hoja_reso", resolucion_selected.getHoja_reso());
//            int ejem_reso = 0;
//            for (int i = 0; i < resolucion_selected.getDestinos().size(); i++) {
//                ejem_reso += resolucion_selected.getDestinos().get(i).getCopias();
//            }
//            parametros.put("ejem_reso", ejem_reso);
//            parametros.put("fesa_reso", resolucion_selected.getFesa_reso());
//            parametros.put("asun_reso", resolucion_selected.getAsun_reso());
//            for (int i = 0; i < lst_asre.size(); i++) {
//                if (lst_asre.get(i).x == resolucion_selected.getCodi_asre()) {
//                    parametros.put("asre_reso", lst_asre.get(i).y);
//                    break;
//                }
//            }
//            Expediente_dao objExpediente_dao = new Expediente_dao();
//            parametros.put("expe_adj", objExpediente_dao.get_exp_adj(resolucion_selected.getCodi_reso()));
//            objExpediente_dao.close();
//            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
//            generarReporte("/reportes/Visualizar_Cargo_Resolucion.jasper", parametros, "Visualizar cargo " + resolucion_selected.getNume_reso() + ".pdf", sv);

            if (resolucion_selected!= null) {
                Map parametros = new HashMap();

                ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
                main.generarReporteCargoResExpediente("/reportes/R_Acuse_Resolucion.jasper", parametros, "Cargo Resolución: " + resolucion_selected.getNume_reso()+ ".pdf", sv, resolucion_selected.getCodi_reso()+"");
            } else {
                Messagebox.show("Seleccione un expediente");
            }
        } else {
            Messagebox.show("Por favor, marque al menos una resolución", "Error", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void actualizar() {
        boolean sw = false;
        int codi_reso = -1;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                sw = true;
                codi_reso = lst_resolucion.get(i).getCodi_reso();
                break;
            }
        }
        if (sw) {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("codi_reso", codi_reso);
            Executions.createComponents("proceso/win/search_cargo.zul", null, map);
        } else {
            Messagebox.show("Por favor, marque al menos una resolución", "Error", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @NotifyChange("*")
    private void loadDataControls() {
        try {

            general_dao obj_general_dao = new general_dao();
            lst_tire = obj_general_dao.get_select_tipo_resolucion();
            if (lst_tire.size() > 0) {
                cur_tire = lst_tire.get(0);
            } else {
                cur_tire = null;
            }

            Asunto_Resolucion_dao obj_Asunto_Resolucion_dao = new Asunto_Resolucion_dao();
            lst_asre = obj_Asunto_Resolucion_dao.get_select();
            if (lst_asre.size() > 0) {
                cur_asre = lst_asre.get(0);
            } else {
                cur_asre = null;
            }
        } catch (Exception ex) {
            Logger.getLogger(resolucion_notificar_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Pair<Integer, String>> getLst_tire() {
        return lst_tire;
    }

    public void setLst_tire(List<Pair<Integer, String>> lst_tire) {
        this.lst_tire = lst_tire;
    }

    public Pair<Integer, String> getCur_tire() {
        return cur_tire;
    }

    public void setCur_tire(Pair<Integer, String> cur_tire) {
        this.cur_tire = cur_tire;
    }

    public List<Pair<Integer, String>> getLst_asre() {
        return lst_asre;
    }

    public void setLst_asre(List<Pair<Integer, String>> lst_asre) {
        this.lst_asre = lst_asre;
    }

    public Pair<Integer, String> getCur_asre() {
        return cur_asre;
    }

    public void setCur_asre(Pair<Integer, String> cur_asre) {
        this.cur_asre = cur_asre;
    }

    public List<resolucion> getLst_resolucion() {
        return lst_resolucion;
    }

    public void setLst_resolucion(List<resolucion> lst_resolucion) {
        this.lst_resolucion = lst_resolucion;
    }

    public resolucion getCur_resolucion() {
        return cur_resolucion;
    }

    public void setCur_resolucion(resolucion cur_resolucion) {
        this.cur_resolucion = cur_resolucion;
    }

    public usuario getObj_usua() {
        return obj_usua;
    }

    public void setObj_usua(usuario obj_usua) {
        this.obj_usua = obj_usua;
    }

}
