package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.beans_view.BeanReporteEstadoExpediente;
import com.marjuv.connect.DataTransaction;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;
import com.marjuv.entity.administracion;
import com.marjuv.entity.historial_expediente;
import com.marjuv.entity.usuario;
import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.zkoss.util.CollectionsX;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author Joel
 */
public class ReporteMovimientoExpediente extends GenericForwardComposer implements Serializable {

    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;
    private String nume_expe;
    public static List<ExpedienteMovimientos> lista;

    protected DataTransaction dt;
    protected Connection cn;

    public ReporteMovimientoExpediente() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    public void onCreate$winReport() throws IOException {

        name = winReport.getAttribute("name").toString();
        url = winReport.getAttribute("url").toString();
        nume_expe = winReport.getAttribute("nume_expe").toString();
        cargarDatos();
        reportar();
    }

    public void reportar() throws IOException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();
            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            params.put("realPath", sv.getRealPath("/reportes/"));
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, new JRBeanCollectionDataSource(lista, true));

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (is != null) {
                is.close();
            }
        }
    }

    private void cargarDatos() {
        try {
            Expediente_dao obj_Expediente_dao = new Expediente_dao();
            lista = obj_Expediente_dao.get_expedientes_movimientos(nume_expe);
            obj_Expediente_dao.close();

            obj_Expediente_dao = new Expediente_dao();
            List<ExpedienteCliente> cabecera = obj_Expediente_dao.get_expedientes_consultar_unico(nume_expe);
            obj_Expediente_dao.close();

            obj_Expediente_dao = new Expediente_dao();
            List<BeanExpediente> lista_temp = obj_Expediente_dao.get_expediente(nume_expe);
            obj_Expediente_dao.close();
            
            
            //usuario obj_usua = obj_usuario_dao.get_usuario(lista_temp.get(0).getCodi_usua());
            
            administracion_dao obj_administracion_dao = new administracion_dao();
            Calendar cal = Calendar.getInstance();
            cal.setTime(lista_temp.get(0).getFech_expe());
            int year = cal.get(Calendar.YEAR);
            administracion obj_admin = obj_administracion_dao.get_administracion(year);
            obj_administracion_dao.close();
            
            
            String tipo_admi = "";
            String enti_admin = "";
            String arde_admin = "";
            String carg_admin = "";
            String nomb_admin = "";
            
            Dependencia_dao obj_Dependencia_dao = new Dependencia_dao();
            cargos_dao obj_cargos_dao = new cargos_dao();
            Expediente_dao obj_expe = new Expediente_dao();
            BeanExpediente obj_bean_expe = obj_expe.cargar_expediente(nume_expe);
            obj_expe.close();
            switch (obj_bean_expe.getCodi_enex()) {
                case 1:
                    tipo_admi = "MUNICIPALIDAD DISTRITAL DE HUALMAY";
                    Entidad_dao obj_Entidad_dao = new Entidad_dao();
                    enti_admin = obj_Entidad_dao.get_nomb_for_carg(obj_bean_expe.getCodi_carg());
                    obj_Entidad_dao.close();
                    arde_admin = obj_Dependencia_dao.get_nomb(obj_bean_expe.getCodi_carg());
                    carg_admin = obj_cargos_dao.get_nomb(obj_bean_expe.getCodi_carg());
                    break;
                case 2:
                    tipo_admi = "PERSONA NATURAL";
                    Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
                    nomb_admin = obj_Contribuyente_dao.get_nomb(obj_bean_expe.getCodi_cont());
                    obj_Contribuyente_dao.close();
                    break;
                case 3:
                    tipo_admi = "PERSONA JURÍDICA";
                    Entidad_dao obj_Entidad_dao2 = new Entidad_dao();
                    enti_admin = obj_Entidad_dao2.get_nomb_for_carg(obj_bean_expe.getCodi_carg());
                    obj_Entidad_dao2.close();
                    break;
                default:
                    break;
            }

            params = new HashMap();
            params.put("nume_expe", nume_expe);
            params.put("tipo", tipo_admi);
            params.put("entidad", enti_admin);
            params.put("dependencia", arde_admin);
            params.put("personal", carg_admin);
            params.put("nombre", nomb_admin);
            params.put("documento", cabecera.get(0).getDOCUMENTO());
            params.put("asunto", cabecera.get(0).getPROCEDIMIENTO());
            params.put("fecha", lista_temp.get(0).getFech_expe());
            params.put("noan_admi", obj_admin.getNoaa_admi());
            params.put("noen_admi", obj_admin.getNoen_admi());
            usuario obj_login = (usuario) session.getAttribute("usuario");
            params.put("noar_admi", obj_Dependencia_dao.get_nomb2(obj_login.getCodi_arde()));
            obj_cargos_dao.close();
            obj_Dependencia_dao.close();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
