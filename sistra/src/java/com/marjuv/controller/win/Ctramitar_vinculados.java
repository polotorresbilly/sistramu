/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javax.servlet.ServletContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author YO
 */
public class Ctramitar_vinculados {
    @Wire("#win_vis_expe")
    private Window win;

    private List<ExpedienteCliente> lst_expe;
    @WireVariable
    private Session _sess;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        if (lst_expe.size()==0) {
            this.win.setTitle("Sin expedientes Vinculados");
        }
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("list") List<ExpedienteCliente> lc
            ) throws Exception {
        lst_expe = lc;
        //Messagebox.show(lst_expe.size()+"");
    }
    
    @Command
    public void closeThis() {
        win.detach();
    }

    public List<ExpedienteCliente> getLst_expe() {
        return lst_expe;
    }

    public void setLst_expe(List<ExpedienteCliente> lst_expe) {
        this.lst_expe = lst_expe;
    }

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    
}
