/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanDestinatarioRes;
import com.marjuv.connect.DataTransaction;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.administracion;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class ReporteVisualizarCargo extends GenericForwardComposer implements Serializable {

    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;
    private String codi_reso;
    public static List<BeanDestinatarioRes> lista;

    protected DataTransaction dt;
    protected Connection cn;

    public ReporteVisualizarCargo() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    public void onCreate$winReport() throws IOException {
        params = (params == null) ? null : (HashMap) winReport.getAttribute("params");
        name = winReport.getAttribute("name").toString();
        url = winReport.getAttribute("url").toString();
        codi_reso = winReport.getAttribute("codi_reso").toString();
        cargarDatos();
        reportar();
    }

    public void reportar() throws IOException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();

            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            params.put("realPath", sv.getRealPath("/reportes/"));
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, new JRBeanCollectionDataSource(lista, true));

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (is != null) {
                is.close();
            }
        }
    }

    private void cargarDatos() {
        try {
            lista = new ArrayList<BeanDestinatarioRes>();
            resolucion_dao obj_des_dao = new resolucion_dao();
            lista = obj_des_dao.search_destinatarios(Integer.parseInt(codi_reso));
            obj_des_dao.close();

            for (int i = 0; i < lista.size(); i++) {
                for (int j = 0; j < lista.get(i).getList_pare().size(); j++) {
                    if (lista.get(i).getCodi_pare() == lista.get(i).getList_pare().get(j).getX()) {
                        lista.get(i).setCur_pare(lista.get(i).getList_pare().get(j));
                        break;
                    }
                }
            }
            
            for (BeanDestinatarioRes beanDestinatarioRes : lista) {
                if (beanDestinatarioRes.getCodi_arde()>0) { // DESTINATARIO AREA
                    Dependencia_dao dep_dao = new Dependencia_dao();
                    BeanDependencias dep = dep_dao.getDependencia(beanDestinatarioRes.getCodi_arde());
                    beanDestinatarioRes.setRazon( dep.getNomb_enti() +" / "+dep.getNomb_arde());
                    beanDestinatarioRes.setDni("-");   
                    beanDestinatarioRes.setDireccion("-");
                    dep_dao.close();
                }
                else if (beanDestinatarioRes.getCodi_cont()>0) {
                    Contribuyente_dao con_dao = new Contribuyente_dao();
                    Contribuyente con = con_dao.get_contribuyente(beanDestinatarioRes.getCodi_cont());
                    beanDestinatarioRes.setRazon(con.getNomb_cont());
                    beanDestinatarioRes.setDni(con.getDni_cont()+"");   
                    beanDestinatarioRes.setDireccion(con.getDire_cont());
                    con_dao.close();
                }
                else if (beanDestinatarioRes.getCodi_enti()>0) {
                    Entidad_dao ent_dao = new Entidad_dao();
                    Entidad en = ent_dao.get_entidad(beanDestinatarioRes.getCodi_enti());
                    beanDestinatarioRes.setRazon(en.getNomb_enti());
                    beanDestinatarioRes.setDni("-");   
                    beanDestinatarioRes.setDireccion(en.getDire_enti());
                    ent_dao.close();
                }
            }
            administracion_dao obj_administracion_dao = new administracion_dao();
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            administracion obj_admin = obj_administracion_dao.get_administracion(year);
            resolucion_dao resdao = new resolucion_dao();
            resolucion res = resdao.search_resolucion(Integer.parseInt(codi_reso));
             
            params = new HashMap();
            params.put("noen_admi", obj_admin.getNoen_admi());
            usuario obj_login = (usuario) session.getAttribute("usuario");
            params.put("noar_admi", res.getNomb_arde());
            params.put("nume_reso", res.getNume_reso());
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            params.put("feem_reso", df.format(res.getFeem_reso()));
            params.put("apno_usno", res.getApno_usno());
            params.put("dni_usno", res.getDni_usno()+"");
            params.put("nuno_reso", res.getNuno_reso());
            

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
