package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanSubDependencias;
import com.marjuv.dao.DependenciaDAO;
import com.marjuv.dao.SubDependenciaDAO;
import com.marjuv.entity.DependenciaTO;
import com.marjuv.entity.SubDependenciaTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class SubDependenciaControllerMan {
    
    
    @Wire("#sub_dependencia_man")
    private Window win;
    private BeanSubDependencias beanSubDependencia;
    private String recordMode;
    private List<Pair<Integer, String>> listDependencia;
    private Pair<Integer, String> curDependencia;
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("objSubDependencia") BeanSubDependencias c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        loadDataDependencia();
        if (recordMode.equals("NEW")) {
            this.beanSubDependencia = new BeanSubDependencias();

        }

        if (recordMode.equals("EDIT")) {
            this.beanSubDependencia = (BeanSubDependencias) c1.clone();
            for(int i = 0; i < listDependencia.size(); i++) {
                if(listDependencia.get(i).getX() == this.beanSubDependencia.getCodi_arde()){
                    this.curDependencia = listDependencia.get(i);
                    break;
                }
            }
        }
    }

    public BeanSubDependencias getBeanSubDependencia() {
        return beanSubDependencia;
    }

    public void setBeanSubDependencia(BeanSubDependencias beanSubDependencia) {
        this.beanSubDependencia = beanSubDependencia;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<Pair<Integer, String>> getListDependencia() {
        return listDependencia;
    }

    public void setListDependencia(List<Pair<Integer, String>> listDependencia) {
        this.listDependencia = listDependencia;
    }

    public Pair<Integer, String> getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(Pair<Integer, String> curDependencia) {
        this.curDependencia = curDependencia;
    }
    
    public void loadDataDependencia() {
        try {
            SubDependenciaDAO objSubDependenciaDAO = new SubDependenciaDAO();
            setListDependencia(objSubDependenciaDAO.getDependencia());
            objSubDependenciaDAO.close();
            if (getListDependencia().size() > 0) {
                setCurDependencia(getListDependencia().get(0));
            }
        } catch (Exception e) {
        }
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            SubDependenciaDAO objSubDependenciaDAO = new SubDependenciaDAO();
            
            if ((getRecordMode().equals("NEW") && objSubDependenciaDAO.validate_desc_nuevo(beanSubDependencia.getDesc_sude())) || (getRecordMode().equals("EDIT") && objSubDependenciaDAO.validate_desc_editar(beanSubDependencia.getCodi_sude(),beanSubDependencia.getDesc_sude()))) {
                    SubDependenciaTO obSubjDependenciaTO = new SubDependenciaTO();
                    obSubjDependenciaTO.setCodi_sude(beanSubDependencia.getCodi_sude());
                    obSubjDependenciaTO.setDesc_sude(beanSubDependencia.getDesc_sude());
                    obSubjDependenciaTO.setCodi_arde(curDependencia.x);

                    Map args = new HashMap();
                    args.put("objSubDependencia", obSubjDependenciaTO);
                    args.put("accion", this.recordMode);
                    if (this.recordMode.equals("EDIT")) {
                        Messagebox.show("La dependencia fue actualizada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                    } else {
                        if (this.recordMode.equals("NEW")) {
                            Messagebox.show("La dependencia fue registrada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                        }
                    }
                    BindUtils.postGlobalCommand(null, null, "updateSubDepndenciaList", args);
                    win.detach();
                

            } else {
                Messagebox.show("El nombre de la dependecia ingresado ya Existe, eliga otro", "Mantenimiento de Dependencias", Messagebox.OK, Messagebox.ERROR);
            }
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }
    
    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "allSubDependencias", null);
    }
}
