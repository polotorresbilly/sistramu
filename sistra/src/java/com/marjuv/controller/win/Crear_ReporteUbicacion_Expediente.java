
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanReporteEstadoExpediente;
import com.marjuv.beans_view.BeanUbicacionExpediente;
import com.marjuv.connect.DataTransaction;
import static com.marjuv.controller.win.ReporteEstadoExpediente.lista;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.entity.administracion;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class Crear_ReporteUbicacion_Expediente extends GenericForwardComposer implements Serializable {
    
    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;
    private String nume_expe;
    public static List<BeanUbicacionExpediente> lista;

    protected DataTransaction dt;
    protected Connection cn;
    
    public Crear_ReporteUbicacion_Expediente() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }
    
    public void onCreate$winReport() throws IOException {
        params = (params == null) ? null : (HashMap) winReport.getAttribute("params");
        name = winReport.getAttribute("name").toString();
        url = winReport.getAttribute("url").toString();
        nume_expe = winReport.getAttribute("nume_expe").toString();
        CargarDatos();
        reportar();
    }
    
    public void reportar() throws IOException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();

            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_logo_admin();
            obj_administracion_dao.close();
            
            params.put("realPath",URL_FILE);
            
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, new JRBeanCollectionDataSource(lista, true));

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (is != null) {
                is.close();
            }
        }
    }
    public void CargarDatos(){
        try {
            lista = new ArrayList<BeanUbicacionExpediente>();
            Expediente_dao objExpedienteDAO = new Expediente_dao();
            lista = objExpedienteDAO.Ubicacion_Actual_Expediente((String) params.get("NUMERO_EXPEDIENTE"));
            objExpedienteDAO.close();
            administracion_dao ObjAdministracionDAO = new administracion_dao();
            administracion ObjAdministracionTO = new administracion();
            Calendar fecha = Calendar.getInstance();
            int año = fecha.get(Calendar.YEAR);
            ObjAdministracionTO = ObjAdministracionDAO.get_administracion(año);
            ObjAdministracionDAO.close();
            params.put("NOMBRE_ANO", ObjAdministracionTO.getNoaa_admi());
            params.put("NOMBRE_ENTIDAD", ObjAdministracionTO.getNoen_admi());
            
        } catch (Exception e) {
            Messagebox.show("Mensaje de Error3: " + e.getMessage());
        }
    }
}
