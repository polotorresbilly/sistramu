package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanAsuntoResolucion;
import com.marjuv.dao.AsuntoResolucionDAO;
import com.marjuv.entity.AsuntoResolucionTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class AsuntoResolucionControllerMain {

    private BeanAsuntoResolucion BeanAsuntoResolucion;
    private List<BeanAsuntoResolucion> ListAsuntoResolucion;
private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
     @WireVariable
    private Session _sess;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Asunto"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("listaAR")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("listaAR")
    public void updateGrid() {
        try {
            AsuntoResolucionDAO dao = new AsuntoResolucionDAO();
            ListAsuntoResolucion = dao.getListAsuntoResolucion();
            dao.close();
            List<BeanAsuntoResolucion> new_list = new ArrayList<BeanAsuntoResolucion>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanAsuntoResolucion contribuyente : ListAsuntoResolucion) {
                            if ((contribuyente.getDesc_asre().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                ListAsuntoResolucion = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
try {
            loadDataControls();
            AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
            ListAsuntoResolucion = objAsuntoResolucionDAO.getListAsuntoResolucion();
            objAsuntoResolucionDAO.close();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en ListaAR " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    
    
    @Command
    public void editAsuntoResolucion(){
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyAsuntoResolucion", this.getBeanAsuntoResolucion());
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_asunto_resolucion.zul", null, map);
            
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception en Edit(Asunto Resolucion): " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void newAsuntoResolucion(){
        try {
            final HashMap<String,Object> map = new HashMap<String, Object>();
            map.put("keyAsuntoResolucion", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/man/man_asunto_resolucion.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en New(Asunto Resolucion): " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @GlobalCommand
    @NotifyChange("listaAR")
    public void updateAR(@BindingParam("keyAsuntoResolucion") AsuntoResolucionTO objAsuntoResolucionTO, @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
            if (recordMode.equals("NEW")) {
                objAsuntoResolucionDAO.insert(objAsuntoResolucionTO);
                objAsuntoResolucionDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                    objAsuntoResolucionDAO.update(objAsuntoResolucionTO);
                    objAsuntoResolucionDAO.close();
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateAR: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }
               
            }
        }
        updateGrid();
    }
    
    public List<BeanAsuntoResolucion> getListaAR(){
        
        return ListAsuntoResolucion;
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteAsuntoResolucion(){
        String str = "¿Está seguro de que desea eliminar el asunto de Resolución \"" + BeanAsuntoResolucion.getDesc_asre() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {

            @Override
            public void onEvent(Event t) throws Exception {
                if(Messagebox.ON_YES.equals(t.getName())){
                    try {
                        AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
                        objAsuntoResolucionDAO.delete(BeanAsuntoResolucion.getCodi_asre());
                        BindUtils.postNotifyChange(null, null,AsuntoResolucionControllerMain.this, "listaAR");
                        objAsuntoResolucionDAO.close();
                        updateGrid();
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        });
    }

    public BeanAsuntoResolucion getBeanAsuntoResolucion() {
        return BeanAsuntoResolucion;
    }

    public void setBeanAsuntoResolucion(BeanAsuntoResolucion BeanAsuntoResolucion) {
        this.BeanAsuntoResolucion = BeanAsuntoResolucion;
    }

    public List<BeanAsuntoResolucion> getListAsuntoResolucion() {
        return ListAsuntoResolucion;
    }

    public void setListAsuntoResolucion(List<BeanAsuntoResolucion> ListAsuntoResolucion) {
        this.ListAsuntoResolucion = ListAsuntoResolucion;
    }

}
