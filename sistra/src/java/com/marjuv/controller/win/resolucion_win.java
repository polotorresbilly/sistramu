package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDestinoDis;
import static com.marjuv.controller.win.main.generarReporte;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.Firma_Resolucion_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.administracion;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.Pair;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class resolucion_win {

    @Wire("#resolucion_win")
    private Window win;
    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> listTipoResolucion, listAsuntoResolucion, list_fire;
    private Pair<Integer, String> curTipoResolucion, curAsuntoResolucion, cur_fire;

    private List<BeanDestinoDis> lst_destino_des;
    private BeanDestinoDis cur_destino_des;

    private List<ExpedienteCliente> lst_exp_vin;
    private ExpedienteCliente cur_exp_vin;

    private List<BeanDestinoDis> lst_destino_exp_block;

    private resolucion resolucion_selected;

    private String recordMode;

    @Wire
    private Combobox codi_tire;
    @Wire
    private Combobox codi_asre;
    @Wire
    private Textbox hoja_reso;
    @Wire
    private Textbox nume_reso;
    @Wire
    private Datebox feem_reso;
    @Wire
    private Textbox asun_reso;
    @Wire
    private Datebox fetr_reso;
    @Wire
    private Combobox codi_fire;
    @Wire
    private Checkbox ver_int_trans;
    @Wire
    private Checkbox final_expe;
    @Wire
    private Button btn_nume;
    @Wire
    private Button btnCarDis;
    @Wire
    private Button btnAddDes;
    @Wire
    private Button btnAgregarExpe;

    @Wire
    private Button btnNew;
    @Wire
    private Button btnEdit;
    @Wire
    private Button btnSave;
    @Wire
    private Button btnSearch;
    @Wire
    private Button btnCancel;
    @Wire
    private Button btnCargar;
    @Wire
    private Button btnTrans;

    @Wire
    private Iframe report;

    private boolean sw_quitar = false;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("win_resolucion")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Crecepcionarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Logger.getLogger(Crecepcionarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (sw == false) {
                Executions.sendRedirect("index.zul");
            }
        } else {
            Executions.sendRedirect("index.zul");
        }
    }

    @NotifyChange("*")
    @Init
    public void initSetup() {
        check_permisos();
        loadDataControls();
        lst_destino_des = new ArrayList<BeanDestinoDis>();
        lst_destino_exp_block = new ArrayList<BeanDestinoDis>();
        lst_exp_vin = new ArrayList<ExpedienteCliente>();

    }

    @Command
    public void open_expe() {
        Map parametros = new HashMap();
        parametros.put("nume_expe", cur_exp_vin.getEXPNUM());
        parametros.put("nude_hist", cur_exp_vin.getDESTINO());
        parametros.put("correlativo", cur_exp_vin.getCORRELATIVO());
        if (nume_reso.getValue().isEmpty()) {
            parametros.put("nume_reso", "");
        } else {
            parametros.put("nume_reso", nume_reso.getValue());
        }
        Executions.createComponents("proceso/win/win_visualizar_expe.zul", null, parametros);
    }

    @NotifyChange("*")
    @Command
    public void quitar_expe() {
        if (sw_quitar) {
            for (int i = 0; i < lst_destino_exp_block.size(); i++) {
                if (lst_destino_exp_block.get(i).getNume_expe().equals(cur_exp_vin.getEXPNUM())) {
                    for (int j = 0; j < lst_destino_des.size(); j++) {
                        if (lst_destino_des.get(j).getCodi_arde() == lst_destino_exp_block.get(i).getCodi_arde()
                                && lst_destino_des.get(j).getCodi_cont() == lst_destino_exp_block.get(i).getCodi_cont()
                                && lst_destino_des.get(j).getCodi_enti() == lst_destino_exp_block.get(i).getCodi_enti()) {
                            lst_destino_des.remove(lst_destino_des.get(j));
                            break;
                        }
                    }
                    lst_destino_exp_block.remove(lst_destino_exp_block.get(i));
                    break;
                }
            }
            for (int i = 0; i < lst_exp_vin.size(); i++) {
                if (lst_exp_vin.get(i).getEXPNUM().equals(cur_exp_vin.getEXPNUM())) {
                    lst_exp_vin.remove(lst_exp_vin.get(i));
                    break;
                }
            }
        }
    }

    @NotifyChange("*")
    @Command
    public void ver_trans() throws Exception {
        administracion_dao obj_administracion_dao = new administracion_dao();
        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        Date fecha = new Date(currentDate.getTime());
        int year = fecha.getYear() + 1900;
        administracion obj_administracion = obj_administracion_dao.get_administracion(year);
        obj_administracion_dao.close();
        Map parametros = new HashMap();
        parametros.put("noan_admi", obj_administracion.getNoaa_admi());
        parametros.put("noen_admi", obj_administracion.getNoen_admi());
        parametros.put("codigo", resolucion_selected.getCodi_reso());
        parametros.put("entidad", obj_administracion.getNoen_admi());
        parametros.put("resolucion", "copia de la " + getCurTipoResolucion().y + ", N°" + resolucion_selected.getNume_reso() + " que es la transcripción oficial del original de la Resolución respectiva.");
        parametros.put("ciudad", obj_administracion.getNoci_admi());
        general_dao obj_general_dao = new general_dao();
        parametros.put("usuario", obj_general_dao.get_pofi_fire());
        parametros.put("cargo_usuario", obj_general_dao.get_carg_fire());
        parametros.put("cont", String.valueOf(lst_destino_des.size()));
        Firma_Resolucion_dao obj_firma = new Firma_Resolucion_dao();
        parametros.put("archivo", obj_firma.get_archivo(resolucion_selected.getCodi_fire()));

        obj_general_dao.close();

        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
        if (ver_int_trans.isChecked()) {
            generarReporte("/reportes/Transcripcion_Resolucion.jasper", parametros, "Transcripción de resolución " + resolucion_selected.getNume_reso() + ".pdf", sv);
        } else {
            generarReporte("/reportes/Transcripcion_Resolucion_1.jasper", parametros, "Transcripción de resolución " + resolucion_selected.getNume_reso() + ".pdf", sv);
        }

    }

    @NotifyChange("*")
    @Command
    public void quitar_des() {
        if (sw_quitar) {
            String nume_expe = "";
            this.sw = true;
            for (int i = 0; i < lst_destino_exp_block.size(); i++) {
                if (lst_destino_exp_block.get(i).getCodi_cont() == cur_destino_des.getCodi_cont()
                        && lst_destino_exp_block.get(i).getCodi_enti() == cur_destino_des.getCodi_enti()
                        && lst_destino_exp_block.get(i).getCodi_arde() == cur_destino_des.getCodi_arde()) {
                    sw = false;
                    nume_expe = lst_destino_exp_block.get(i).getNume_expe();
                    break;
                }
            }
//            if (this.sw){
                for (int i = 0; i < lst_destino_des.size(); i++) {
                    if (lst_destino_des.get(i).getCodi_cont() == cur_destino_des.getCodi_cont()
                            && lst_destino_des.get(i).getCodi_enti() == cur_destino_des.getCodi_enti()
                            && lst_destino_des.get(i).getCodi_arde() == cur_destino_des.getCodi_arde()) {
                        lst_destino_des.remove(lst_destino_des.get(i));
                        break;
                    }
                }
//            }else {
//                Messagebox.show("El destinatario " + cur_destino_des.getNomb_des() + " está vinculado al expediente " + nume_expe, "Información", Messagebox.OK, Messagebox.EXCLAMATION);
//            }
        }
    }

    @Command
    public void search_expe2() {
        Executions.createComponents("proceso/win/search_expediente_reso.zul", null, null);
    }

    @NotifyChange("lst_destino_des")
    @Command
    public void change_hojas() {
        Map parametros = new HashMap();
        parametros.put("codi_arde", cur_destino_des.getCodi_arde());
        parametros.put("codi_enti", cur_destino_des.getCodi_enti());
        parametros.put("codi_cont", cur_destino_des.getCodi_cont());
        parametros.put("hoja_dest", cur_destino_des.getCopias());
        Executions.createComponents("proceso/win/hojas_dest.zul", null, parametros);
    }

    @Command
    public void search_desti() {
        Executions.createComponents("proceso/win/search_reso.zul", null, null);
    }

    @GlobalCommand
    @NotifyChange("*")
    public void updateHojasDest(
            @BindingParam("codi_arde") int codi_arde,
            @BindingParam("codi_enti") int codi_enti,
            @BindingParam("codi_cont") int codi_cont,
            @BindingParam("hoja_dest") int hoja_dest
    ) throws Exception {
        for (int i = 0; i < lst_destino_des.size(); i++) {
            if (lst_destino_des.get(i).getCodi_arde() == codi_arde
                    && lst_destino_des.get(i).getCodi_enti() == codi_enti
                    && lst_destino_des.get(i).getCodi_cont() == codi_cont) {
                lst_destino_des.get(i).setCopias(hoja_dest);
                break;
            }
        }
    }

    private boolean sw;

    @GlobalCommand
    @NotifyChange("*")
    public void setExpedienteReso(@BindingParam("ExpedienteCliente") ExpedienteCliente obj_expediente) throws Exception {
        this.sw = true;
        for (int i = 0; i < lst_exp_vin.size(); i++) {
            if (lst_exp_vin.get(i).getEXPNUM().equals(obj_expediente.getEXPNUM())) {
                this.sw = false;
                break;
            }
        }
        if (this.sw) {
            this.lst_exp_vin.add(obj_expediente);
            this.sw = true;
            for (int i = 0; i < lst_destino_des.size(); i++) {
                if (lst_destino_des.get(i).getCodi_arde() == obj_expediente.getCodi_arde()
                        && lst_destino_des.get(i).getCodi_cont() == obj_expediente.getCodi_cont()
                        && lst_destino_des.get(i).getCodi_enti() == obj_expediente.getCodi_enti()) {
                    this.sw = false;
                    break;
                }
            }
//            if (this.sw) {
//                BeanDestinoDis obj_bean = new BeanDestinoDis();
//                obj_bean.setCodi_arde(obj_expediente.getCodi_arde());
//                obj_bean.setCodi_cont(obj_expediente.getCodi_cont());
//                obj_bean.setCodi_enti(obj_expediente.getCodi_enti());
//                obj_bean.setNomb_des(obj_expediente.getNomb_hist());
//                obj_bean.setCopias(1);
//                obj_bean.setNume_expe(obj_expediente.getEXPNUM());
//                this.lst_destino_des.add(obj_bean);
//                this.lst_destino_exp_block.add(obj_bean);
//            }

        }
    }

    @NotifyChange("*")
    @Command
    public void cargar_desti() throws SQLException {
        general_dao obj_general_dao = new general_dao();
        List<BeanDestinoDis> temp = obj_general_dao.cargar_distribucion(getCurAsuntoResolucion().x);
        obj_general_dao.close();
        BeanDestinoDis obj_bean;
        lst_destino_des = new ArrayList<BeanDestinoDis>();
        //lst_destino_des = lst_destino_exp_block;
        for (int j = 0; j < temp.size(); j++) {
            this.sw = true;
            for (int i = 0; i < lst_destino_des.size(); i++) {
                if (lst_destino_des.get(i).getCodi_arde() == temp.get(j).getCodi_arde()
                        && lst_destino_des.get(i).getCodi_cont() == temp.get(j).getCodi_cont()
                        && lst_destino_des.get(i).getCodi_enti() == temp.get(j).getCodi_enti()) {
                    this.sw = false;
                    break;
                }
            }
            if (this.sw) {
                obj_bean = new BeanDestinoDis();
                obj_bean.setCodi_arde(temp.get(j).getCodi_arde());
                obj_bean.setCodi_cont(temp.get(j).getCodi_cont());
                obj_bean.setCodi_enti(temp.get(j).getCodi_enti());
                obj_bean.setNomb_des(temp.get(j).getNomb_des());
                obj_bean.setCopias(1);
                this.lst_destino_des.add(obj_bean);
            }
        }
        for (int i = 0; i < lst_destino_exp_block.size(); i++) {
            for (int j = 0; j < lst_destino_des.size(); j++) {
                if (lst_destino_exp_block.get(i).getNomb_des().equals(lst_destino_des.get(j).getNomb_des())) {
                    lst_destino_des.remove(lst_destino_des.get(j));
                    break;
                }
            }
            lst_destino_des.add(lst_destino_exp_block.get(i));
        }
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setDestinatarioReso(
            @BindingParam("codi_arde") Integer codi_arde,
            @BindingParam("codi_cont") Integer codi_cont,
            @BindingParam("codi_enti") Integer codi_enti,
            @BindingParam("nomb_enti") String nomb_enti,
            @BindingParam("nomb_arde") String nomb_arde,
            @BindingParam("nomb_cont") String nomb_cont,
            @BindingParam("copia_des") String copia_des
            
    ) {
//        Messagebox.show("Codi_arde: " + codi_arde);
//        Messagebox.show("Codi_cont: " + codi_cont);
//        Messagebox.show("Codi_enti: " + codi_enti);
//        Messagebox.show("nomb_enti: " + nomb_enti);
//        Messagebox.show("nomb_arde: " + nomb_arde);
//        Messagebox.show("nomb_cont: " + nomb_cont);
//        Messagebox.show("copia: " + copia_des);
        
        BeanDestinoDis obj_bean = new BeanDestinoDis();
        obj_bean.setCodi_arde(codi_arde);
        obj_bean.setCodi_cont(codi_cont);
        obj_bean.setCodi_enti(codi_enti);
        if (!nomb_arde.equals("")) {
            obj_bean.setNomb_des(nomb_arde);
        } else if (!nomb_enti.equals("")) {
            obj_bean.setNomb_des(nomb_enti);
        } else if (!nomb_cont.equals("")) {
            obj_bean.setNomb_des(nomb_cont + " - CONTRIBUYENTE");
        }
        obj_bean.setCopias(1);
        this.sw = true;
        for (int i = 0; i < lst_destino_des.size(); i++) {
            if (lst_destino_des.get(i).getNomb_des().equals(obj_bean.getNomb_des())) {
                this.sw = false;
                break;
            }
        }
        if (this.sw) {
            this.lst_destino_des.add(obj_bean);
        } else {
            Messagebox.show("El destinario ya existe", "Información", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setResolucion(
            @BindingParam("resolucion") resolucion obj_resolucion
    ) throws Exception {
        limpiar_campos();
        this.resolucion_selected = obj_resolucion;

        for (int i = 0; i < listTipoResolucion.size(); i++) {
            if (listTipoResolucion.get(i).getX() == obj_resolucion.getCodi_tire()) {
                setCurTipoResolucion(listTipoResolucion.get(i));
                break;
            }
        }
        for (int i = 0; i < listAsuntoResolucion.size(); i++) {
            if (listAsuntoResolucion.get(i).getX() == obj_resolucion.getCodi_asre()) {
                setCurAsuntoResolucion(listAsuntoResolucion.get(i));
                break;
            }
        }
        for (int i = 0; i < list_fire.size(); i++) {
            if (list_fire.get(i).getX() == obj_resolucion.getCodi_fire()) {
                setCur_fire(list_fire.get(i));
                break;
            }
        }
        nume_reso.setValue(obj_resolucion.getNume_reso());
        hoja_reso.setValue(String.valueOf(obj_resolucion.getHoja_reso()));
        asun_reso.setValue(obj_resolucion.getAsun_reso());
        fetr_reso.setValue(obj_resolucion.getFetr_reso());
        feem_reso.setValue(obj_resolucion.getFeem_reso());

        if (obj_resolucion.getEstr_reso().equals("1")) {
            ver_int_trans.setChecked(true);
        } else {
            ver_int_trans.setChecked(false);
        }

        if (obj_resolucion.getEsfin_reso().equals("1")) {
            final_expe.setChecked(true);
        } else {
            final_expe.setChecked(false);
        }

        lst_exp_vin = obj_resolucion.getExpedientes();
        lst_destino_des = obj_resolucion.getDestinos();

        btnEdit.setDisabled(false);
//        btnTrans.setDisabled(false);
    }

    private Media media;
    private String file_name;
    boolean exists = true;

    @Command
    @NotifyChange("*")
    public void cargar(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) throws SQLException {
        if (!nume_reso.getValue().isEmpty()) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            if ((getRecordMode().equals("NEW") && obj_resolucion_dao.comprobar_nuevo(nume_reso.getValue()))
                    || (getRecordMode().equals("EDIT") && obj_resolucion_dao.comprobar_editar(this.resolucion_selected.getCodi_reso(), nume_reso.getValue()))) {
                if (event.getMedia() != null) {
                    if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                            || event.getMedia().getContentType().equals("application/msword")
                            || event.getMedia().getContentType().equals("application/vnd.ms-excel")
                            || event.getMedia().getContentType().equals("application/pdf")
                            || event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        String extension = "";
                        if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                            extension = "docx";
                        } else if (event.getMedia().getContentType().equals("application/msword")) {
                            extension = "doc";
                        } else if (event.getMedia().getContentType().equals("application/vnd.ms-excel")) {
                            extension = "xls";
                        } else if (event.getMedia().getContentType().equals("application/pdf")) {
                            extension = "pdf";
                        } else if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            extension = "xlsx";
                        }

                        media = event.getMedia();
                        file_name = nume_reso.getValue().replace(' ', '_').replace('\\', '_').replace('/', '_').replace(':', '_').replace('*', '_').replace('?', '_').replace('"', '_').replace('<', '_').replace('>', '_').replace('|', '_') + "." + extension;
                    } else {
                        Messagebox.show("Tipo de archivo no válido. Sólo se permite *.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                    }
                }
            } else {
                Messagebox.show("El número de resolución ya existe", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        } else {
            Messagebox.show("Por favor, ingrese el número de resolución", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void guardar_file() {
        try {
            ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin()+"\\resoluciones\\";
            obj_administracion_dao.close();
            File destino = new File(URL_FILE + "/" + file_name);

            if (destino.exists()) {
                Messagebox.show("El archivo " + file_name + " existe, ¿Está seguro de que desea reemplazar?", "Confirmarción", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    @Override
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            exists = true;
                        } else {
                            exists = false;
                        }
                    }
                });
            } else {
                exists = true;
            }

            if (exists) {
                if (media != null) {
                    if (media.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                            || media.getContentType().equals("application/msword")
                            || media.getContentType().equals("application/vnd.ms-excel")
                            || media.getContentType().equals("application/pdf")
                            || media.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                        try {
                            Files.copy(destino, media.getStreamData());
                        } catch (Exception e) {
                        }
                    } else {
                        Messagebox.show("Tipo de archivo no válido. Sólo se permite *.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                    }
                }

            }
        } catch (Exception e) {
        }
    }

    @Command
    @NotifyChange("*")
    public void nuevo() {
        limpiar_campos();
        habilitar_botones(true);
        btnNew.setDisabled(true);
        btnSave.setDisabled(false);
        btnEdit.setDisabled(true);
        btnSearch.setDisabled(true);
        btnCancel.setDisabled(false);
        sw_quitar = true;
        setRecordMode("NEW");
//        btnTrans.setDisabled(true);
    }

    @Command
    @NotifyChange("*")
    public void comprobar() throws SQLException {
        if (!nume_reso.getValue().isEmpty()) {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            if ((getRecordMode().equals("NEW") && obj_resolucion_dao.comprobar_nuevo(nume_reso.getValue()))
                    || (getRecordMode().equals("EDIT") && obj_resolucion_dao.comprobar_editar(this.resolucion_selected.getCodi_reso(), nume_reso.getValue()))) {
                Messagebox.show("El número de resolución está disponible", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            } else {
                Messagebox.show("El número de resolución ya existe", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        } else {
            Messagebox.show("El número de resolución está vacío", "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    @Command
    @NotifyChange("*")
    public void modificar() {
        if (resolucion_selected.getEsta_reso().equals("1")) {
            habilitar_botones(true);
            btnNew.setDisabled(true);
            btnSave.setDisabled(false);
            btnEdit.setDisabled(true);
            btnSearch.setDisabled(true);
            btnCancel.setDisabled(false);
            sw_quitar = true;
            setRecordMode("EDIT");
//            btnTrans.setDisabled(true);
            btnCargar.setDisabled(true);
        } else {
            Messagebox.show("La resolución " + resolucion_selected.getNume_reso() + " ya se encuentra recepcionada", "Advertencia", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Command
    @NotifyChange("*")
    public void cancelar() throws SQLException {
        limpiar_campos();
        habilitar_botones(false);
        btnNew.setDisabled(false);
        btnSave.setDisabled(true);
        btnEdit.setDisabled(true);
        btnSearch.setDisabled(false);
        btnCancel.setDisabled(true);
        sw_quitar = false;
//        btnTrans.setDisabled(true);
    }

    @Command
    public void buscar() {
        Executions.createComponents("proceso/win/search_resolucion.zul", null, null);
//        btnTrans.setDisabled(false);
        
    }

    @Command
    @NotifyChange("*")
    public void guardar() throws SQLException, Exception {

        String mensaje = "";
        if (getCurTipoResolucion() != null) {
            if (!nume_reso.getValue().isEmpty()) {
                if (nume_reso.getValue().length() <= 30) {
                    if (!asun_reso.getValue().isEmpty()) {
                        if (asun_reso.getValue().length() <= 255) {
                            if (!hoja_reso.getValue().isEmpty()) {
                                if (hoja_reso.getValue().matches("\\d+")) {
                                    if (cur_fire != null) {
                                        if (lst_exp_vin.size() > 0) {
                                            if (lst_destino_des.size() > 0) {
                                                resolucion_dao obj_resolucion_dao = new resolucion_dao();
                                                if ((getRecordMode().equals("NEW") && obj_resolucion_dao.comprobar_nuevo(nume_reso.getValue()))
                                                        || (getRecordMode().equals("EDIT") && obj_resolucion_dao.comprobar_editar(this.resolucion_selected.getCodi_reso(), nume_reso.getValue()))) {

                                                    resolucion obj_resolucion = new resolucion();

                                                    int codi_usua = -1;

                                                    if (getRecordMode().equals("EDIT")) {
                                                        codi_usua = this.resolucion_selected.getUsre_reso();
                                                        obj_resolucion.setCodi_reso(this.resolucion_selected.getCodi_reso());
                                                    } else if (getRecordMode().equals("NEW")) {
                                                        usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
                                                        codi_usua = obj_usuario.getCodi_usua();
                                                    }
                                                    obj_resolucion.setUsre_reso(codi_usua);
                                                    obj_resolucion.setNume_reso(this.nume_reso.getValue());
                                                    if (ver_int_trans.isChecked()) {
                                                        obj_resolucion.setEstr_reso("1");
                                                    } else {
                                                        obj_resolucion.setEstr_reso("0");
                                                    }
                                                    if (final_expe.isChecked()) {
                                                        obj_resolucion.setEsfin_reso("1");
                                                    } else {
                                                        obj_resolucion.setEsfin_reso("0");
                                                    }

                                                    obj_resolucion.setHoja_reso(Integer.parseInt(hoja_reso.getValue()));
                                                    obj_resolucion.setFeem_reso(new java.sql.Date(feem_reso.getValue().getTime()));
                                                    obj_resolucion.setAsun_reso(asun_reso.getValue());
                                                    obj_resolucion.setFetr_reso(new java.sql.Date(fetr_reso.getValue().getTime()));
                                                    obj_resolucion.setRubr_reso(cur_fire.y);
                                                    obj_resolucion.setCodi_tire(getCurTipoResolucion().x);
                                                    obj_resolucion.setCodi_asre(getCurAsuntoResolucion().x);
                                                    obj_resolucion.setCodi_aras(0);
                                                    obj_resolucion.setCodi_enre(0);
                                                    obj_resolucion.setCodi_fire(cur_fire.x);
                                                    obj_resolucion.setEsta_reso("1");

                                                    obj_resolucion.setExpedientes(lst_exp_vin);
                                                    obj_resolucion.setDestinos(lst_destino_des);

                                                    usuario obj_usua = (usuario) _sess.getAttribute("usuario");
                                                    obj_resolucion.setUsem_reso(obj_usua.getCodi_usua());
                                                    guardar_file();
                                                    if (getRecordMode().equals("NEW")) {
                                                        obj_resolucion_dao.insert(obj_resolucion, obj_usua.getCodi_usua());
                                                    } else if (getRecordMode().equals("EDIT")) {
                                                        obj_resolucion_dao.update(obj_resolucion, obj_usua.getCodi_usua());
                                                    }
                                                    obj_resolucion_dao.close();
                                                    limpiar_campos();
                                                    btnSave.setDisabled(true);
                                                    btnCancel.setDisabled(true);
                                                    btnNew.setDisabled(false);
                                                    btnSearch.setDisabled(false);
                                                    btnEdit.setDisabled(true);
                                                    sw_quitar = false;
                                                    habilitar_botones(false);
                                                    Messagebox.show("Resolución guardado con éxito", "Registro", Messagebox.OK, Messagebox.INFORMATION);
                                                } else {
                                                    mensaje = "El número de resolución ya existe";
                                                }
                                            } else {
                                                mensaje = "Por favor, agregue un destinatario";
                                            }
                                        } else {
                                            mensaje = "Por favor, vincule un expediente";
                                        }
                                    } else {
                                        mensaje = "Por favor, ingrese rúbrica";
                                    }
                                } else {
                                    mensaje = "El valor de Hojas sólo se admite números enteros";
                                }
                            } else {
                                mensaje = "Por favor, ingrese hojas";
                            }
                        } else {
                            mensaje = "El asunto sólo se admite máximo 255 carácteres";
                        }
                    } else {
                        mensaje = "Por favor, ingrese asunto";
                    }
                } else {
                    mensaje = "El número de resolución sólo se admite máximo 30 carácteres";
                }
            } else {
                mensaje = "Por favor, ingrese número de resolución";
            }
        } else {
            mensaje = "Por favor, seleccione un tipo de resolución";
        }
        if (!mensaje.equals("")) {
            Messagebox.show(mensaje, "Error", Messagebox.OK, Messagebox.ERROR);
        }
    }

    public void limpiar_campos() {
        if (listTipoResolucion.size() > 0) {
            setCurTipoResolucion(listTipoResolucion.get(0));
        } else {
            setCurTipoResolucion(new Pair<Integer, String>(-1, ""));
        }
        if (listAsuntoResolucion.size() > 0) {
            setCurAsuntoResolucion(listAsuntoResolucion.get(0));
        } else {
            setCurAsuntoResolucion(new Pair<Integer, String>(-1, ""));
        }
        if (list_fire.size() > 0) {
            setCur_fire(list_fire.get(0));
        } else {
            setCur_fire(null);
        }
        hoja_reso.setValue("");
        nume_reso.setValue("");
        feem_reso.setValue(new java.util.Date());
        asun_reso.setValue("");
        fetr_reso.setValue(new java.util.Date());
        ver_int_trans.setChecked(false);
        final_expe.setChecked(false);
        lst_destino_des = new ArrayList<BeanDestinoDis>();
        lst_exp_vin = new ArrayList<ExpedienteCliente>();
    }

    public void habilitar_botones(boolean bw) {
        codi_tire.setDisabled(!bw);
        hoja_reso.setReadonly(!bw);
        nume_reso.setReadonly(!bw);
        feem_reso.setDisabled(!bw);
        asun_reso.setReadonly(!bw);
        fetr_reso.setDisabled(!bw);
        ver_int_trans.setDisabled(!bw);
        
        final_expe.setDisabled(!bw);
        btn_nume.setDisabled(!bw);
        btnAddDes.setDisabled(!bw);
        //btnCarDis.setDisabled(!bw);
        btnAgregarExpe.setDisabled(!bw);
        //codi_asre.setDisabled(!bw);
        codi_fire.setDisabled(!bw);
//        btnTrans.setDisabled(!bw);
        btnCargar.setDisabled(!bw);
    }

    @NotifyChange("*")
    private void loadDataControls() {
        try {
            general_dao obj_general_dao = new general_dao();
            listTipoResolucion = obj_general_dao.get_select_tipo_resolucion();
            obj_general_dao.close();
            if (listTipoResolucion.size() > 0) {
                setCurTipoResolucion(listTipoResolucion.get(0));
            }

            Asunto_Resolucion_dao obj_Asunto_Resolucion_dao = new Asunto_Resolucion_dao();
            listAsuntoResolucion = obj_Asunto_Resolucion_dao.get_select();
            if (listAsuntoResolucion.size() > 0) {
                setCurAsuntoResolucion(listAsuntoResolucion.get(0));
            }
            obj_Asunto_Resolucion_dao.close();
            obj_general_dao = new general_dao();
            list_fire = obj_general_dao.get_select_fire();
            obj_general_dao.close();
            if (list_fire.size() > 0) {
                setCur_fire(list_fire.get(0));
            }
        } catch (Exception ex) {
            Logger.getLogger(resolucion_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<Pair<Integer, String>> getListTipoResolucion() {
        return listTipoResolucion;
    }

    public void setListTipoResolucion(List<Pair<Integer, String>> listTipoResolucion) {
        this.listTipoResolucion = listTipoResolucion;
    }

    public List<Pair<Integer, String>> getListAsuntoResolucion() {
        return listAsuntoResolucion;
    }

    public void setListAsuntoResolucion(List<Pair<Integer, String>> listAsuntoResolucion) {
        this.listAsuntoResolucion = listAsuntoResolucion;
    }

    public Pair<Integer, String> getCurTipoResolucion() {
        return curTipoResolucion;
    }

    public void setCurTipoResolucion(Pair<Integer, String> curTipoResolucion) {
        this.curTipoResolucion = curTipoResolucion;
    }

    public Pair<Integer, String> getCurAsuntoResolucion() {
        return curAsuntoResolucion;
    }

    public void setCurAsuntoResolucion(Pair<Integer, String> curAsuntoResolucion) {
        this.curAsuntoResolucion = curAsuntoResolucion;
    }

    public List<ExpedienteCliente> getLst_exp_vin() {
        return lst_exp_vin;
    }

    public void setLst_exp_vin(List<ExpedienteCliente> lst_exp_vin) {
        this.lst_exp_vin = lst_exp_vin;
    }

    public ExpedienteCliente getCur_exp_vin() {
        return cur_exp_vin;
    }

    public void setCur_exp_vin(ExpedienteCliente cur_exp_vin) {
        this.cur_exp_vin = cur_exp_vin;
    }

    public List<BeanDestinoDis> getLst_destino_des() {
        return lst_destino_des;
    }

    public void setLst_destino_des(List<BeanDestinoDis> lst_destino_des) {
        this.lst_destino_des = lst_destino_des;
    }

    public BeanDestinoDis getCur_destino_des() {
        return cur_destino_des;
    }

    public void setCur_destino_des(BeanDestinoDis cur_destino_des) {
        this.cur_destino_des = cur_destino_des;
    }

    public List<Pair<Integer, String>> getList_fire() {
        return list_fire;
    }

    public void setList_fire(List<Pair<Integer, String>> list_fire) {
        this.list_fire = list_fire;
    }

    public Pair<Integer, String> getCur_fire() {
        return cur_fire;
    }

    public void setCur_fire(Pair<Integer, String> cur_fire) {
        this.cur_fire = cur_fire;
    }

}
