package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanExpedientes_Vinculados;
import com.marjuv.dao.Expediente_VinculadoDAO;
import java.util.HashMap;
import java.util.List;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class ver_expedientes_vinculados_win {
    
    /*Instacia de la interfaz*/
    @Wire("#win_ver_expedientes_vinculados")
    private Window win;
    
    /*Lista*/
    private List<BeanExpedientes_Vinculados> listExpVinculados;
    
    /*Selector*/
    private BeanExpedientes_Vinculados selector;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("nume_expe") String Numero_Expediente) throws Exception {
        Expediente_VinculadoDAO ObjExpedintes_Vinculados = new Expediente_VinculadoDAO();
        setListExpVinculados(ObjExpedintes_Vinculados.getList_Expedientes_Vinculados(Numero_Expediente));
    }
    
    @Command
    public void ver_documentos_subidos(){
        final HashMap<String, String> map = new HashMap<String, String>();
        map.put("nume_expe", selector.getNumero_expediente_adjuntado());
        Executions.createComponents("proceso/win/ver_documentos_subidos.zul", null, map);
    }

    public List<BeanExpedientes_Vinculados> getListExpVinculados() {
        return listExpVinculados;
    }

    public void setListExpVinculados(List<BeanExpedientes_Vinculados> listExpVinculados) {
        this.listExpVinculados = listExpVinculados;
    }

    public BeanExpedientes_Vinculados getSelector() {
        return selector;
    }

    public void setSelector(BeanExpedientes_Vinculados selector) {
        this.selector = selector;
    }

    
    
}
