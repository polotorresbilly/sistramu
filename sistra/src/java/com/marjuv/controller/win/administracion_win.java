package com.marjuv.controller.win;

import com.marjuv.controller.man.*;
import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.permisos_usuario_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.administracion;
import com.marjuv.entity.permiso;
import com.marjuv.entity.usuario;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.swing.JFileChooser;
import javax.swing.plaf.FileChooserUI;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;


import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class administracion_win {

    @Wire("#admin_win")
    private Window win;
    private administracion selectedAdmin;
    private String recordMode;
    @WireVariable
    private Session _sess;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sUsuario") BeanUsuarioMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException, Exception {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);

        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        Date fecha = new Date(currentDate.getTime());
        int year = fecha.getYear() + 1900;

        administracion_dao obj_administracion_dao = new administracion_dao();
        if (obj_administracion_dao.verificar_admin(year)) {
            setRecordMode("EDIT");
            this.selectedAdmin = obj_administracion_dao.get_administracion(year);
        } else {
            setRecordMode("NEW");
            this.selectedAdmin = new administracion();
            this.selectedAdmin.setYear_admi(year);
        }
        obj_administracion_dao.close();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

//        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
//        Messagebox.show("URL: " + sv.getRealPath("/archivos/"));
        administracion_dao obj_administracion_dao = new administracion_dao();
        try {
//            Calendar fecha2 = new GregorianCalendar();
//            int año = fecha2.get(Calendar.YEAR);
//            Messagebox.show("Año actual: " + año);
//            
//            administracion ObjAdministracionPrueba = new administracion();
//            ObjAdministracionPrueba = obj_administracion_dao.get_administracion(año);
//            Messagebox.show("URL File: " + ObjAdministracionPrueba.getUrl_file_admin()+"\\archivos\\");
//            Messagebox.show("URL File: " + System.getProperty("user.dir"));

            if (getRecordMode().equals("NEW")) {
                obj_administracion_dao.insert(selectedAdmin);
                Messagebox.show("La administración ha sido registrada con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
                Executions.sendRedirect("page.zul");
            } else if (getRecordMode().equals("EDIT")) {
                obj_administracion_dao.update(selectedAdmin);
                Messagebox.show("La administración ha sido actualizada con éxito", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
            }
            obj_administracion_dao.close();
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
        }

    }

    public administracion getSelectedAdmin() {
        return selectedAdmin;
    }

    public void setSelectedAdmin(administracion selectedAdmin) {
        this.selectedAdmin = selectedAdmin;
    }

    @Command
    @NotifyChange("*")
    public static void BackupSisTra() throws IOException, InterruptedException, Exception {   
       
        try {
       administracion obj = new administracion();
       administracion_dao ob = new administracion_dao();
       obj=ob.administracion_backup();
       String fecha=""+Date.valueOf(LocalDate.now());
       String h=""+LocalDateTime.now().getHour();
       String m=""+LocalDateTime.now().getMinute();
       String s=""+LocalDateTime.now().getSecond();
       obj.setNombreBackup(fecha+"-db_sistra_muni-"+h+"h-"+m+"m-"+s+"s.backup");
        ob.updateBackup(obj);
        Runtime rt = Runtime.getRuntime();
        Process p;
        ProcessBuilder pb;
        rt = Runtime.getRuntime();
         File folder = new File(obj.getUrl_backup_admin());
           if(folder.exists()){
        
        pb = new ProcessBuilder(
                "C:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_dump.exe",
                "--host", "localhost",
                "--port", "5432",
                "--username", "postgres",
                "--no-password",
                "--format", "custom",
                "--blobs",
                "--verbose", "--file",""+obj.getUrl_backup_admin()+obj.getNombreBackup(), "db_sistra_muni");
        
       
            final Map<String, String> env = pb.environment();
            env.put("PGPASSWORD", "123456");
            p = pb.start();
            final BufferedReader r = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            String line = r.readLine();
            while (line != null) {
                System.err.println(line);
                line = r.readLine();
              
            }
            r.close();
            p.waitFor();
            System.out.println(p.exitValue());
             Messagebox.show("Backup Exitoso!!!", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
              }else{
                Messagebox.show("Backup No Exitoso, Disco "+obj.getUrl_backup_admin()+" No Existe!!!", "Resultado", Messagebox.OK, Messagebox.ERROR);
           }
        } catch (IOException  e) {
            System.out.println(e.getMessage());
       }
    }
    
    @Command
    @NotifyChange("*")
    public void RestoreSistra()throws IOException,InterruptedException, Exception{
        try{ 
       administracion obj = new administracion();
       administracion_dao ob = new administracion_dao();
       obj=ob.administracion_backup();
        Runtime rt = Runtime.getRuntime();
        Process p;
        ProcessBuilder pb;
        rt = Runtime.getRuntime();
         File folder = new File(obj.getUrl_backup_admin());
           if(folder.exists()){
        pb = new ProcessBuilder(
                "C:\\Program Files\\PostgreSQL\\9.4\\bin\\pg_restore.exe",
                "-i","-h","localhost",
                "-p","5432",
                "-U","postgres",
                "-d","db_sistra_muni",
                "-v", obj.getUrl_backup_admin()+obj.getNombreBackup());
     pb.environment().put("PGPASSWORD", "123456");
      p = pb.start();
       BufferedReader r = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            r.close();
            p.waitFor();
            System.out.println(p.exitValue());
            Messagebox.show("Restore Exitoso!!!", "Resultado", Messagebox.OK, Messagebox.INFORMATION);
              }else{
                Messagebox.show("Restore No Exitoso, Fichero "+obj.getUrl_backup_admin()+obj.getNombreBackup()+" No Existe!!!", "Resultado", Messagebox.OK, Messagebox.ERROR);
           }
    } catch (Exception e) { 
    System.out.println(e.getMessage());
    }
}

}
