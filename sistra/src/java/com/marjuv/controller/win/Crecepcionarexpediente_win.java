/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Crecepcionarexpediente_win {

    @WireVariable
    private Session _sess;
    private Expediente_dao dao;
    private Date curFechaDesde;
    private Date curFechaHasta;
    private int tipoCNS;
    private String curNumExp;
    private List<ExpedienteCliente> lista_expediente;

    private Set<ExpedienteCliente> pickedItemSet = new HashSet<ExpedienteCliente>();
    private List<ExpedienteCliente> listVinculados;

    public List<ExpedienteCliente> getListVinculados() {
        return listVinculados;
    }

    public void setListVinculados(List<ExpedienteCliente> listVinculados) {
        this.listVinculados = listVinculados;
    }

    public Set<ExpedienteCliente> getPickedItemSet() {
        return pickedItemSet;
    }

    public void setPickedItemSet(Set<ExpedienteCliente> pickedItemSet) {
        this.pickedItemSet = pickedItemSet;
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
        }
        return fin;
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("recepcionarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Init
    public void initSetup() {
        check_permisos();
        dao = new Expediente_dao();
        curFechaDesde = new Date();
        curFechaHasta = new Date();
        curNumExp = "";
        tipoCNS = 1;
        actualizar_lista_expediente();

    }

    @Command
    public void BuscarExpediente() {
        actualizar_lista_expediente();
        BindUtils.postNotifyChange(null, null, Crecepcionarexpediente_win.this, "lista_expediente");
    }

    List<ExpedienteCliente> exps;

    @Command
    public void RecepcionarExpediente() throws SQLException {
        exps = new ArrayList<ExpedienteCliente>();
        for (ExpedienteCliente expedienteCliente : getPickedItemSet()) {
            boolean sw = false;
            for (ExpedienteCliente expedienteCliente2 : exps) {

                if (expedienteCliente.getEXPNUM().equals(expedienteCliente2.getEXPNUM())) {
                    sw = true;
                }
            }
            if (!sw) {
                exps.add(expedienteCliente);
            }
        }

        if (exps.size() > 0) {
            final com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            int cont_real = 0;
            String msgerror = "";
            boolean only = false;
            dao = new Expediente_dao();
            List<ExpedienteCliente> lista_agregar = new ArrayList<ExpedienteCliente>();
            final List<ExpedienteCliente> lista_total = dao.get_expedientes_recepcionar(obj_usuario.getCodi_arde(), 0, curNumExp);
            for (ExpedienteCliente expedienteCliente : exps) {
                try {
                    setListVinculados(dao.get_expediente_vinculados_total(expedienteCliente.getEXPNUM(), expedienteCliente.getDESTINO()));
                    if (listVinculados == null || listVinculados.isEmpty()) {
                        only = true;
                        lista_agregar.add(expedienteCliente);
                        cont_real++;
                    } else {
                        only = false;
                        lista_agregar = new ArrayList<ExpedienteCliente>();
                        for (ExpedienteCliente expe : listVinculados) {
                            boolean sw = false;
                            for (ExpedienteCliente expedienteCliente1 : lista_total) {
                                if (expe.getEXPNUM().equals(expedienteCliente1.getEXPNUM())) {
                                    sw = true;
                                    lista_agregar.add(expedienteCliente1);
                                    break;
                                }
                            }
                            if (!sw) {
                                msgerror = "Uno de los expedientes vinculados no se encuentra en la dependencia no es posible proceder, " + expe.getEXPNUM();
                                break;
                            }
                        }
                    }
                    cont_real++;
                } catch (Exception ex) {
                    Logger.getLogger(Crecepcionarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (only) {
                Messagebox.show("Se van a recepcionar  " + exps.size() + " Expediente(s) y 0 vinculaciones"
                        + "\n ¿Desea continuar?", "Recepcionar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            int cont = 0;
                            dao = new Expediente_dao();
                            try {
                                for (ExpedienteCliente expedienteCliente : exps) {
                                    dao.expediente_recepcionar(expedienteCliente.getEXPNUM(), expedienteCliente.getCORRELATIVO(), expedienteCliente.getDESTINO(), obj_usuario.getCodi_usua());
                                    cont++;
                                }
                                Messagebox.show(cont + " Expediente(s) Recepcionados");
                            } catch (Exception ex) {
                                Messagebox.show("Error al recepcionar. " + ex.getMessage());
                            } finally {
                                dao.close();
                            }
                            BuscarExpediente();
                        }
                    }
                });
            } else if (msgerror.isEmpty()) {
                final List<ExpedienteCliente> listClone = lista_agregar;
                Messagebox.show("Se van a recepcionar  " + cont_real + " Expediente(s) y " + (lista_agregar.size() - cont_real) + " vinculacion(es)"
                        + "\n ¿Desea continuar?", "Recepcionar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) throws SQLException {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            int cont = 0;
                            dao = new Expediente_dao();
                            try {
                                for (ExpedienteCliente expe : listClone) {
                                    dao.expediente_recepcionar(expe.getEXPNUM(), expe.getCORRELATIVO(), expe.getDESTINO(), obj_usuario.getCodi_usua());
                                    lista_total.remove(expe);
                                    cont++;
                                }
                                Messagebox.show(cont + " Expediente(s) Recepcionados");
                            } catch (Exception ex) {
                                Messagebox.show("Error al recepcionar. " +ex.getMessage());
                            } finally {
                                dao.close();
                            }
                            BuscarExpediente();
                        }
                    }
                });
            } else {
                Messagebox.show(msgerror);
            }

        } else {
            Messagebox.show("No se ha seleccionado ningun expediente",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
        }
        BindUtils.postNotifyChange(null, null, Crecepcionarexpediente_win.this, "lista_expediente");
    }

    private void actualizar_lista_expediente() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            com.marjuv.entity.usuario obj_usuario = (com.marjuv.entity.usuario) _sess.getAttribute("usuario");
            try {
                String parametro = "";
                if (tipoCNS == 1) {
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String fechadesde = formatter.format(curFechaDesde);
                    String fechahasta = formatter.format(curFechaHasta);
                    parametro = fechadesde + fechahasta;
                } else {
                    parametro = curNumExp;
                }
                setLista_expediente(dao.get_expedientes_recepcionar(obj_usuario.getCodi_arde(), tipoCNS, parametro));
            } catch (Exception ex) {
                Logger.getLogger(Crecepcionarexpediente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public List<ExpedienteCliente> getLista_expediente() {
        return lista_expediente;
    }

    public void setLista_expediente(List<ExpedienteCliente> lista_expediente) {
        this.lista_expediente = lista_expediente;
    }

    public Expediente_dao getDao() {
        return dao;
    }

    public void setDao(Expediente_dao dao) {
        this.dao = dao;
    }

    public Date getCurFechaDesde() {
        return curFechaDesde;
    }

    public void setCurFechaDesde(Date curFechaDesde) {
        this.curFechaDesde = curFechaDesde;
    }

    public Date getCurFechaHasta() {
        return curFechaHasta;
    }

    public void setCurFechaHasta(Date curFechaHasta) {
        this.curFechaHasta = curFechaHasta;
    }

    public String getCurNumExp() {
        return curNumExp;
    }

    public void setCurNumExp(String curNumExp) {
        this.curNumExp = curNumExp;
    }

    public int getTipoCNS() {
        return tipoCNS;
    }

    public void setTipoCNS(int tipoCNS) {
        this.tipoCNS = tipoCNS;
    }

}
