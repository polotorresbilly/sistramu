package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.Bean_Reporte_Estados_Expediente;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.entity.area_dependencia;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.mail.Message;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

public class Reporte_Estado_Expediente_Area_Win {

    /*Listas ComboBox*/
//    private List<Pair<Integer, String>> lista_ComboBox_Dependencias;
    private List<Pair<Integer, String>> lista_ComboBox_SubGerencias;
    private List<Pair<Integer, String>> lista_ComboBox_Estados;

    /*Selectores*/
//    private Pair<Integer, String> selector_ComboBox_Dependencias;
    private BeanDependencias selector_ComboBox_Dependencias;
    private BeanDependencias selector_ComboBox_SubDependencia;

    private Pair<Integer, String> selector_ComboBox_Estados;
    private Date selector_Fecha_Inicio;
    private Date selector_Fecha_Fin;
    private Bean_Reporte_Estados_Expediente selector_Busqueda;

    /*Variables*/
    int Opcion_Busqueda_Avanzada = 0;

    /*Lista del LixtBox*/
    private List<Bean_Reporte_Estados_Expediente> lista_Busqueda;
    private List<BeanDependencias> lista_combo_dependencias;
    private List<BeanDependencias> lista_combo_subDependencias;

    /*Elementos ZK*/
    @Wire
    Combobox ComboBox_Dependencias;
    @Wire
    Combobox RadioButton_Numero_Expediente;
    @Wire
    Combobox RadioButton_Procedimiento_Expediente;
    @Wire
    private Textbox txtCodiProc;
    @Wire
    private Textbox txtNumero_expediente;
    @Wire
    private Textbox txtNombProc;
    @Wire
    private Datebox txtFecha_Inicio;
    @Wire
    private Datebox txtFecha_Fin;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String get_fecha_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(0, 10);
        } catch (Exception e) {
        }
        return fin;
    }

    public String get_finalizo(String finalizo) {
        String fin = "";
        try {
            fin = finalizo.substring(10, 11);
        } catch (Exception e) {
        }
        return fin;
    }

    @Init
    public void initSetup() {
        try {
            Cargar_ComboBox_Dependencias();
            Cargar_ComboBox_Estados();
            Cargar_DateBox_Fechas();
        } catch (Exception e) {
        }

    }

    public void Cargar_ComboBox_Dependencias() {
        try {
            area_dependencia_dao ObjDependenciasDAO = new area_dependencia_dao();
            setLista_combo_dependencias(ObjDependenciasDAO.Llenar_Combo_Dependencias());
            ObjDependenciasDAO.close();
        } catch (Exception e) {
            Messagebox.show("Error Cargar Combo de las Depedencias: " + e.getMessage());
        }
    }
    @NotifyChange("*")
    @Command
    public void Cargar_ComboBox_SubGerencias() {
        try {
            area_dependencia_dao ObjDependenciasDAO = new area_dependencia_dao();
            setLista_ComboBox_SubGerencias(ObjDependenciasDAO.get_select_Subgerencia(selector_ComboBox_SubDependencia.getJera_arde()));
            ObjDependenciasDAO.close();
        } catch (Exception e) {
            Messagebox.show("Error Cargar Combo de las Depedencias: " + e.getMessage());
        }
    }

    public void Cargar_ComboBox_Estados() {
        try {
            lista_ComboBox_Estados = new ArrayList<Pair<Integer, String>>();
            lista_ComboBox_Estados.add(new Pair<Integer, String>(1, "Enviados"));
            lista_ComboBox_Estados.add(new Pair<Integer, String>(2, "Recepcionados"));
            lista_ComboBox_Estados.add(new Pair<Integer, String>(3, "Observados"));
            lista_ComboBox_Estados.add(new Pair<Integer, String>(3, "Finalizados"));
            selector_ComboBox_Estados = lista_ComboBox_Estados.get(0);
        } catch (Exception e) {
            Messagebox.show("Error Cargar Combo de los Estados: " + e.getMessage());
        }
    }

    public void Cargar_DateBox_Fechas() {
        try {
            selector_Fecha_Inicio = new Date();
            selector_Fecha_Fin = new Date();
        } catch (Exception e) {
            Messagebox.show("Error Cargar DateTexbox: " + e.getMessage());
        }
    }

    @Command
    public void Seleccionar_N_Expediente() {
        try {
            Opcion_Busqueda_Avanzada = 1;
        } catch (Exception e) {
            Messagebox.show("Error Cargar Seleccionar por Numero de Expediente: " + e.getMessage());
        }
    }

    @Command
    public void Seleccionar_Procedimiento_Expediente() {
        try {
            Opcion_Busqueda_Avanzada = 2;
        } catch (Exception e) {
            Messagebox.show("Error Cargar Seleccionar por Procedimiento de Expediente: " + e.getMessage());
        }
    }

    @NotifyChange("*")
    @Command
    public void Mostrar_Modal_Procedimiento() {
        Executions.createComponents("ZonaCompartida/add_procedimiento.zul", null, null);
    }

    @Command
    public void Buscar_Expediente() {
        try {
            if (getSelector_ComboBox_Dependencias() != null) {
                if (selector_ComboBox_Estados != null) {
                    if (Opcion_Busqueda_Avanzada == 1) {
                        Expediente_dao ObjExpedienteDAO = new Expediente_dao();
                        setLista_Busqueda(ObjExpedienteDAO.Buscar_Expedientes_por_Numero(txtNumero_expediente.getValue(), txtFecha_Inicio.getValue().toString(), txtFecha_Fin.getValue().toString()));
                        Messagebox.show("Numero de Expediente Controlador: " + lista_Busqueda.get(0).getNUMERO_EXPEDIENTE());
                        Messagebox.show("Numero de Expediente Controlador: " + lista_Busqueda.get(0).getADMINISTRADO_CORRECTO());
                        ObjExpedienteDAO.close();
                    } else {
                        if (Opcion_Busqueda_Avanzada == 2) {

                        }
                    }

                } else {
                    Messagebox.show("Debe Seleccionar un Estado");
                }
            } else {
                Messagebox.show("Debe Seleccionar una Dependencia");
            }
        } catch (Exception e) {
            Messagebox.show("Mensaje de Error Buscar Expediente: " + e.getMessage());
        }
        BindUtils.postNotifyChange(null, null, Reporte_Estado_Expediente_Area_Win.this, "lista_Busqueda");
    }

    @GlobalCommand
    @NotifyChange("*")
    public void setProcedimiento(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc) {
        this.txtCodiProc.setValue(String.valueOf(codi_proc));
        this.txtNombProc.setValue(nomb_proc);
    }

    public List<Pair<Integer, String>> getLista_ComboBox_Estados() {
        return lista_ComboBox_Estados;
    }

    public void setLista_ComboBox_Estados(List<Pair<Integer, String>> lista_ComboBox_Estados) {
        this.lista_ComboBox_Estados = lista_ComboBox_Estados;
    }

    public Pair<Integer, String> getSelector_ComboBox_Estados() {
        return selector_ComboBox_Estados;
    }

    public void setSelector_ComboBox_Estados(Pair<Integer, String> selector_ComboBox_Estados) {
        this.selector_ComboBox_Estados = selector_ComboBox_Estados;
    }

    public Date getSelector_Fecha_Inicio() {
        return selector_Fecha_Inicio;
    }

    public void setSelector_Fecha_Inicio(Date selector_Fecha_Inicio) {
        this.selector_Fecha_Inicio = selector_Fecha_Inicio;
    }

    public Date getSelector_Fecha_Fin() {
        return selector_Fecha_Fin;
    }

    public void setSelector_Fecha_Fin(Date selector_Fecha_Fin) {
        this.selector_Fecha_Fin = selector_Fecha_Fin;
    }

    public Bean_Reporte_Estados_Expediente getSelector_Busqueda() {
        return selector_Busqueda;
    }

    public void setSelector_Busqueda(Bean_Reporte_Estados_Expediente selector_Busqueda) {
        this.selector_Busqueda = selector_Busqueda;
    }

    public List<Bean_Reporte_Estados_Expediente> getLista_Busqueda() {
        return lista_Busqueda;
    }

    public void setLista_Busqueda(List<Bean_Reporte_Estados_Expediente> lista_Busqueda) {
        this.lista_Busqueda = lista_Busqueda;
    }

    public List<Pair<Integer, String>> getLista_ComboBox_SubGerencias() {
        return lista_ComboBox_SubGerencias;
    }

    public void setLista_ComboBox_SubGerencias(List<Pair<Integer, String>> lista_ComboBox_SubGerencias) {
        this.lista_ComboBox_SubGerencias = lista_ComboBox_SubGerencias;
    }

    public List<BeanDependencias> getLista_combo_dependencias() {
        return lista_combo_dependencias;
    }

    public void setLista_combo_dependencias(List<BeanDependencias> lista_combo_dependencias) {
        this.lista_combo_dependencias = lista_combo_dependencias;
    }

    public BeanDependencias getSelector_ComboBox_Dependencias() {
        return selector_ComboBox_Dependencias;
    }

    public void setSelector_ComboBox_Dependencias(BeanDependencias selector_ComboBox_Dependencias) {
        this.selector_ComboBox_Dependencias = selector_ComboBox_Dependencias;
    }

    public BeanDependencias getSelector_ComboBox_SubDependencia() {
        return selector_ComboBox_SubDependencia;
    }

    public void setSelector_ComboBox_SubDependencia(BeanDependencias selector_ComboBox_SubDependencia) {
        this.selector_ComboBox_SubDependencia = selector_ComboBox_SubDependencia;
    }

    public List<BeanDependencias> getLista_combo_subDependencias() {
        return lista_combo_subDependencias;
    }

    public void setLista_combo_subDependencias(List<BeanDependencias> lista_combo_subDependencias) {
        this.lista_combo_subDependencias = lista_combo_subDependencias;
    }

}
