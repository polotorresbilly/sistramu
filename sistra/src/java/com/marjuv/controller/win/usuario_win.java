package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class usuario_win {

    private List<BeanUsuarioMan> lst_usuario;
    private BeanUsuarioMan cur_usuario;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Apellidos y Nombres"));
        listBusqueda.add(new Pair<Integer, String>(3, "DNI"));
        listBusqueda.add(new Pair<Integer, String>(4, "Nick"));
        listBusqueda.add(new Pair<Integer, String>(5, "Nombre de Rol"));
        listBusqueda.add(new Pair<Integer, String>(6, "Area Dependencia"));
        listBusqueda.add(new Pair<Integer, String>(7, "Cargo"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("lst_usuario")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_usuario")
    public void updateGrid() {
        try {
            usuario_dao dao = new usuario_dao();
            lst_usuario = dao.get_usuarios();
            List<BeanUsuarioMan> new_list = new ArrayList<BeanUsuarioMan>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getApel_usua().toUpperCase() + " " + contribuyente.getNomb_usua().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getDni_usua() + "").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 4:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getNick_usua() + "").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 5:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getNomb_rol() + "").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 6:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getNomb_arde() + "").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 7:
                        for (BeanUsuarioMan contribuyente : lst_usuario) {
                            if ((contribuyente.getNomb_carg() + "").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lst_usuario = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        loadDataControls();
        try {
            usuario_dao obj_usuario_dao = new usuario_dao();
            lst_usuario = obj_usuario_dao.get_usuarios();
            obj_usuario_dao.close();
        } catch (Exception ex) {
        }

    }

    public List<BeanUsuarioMan> getLst_usuario() {
        return lst_usuario;
    }

    @Command
    public void nuevo_usuario() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sUsuario", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_usuario.zul", null, map);
    }

    @Command
    public void editThisUSuario() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sUsuario", this.cur_usuario);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_usuario.zul", null, map);
        } catch (Exception ex) {
        }
    }

    @Command
    public void changeThisUSuario() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sUsuario", this.cur_usuario);
            map.put("accion", "CHANGE");
            Executions.createComponents("mantenimiento/man/man_pass_usuario.zul", null, map);
        } catch (Exception ex) {
        }
    }

    public void actualizar_usuario(final int codigo, boolean sw) throws Exception {
        usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
        if (codigo == obj_usuario.getCodi_usua()) {
            if (sw) {
                String str = "Se recargará la página para actualizar los datos de tu sesión.";
                Messagebox.show(str, "Confirm", Messagebox.OK, Messagebox.INFORMATION, new EventListener() {
                    public void onEvent(Event e) throws Exception {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            usuario_dao obj_usuario_dao = new usuario_dao();
                            usuario obj_usuario = obj_usuario_dao.get_usuario(codigo);
                            _sess.setAttribute("usuario", obj_usuario);
                            Executions.sendRedirect("page.zul");
                        }
                    }
                });

            } else {
                String str = "Por favor, vuelva a iniciar sesión para aplicar los cambios";
                Messagebox.show(str, "Confirm", Messagebox.OK, Messagebox.INFORMATION, new EventListener() {
                    public void onEvent(Event e) throws Exception {
                        if (Messagebox.ON_OK.equals(e.getName())) {
                            _sess.setAttribute("usuario", null);
                            Executions.sendRedirect("index.zul");
                        }
                    }
                });

            }
        }
    }

    @GlobalCommand
    @NotifyChange("lst_usuario")
    public void updateUsuarioList(@BindingParam("sUsuario") usuario cur_usuario,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            usuario_dao obj_usuario_dao = new usuario_dao();
            if (recordMode.equals("NEW")) {
                obj_usuario_dao.insert(cur_usuario);
                Messagebox.show("Se inserto el nuevo usuario",
                        "Usuarios", Messagebox.OK, Messagebox.INFORMATION);
                obj_usuario_dao.close();
                insert_auditoria("I", "USUARIO");
                actualizar_usuario(cur_usuario.getCodi_usua(), true);
            } else if (recordMode.equals("EDIT")) {
                obj_usuario_dao.update(cur_usuario);
                Messagebox.show("Se Modifico al usuario",
                        "Usuarios", Messagebox.OK, Messagebox.INFORMATION);
                obj_usuario_dao.close();
                insert_auditoria("U", "USUARIO");
                actualizar_usuario(cur_usuario.getCodi_usua(), true);
            } else if (recordMode.equals("CHANGE")) {
                obj_usuario_dao.update_pass(cur_usuario);
                Messagebox.show("Se actualizo la contraseña al usuario",
                        "Usuarios", Messagebox.OK, Messagebox.INFORMATION);
                obj_usuario_dao.close();
                insert_auditoria("U", "USUARIO");
                actualizar_usuario(cur_usuario.getCodi_usua(), false);
            }
            try {
                usuario_dao dao = new usuario_dao();
                lst_usuario = dao.get_usuarios();
                dao.close();
            } catch (Exception ex) {
            }
        }
        updateGrid();
    }

    @GlobalCommand
    @NotifyChange("Lst_usuario")
    public void updateUsuarioList2() {

    }
    
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisUsuario() {
        String str = "¿Está seguro de que desea eliminar el usuario \""
                + cur_usuario.getNomb_usua() + " " + cur_usuario.getApel_usua() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
                        boolean sw = true;
                        if (obj_usuario.getCodi_usua() == cur_usuario.getCodi_usua()) {
                            sw = false;
                        }
                        if (sw) {
                            usuario_dao obj_usuario_dao = new usuario_dao();
                            obj_usuario_dao.delete(cur_usuario.getCodi_usua());
                            insert_auditoria("D", "USUARIO");
                            BindUtils.postNotifyChange(null, null, usuario_win.this, "lst_usuario");
                            updateGrid();
                        } else {
                            usuario_dao obj_usuario_dao = new usuario_dao();
                            obj_usuario_dao.delete(cur_usuario.getCodi_usua());
                            insert_auditoria("D", "USUARIO");
                            String str = "Tu usuario ha sido eliminado, se cerrará la sesión.";
                            Messagebox.show(str, "Confirm", Messagebox.OK, Messagebox.INFORMATION, new EventListener() {
                                public void onEvent(Event e) throws Exception {
                                    if (Messagebox.ON_OK.equals(e.getName())) {
                                        _sess.setAttribute("usuario", null);
                                        Executions.sendRedirect("index.zul");
                                    }
                                }
                            });
                        }

                    } catch (Exception ex) {
                        Logger.getLogger(usuario_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_usuario(List<BeanUsuarioMan> lst_usuario) {
        this.lst_usuario = lst_usuario;
    }

    public BeanUsuarioMan getCur_usuario() {
        return cur_usuario;
    }

    public void setCur_usuario(BeanUsuarioMan cur_usuario) {
        this.cur_usuario = cur_usuario;
    }

}
