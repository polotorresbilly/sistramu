package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.requisito_procedimiento;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class procedimiento_expediente_win {

    private List<BeanProcedimientoMan> lst_procedimiento_expediente;
    private List<BeanRequisitoMan> lst_requisito_procedimiento;
    private BeanRequisitoMan cur_requisito_procedimiento;
    private BeanProcedimientoMan cur_procedimiento_expediente;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
    
    public BeanRequisitoMan getCur_requisito_procedimiento() {
        return cur_requisito_procedimiento;
    }

    public void setCur_requisito_procedimiento(BeanRequisitoMan cur_requisito_procedimiento) {
        this.cur_requisito_procedimiento = cur_requisito_procedimiento;
    }
    
    public List<BeanRequisitoMan> getLst_requisito_procedimiento() {
        return lst_requisito_procedimiento;
    }

    public void setLst_requisito_procedimiento(List<BeanRequisitoMan> lst_requisito_procedimiento) {
        this.lst_requisito_procedimiento = lst_requisito_procedimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }
    
    
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Procedimiento"));
        listBusqueda.add(new Pair<Integer, String>(3, "Asunto"));
        curBusqueda = listBusqueda.get(0);
    }
    
    

    @Command
    @NotifyChange("lst_procedimiento_expediente")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lst_procedimiento_expediente")
    public void updateGrid() {
        try {
            procedimiento_expediente_dao dao = new procedimiento_expediente_dao();
            lst_procedimiento_expediente = dao.get_procedimiento_expediente();
            List<BeanProcedimientoMan> new_list = new ArrayList<BeanProcedimientoMan>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanProcedimientoMan contribuyente : lst_procedimiento_expediente) {
                            if ((contribuyente.getNomb_proc().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (BeanProcedimientoMan contribuyente : lst_procedimiento_expediente) {
                            if ((contribuyente.getDesc_asex()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lst_procedimiento_expediente = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            lst_procedimiento_expediente = obj_procedimiento_expediente_dao.get_procedimiento_expediente();
            obj_procedimiento_expediente_dao.close();
        } catch (Exception ex) {
        }
    }

    public List<BeanProcedimientoMan> getLst_procedimiento_expediente() {
        return lst_procedimiento_expediente;
    }

    @Command
    public void nuevo_procedimiento_expediente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sProcedimiento", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_procedimiento_expediente.zul", null, map);
    }

    @Command
    public void editThisProcedimiento() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("sProcedimiento", this.cur_procedimiento_expediente);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_procedimiento_expediente.zul", null, map);
        } catch (Exception ex) {
        }
    }
    
    @Command
    public void agregar_Requisito() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sProcedimiento", this.cur_procedimiento_expediente);
        map.put("accion", "Asignar");
        Executions.createComponents("mantenimiento/man/asignar_requisitos.zul", null, map);
    }
    
    @GlobalCommand
    @NotifyChange("lst_procedimiento_expediente")
    public void updateProcedimientoList(@BindingParam("sRequisito") requisito_procedimiento cur_requisito ,@BindingParam("sProcedimiento") Procedimiento_Expediente cur_procedimiento_expediente,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            if (recordMode.equals("NEW")) {
                obj_procedimiento_expediente_dao.insert(cur_procedimiento_expediente);
                obj_procedimiento_expediente_dao.close();
                insert_auditoria("I", "PROCEDIMIENTO_EXPEDIENTE");
                Messagebox.show("El procedimiento fue registrado");
            } else if (recordMode.equals("EDIT")) {
                obj_procedimiento_expediente_dao.update(cur_procedimiento_expediente);
                obj_procedimiento_expediente_dao.close();
                insert_auditoria("U", "PROCEDIMIENTO_EXPEDIENTE");
                Messagebox.show("El procedimiento fue actualizado");
            }else if (recordMode.equals("Asignar")){
                requisito_procedimiento_dao ObjRequisito = new requisito_procedimiento_dao();
                ObjRequisito.insert(cur_requisito);
                Messagebox.show("Se asginaron correctamente los requisitos");
            }
        }
        updateGrid();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteThisProcedimiento() {
        String str = "¿Está seguro de que desea eliminar el procedimiento de expediente \""
                + cur_procedimiento_expediente.getNomb_proc() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
                        obj_procedimiento_expediente_dao.delete(cur_procedimiento_expediente.getCodi_proc());
                        insert_auditoria("D", "PROCEDIMIENTO_EXPEDIENTE");
                        BindUtils.postNotifyChange(null, null, procedimiento_expediente_win.this, "lst_procedimiento_expediente");
                        updateGrid();
                    } catch (Exception ex) {
                        Logger.getLogger(procedimiento_expediente_win.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public void setLst_procedimiento_expediente(List<BeanProcedimientoMan> lst_procedimiento_expediente) {
        this.lst_procedimiento_expediente = lst_procedimiento_expediente;
    }

    public BeanProcedimientoMan getCur_procedimiento_expediente() {
        return cur_procedimiento_expediente;
    }

    public void setCur_procedimiento_expediente(BeanProcedimientoMan cur_procedimiento_expediente) {
        this.cur_procedimiento_expediente = cur_procedimiento_expediente;
    }

}
