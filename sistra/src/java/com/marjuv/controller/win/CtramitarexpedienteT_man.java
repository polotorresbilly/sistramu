/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Cargo_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.Cargo;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.administracion;
import com.marjuv.entity.usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.docx4j.Docx4jProperties;
import org.docx4j.convert.out.pdf.PdfConversion;
import org.docx4j.convert.out.pdf.viaXSLFO.PdfSettings;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.utils.Log4jConfigurator;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.Pair;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class CtramitarexpedienteT_man {

    @Wire("#tramitarexpedienteT_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private List<BeanDependencias> list_dependencias;
    private BeanDependencias curDependencia;
    private BeanDependencias curDependencia2;
    private List<Cargo> list_cargo;
    private Cargo curCargo;
    private int curFolios;
    private String nProveido;
    private String resuProveido;
    private boolean CurDocumento;
    private String mensaje;
    private Double costosVarios;

    private Expediente_dao dao_exp;
    private Dependencia_dao dao_dep;
    private Cargo_dao dao_carg;

    private Media media;
    private String file_name;
    private String ext;

    @Wire
    private Button submit;

    private boolean sw_save;
    private String cNumDoc;

    @WireVariable
    private Session _sess;

    private List<Pair<Integer, String>> listTipoDocumento;
    private Pair<Integer, String> curTipoDocumento;

    public Pair<Integer, String> getCurTipoDocumento() {
        return curTipoDocumento;
    }

    public void setCurTipoDocumento(Pair<Integer, String> curTipoDocumento) {
        this.curTipoDocumento = curTipoDocumento;
    }

    public List<Pair<Integer, String>> getListTipoDocumento() {
        return listTipoDocumento;
    }

    public void setListTipoDocumento(List<Pair<Integer, String>> listTipoDocumento) {
        this.listTipoDocumento = listTipoDocumento;
    }

    public String getcNumDoc() {
        return cNumDoc;
    }

    public void setcNumDoc(String cNumDoc) {
        this.cNumDoc = cNumDoc;
    }

    private List<ExpedienteCliente> listVinculados;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public List<ExpedienteCliente> getListVinculados() {
        return listVinculados;
    }

    public void setListVinculados(List<ExpedienteCliente> listVinculados) {
        this.listVinculados = listVinculados;
    }

    public int get_total_vinculados() {
        return listVinculados.size();
    }

    @Command
    public void ver_expe() {
        Map parametros = new HashMap();
        parametros.put("list", listVinculados);
        Executions.createComponents("ZonaCompartida/win_visualizar_cargo.zul", null, parametros);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("tramitarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    public BeanDependencias getCurDependencia2() {
        return curDependencia2;
    }

    public void setCurDependencia2(BeanDependencias curDependencia2) {
        this.curDependencia2 = curDependencia2;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Double getCostosVarios() {
        return costosVarios;
    }

    public void setCostosVarios(Double costosVarios) {
        this.costosVarios = costosVarios;
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1)
            throws CloneNotSupportedException {
        check_permisos();
        sw_save = true;
        Selectors.wireComponents(view, this, false);
        dao_exp = new Expediente_dao();
        dao_dep = new Dependencia_dao();
        dao_carg = new Cargo_dao();
        setCurDocumento(false);
        setCostosVarios(0.0);
        try {
            curExpediente = c1;
            setListVinculados(dao_exp.get_expediente_vinculados_total(curExpediente.getEXPNUM(), curExpediente.getDESTINO()));
            dao_exp.close();
            List<BeanDependencias> list = new ArrayList<BeanDependencias>();
            win.setTitle("Tramitando Expediente N° : " + curExpediente.getEXPNUM());
            for (BeanDependencias beanDependencias : dao_dep.getDependencias()) {

                list.add(beanDependencias);

            }
            general_dao obj_general_dao = new general_dao();
            setListTipoDocumento(obj_general_dao.get_select_tipo_expediente());
            obj_general_dao.close();
            if (getListTipoDocumento().size() > 0) {
                curTipoDocumento = getListTipoDocumento().get(0);
            }
            setList_dependencias(list);
            dao_dep.close();
            dao_carg.close();
            //setList_dependencias(dao_dep.getDependencias());
        } catch (Exception ex) {
            Logger.getLogger(CtramitarexpedienteT_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    boolean exists = true;

    @NotifyChange({"media", "file_name"})
    @Command
    public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
        if (event.getMedia() != null) {
            //Messagebox.show(event.getMedia().getContentType());
            if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                    || event.getMedia().getContentType().equals("application/msword")
                    || event.getMedia().getContentType().equals("application/vnd.ms-excel")
                    || event.getMedia().getContentType().equals("application/pdf")
                    || event.getMedia().getContentType().equals("application/x-zip-compressed")
                    || event.getMedia().getContentType().equals("application/x-rar-compressed")
                    || event.getMedia().getContentType().equals("application/zip")
                    || event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                String extension = "";
                if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
                    extension = "docx";
                } else if (event.getMedia().getContentType().equals("application/msword")) {
                    extension = "doc";
                } else if (event.getMedia().getContentType().equals("application/vnd.ms-excel")) {
                    extension = "xls";
                } else if (event.getMedia().getContentType().equals("application/pdf")) {
                    extension = "pdf";
                } else if (event.getMedia().getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                    extension = "xlsx";
                } else if (event.getMedia().getContentType().equals("application/zip")) {
                    extension = "zip";
                } else if (event.getMedia().getContentType().equals("application/x-zip-compressed")) {
                    extension = "zip";
                } else if (event.getMedia().getContentType().equals("application/x-rar-compressed")) {
                    extension = "rar";
                }
                media = event.getMedia();
                if (listVinculados != null && !listVinculados.isEmpty()) {

                }
                file_name = curExpediente.getEXPNUM() + "_" + curExpediente.getDESTINO()
                        + "_" + (curExpediente.getCORRELATIVO() + 1) + "." + extension;
                ext = extension;
            } else {
                Messagebox.show("Tipo de archivo no válido. Sólo se permite *.zip,*.rar ,*.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"media", "file_name"})
    public void clearupload() {
        media = null;
        file_name = "";
    }

    public void guardar_file() {
        ServletContext sv = (ServletContext) _sess.getWebApp().getNativeContext();
        try {
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin()+"\\archivos\\";
            obj_administracion_dao.close();
//            Messagebox.show("URL: " + sv.getRealPath("/archivos/"));
            ArrayList<File> files = new ArrayList<File>();
            for (ExpedienteCliente exp : listVinculados) {
                String[] correlativos = {};
                int correlativo = 0;
                if (exp.getCORRELATIVOS().length() > 0 && exp.getCORRELATIVOS().contains(",")) {
                    correlativos = exp.getCORRELATIVOS().trim().split(",");
                    correlativo = Integer.parseInt(correlativos[correlativos.length - 1]);
                } else if (exp.getCORRELATIVOS().length() > 0) {
                    correlativo = Integer.parseInt(exp.getCORRELATIVOS().trim());
                }
//                File fi = new File(sv.getRealPath("/archivos/") + "/" + exp.getEXPNUM() + "_" + exp.getDESTINO()+ "_" + (correlativo + 1) + "." + ext);
                File fi = new File(URL_FILE + "/" + exp.getEXPNUM() + "_" + exp.getDESTINO()+ "_" + (correlativo + 1) + "." + ext);
                files.add(fi);
            }

            if (files.isEmpty()) {
                File destino = new File(URL_FILE + "/" + file_name);

                if (destino.exists()) {
                    Messagebox.show("El archivo " + file_name + " existe, ¿Está seguro de que desea reemplazar?", "Confirmarción", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                        @Override
                        public void onEvent(Event e) {
                            if (Messagebox.ON_YES.equals(e.getName())) {
                                exists = true;
                            } else {
                                exists = false;
                            }
                        }
                    });
                }

                if (exists) {
                    if (media != null) {
                        if (media.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                                || media.getContentType().equals("application/msword")
                                || media.getContentType().equals("application/vnd.ms-excel")
                                || media.getContentType().equals("application/x-zip-compressed")
                                || media.getContentType().equals("application/x-rar-compressed")
                                || media.getContentType().equals("application/zip")
                                || media.getContentType().equals("application/pdf")
                                || media.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            try {
                                Files.copy(destino, media.getStreamData());
                            } catch (Exception e) {
                            }
                        } else {
                            Messagebox.show("Tipo de archivo no válido. Sólo se permite *.zip,*.rar ,*.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                        }
                    }

                }
            } else {
                for (File file : files) {
                    if (media != null) {
                        if (media.getContentType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                                || media.getContentType().equals("application/msword")
                                || media.getContentType().equals("application/vnd.ms-excel")
                                || media.getContentType().equals("application/pdf")
                                || media.getContentType().equals("application/x-zip-compressed")
                                || media.getContentType().equals("application/x-rar-compressed")
                                || media.getContentType().equals("application/zip")
                                || media.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
                            try {
                                Files.copy(file, media.getStreamData());
                            } catch (Exception e) {
                            }
                        } else {
                            //Messagebox.show("Tipo de archivo no válido. Sólo se permite *.doc, *.docx, *.xls, *.xlsx y *.pdf", "Error", Messagebox.OK, Messagebox.EXCLAMATION);
                        }
                    }
                }
            }
        } catch (Exception e) {
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (curExpediente != null) {
            if (curDependencia != null && curCargo != null) {
                if (sw_save) {
                    Messagebox.show("Se tramitará el expediente  " + curExpediente.getEXPNUM() + ((listVinculados != null && !listVinculados.isEmpty()) ? " y sus vinculaciones" : "")
                            + "\n ¿Desea continuar?", "Tramitar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                                public void onEvent(Event e) throws SQLException {
                                    if (Messagebox.ON_YES.equals(e.getName())) {
                                        sw_save = false;
                                        Map args = new HashMap();
                                        args.put("curCargo", curCargo);
                                        curExpediente.setFOLIOS(curExpediente.getFOLIOS() + getCurFolios());
//                            curExpediente.setDESTINO(curDependencia2.getCodi_arde());
                                        args.put("foliosAdd", getCurFolios());
                                        args.put("curExpediente", curExpediente);
                                        args.put("curNProveido", getCurTipoDocumento().getY() + " - " + cNumDoc);
                                        args.put("curRProveido", resuProveido);
                                        args.put("curDocumento", CurDocumento);
                                        args.put("curCosto", costosVarios);
                                        args.put("listVinculados", listVinculados);
                                        guardar_file();
                                        BindUtils.postGlobalCommand(null, null, "updateTramitarExpediente", args);
                                        win.detach();
                                    }
                                }
                            });
                } else {
                    Messagebox.show("Estamos procesando, por favor espere...",
                            "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
                }
            } else {
                Messagebox.show("Debe de seleccioanr Area y Cargo",
                        "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
            }
        } else {
            Messagebox.show("Seleccione el expediente");
        }

    }

    @Command
    @NotifyChange("*")
    public void atrasDependencia() {
        try {
            dao_dep = new Dependencia_dao();
            dao_carg = new Cargo_dao();
            BeanDependencias beforexpe = dao_dep.getDependencia(curDependencia.getJera_arde());
            if (beforexpe != null) {
                if (beforexpe.getNive_arde() > 1) {
                    List<BeanDependencias> obj = dao_dep.getDependenciasJera(beforexpe.getJera_arde());
                    setList_dependencias(obj);
                    setCurDependencia2(null);
                    setCurDependencia(getList_dependencias().get(get_dependencia(beforexpe)));
                } else {
                    List<BeanDependencias> list = new ArrayList<BeanDependencias>();
                    for (BeanDependencias beanDependencias : dao_dep.getDependencias()) {
                        if (beanDependencias.getNive_arde() == 1) {
                            list.add(beanDependencias);
                        }
                    }
                    setList_dependencias(list);
                    setCurDependencia2(null);
                    setCurDependencia(getList_dependencias().get(get_dependencia(beforexpe)));
                }
                setMensaje("Nivel " + curDependencia.getNive_arde() + ": " + curDependencia.getNomb_arde());
                setList_cargo(dao_carg.get_cargo_area(curDependencia.getCodi_arde()));

                setCurCargo(null);
            }
            dao_dep.close();
            dao_carg.close();
        } catch (Exception ex) {
            Logger.getLogger(CtramitarexpedienteT_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int get_dependencia(BeanDependencias obj) {
        for (int i = 0; i < list_dependencias.size(); i++) {
            if (list_dependencias.get(i).getCodi_arde() == obj.getCodi_arde()) {
                return i;
            }
        }
        return 0;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("*")
    public void updateCargo() throws SQLException {
        dao_dep = new Dependencia_dao();
        dao_carg = new Cargo_dao();
        try {

            List<BeanDependencias> obj = dao_dep.getDependenciasJera(curDependencia2.getCodi_arde());
            if (obj.size() > 0) {
                setList_dependencias(obj);
            }
            curDependencia = curDependencia2;
            setMensaje("Nivel " + curDependencia.getNive_arde() + ": " + curDependencia.getNomb_arde());
            setList_cargo(dao_carg.get_cargo_area(curDependencia.getCodi_arde()));
            setCurCargo(null);

        } catch (Exception ex) {
            //Logger.getLogger(CtramitarexpedienteT_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        dao_dep.close();
        dao_carg.close();
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("*")
    public void updateCargo2() throws SQLException {
        dao_dep = new Dependencia_dao();

        try {

            List<BeanDependencias> obj = dao_dep.getDependenciasJera2();
            if (obj.size() > 0) {
                setList_dependencias(obj);
            }
            curDependencia = curDependencia2;
            setMensaje("Nivel " + curDependencia.getNive_arde() + ": " + curDependencia.getNomb_arde());
            setList_cargo(dao_carg.get_cargo_area(curDependencia.getCodi_arde()));
            setCurCargo(null);

        } catch (Exception ex) {
            //Logger.getLogger(CtramitarexpedienteT_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        dao_dep.close();
        dao_carg.close();
    }

    @Command
    public void cancelar() {
        win.detach();
    }

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public boolean getCurDocumento() {
        return CurDocumento;
    }

    public void setCurDocumento(boolean CurDocumento) {
        this.CurDocumento = CurDocumento;
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public List<BeanDependencias> getList_dependencias() {
        return list_dependencias;
    }

    public void setList_dependencias(List<BeanDependencias> list_dependencias) {
        this.list_dependencias = list_dependencias;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    public List<Cargo> getList_cargo() {
        return list_cargo;
    }

    public void setList_cargo(List<Cargo> list_cargo) {
        this.list_cargo = list_cargo;
    }

    public Cargo getCurCargo() {
        return curCargo;
    }

    public void setCurCargo(Cargo curCargo) {
        this.curCargo = curCargo;
    }

    public int getCurFolios() {
        return curFolios;
    }

    public void setCurFolios(int curFolios) {
        this.curFolios = curFolios;
    }

    public String getnProveido() {
        return nProveido;
    }

    public void setnProveido(String nProveido) {
        this.nProveido = nProveido;
    }

    public String getResuProveido() {
        return resuProveido;
    }

    public void setResuProveido(String resuProveido) {
        this.resuProveido = resuProveido;
    }

    /**
     * @return the media
     */
    public Media getMedia() {
        return media;
    }

    /**
     * @return the file_name
     */
    public String getFile_name() {
        return file_name;
    }

    /**
     * @param file_name the file_name to set
     */
    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

}
