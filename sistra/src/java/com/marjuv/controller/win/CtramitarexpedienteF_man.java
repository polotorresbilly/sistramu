/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class CtramitarexpedienteF_man {

    @Wire("#tramitarexpedienteF_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private String curAObservacion;
    private String curNObservacion;

    @WireVariable
    private Session _sess;
    private List<ExpedienteCliente> listVinculados;
    
    public List<ExpedienteCliente> getListVinculados() {
        return listVinculados;
    }

    public void setListVinculados(List<ExpedienteCliente> listVinculados) {
        this.listVinculados = listVinculados;
    }

    public int get_total_vinculados() {
        return listVinculados.size();
    }
    
    @Command
    public void ver_expe() {
        Map parametros = new HashMap();
        parametros.put("list", listVinculados);
        Executions.createComponents("ZonaCompartida/win_visualizar_cargo.zul", null, parametros);
    }

    public void check_permisos() {
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("tramitarexpediente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../index.zul");
            } finally {
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../index.zul");
            }
        } else {
            Executions.sendRedirect("../index.zul");
        }
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1)
            throws CloneNotSupportedException, Exception {
        check_permisos();
        Expediente_dao dao_exp = new Expediente_dao();
        setListVinculados(dao_exp.get_expediente_vinculados_total(c1.getEXPNUM(), c1.getDESTINO()));
        dao_exp.close();
        Selectors.wireComponents(view, this, false);
        curExpediente = c1;
        setCurNObservacion("");
        win.setTitle("Finalizando Expediente N° : " + curExpediente.getEXPNUM());
        setCurAObservacion(curExpediente.getOBSERVACION());
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (!curNObservacion.isEmpty()) {
            Messagebox.show("Se finalizará el expediente  " + curExpediente.getEXPNUM()+ ((listVinculados!=null && !listVinculados.isEmpty())?" y sus vinculaciones":"")
                    + "\n ¿Desea continuar?", "Finalizar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                public void onEvent(Event e) throws SQLException {
                    if (Messagebox.ON_YES.equals(e.getName())) {
                        Map args = new HashMap();
                        args.put("curNObservacion", curNObservacion);
                        args.put("curExpediente", curExpediente);
                        args.put("listVinculados", listVinculados);
                        BindUtils.postGlobalCommand(null, null, "updateFinalizarExpediente", args);
                        win.detach();
                    }
                }
            });
        } else {
            Messagebox.show("Debe de escribir un motivo de Finalizacion",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
        }
    }

    @Command
    public void cancelar() {
        win.detach();
    }

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public String getCurAObservacion() {
        return curAObservacion;
    }

    public void setCurAObservacion(String curAObservacion) {
        this.curAObservacion = curAObservacion;
    }

    public String getCurNObservacion() {
        return curNObservacion;
    }

    public void setCurNObservacion(String curNObservacion) {
        this.curNObservacion = curNObservacion;
    }

}
