package com.marjuv.controller.win;

import com.marjuv.dao.Entidad_dao;
import com.marjuv.entity.Entidad;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_entidad_rem_win {

    @Wire("#search_enti_rem")
    private Window win;
    private List<Entidad> lst_entidad;
    private Entidad cur_entidad;
    @WireVariable
    private Session _sess;
    
    @Wire
    private Textbox label_busq;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
    }
    

    public List<Entidad> getLst_entidad() {
        try {
            Entidad_dao obj_entidad_dao = new Entidad_dao();
            lst_entidad = obj_entidad_dao.search_entidad(label_busq.getValue());
            obj_entidad_dao.close();
        } catch (Exception ex) {
        }
        return lst_entidad;
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("codi_enti", this.cur_entidad.getCodi_enti());
        args.put("nomb_enti", this.cur_entidad.getNomb_enti());
        args.put("dire_enti", this.cur_entidad.getDire_enti());
        BindUtils.postGlobalCommand(null, null, "setEntidadRem", args);
        win.detach();
    }
    
    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_entidad(List<Entidad> lst_entidad) {
        this.lst_entidad = lst_entidad;
    }

    public Entidad getCur_entidad() {
        return cur_entidad;
    }

    public void setCur_entidad(Entidad cur_entidad) {
        this.cur_entidad = cur_entidad;
    }

}
