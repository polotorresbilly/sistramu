package com.marjuv.controller.win;

import com.marjuv.connect.DataTransaction;
import com.marjuv.dao.administracion_dao;
import com.marjuv.entity.administracion;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JasperRunManager;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

/**
 *
 * @author Joel
 */
public class ReporteCtrl extends GenericForwardComposer implements Serializable {

    private Window winReport;
    private Iframe report;
    private String name;
    private String url;
    private Map params;

    protected DataTransaction dt;
    protected Connection cn;

    public ReporteCtrl() {
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);

    }

    public void onCreate$winReport() throws IOException {
        try {
            if (params == null) {
                params = new HashMap();
            } else {
                params = (HashMap) winReport.getAttribute("params");
            }
            name = winReport.getAttribute("name").toString();
            url = winReport.getAttribute("url").toString();
            ServletContext sv = (ServletContext) session.getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_logo_admin();
            obj_administracion_dao.close();
            params.put("realPath", URL_FILE);
            reportar();
        } catch (Exception e) {
        }

    }

    public void reportar() throws IOException, SQLException {
        InputStream is = null;
        try {
            String urlReal = Sessions.getCurrent().getWebApp().getRealPath(url);
            is = new FileInputStream(urlReal);
            dt = new DataTransaction();
            cn = dt.getConnection();
            final byte[] buf = JasperRunManager.runReportToPdf(is, params, cn);

            final InputStream mediais = new ByteArrayInputStream(buf);
            final AMedia amedia = new AMedia(name, "pdf", "application/pdf", mediais);
            //final AMedia amedia = new AMedia(name, "xlsx", "application/file", mediais);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cn != null){
                cn.close();
            }
            if (is != null) {
                is.close();
            }
        }
    }
}
