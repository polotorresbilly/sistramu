package com.marjuv.controller.win;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

public class VistaArchivo {

    @Wire
    private Window winReport;
    @Wire
    private Iframe report;

    private String ruta;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        mostrar(ruta);
    }

    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("ruta") String ruta)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        this.ruta = ruta;
    }

    public void mostrar(String ruta) {
        try {
            InputStream is = new FileInputStream(ruta);
            AMedia amedia = new AMedia(ruta, "pdf", "application/pdf", is);
            winReport.setMaximizable(true);
            report.setContent(amedia);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
