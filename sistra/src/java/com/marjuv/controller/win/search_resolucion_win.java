package com.marjuv.controller.win;

import com.marjuv.dao.resolucion_dao;
import com.marjuv.entity.resolucion;
import com.marjuv.entity.usuario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class search_resolucion_win {

    @Wire("#search_resolucion")
    private Window win;
    private List<resolucion> lst_resolucion;
    private resolucion cur_resolucion;
    @WireVariable
    private Session _sess;
    
    @Wire
    private Textbox label_busq;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
        if (_sess.getAttribute("acceso") == null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 0) {
            return;
        }
        Selectors.wireComponents(view, this, false);
    }
    

    public List<resolucion> getLst_resolucion() {
        try {
            resolucion_dao obj_resolucion_dao = new resolucion_dao();
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            lst_resolucion = obj_resolucion_dao.search_resolucion(label_busq.getValue(), obj_usuario.getCodi_arde());
            obj_resolucion_dao.close();
        } catch (Exception ex) {
        }
        return lst_resolucion;
    }

    @Command
    public void seleccionar() {
        Map args = new HashMap();
        args.put("resolucion", this.cur_resolucion);
        BindUtils.postGlobalCommand(null, null, "setResolucion", args);
        win.detach();
    }
    
    @NotifyChange("*")
    @Command
    public void changeFilter() {
    }

    public void setLst_resolucion(List<resolucion> lst_resolucion) {
        this.lst_resolucion = lst_resolucion;
    }

    public resolucion getCur_resolucion() {
        return cur_resolucion;
    }

    public void setCur_resolucion(resolucion cur_resolucion) {
        this.cur_resolucion = cur_resolucion;
    }

}
