package com.marjuv.controller.win;

import com.marjuv.beans_view.BeanAsuntoDestinoDefecto;
import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.asunto_destino_defecto_dao;
import com.marjuv.dao.cargos_dao;

import com.marjuv.entity.Entidad;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;

public class asunto_destino_defecto_win {

    private List<BeanAsuntoDestinoDefecto> lista_asunto_defecto;
    private BeanAsuntoDestinoDefecto seleccion_asunto_defecto;
    private List<Pair<Integer, String>> lista_dependencia;
   
    private Pair<Integer, String> seleccion_dependencia;

    private List<Pair<Integer, String>> lista_asunto_win;
    private Pair<Integer, String> seleccion_asunto_win;

    @WireVariable
    private Session _sess;

    @Wire()
    private Combobox cboAsunto;

    @NotifyChange("*")
    @Init
    public void initSetup() throws SQLException, Exception {

        asunto_destino_defecto_dao ObjAsuntoDestinoDefecto = new asunto_destino_defecto_dao();
        setLista_asunto_win(ObjAsuntoDestinoDefecto.cboAsunto());
        if (getLista_asunto_win().size() > 0) {
            setSeleccion_asunto_win(lista_asunto_win.get(0));
        }

        if (seleccion_asunto_win!=null) {
            lista_asunto_defecto = ObjAsuntoDestinoDefecto.getAsuntosDefecto(seleccion_asunto_win.x);
        }
        ObjAsuntoDestinoDefecto.close();

    }

    public List<BeanAsuntoDestinoDefecto> getLista_asunto_defecto() {
        /*try {
         asunto_destino_defecto_dao ObjAsuntoDestinoDefecto = new asunto_destino_defecto_dao();
         lista_asunto_defecto = ObjAsuntoDestinoDefecto.getAsuntosDefecto(seleccion_dependencia.x);
         ObjAsuntoDestinoDefecto.close();
         } catch (Exception e) {
         }*/
        return lista_asunto_defecto;
    }

    @NotifyChange("*")
    @Command
    public void updateLista() throws SQLException, Exception {
        asunto_destino_defecto_dao ObjAsuntoDestinoDefecto = new asunto_destino_defecto_dao();
        lista_asunto_defecto = ObjAsuntoDestinoDefecto.getAsuntosDefecto(seleccion_asunto_win.x);
        ObjAsuntoDestinoDefecto.close();
        BindUtils.postNotifyChange(null, null, asunto_destino_defecto_win.this, "lista_asunto_defecto");
    }

    @Command
    public void agregar_nuevo() {

        Executions.createComponents("mantenimiento/man/man_asunto_destino_defecto.zul", null, null);
    }

    @NotifyChange("*")
    @GlobalCommand
    public void updateList(@BindingParam("accion") int accion, @BindingParam("codi_arde") int codi_arde, @BindingParam("codi_asre") int codi_asre) throws Exception {
        if (accion == 1) {
            asunto_destino_defecto_dao ObjAsunto = new asunto_destino_defecto_dao();
            ObjAsunto.insert(codi_arde, codi_asre);
            Messagebox.show("Se registro el asunto por defecto");
            updateLista();
        } else {
            if (accion == 2) {

            }
        }
    }

    
    @NotifyChange("*")
    @Command
    public void delete() {
        Messagebox.show("Esta Seguro que desea Eliminar este registro \""
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            try {
                                asunto_destino_defecto_dao ObjAsunto = new asunto_destino_defecto_dao();
                                ObjAsunto.delete(seleccion_asunto_defecto.getCodi_arde(), seleccion_asunto_defecto.getCodi_asre());
                                //BindUtils.postNotifyChange(null, null, asunto_destino_defecto_win.this, "updateList");
                               
                                lista_asunto_defecto = ObjAsunto.getAsuntosDefecto(seleccion_asunto_win.x);
                                BindUtils.postNotifyChange(null, null, asunto_destino_defecto_win.this, "lista_asunto_defecto");
                                 //Messagebox.show(String.valueOf(lista_asunto_defecto.size()));
                                ObjAsunto.close();
                            } catch (Exception ex) {
                                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                        "Lista de Entidades", Messagebox.CANCEL, Messagebox.ERROR);

                            }
                        }
                    }

                });

    }

    public void setLista_asunto_defecto(List<BeanAsuntoDestinoDefecto> lista_asunto_defecto) {
        this.lista_asunto_defecto = lista_asunto_defecto;
    }

    public BeanAsuntoDestinoDefecto getSeleccion_asunto_defecto() {
        return seleccion_asunto_defecto;
    }

    public void setSeleccion_asunto_defecto(BeanAsuntoDestinoDefecto seleccion_asunto_defecto) {
        this.seleccion_asunto_defecto = seleccion_asunto_defecto;
    }

    public List<Pair<Integer, String>> getLista_dependencia() {
        return lista_dependencia;
    }

    public void setLista_dependencia(List<Pair<Integer, String>> lista_dependencia) {
        this.lista_dependencia = lista_dependencia;
    }

    public Pair<Integer, String> getSeleccion_dependencia() {
        return seleccion_dependencia;
    }

    public void setSeleccion_dependencia(Pair<Integer, String> seleccion_dependencia) {
        this.seleccion_dependencia = seleccion_dependencia;
    }

    public List<Pair<Integer, String>> getLista_asunto_win() {
        return lista_asunto_win;
    }

    public void setLista_asunto_win(List<Pair<Integer, String>> lista_asunto_win) {
        this.lista_asunto_win = lista_asunto_win;
    }

    public Pair<Integer, String> getSeleccion_asunto_win() {
        return seleccion_asunto_win;
    }

    public void setSeleccion_asunto_win(Pair<Integer, String> seleccion_asunto_win) {
        this.seleccion_asunto_win = seleccion_asunto_win;
    }

   

}
