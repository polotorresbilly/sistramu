package com.marjuv.controller.win;

import com.marjuv.dao.administracion_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.ExpedienteReportes;
import com.marjuv.entity.usuario;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class main extends GenericForwardComposer {

    private Include contenedor;
    private Label welcome;
    private Menubar miMenu;
    private Label area_dependencia;
    protected Window pagemenu;
    public Borderlayout clayout;
    private int codi_usua;

    @Override
    public void doAfterCompose(Component comp) {
        //To change body of generated methods, choose Tools | Templates.
        try {
            super.doAfterCompose(comp);
            if (session.getAttribute("acceso") != null && Integer.parseInt(session.getAttribute("acceso").toString()) == 1) {
                usuario obj_usuario = (usuario) session.getAttribute("usuario");
                welcome.setValue("Bienvenido, " + obj_usuario.getNomb_usua() + " " + obj_usuario.getApel_usua());
                usuario_dao objusuario_dao = new usuario_dao();
                area_dependencia.setValue("Área: " + objusuario_dao.get_Dependencia(obj_usuario.getCodi_arde()));
                objusuario_dao.close();
                this.codi_usua = obj_usuario.getCodi_usua();
                Calendar calendar = Calendar.getInstance();
                java.util.Date currentDate = calendar.getTime();
                Date fecha = new Date(currentDate.getTime());
                int year = fecha.getYear() + 1900;
                //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
                //clayout.setHeight(gd.getDisplayMode().getHeight()-130+"px");
                //clayout.setStyle("min-height: 500px;");
                
                administracion_dao obj_administracion_dao = new administracion_dao();

                if (obj_administracion_dao.verificar_admin(year)) {
                    createMenu();
                    obj_administracion_dao.close();
                } else {
                    Messagebox.show("Debe completar el formulario para poder habilitar el sistema", "Información", Messagebox.OK, Messagebox.INFORMATION);
                    obj_administracion_dao.close();
                    contenedor.setSrc("mantenimiento/win/win_administracion.zul");
                }
            } else {
                Executions.sendRedirect("index.zul");
            }
        } catch (Exception e) {
        }

    }
    public static void generarReporteEstadoExpediente2(String ruta, Map parametros, String name, 
            ServletContext serv, String nume_expe) {
//        Messagebox.show("Numero2: " + parametros.get("NUMERO_EXPEDIENTE"));
//        Window winRpt = (Window) Executions.createComponents("zkReporteEstadoExpediente.zul", null, null);
        Window winRpt = (Window) Executions.createComponents("Reporte_Ubicacion_Expediente.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.setAttribute("nume_expe", nume_expe);
        winRpt.doModal();
    }

    public static void generarReporteEstadoExpediente(String ruta, Map parametros, String name, 
            ServletContext serv, String nume_expe) {
//        Messagebox.show("Numero2: " + parametros.get("NUMERO_EXPEDIENTE"));
        Window winRpt = (Window) Executions.createComponents("zkReporteEstadoExpediente.zul", null, null);
//        Window winRpt = (Window) Executions.createComponents("Reporte_Ubicacion_Expediente.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.setAttribute("nume_expe", nume_expe);
        winRpt.doModal();
    }
    
    

    public static void generarReporteCargoResExpediente(String ruta, Map parametros, String name, ServletContext serv, String codi_reso) {

        Window winRpt = (Window) Executions.createComponents("zkReporteCargoResolucion.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.setAttribute("codi_reso", codi_reso);
        winRpt.doModal();
    }

    public static void generarReporteEstadosExp(String ruta, Map parametros, String name, ServletContext serv, String nomb_arde, int codi_arde,String[] params) {

        Window winRpt = (Window) Executions.createComponents("zkReporteEstadosExp.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.setAttribute("nomb_arde", nomb_arde);
        winRpt.setAttribute("codi_arde", codi_arde);
        winRpt.setAttribute("codices", params);
        winRpt.doModal();
    }

    public static void generarReporteMovimientoExpediente(String ruta, Map parametros, String name, ServletContext serv, String nume_expe) {

        Window winRpt = (Window) Executions.createComponents("zkReporteMovimientoExpediente.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.setAttribute("nume_expe", nume_expe);
        winRpt.doModal();
    }

    public static void generarReporte(String ruta, Map parametros, String name, ServletContext serv) {

        Window winRpt = (Window) Executions.createComponents("zkReporte.zul", null, null);
        winRpt.setAttribute("params", parametros);
        winRpt.setAttribute("name", name);
        winRpt.setAttribute("url", ruta);
        winRpt.doModal();
    }

    private void createMenu() throws SQLException {

        general_dao obj = new general_dao();
        ArrayList<String> menu = obj.get_menu(this.codi_usua);

        //ArrayList<String> menu = new ArrayList<String>();
        ArrayList<ArrayList<String>> menu_items = obj.get_menu_items(this.codi_usua);
        obj.close();    

        for (int i = 0; i < menu.size(); i++) {

            Menu menu_master_sub = new Menu(menu.get(i));
            Menupopup popup = new Menupopup();

            for (int j = 0; j < menu_items.size(); j++) {
                if (menu_items.get(j).get(0).equals(menu.get(i))) {
                    Menuitem obj_menuitem = new Menuitem(menu_items.get(j).get(1));
                    final String src = menu_items.get(j).get(2);
                    //final String size = menu_items.get(j).get(3);
                    obj_menuitem.setParent(popup);
                    obj_menuitem.addEventListener("onClick", new EventListener() {
                        public void onEvent(Event t) throws Exception {
                            //setHeight(700+"px");
                            contenedor.setSrc(src);
                        }
                    });
                }
            }

            popup.setParent(menu_master_sub);
            menu_master_sub.setParent(miMenu);
        }

    }
    
    public void setHeight(String size) {
        this.clayout.setStyle("min-height: "+size+";");
    }

    public void onClick$logout() {
        session.removeAttribute("usuario");
        session.removeAttribute("acceso");
        Executions.sendRedirect("index.zul");
    }

}
