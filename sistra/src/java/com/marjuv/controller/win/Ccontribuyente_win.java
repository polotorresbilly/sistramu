/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.win;

import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.general_dao;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Initiator;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Ccontribuyente_win{

    private List<Contribuyente> lista_contribuyente;
    private Contribuyente_dao dao;
    @WireVariable
    private Contribuyente curSelectedContribuyente;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;
    
    
    @WireVariable
    private Session _sess;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Apellidos y Nombres"));
        listBusqueda.add(new Pair<Integer, String>(3, "DNI"));
        curBusqueda = listBusqueda.get(0);
    }
    
    public void check_permisos(){
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            general_dao dao = new general_dao();
            boolean sw = false;
            try {
                for (ArrayList<String> array : dao.get_menu_items(obj_usuario.getCodi_usua())) {
                    if (array.get(2).contains("contribuyente_win")) {
                        sw = true;
                    }
                }
            } catch (SQLException ex) {
                Executions.sendRedirect("../../index.zul");
            }finally{
                try {
                    dao.close();
                } catch (SQLException ex) {
                    Executions.sendRedirect("../../index.zul");
                }
            }
            if (sw == false) {
                Executions.sendRedirect("../../index.zul");
            }
        }else
        {
            Executions.sendRedirect("../../index.zul");
        }
    }
    

    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }
    
    @Init
    public void initSetup() {
        check_permisos();
        try {
            dao = new Contribuyente_dao();
            lista_contribuyente = dao.get_contribuyentes();
            dao.close();
            loadDataControls();
            
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Command
    @NotifyChange("lista_contribuyente")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("lista_contribuyente")
    public void updateGrid() {
        try {
            dao = new Contribuyente_dao();
            lista_contribuyente = dao.get_contribuyentes();
            List<Contribuyente> new_list = new ArrayList<Contribuyente>();
            dao.close();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (Contribuyente contribuyente : lista_contribuyente) {
                            if ((contribuyente.getApel_cont().toUpperCase() + " " + contribuyente.getNomb_cont().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (Contribuyente contribuyente : lista_contribuyente) {
                            if ((contribuyente.getDni_cont()+"").contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                lista_contribuyente = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Contribuyente getCurSelectedContribuyente() {
        return curSelectedContribuyente;
    }

    public void setCurSelectedContribuyente(Contribuyente curSelectedContribuyente) {
        this.curSelectedContribuyente = curSelectedContribuyente;
    }

    

    public List<Contribuyente> getLista_contribuyente() {
        return lista_contribuyente;
    }

    public void setLista_contribuyente(List<Contribuyente> lista_contribuyente) {
        this.lista_contribuyente = lista_contribuyente;
    }

    

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteContribuyente() {
        Messagebox.show("Esta Seguro que desea Eliminar la Contribuyente \"" + curSelectedContribuyente.getApel_cont()
                + "\" ?.", "Eliminar", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
                    public void onEvent(Event e) {
                        if (Messagebox.ON_YES.equals(e.getName())) {
                            try {
                                dao = new Contribuyente_dao();
                                dao.delete_contribuyente(curSelectedContribuyente.getCodi_cont());
                                BindUtils.postNotifyChange(null, null, Ccontribuyente_win.this, "lista_contribuyente");
                                dao.close();
                                insert_auditoria("D", "CONTRIBUYENTE");
                                updateGrid();
                            } catch (Exception ex) {
                                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                                        "Lista de Bancos", Messagebox.CANCEL, Messagebox.ERROR);
                                Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                });

    }

    @Command
    public void addContribuyente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("curContribuyente", null);
        map.put("accion", "NEW");
        Executions.createComponents("/mantenimiento/man/contribuyente_man.zul", null, map);
    }

    @Command
    public void editContribuyente() {
        final HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("curContribuyente", this.curSelectedContribuyente);
        map.put("accion", "EDIT");
        Executions.createComponents("/mantenimiento/man/contribuyente_man.zul", null, map);
    }

    @GlobalCommand
    @NotifyChange("lista_contribuyente")
    public void updateListContribuyente(@BindingParam("curContribuyente") Contribuyente contribuyente, @BindingParam("accion") String recordMode) {
        if (recordMode.equals("NEW")) {
            try {
                dao = new Contribuyente_dao();
                dao.insert_contribuyente(contribuyente);
                Messagebox.show("El contribuyente fue registrado");
                dao.close();
                insert_auditoria("I", "CONTRIBUYENTE");
                //BindUtils.postNotifyChange(null, null, Ccontribuyente_win.this, "lista_contribuyente");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Contribuyentes", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (recordMode.equals("EDIT")) {
            try {
                dao = new Contribuyente_dao();
                dao.update_contribuyente(contribuyente);
                BindUtils.postNotifyChange(null, null, Ccontribuyente_win.this, "allContribuyentes");
                Messagebox.show("El contribuyente fue actualizado");
                dao.close();
                insert_auditoria("U", "CONTRIBUYENTE");
            } catch (Exception ex) {
                Messagebox.show("El Proceso no se pudo Completar Debido a un Error Inesperado",
                        "Lista de Contribuyentes", Messagebox.OK, Messagebox.INFORMATION);
                Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        updateGrid();
    }

}
