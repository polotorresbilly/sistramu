package com.marjuv.controller;

import com.marjuv.beans_view.BeanAsuntoResolucion;
import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.dao.AsuntoResolucionDAO;
import com.marjuv.dao.TipoResolucionDAO;
import com.marjuv.entity.AsuntoResolucionTO;
import com.marjuv.entity.TipoResolucionTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class AsuntoResolucionControllerMan {
    
     @Wire("#asunto_resolucion_man")
    private Window win;
    private String recordMode;
    private List<Pair<Integer, String>> listAsuntoResolucion;
    private Pair<Integer, String> curAsuntoResolucion;
    private BeanAsuntoResolucion BeanMan;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<Pair<Integer, String>> getListAsuntoResolucion() {
        return listAsuntoResolucion;
    }

    public void setListAsuntoResolucion(List<Pair<Integer, String>> listAsuntoResolucion) {
        this.listAsuntoResolucion = listAsuntoResolucion;
    }

    public Pair<Integer, String> getCurAsuntoResolucion() {
        return curAsuntoResolucion;
    }

    public void setCurAsuntoResolucion(Pair<Integer, String> curAsuntoResolucion) {
        this.curAsuntoResolucion = curAsuntoResolucion;
    }

    public BeanAsuntoResolucion getBeanMan() {
        return BeanMan;
    }

    public void setBeanMan(BeanAsuntoResolucion BeanMan) {
        this.BeanMan = BeanMan;
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("keyAsuntoResolucion") BeanAsuntoResolucion c1,@ExecutionArgParam("accion") String recordMode)throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.BeanMan = new BeanAsuntoResolucion();
        }
        if (recordMode.equals("EDIT")) {
            this.BeanMan = (BeanAsuntoResolucion) c1.clone();
            
        }
    }
    
    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "listaAR", null);
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
            
            if ((getRecordMode().equals("NEW") && objAsuntoResolucionDAO.v_desc_asre_new(BeanMan.getDesc_asre())) || (getRecordMode().equals("EDIT") && objAsuntoResolucionDAO.v_desc_asre_edit(BeanMan.getCodi_asre(),BeanMan.getDesc_asre()))) {
                    AsuntoResolucionTO objAsuntoResolucionTO = new AsuntoResolucionTO();
                    objAsuntoResolucionTO.setCodi_asre(BeanMan.getCodi_asre());
                    objAsuntoResolucionTO.setDesc_asre(BeanMan.getDesc_asre());

                    Map args = new HashMap();
                    args.put("keyAsuntoResolucion", objAsuntoResolucionTO);
                    args.put("accion", this.recordMode);
                    if (this.recordMode.equals("EDIT")) {
                        Messagebox.show("El asunto de resolución fue actualizada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                    } else {
                        if (this.recordMode.equals("NEW")) {
                            Messagebox.show("El asunto de resolución fue registrada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                        }
                    }
                    BindUtils.postGlobalCommand(null, null, "updateAR", args);
                    win.detach();
                

            } else {
                Messagebox.show("El ansunto de resolución ingresado ya Existe, eliga otro", "Mantenimiento de Asunto de Resolución", Messagebox.OK, Messagebox.ERROR);
            }
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }
    
}
