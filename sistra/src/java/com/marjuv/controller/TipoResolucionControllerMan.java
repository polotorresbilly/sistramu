package com.marjuv.controller;

import com.marjuv.beans_view.BeanSubDependencias;
import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.dao.SubDependenciaDAO;
import com.marjuv.dao.TipoResolucionDAO;
import com.marjuv.entity.SubDependenciaTO;
import com.marjuv.entity.TipoResolucionTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class TipoResolucionControllerMan {

    @Wire("#tipo_resolucion_man")
    private Window win;
    private String recordMode;
    private List<Pair<Integer, String>> listTipoResolucion;
    private Pair<Integer, String> curTipoResolucion;
    private BeanTipoResolucion BeanMan;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<Pair<Integer, String>> getListTipoResolucion() {
        return listTipoResolucion;
    }

    public void setListTipoResolucion(List<Pair<Integer, String>> listTipoResolucion) {
        this.listTipoResolucion = listTipoResolucion;
    }

    public Pair<Integer, String> getCurTipoResolucion() {
        return curTipoResolucion;
    }

    public void setCurTipoResolucion(Pair<Integer, String> curTipoResolucion) {
        this.curTipoResolucion = curTipoResolucion;
    }

    public BeanTipoResolucion getBeanMan() {
        return BeanMan;
    }

    public void setBeanMan(BeanTipoResolucion BeanMan) {
        this.BeanMan = BeanMan;
    }
    
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("keyTipoResolucion") BeanTipoResolucion c1,@ExecutionArgParam("accion") String recordMode)throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.BeanMan = new BeanTipoResolucion();
        }
        if (recordMode.equals("EDIT")) {
            this.BeanMan = (BeanTipoResolucion) c1.clone();
            
        }
    }
    
    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "listaTR", null);
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
            
            if ((getRecordMode().equals("NEW") && objTipoResolucionDAO.v_desc_tire_new(BeanMan.getDesc_tire())) || (getRecordMode().equals("EDIT") && objTipoResolucionDAO.v_desc_tire_edit(BeanMan.getCodi_tire(),BeanMan.getDesc_tire()))) {
                    TipoResolucionTO objTipoResolucionTO = new TipoResolucionTO();
                    objTipoResolucionTO.setCodi_tire(BeanMan.getCodi_tire());
                    objTipoResolucionTO.setDesc_tire(BeanMan.getDesc_tire());

                    Map args = new HashMap();
                    args.put("keyTipoResolucion", objTipoResolucionTO);
                    args.put("accion", this.recordMode);
                    
                    BindUtils.postGlobalCommand(null, null, "updateTR", args);
                    win.detach();
                

            } else {
                Messagebox.show("El nombre del tipo de resolución ingresado ya Existe, eliga otro");
            }
        } catch (Exception e) {
            Messagebox.show("Error capturado:CONTROLADOR " + e.getMessage(),
                    "Mantenimiento", Messagebox.OK, Messagebox.ERROR);
        }

    }
    
    
}
