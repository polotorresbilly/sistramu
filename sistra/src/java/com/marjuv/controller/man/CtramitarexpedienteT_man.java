/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Cargo_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.Distrito_dao;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.entity.Cargo;
import com.marjuv.entity.ExpedienteCliente;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class CtramitarexpedienteT_man {
    @Wire("#tramitarexpedienteT_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private List<BeanDependencias> list_dependencias;
    private BeanDependencias curDependencia;
    private List<Cargo> list_cargo;
    private Cargo curCargo;
    private int curFolios;
    private String nProveido;
    private String resuProveido;
    private boolean CurDocumento;
    private String cNumDoc;
    
    private Expediente_dao dao_exp;
    private Dependencia_dao dao_dep;
    private Cargo_dao dao_carg;

    public String getcNumDoc() {
        return cNumDoc;
    }

    public void setcNumDoc(String cNumDoc) {
        this.cNumDoc = cNumDoc;
    }
    
    
    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1)
            throws CloneNotSupportedException, SQLException {
        Selectors.wireComponents(view, this, false);
        dao_exp = new Expediente_dao();
        dao_dep = new Dependencia_dao();
        dao_carg = new Cargo_dao();
        setCurDocumento(false);
        try {
            curExpediente = c1;
            win.setTitle("Tramitando Expediente N° : " + curExpediente.getEXPNUM());
            setList_dependencias(dao_dep.getDependencias());
        } catch (Exception ex) {
            Logger.getLogger(Cdestinoasunto_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        dao_carg.close();
        dao_dep.close();
        dao_exp.close();
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (curDependencia != null && curCargo!=null) {
            Map args = new HashMap();
            args.put("curCargo", this.curCargo);
            this.curExpediente.setFOLIOS(this.curExpediente.getFOLIOS()+getCurFolios());
            args.put("curExpediente", this.curExpediente);
            args.put("curNProveido", this.nProveido + " - " + this.cNumDoc);
            args.put("curRProveido", this.resuProveido);
            args.put("curDocumento", this.CurDocumento);
            
            BindUtils.postGlobalCommand(null, null, "updateTramitarExpediente", args);
            win.detach();
        } else {
            Messagebox.show("Debe de seleccioanr Area y Cargo",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
        }
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("list_cargo")
    public void updateCargo(){
        try {
            dao_carg = new Cargo_dao();
            setList_cargo(dao_carg.get_cargo_area(curDependencia.getCodi_arde()));
            dao_carg.close();
        } catch (Exception ex) {
            //Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Command
    public void cancelar(){
        win.detach();
    }

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public boolean getCurDocumento() {
        return CurDocumento;
    }

    public void setCurDocumento(boolean CurDocumento) {
        this.CurDocumento = CurDocumento;
    }
    

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public List<BeanDependencias> getList_dependencias() {
        return list_dependencias;
    }

    public void setList_dependencias(List<BeanDependencias> list_dependencias) {
        this.list_dependencias = list_dependencias;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    public List<Cargo> getList_cargo() {
        return list_cargo;
    }

    public void setList_cargo(List<Cargo> list_cargo) {
        this.list_cargo = list_cargo;
    }

    public Cargo getCurCargo() {
        return curCargo;
    }

    public void setCurCargo(Cargo curCargo) {
        this.curCargo = curCargo;
    }

    public int getCurFolios() {
        return curFolios;
    }

    public void setCurFolios(int curFolios) {
        this.curFolios = curFolios;
    }

    public String getnProveido() {
        return nProveido;
    }

    public void setnProveido(String nProveido) {
        this.nProveido = nProveido;
    }

    public String getResuProveido() {
        return resuProveido;
    }

    public void setResuProveido(String resuProveido) {
        this.resuProveido = resuProveido;
    }
    
    
    
}
