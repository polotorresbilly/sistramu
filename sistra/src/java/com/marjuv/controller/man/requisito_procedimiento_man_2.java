/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author Andy
 */
public class requisito_procedimiento_man_2 {
    
    @Wire("#winadd")
    private Window win;
    
    @Wire("#winadd23")
    private Window win23;
    @WireVariable
    private Session _sess;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String varBusqueda;
    private List<BeanRequisitoMan> lst_requisito_procedimiento;
    private BeanRequisitoMan cur_requisito_procedimiento;

    
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup() {
        if (getSess().getAttribute("acceso") == null && Integer.parseInt(getSess().getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            loadDataControls();
            requisito_procedimiento_dao ObjRequisito = new requisito_procedimiento_dao();
            setLst_requisito_procedimiento(ObjRequisito.listaRequisitoAgregar());
            ObjRequisito.close();
        } catch (Exception ex) {
            Messagebox.show("Error: " + ex.getMessage());
        }
    }
    
    @Command
    @NotifyChange("lst_requisito_procedimiento")
    public void changeFilter() {
        if (getVarBusqueda() != null) {
            updateGrid();
        }
    }
    
    @Command
    @NotifyChange("lst_requisito_procedimiento")
    public void updateGrid() {
        try {
            requisito_procedimiento_dao ObjReuisito = new requisito_procedimiento_dao();
            lst_requisito_procedimiento = ObjReuisito.get_requisito_procedimiento();
            List<BeanRequisitoMan> new_list = new ArrayList<BeanRequisitoMan>();
            if (curBusqueda.x == 1) {
                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanRequisitoMan requisito : lst_requisito_procedimiento) {
                            if ((requisito.getNomb_repr().toUpperCase()).contains(getVarBusqueda().toUpperCase())) {
                                new_list.add(requisito);
                            }
                        }
                        break;
                    
                }
                lst_requisito_procedimiento = new_list;
            }
        } catch (Exception ex) {
            //Logger.getLogger(usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Command
    public void addRequisito() {
        try {
            Map map = new HashMap();
            map.put("codi_repr", cur_requisito_procedimiento.getCodi_repr());
            map.put("nomb_repr", cur_requisito_procedimiento.getNomb_repr());
           
            BindUtils.postGlobalCommand(null, null, "setRecibir_Requisito2", map);
           
            win.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK",
                     Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    @Command
    public void addRequisito2() {
        try {
            Map map = new HashMap();
            map.put("codi_repr", cur_requisito_procedimiento.getCodi_repr());
            map.put("nomb_repr", cur_requisito_procedimiento.getNomb_repr());
           
            BindUtils.postGlobalCommand(null, null, "setRecibir_Requisito3", map);
           
            win.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK",
                     Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        
        curBusqueda = listBusqueda.get(0);
    }
    
    @Command
    public void nuevo_requisito() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "NEW");
        Executions.createComponents("mantenimiento/man/man_requisito_procedimiento.zul", null, map);
    }
    
    @Command
    public void closeAddProcedimiento() {
        win.detach();
    }
    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }

    public List<BeanRequisitoMan> getLst_requisito_procedimiento() {
        return lst_requisito_procedimiento;
    }

    public void setLst_requisito_procedimiento(List<BeanRequisitoMan> lst_requisito_procedimiento) {
        this.lst_requisito_procedimiento = lst_requisito_procedimiento;
    }

    public BeanRequisitoMan getCur_requisito_procedimiento() {
        return cur_requisito_procedimiento;
    }

    public void setCur_requisito_procedimiento(BeanRequisitoMan cur_requisito_procedimiento) {
        this.cur_requisito_procedimiento = cur_requisito_procedimiento;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    public String getVarBusqueda() {
        return varBusqueda;
    }

    public void setVarBusqueda(String varBusqueda) {
        this.varBusqueda = varBusqueda;
    }

    public Window getWin23() {
        return win23;
    }

    public void setWin23(Window win23) {
        this.win23 = win23;
    }

    
    
}
