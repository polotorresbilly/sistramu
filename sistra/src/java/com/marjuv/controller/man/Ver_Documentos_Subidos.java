package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanVerDocumentos;
import com.marjuv.dao.Expediente_VinculadoDAO;
import com.marjuv.dao.Expediente_dao;
import com.marjuv.dao.administracion_dao;
import com.marjuv.entity.administracion;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.ServletContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

public class Ver_Documentos_Subidos {
    
    private BeanVerDocumentos selectorDoc;
    private List<BeanVerDocumentos> listDoc = new ArrayList<BeanVerDocumentos>();
    private String Numero_Expediente;
    
    @WireVariable
    private Session _sess;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,@ExecutionArgParam("nume_expe") String Nume_Expediente) throws Exception {
        setNumero_Expediente(Nume_Expediente);
        Expediente_dao ObjExpedienteDAO = new Expediente_dao();
        setListDoc(ObjExpedienteDAO.getListDocumentos(getNumero_Expediente()));
    }
    
    @Command
    public void ver_documentos_subidos() throws FileNotFoundException {
        try {
            ServletContext sv = (ServletContext) getSess().getWebApp().getNativeContext();
            Calendar fecha2 = new GregorianCalendar();
            int año = fecha2.get(Calendar.YEAR);
            administracion_dao obj_administracion_dao = new administracion_dao();
            administracion ObjAdministracion = new administracion();
            ObjAdministracion = obj_administracion_dao.get_administracion(año);
            String URL_FILE = ObjAdministracion.getUrl_file_admin() + "\\archivos\\";
            obj_administracion_dao.close();
//            String ruta = URL_FILE + "/" + this.getNume_expe() + "_" + cur_expe.getNRODESTINO() + "_" + cur_expe.getMOVIMIENTO();
            String ruta = URL_FILE + "/" + getNumero_Expediente() + "_" + 1 + "_" + selectorDoc.getMovimiento();
            File archivo = new File(ruta + ".pdf");
            if (!archivo.exists()) {
                archivo = new File(ruta + ".docx");
                if (!archivo.exists()) {
                    archivo = new File(ruta + ".xlsx");
                    if (!archivo.exists()) {
                        archivo = new File(ruta + ".doc");
                        if (!archivo.exists()) {
                            archivo = new File(ruta + ".xls");
                            if (!archivo.exists()) {
                                archivo = new File(ruta + ".zip");
                                if (!archivo.exists()) {
                                    archivo = new File(ruta + ".rar");
                                    if (!archivo.exists()) {
                                        Messagebox.show("No se encontró documento " + this.getNumero_Expediente() + "_" + 1 + "_" + selectorDoc.getMovimiento());
                                        return;
                                    }
                                }
                            }
                        }
                    }

                }
            }
            Filedownload.save(archivo, null);
        } catch (Exception e) {
        }
    }

    public BeanVerDocumentos getSelectorDoc() {
        return selectorDoc;
    }

    public void setSelectorDoc(BeanVerDocumentos selectorDoc) {
        this.selectorDoc = selectorDoc;
    }

    public List<BeanVerDocumentos> getListDoc() {
        return listDoc;
    }

    public void setListDoc(List<BeanVerDocumentos> listDoc) {
        this.listDoc = listDoc;
    }

    public String getNumero_Expediente() {
        return Numero_Expediente;
    }

    public void setNumero_Expediente(String Numero_Expediente) {
        this.Numero_Expediente = Numero_Expediente;
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }
    
}
