/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.controller.man;

import com.marjuv.entity.EnviaHistorial;
import com.marjuv.entity.ExpedienteCliente;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Crestaurarexpediente_man {
    @Wire("#restaurarexpediente_man")
    private Window win;
    private ExpedienteCliente curExpediente;
    private EnviaHistorial curEnvia;
    private String curAObservacion;
    private String curNObservacion;
    
    @Init
    @NotifyChange
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curExpediente") ExpedienteCliente c1,
            @ExecutionArgParam("curAEnvia") EnviaHistorial c2)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        curExpediente = c1;
        curEnvia = c2;
        setCurNObservacion("");
        setCurAObservacion(curExpediente.getOBSERVACION());
        win.setTitle("Restaurando Expediente "+ curEnvia.getNombEstado() + " N° : " + curExpediente.getEXPNUM());
        setCurAObservacion(curExpediente.getOBSERVACION());
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (!curNObservacion.isEmpty()) {
            Map args = new HashMap();
            args.put("curNObservacion", this.curNObservacion);
            args.put("curExpediente", this.curExpediente);
            args.put("curAEnvia", this.curEnvia);
            BindUtils.postGlobalCommand(null, null, "updateRestaurarExpediente", args);
            win.detach();
        } else {
            Messagebox.show("Debe de escribir un motivo de Restauracion",
                    "Mensaje", Messagebox.OK, Messagebox.INFORMATION);
        }
    }
    
    @Command
    public void cancelar(){
        win.detach();
    }

    public String getCurNObservacion() {
        return curNObservacion;
    }

    public void setCurNObservacion(String curNObservacion) {
        this.curNObservacion = curNObservacion;
    }

    public String getCurAObservacion() {
        return curAObservacion;
    }

    public void setCurAObservacion(String curAObservacion) {
        this.curAObservacion = curAObservacion;
    }
    

    public ExpedienteCliente getCurExpediente() {
        return curExpediente;
    }

    public void setCurExpediente(ExpedienteCliente curExpediente) {
        this.curExpediente = curExpediente;
    }

    public EnviaHistorial getCurEnvia() {
        return curEnvia;
    }

    public void setCurEnvia(EnviaHistorial curEnvia) {
        this.curEnvia = curEnvia;
    }
    
}