package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.envio_expediente_dao;
import com.marjuv.entity.envio_expediente;
import com.marjuv.entity.usuario;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class envio_expediente_man {

    @Wire("#envio_expediente_man")
    private Window win;
    private envio_expediente selectedEnvioExpediente;
    private String recordMode;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sEnvio_expediente") envio_expediente c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedEnvioExpediente = new envio_expediente();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedEnvioExpediente = (envio_expediente) c1.clone();
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            envio_expediente_dao obj_envio_expediente_dao = new envio_expediente_dao();
            if ((getRecordMode().equals("NEW") && obj_envio_expediente_dao.verificar_nuevo_envio_expediente(selectedEnvioExpediente.getDesc_enex()))
                    || (getRecordMode().equals("EDIT") && obj_envio_expediente_dao.verificar_editar_envio_expediente(selectedEnvioExpediente.getCodi_enex(), selectedEnvioExpediente.getDesc_enex()))) {
                envio_expediente envio_expediente = new envio_expediente();
                envio_expediente.setCodi_enex(selectedEnvioExpediente.getCodi_enex());
                envio_expediente.setDesc_enex(selectedEnvioExpediente.getDesc_enex());
                envio_expediente.setEsta_enex("1");
                
                Map args = new HashMap();
                args.put("sEnvio_expediente", envio_expediente);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateEnvioExpedienteList", args);
                obj_envio_expediente_dao.close();
                win.detach();
            } else {
                Messagebox.show("El envio de expediente ingresado ya existe, eliga otro",
                        "Mantenimiento de Envios de Expediente", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Envios de Expediente", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateEnvioExpedienteList", null);
    }

    public envio_expediente getSelectedEnvioExpediente() {
        return selectedEnvioExpediente;
    }

    public void setSelectedEnvioExpediente(envio_expediente selectedEnvioExpediente) {
        this.selectedEnvioExpediente = selectedEnvioExpediente;
    }
}
