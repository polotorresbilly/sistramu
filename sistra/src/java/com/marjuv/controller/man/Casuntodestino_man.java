/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.entity.Asunto_Resolucion;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Casuntodestino_man {

    @Wire("#asuntodestinodefecto_man")
    private Window win;
    private BeanDependencias selectedDependencia;
    private Asunto_Resolucion curAsunto;
    private List<Asunto_Resolucion> list_asunto;

    private Asunto_Resolucion_dao dao;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public BeanDependencias getSelectedDependencia() {
        return selectedDependencia;
    }

    public void setSelectedDependencia(BeanDependencias selectedDependencia) {
        this.selectedDependencia = selectedDependencia;
    }

    public Asunto_Resolucion getCurAsunto() {
        return curAsunto;
    }

    public void setCurAsunto(Asunto_Resolucion curAsunto) {
        this.curAsunto = curAsunto;
    }

    public List<Asunto_Resolucion> getList_asunto() {
        return list_asunto;
    }

    public void setList_asunto(List<Asunto_Resolucion> list_asunto) {
        this.list_asunto = list_asunto;
    }

    @Init
    @NotifyChange("list_asunto")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curDependencia") BeanDependencias c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        dao = new Asunto_Resolucion_dao();
        try {
            selectedDependencia = c1;
            setList_asunto(dao.get_asunto_no_dependencia(selectedDependencia.getCodi_arde()));
            dao.close();
        } catch (Exception ex) {
            Logger.getLogger(Cdestinoasunto_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (curAsunto != null && selectedDependencia!=null) {
            Map args = new HashMap();
            args.put("curAsunto", this.curAsunto);
            args.put("curDependencia", this.selectedDependencia);
            args.put("accion", "NEW");
            BindUtils.postGlobalCommand(null, null, "updateListAsunto", args);
            win.detach();
        } else {
            Messagebox.show("Debe de Seleccionar un Asunto",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
        }
    }

    @Command
    public void closeThis() {
        win.detach();
    }

}
