/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.entity.Asunto_Resolucion;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Cdestinoasunto_man {

    @Wire("#destinoasuntodefecto_man")
    private Window win;
    private BeanDependencias curDependencia;
    private Asunto_Resolucion selectedAsunto;
    private List<BeanDependencias> list_dependencias;

    private Asunto_Resolucion_dao dao;

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }

    public Asunto_Resolucion getSelectedAsunto() {
        return selectedAsunto;
    }

    public void setSelectedAsunto(Asunto_Resolucion selectedAsunto) {
        this.selectedAsunto = selectedAsunto;
    }

    public List<BeanDependencias> getList_dependencias() {
        return list_dependencias;
    }

    public void setList_dependencias(List<BeanDependencias> list_dependencias) {
        this.list_dependencias = list_dependencias;
    }
    
    @Init
    @NotifyChange("list_dependencias")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curAsunto") Asunto_Resolucion c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        dao = new Asunto_Resolucion_dao();
        try {
            selectedAsunto = c1;
            setList_dependencias(dao.get_dependencia_no_asunto(selectedAsunto.getCodi_asre()));
            dao.close();
        } catch (Exception ex) {
            Logger.getLogger(Cdestinoasunto_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        if (curDependencia != null && selectedAsunto!=null) {
            Map args = new HashMap();
            args.put("curAsunto", this.selectedAsunto);
            args.put("curDependencia", this.curDependencia);
            args.put("accion", "NEW");
            BindUtils.postGlobalCommand(null, null, "updateListDependencias", args);
            win.detach();
        } else {
            Messagebox.show("Debe de Seleccionar una Dependencias",
                    "Mensaje", Messagebox.CANCEL, Messagebox.INFORMATION);
        }
    }

    @Command
    public void closeThis() {
        win.detach();
    }

}
