/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.controller.man;

import com.marjuv.dao.Departamento_dao;
import com.marjuv.dao.Distrito_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Provincia_dao;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.Provincia;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Centidad_man {

    @Wire("#entidad_man")
    private Window win;
    private Entidad selectedEntidad;
    private String recordMode;
    private List<Departamento> list_depa;
    private List<Provincia> list_prov;
    private List<Distrito> list_dist;
    private Departamento curDepartamento;
    private Provincia curProvincia;
    private Distrito curDistrito;

    public Provincia getCurProvincia() {
        return curProvincia;
    }

    public void setCurProvincia(Provincia curProvincia) {
        this.curProvincia = curProvincia;
    }

    public Distrito getCurDistrito() {
        return curDistrito;
    }

    public void setCurDistrito(Distrito curDistrito) {
        this.curDistrito = curDistrito;
    }

    public List<Departamento> getList_depa() {
        return list_depa;
    }

    public void setList_depa(List<Departamento> list_depa) {
        this.list_depa = list_depa;
    }

    public List<Provincia> getList_prov() {
        return list_prov;
    }

    public void setList_prov(List<Provincia> list_prov) {
        this.list_prov = list_prov;
    }

    public List<Distrito> getList_dist() {
        return list_dist;
    }

    public void setList_dist(List<Distrito> list_dist) {
        this.list_dist = list_dist;
    }

    public Departamento getCurDepartamento() {
        return curDepartamento;
    }

    public void setCurDepartamento(Departamento curDepartamento) {
        this.curDepartamento = curDepartamento;
    }

    public List<Provincia> getList_prov(int codi_depa) {
        try {
            Provincia_dao objProvinciaDAO = new Provincia_dao();
            List<Provincia> lista = objProvinciaDAO.listar_provincia(codi_depa);
            objProvinciaDAO.close();
            return lista;
        } catch (Exception ex) {
            Logger.getLogger(Centidad_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Init
    @NotifyChange("list_prov")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curEntidad") Entidad c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        try {
            Departamento_dao objDepartamentoDAO = new Departamento_dao();
            setList_depa(objDepartamentoDAO.listar_departamento());
            objDepartamentoDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Centidad_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedEntidad = new Entidad();
        }

        if (recordMode.equals("EDIT")) {
            try {
                this.selectedEntidad = c1;
                Departamento_dao objDepartamentoDAO = new Departamento_dao();
                Provincia_dao objProvinciaDAO = new Provincia_dao();
                Distrito_dao objDistritoDAO = new Distrito_dao();
                setList_depa(objDepartamentoDAO.listar_departamento());
                this.curDepartamento = c1.getObj_dist().getProvincia().getDepartamento();
                setList_prov(objProvinciaDAO.listar_provincia(curDepartamento.getCodi_depa()));
                this.curProvincia = c1.getObj_dist().getProvincia();
                setList_dist(objDistritoDAO.listar_distrito(curProvincia.getCodi_prov()));
                this.curDistrito = c1.getObj_dist();
                
                objDepartamentoDAO.close();
                objDistritoDAO.close();
                objProvinciaDAO.close();
            } catch (Exception ex) {
                Logger.getLogger(Centidad_man.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() throws SQLException {
        this.selectedEntidad.setObj_dist(curDistrito);
        if (getSelectedEntidad().getObj_dist() == null) {
            Messagebox.show("Seleccione un Distrito");
            return;
        }
        Entidad_dao obj_Entidad_dao = new Entidad_dao();
        if ((getRecordMode().equals("NEW") && obj_Entidad_dao.verificar_nuevo(selectedEntidad.getNomb_enti()))
                || (getRecordMode().equals("EDIT") && obj_Entidad_dao.verificar_editar(selectedEntidad.getCodi_enti(), selectedEntidad.getNomb_enti()))) {
            Map args = new HashMap();

            args.put("curEntidad", this.selectedEntidad);
            args.put("accion", this.getRecordMode());
            BindUtils.postGlobalCommand(null, null, "updateListEntidad", args);
            win.detach();
            obj_Entidad_dao.close();
        } else {
            Messagebox.show("El Nombre de Entidad Ingresado ya Existe, eliga otro",
                    "Mantenimiento de Entidad", Messagebox.OK, Messagebox.ERROR);
            return;
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"list_prov", "list_dist"})
    public void updateProvincia() {
        try {
            Provincia_dao objProvinciaDAO = new Provincia_dao();
            setList_prov(objProvinciaDAO.listar_provincia(curDepartamento.getCodi_depa()));
            setList_dist(null);
            objProvinciaDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Centidad_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("list_dist")
    public void updateDistrito() {
        try {
            Distrito_dao objDistritoDAO = new Distrito_dao();
            setList_dist(objDistritoDAO.listar_distrito(curProvincia.getCodi_prov()));
            objDistritoDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Centidad_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Command
    public void closeThis() {
        win.detach();
    }

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public Entidad getSelectedEntidad() {
        return selectedEntidad;
    }

    public void setSelectedEntidad(Entidad selectedEntidad) {
        this.selectedEntidad = selectedEntidad;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

}
