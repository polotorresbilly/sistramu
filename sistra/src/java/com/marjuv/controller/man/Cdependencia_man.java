package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.controller.win.Ccontribuyente_win;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.Dependencia;
import com.marjuv.entity.usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

public class Cdependencia_man {

    private BeanDependencias BeanDep;
    private List<BeanDependencias> listaDependencia;

    Textbox txtNombre;
    private List<Pair<Integer, String>> listBusqueda;
    private Pair<Integer, String> curBusqueda;
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Pair<Integer, String>> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<Pair<Integer, String>> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    public Pair<Integer, String> getCurBusqueda() {
        return curBusqueda;
    }

    public void setCurBusqueda(Pair<Integer, String> curBusqueda) {
        this.curBusqueda = curBusqueda;
    }

    private void loadDataControls() {
        listBusqueda = new ArrayList<Pair<Integer, String>>();
        listBusqueda.add(new Pair<Integer, String>(1, "Todos"));
        listBusqueda.add(new Pair<Integer, String>(2, "Nombre"));
        listBusqueda.add(new Pair<Integer, String>(3, "Abreviatura"));
        listBusqueda.add(new Pair<Integer, String>(4, "Jerarquia"));
        listBusqueda.add(new Pair<Integer, String>(5, "Entidad"));
        curBusqueda = listBusqueda.get(0);
    }

    @Command
    @NotifyChange("listaDependencia")
    public void changeFilter() {
        if (nombre != null) {
            updateGrid();
        }
    }

    @Command
    @NotifyChange("listaDependencia")
    public void updateGrid() {
        try {
            Dependencia_dao dao = new Dependencia_dao();
            listaDependencia = dao.getDependencias();
            
            List<BeanDependencias> new_list = new ArrayList<BeanDependencias>();
            if (curBusqueda.x == 1) {

                return;
            } else {
                switch (curBusqueda.x) {
                    case 2:
                        for (BeanDependencias contribuyente : listaDependencia) {
                            if ((contribuyente.getNomb_arde().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 3:
                        for (BeanDependencias contribuyente : listaDependencia) {
                            if ((contribuyente.getAbre_arde().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 4:
                        for (BeanDependencias contribuyente : listaDependencia) {
                            if ((contribuyente.getNomb_jera().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                    case 5:
                        for (BeanDependencias contribuyente : listaDependencia) {
                            if ((contribuyente.getNomb_enti().toUpperCase()).contains(nombre.toUpperCase())) {
                                new_list.add(contribuyente);
                            }
                        }
                        break;
                }
                listaDependencia = new_list;
                dao.close();
            }
        } catch (Exception ex) {
            Logger.getLogger(Cdependencia_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Init
    public void initSetup() {
        try {
            loadDataControls();
            Dependencia_dao dao = new Dependencia_dao();
            listaDependencia = dao.getDependencias();
            dao.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_win.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public BeanDependencias getBeanDep() {
        return BeanDep;
    }

    public void setBeanDep(BeanDependencias BeanDep) {
        this.BeanDep = BeanDep;
    }

    void doOk() {
        Messagebox.show("ENTER key is pressed", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        txtNombre.focus();
    }

    
    
    @GlobalCommand
    @NotifyChange("listaDependencia")
    public void updateUsuarioList(@BindingParam("objDependencia") Dependencia objDependenciaTO,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        String usuario = "";
        if (recordMode != null) {
            Dependencia_dao objDependenciaDAO = new Dependencia_dao();
            if (recordMode.equals("NEW")) {
                objDependenciaDAO.insertDependencia(objDependenciaTO);
                insert_auditoria("I","AREA_DEPENDENCIA");
                
                Messagebox.show("La dependencia fue registrada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);

                objDependenciaDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                    //Messagebox.show("Antes del Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                    objDependenciaDAO.updateDependencia(objDependenciaTO);
                    //Messagebox.show("Paso el Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                    Messagebox.show("La dependencia fue actualizada ", "Mantenimiento", Messagebox.OK, Messagebox.ON_OK);
                    objDependenciaDAO.close();
                    insert_auditoria("U", "AREA_DEPENDENCIA");
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateUsuarioList: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }

            }
        }
        updateGrid();
    }
    
    @WireVariable
    private Session _sess;
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    @NotifyChange("listaDependencia")
    @Command
    public void deleteDependencia() {
        String str = "¿Está seguro de que desea eliminar la dependencia \""
                + BeanDep.getNomb_arde() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        Dependencia_dao objDependenciaDAO = new Dependencia_dao();
                        objDependenciaDAO.deleteDependencia(BeanDep.getCodi_arde());
                        objDependenciaDAO.close();
                        insert_auditoria("D", "AREA_DEPENDENCIA");
                        Dependencia_dao dao = new Dependencia_dao();
                        listaDependencia = dao.getDependencias();
                        dao.close();
                        BindUtils.postNotifyChange(null, null,Cdependencia_man.this, "listaDependencia");

                    } catch (Exception ex) {
                        Logger.getLogger(Cdependencia_man.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });
    }

    public List<BeanDependencias> getlistaDependencia() {
        return listaDependencia;
    }

    public void setlistaDependencia(List<BeanDependencias> listaDependencia) {
        this.listaDependencia = listaDependencia;
    }

    @Command
    public void editarDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objDependencia", this.BeanDep);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/win/editar_dependencia.zul", null, map);
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Command
    public void nuevaDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objDependencia", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/win/editar_dependencia.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception 2: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }

}
