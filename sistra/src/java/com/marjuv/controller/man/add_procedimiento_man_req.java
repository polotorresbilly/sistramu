package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.dao.procedimiento_expediente_dao;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class add_procedimiento_man_req {

    @Wire("#winadd_3")
    private Window win;
    @WireVariable
    private Session _sess;
    private List<BeanProcedimientoMan> lst_procedimiento_expediente;
    private BeanProcedimientoMan cur_procedimiento_expediente;
    
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    @Init
    public void initSetup() {
        if (getSess().getAttribute("acceso") == null && Integer.parseInt(getSess().getAttribute("acceso").toString()) == 0) {
            return;
        }
        try {
            procedimiento_expediente_dao ObjProcedimientoExp = new procedimiento_expediente_dao();
            setLst_procedimiento_expediente(ObjProcedimientoExp.get_procedimiento_expediente());
            ObjProcedimientoExp.close();
        } catch (Exception ex) {
        }
    }

    @Command
    public void addprocedimiento() {
        try {
            Map map = new HashMap();
            map.put("codi_proc", cur_procedimiento_expediente.getCodi_proc());
            map.put("nomb_proc", cur_procedimiento_expediente.getNomb_proc());
            BindUtils.postGlobalCommand(null, null, "setProcedimiento_req", map);
            win.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void closeAddProcedimiento() {
        win.detach();
    }

    public Session getSess() {
        return _sess;
    }

    public void setSess(Session _sess) {
        this._sess = _sess;
    }

    public List<BeanProcedimientoMan> getLst_procedimiento_expediente() {
        return lst_procedimiento_expediente;
    }

    public void setLst_procedimiento_expediente(List<BeanProcedimientoMan> lst_procedimiento_expediente) {
        this.lst_procedimiento_expediente = lst_procedimiento_expediente;
    }

    public BeanProcedimientoMan getCur_procedimiento_expediente() {
        return cur_procedimiento_expediente;
    }

    public void setCur_procedimiento_expediente(BeanProcedimientoMan cur_procedimiento_expediente) {
        this.cur_procedimiento_expediente = cur_procedimiento_expediente;
    }

}
