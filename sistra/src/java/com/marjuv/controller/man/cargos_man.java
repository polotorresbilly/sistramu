package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.entity.cargos;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class cargos_man {

    @Wire("#cargos_man")
    private Window win;
    private BeanCargoMan selectedCargo;
    private String recordMode;
    private List<Pair<Integer, String>> listArde;
    private Pair<Integer, String> curArde;

    public BeanCargoMan getSelectedCargo() {
        return selectedCargo;
    }

    public void setSelectedCargo(BeanCargoMan selectedCargo) {
        this.selectedCargo = selectedCargo;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sCargo") BeanCargoMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        loadDataControls();
        if (recordMode.equals("NEW")) {
            this.selectedCargo = new BeanCargoMan();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedCargo = (BeanCargoMan) c1.clone();
            for (int i = 0; i < listArde.size(); i++) {
                if (this.selectedCargo.getCodi_arde() == listArde.get(i).getX()) {
                    this.curArde = listArde.get(i);
                    break;
                }
            }
        }
    }

    private void loadDataControls() {
        try {
            area_dependencia_dao obj_area_dependencia_dao = new area_dependencia_dao();
            listArde = obj_area_dependencia_dao.get_select();
            obj_area_dependencia_dao.close();
            if (listArde.size() > 0) {
                curArde = listArde.get(0);
            }

        } catch (Exception ex) {
            Logger.getLogger(cargos_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            cargos_dao obj_cargos_dao = new cargos_dao();
            if ((getRecordMode().equals("NEW") && obj_cargos_dao.verificar_nuevo_cargos(selectedCargo.getNomb_carg(),curArde.x))
                    || (getRecordMode().equals("EDIT") && obj_cargos_dao.verificar_editar_cargos(selectedCargo.getCodi_carg(), selectedCargo.getNomb_carg(),curArde.x))) {
                cargos cargos = new cargos();
                cargos.setCodi_carg(selectedCargo.getCodi_carg());
                cargos.setNomb_carg(selectedCargo.getNomb_carg());
                cargos.setCodi_arde(curArde.x);
                cargos.setEsta_carg("A");

                Map args = new HashMap();
                args.put("sCargo", cargos);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateCargoList", args);
                obj_cargos_dao.close();
                win.detach();
            } else {
                Messagebox.show("El Nombre de Cargo Ingresado ya Existe, eliga otro",
                        "Mantenimiento de Cargos", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Cargos", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateCargoList", null);
    }

    public List<Pair<Integer, String>> getListArde() {
        return listArde;
    }

    public void setListArde(List<Pair<Integer, String>> listArde) {
        this.listArde = listArde;
    }

    public Pair<Integer, String> getCurArde() {
        return curArde;
    }

    public void setCurArde(Pair<Integer, String> curArde) {
        this.curArde = curArde;
    }

}
