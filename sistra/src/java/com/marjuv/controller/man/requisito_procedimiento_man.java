package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.entity.requisito_procedimiento;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class requisito_procedimiento_man {

    @Wire("#requisito_procedimiento_man")
    private Window win;
    private BeanRequisitoMan selectedRequisito;
    private String recordMode;
    
    @Wire()
    private Textbox txtCodi_req;
    @Wire()
    private Textbox txtNomb_req;
    
    @Wire()
    private Textbox txtCodi_req2;
    @Wire()
    private Textbox txtNomb_req2;
   
    
    @Wire()
    private Textbox txtCodi_req3;
    @Wire()
    private Textbox txtNomb_req3;
    @Wire()
    private Textbox txtNombjerarquia;
    
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public BeanRequisitoMan getSelectedRequisito() {
        return selectedRequisito;
    }

    public void setSelectedRequisito(BeanRequisitoMan selectedRequisito) {
        this.selectedRequisito = selectedRequisito;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sRequisito") BeanRequisitoMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException, Exception {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        
        if (recordMode.equals("NEW")) {
            this.selectedRequisito = new BeanRequisitoMan();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedRequisito = (BeanRequisitoMan) c1.clone();
            
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            this.selectedRequisito.setNomb_proc(obj_procedimiento_expediente_dao.get_nomb_proc(selectedRequisito.getCodi_proc()));
            obj_procedimiento_expediente_dao.close();
//            requisito_procedimiento_dao ObjRequisitoDAO = new requisito_procedimiento_dao();
//            this.selectedRequisito.setNomb_requisito_jera(String.valueOf(ObjRequisitoDAO.get_requisito_Jerarquia(selectedRequisito.getJera_repr())));
//            ObjRequisitoDAO.close();
        }
    }

    @GlobalCommand
    @NotifyChange("selectedRequisito")
    public void setProcedimiento_req(@BindingParam("codi_proc") int codi_proc, @BindingParam("nomb_proc") String nomb_proc) {
        this.selectedRequisito.setCodi_proc(codi_proc);
        this.selectedRequisito.setNomb_proc(nomb_proc);

    }
    
    @NotifyChange("selectedRequisito")
    @GlobalCommand
    public void setRecibir_Requisito2(@BindingParam("codi_repr") int codi_repr, @BindingParam("nomb_repr") String nomb_repr) {
        try {
            this.txtCodi_req.setText(String.valueOf(codi_repr));
            this.txtNombjerarquia.setText(nomb_repr);
        } catch (Exception e) {
            Messagebox.show("Error: " + e.getMessage());
        }
    }
    
    @NotifyChange("selectedRequisito")
    @GlobalCommand
    public void setRecibir_Requisito3(@BindingParam("codi_repr") int codi_repr, @BindingParam("nomb_repr") String nomb_repr) {
        try {
//            this.selectedRequisito.setJera_repr(codi_repr);
//            this.selectedRequisito.setNomb_requisito_jera(nomb_repr);
            this.txtCodi_req3.setText(String.valueOf(codi_repr));
            this.txtNomb_req3.setText(nomb_repr);
        } catch (Exception e) {
            Messagebox.show("Error3: " + e.getMessage());
        }
    }
    
    @NotifyChange("selectedRequisito")
    @GlobalCommand
    public void setRecibir_Requisito4(@BindingParam("codi_repr") int codi_repr, @BindingParam("nomb_repr") String nomb_repr) {
        try {
            this.selectedRequisito.setJera_repr(codi_repr);
            this.selectedRequisito.setNomb_requisito_jera(nomb_repr);
//            this.txtCodi_req2.setText(String.valueOf(codi_repr));
//            this.txtNomb_req2.setText(nomb_repr);
        } catch (Exception e) {
            Messagebox.show("Error4: " + e.getMessage());
        }
    }
    
    @Command
    public void Agregar_Requisito2() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "Agregar");
        Executions.createComponents("mantenimiento/man/addRequisito_Procedimiento3.zul", null, map);
    }
    
    @Command
    public void Agregar_Requisito1() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "Agregar");
        Executions.createComponents("mantenimiento/man/addRequisito_mantenimiento.zul", null, map);
    }
    
    @Command
    public void Agregar_Procedimiento() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "Agregar");
        Executions.createComponents("mantenimiento/man/add_procedimiento_req.zul", null, map);
    }
    
   @Command
    public void addRequisito() {
        try {
            Map map = new HashMap();
            map.put("nomb_repr", this.txtNomb_req.getText());
            map.put("codi_jerarquia", this.txtCodi_req3.getText());
            BindUtils.postGlobalCommand(null, null, "setRecibir_Requisito", map);
            win.detach();
        } catch (Exception ex) {
             Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK",
                     Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
            if ((getRecordMode().equals("NEW") && obj_requisito_procedimiento_dao.verificar_nuevo_requisito_procedimiento(selectedRequisito.getNomb_repr()))
                    || (getRecordMode().equals("EDIT") && obj_requisito_procedimiento_dao.verificar_editar_requisito_procedimiento(selectedRequisito.getCodi_repr(), selectedRequisito.getNomb_repr()))) {
                if (selectedRequisito.getNomb_repr()== null || selectedRequisito.getNomb_repr().equals("")) {
                    Messagebox.show("Por favor, ingrese nombre de requisito",
                            "Mantenimiento de Requisitos", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
                if (selectedRequisito.getCodi_proc() == 0) {
                    Messagebox.show("Por favor, seleccione un procedimiento",
                            "Mantenimiento de Requisitos", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
                requisito_procedimiento requisito_procedimiento = new requisito_procedimiento();
                requisito_procedimiento.setCodi_repr(selectedRequisito.getCodi_repr());
                requisito_procedimiento.setNomb_repr(selectedRequisito.getNomb_repr());
                requisito_procedimiento.setPrec_repr(0.0);
                requisito_procedimiento.setCodi_proc(selectedRequisito.getCodi_proc());
//                if(this.txtCodi_req2.equals("")){
//                    Messagebox.show("Jerarquia en 0");
//                    requisito_procedimiento.setJera_repr(0);
//                }else{
                    requisito_procedimiento.setJera_repr(selectedRequisito.getJera_repr());
                   
//                }
                
//                requisito_procedimiento.setJera_repr(Integer.parseInt(this.txtCodi_req2.getText()));
                requisito_procedimiento.setEsta_repr("1");

                Map args = new HashMap();
                args.put("sRequisito", requisito_procedimiento);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateRequisitoList", args);
                win.detach();
            } else {
                Messagebox.show("El Nombre de Requisito Ingresado ya Existe, eliga otro",
                        "Mantenimiento de Requisitos", Messagebox.OK, Messagebox.ERROR);
                return;
            }
            obj_requisito_procedimiento_dao.close();
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Requisitos", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateRequisitoList", null);
    }


}
