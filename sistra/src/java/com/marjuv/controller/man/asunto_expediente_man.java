package com.marjuv.controller.man;

import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.entity.asunto_expediente;
import java.util.HashMap;
import java.util.Map;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class asunto_expediente_man {

    @Wire("#asunto_expediente_man")
    private Window win;
    private asunto_expediente selectedAsuntoExpediente;
    private String recordMode;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sAsunto_expediente") asunto_expediente c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedAsuntoExpediente = new asunto_expediente();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedAsuntoExpediente = (asunto_expediente) c1.clone();
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
            if ((getRecordMode().equals("NEW") && obj_asunto_expediente_dao.verificar_nuevo_asunto_expediente(selectedAsuntoExpediente.getDesc_asex()))
                    || (getRecordMode().equals("EDIT") && obj_asunto_expediente_dao.verificar_editar_asunto_expediente(selectedAsuntoExpediente.getCodi_asex(), selectedAsuntoExpediente.getDesc_asex()))) {
                asunto_expediente asunto_expediente = new asunto_expediente();
                asunto_expediente.setCodi_asex(selectedAsuntoExpediente.getCodi_asex());
                asunto_expediente.setDesc_asex(selectedAsuntoExpediente.getDesc_asex());
                asunto_expediente.setEsta_asex("A");
                
                Map args = new HashMap();
                args.put("sAsunto_expediente", asunto_expediente);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateAsuntoExpedienteList", args);
                obj_asunto_expediente_dao.close();
                win.detach();
            } else {
                Messagebox.show("El asunto de expediente ingresado ya existe, eliga otro",
                        "Mantenimiento de Asuntos de Expediente", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Asuntos de Expediente", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateAsuntoExpedienteList", null);
    }

    public asunto_expediente getSelectedAsuntoExpediente() {
        return selectedAsuntoExpediente;
    }

    public void setSelectedAsuntoExpediente(asunto_expediente selectedAsuntoExpediente) {
        this.selectedAsuntoExpediente = selectedAsuntoExpediente;
    }
}
