package com.marjuv.controller.man;

import com.marjuv.dao.Asunto_Resolucion_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.asunto_destino_defecto_dao;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.Message;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

public class Asunto_destino_defecto_man {

    @Wire("#asunto_destino_defecto_man")
    private Window win;
    private List<Pair<Integer, String>> lista_dependencia;
    private List<Pair<Integer, String>> lista_asunto;
    private Pair<Integer, String> seleccion_asunto;
    private Pair<Integer, String> seleccion_dependencia;
    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    @Init
    public void initSetup() throws SQLException {
        Dependencia_dao ObjDependenciaDAO = new Dependencia_dao();
        lista_dependencia = ObjDependenciaDAO.get_select2();
        Asunto_Resolucion_dao ObjAsundoResolucionDAO = new Asunto_Resolucion_dao();
        lista_asunto = ObjAsundoResolucionDAO.get_select();
        ObjAsundoResolucionDAO.close();
        ObjDependenciaDAO.close();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        try {
            Map args = new HashMap();
            args.put("codi_arde", seleccion_dependencia.x);
            args.put("codi_asre", seleccion_asunto.x);
            args.put("accion",1);
            BindUtils.postGlobalCommand(null, null, "updateList", args);
            win.detach();
        } catch (Exception e) {
            Messagebox.show("Ocurrió un error inesperado" + e.getMessage());
        }
    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateCargoList", null);
    }

    public List<Pair<Integer, String>> getLista_dependencia() {
        return lista_dependencia;
    }

    public void setLista_dependencia(List<Pair<Integer, String>> lista_dependencia) {
        this.lista_dependencia = lista_dependencia;
    }

    public List<Pair<Integer, String>> getLista_asunto() {
        return lista_asunto;
    }

    public void setLista_asunto(List<Pair<Integer, String>> lista_asunto) {
        this.lista_asunto = lista_asunto;
    }

    public Pair<Integer, String> getSeleccion_asunto() {
        return seleccion_asunto;
    }

    public void setSeleccion_asunto(Pair<Integer, String> seleccion_asunto) {
        this.seleccion_asunto = seleccion_asunto;
    }

    public Pair<Integer, String> getSeleccion_dependencia() {
        return seleccion_dependencia;
    }

    public void setSeleccion_dependencia(Pair<Integer, String> seleccion_dependencia) {
        this.seleccion_dependencia = seleccion_dependencia;
    }

}
