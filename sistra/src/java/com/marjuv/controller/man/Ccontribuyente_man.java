
package com.marjuv.controller.man;

import com.marjuv.dao.Contribuyente_dao;
import com.marjuv.dao.Departamento_dao;
import com.marjuv.dao.Distrito_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Provincia_dao;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Provincia;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Ccontribuyente_man {

    @Wire("#contribuyente_man")
    private Window win;
    private Contribuyente selectedContribuyente;
    private String recordMode;
    private List<Departamento> list_depa;
    private List<Provincia> list_prov;
    private List<Distrito> list_dist;
    private Departamento curDepartamento;
    private Provincia curProvincia;
    private Distrito curDistrito;
    private Date curFecha;

    public Date getCurFecha() {
        return curFecha;
    }

    public void setCurFecha(Date curFecha) {
        this.curFecha = curFecha;
    }

    public Provincia getCurProvincia() {
        return curProvincia;
    }

    public void setCurProvincia(Provincia curProvincia) {
        this.curProvincia = curProvincia;
    }

    public Distrito getCurDistrito() {
        return curDistrito;
    }

    public void setCurDistrito(Distrito curDistrito) {
        this.curDistrito = curDistrito;
    }

    public List<Departamento> getList_depa() {
        return list_depa;
    }

    public void setList_depa(List<Departamento> list_depa) {
        this.list_depa = list_depa;
    }

    public List<Provincia> getList_prov() {
        return list_prov;
    }

    public void setList_prov(List<Provincia> list_prov) {
        this.list_prov = list_prov;
    }

    public List<Distrito> getList_dist() {
        return list_dist;
    }

    public void setList_dist(List<Distrito> list_dist) {
        this.list_dist = list_dist;
    }

    public Departamento getCurDepartamento() {
        return curDepartamento;
    }

    public void setCurDepartamento(Departamento curDepartamento) {
        this.curDepartamento = curDepartamento;
    }

    public List<Provincia> getList_prov(int codi_depa) {
        try {
            Provincia_dao objProvinciaDAO = new Provincia_dao();
            List<Provincia> lista = objProvinciaDAO.listar_provincia(codi_depa);
            objProvinciaDAO.close();
            return lista;
            
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Init
    @NotifyChange("list_prov")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("curContribuyente") Contribuyente c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        try {
            Departamento_dao objDepartamentoDAO = new Departamento_dao();
            setList_depa(objDepartamentoDAO.listar_departamento());
            objDepartamentoDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        setRecordMode(recordMode);
        /*DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (c1 != null) {
                this.curFecha = formatter.parse(c1.getFena_cont());
            }
        } catch (ParseException ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        if (recordMode.equals("NEW")) {
            this.selectedContribuyente = new Contribuyente();
        }

        if (recordMode.equals("EDIT")) {
            try {
                this.selectedContribuyente = c1;
                this.curDepartamento = c1.getObj_dist().getProvincia().getDepartamento();
                this.curProvincia = c1.getObj_dist().getProvincia();
                this.curDistrito = c1.getObj_dist();
                Departamento_dao objDepartamentoDAO = new Departamento_dao();
                Provincia_dao objProvinciaDAO = new Provincia_dao();
                Distrito_dao objDistritoDAO = new Distrito_dao();
                
                setList_depa(objDepartamentoDAO.listar_departamento());
                setList_prov(objProvinciaDAO.listar_provincia(curDepartamento.getCodi_depa()));
                setList_dist(objDistritoDAO.listar_distrito(curProvincia.getCodi_prov()));
                
                objDepartamentoDAO.close();
                objDistritoDAO.close();
                objProvinciaDAO.close();
                
                this.curDepartamento = c1.getObj_dist().getProvincia().getDepartamento();
                this.curProvincia = c1.getObj_dist().getProvincia();
                this.curDistrito = c1.getObj_dist();
            } catch (Exception ex) {
                Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() throws SQLException {
        Map args = new HashMap();
        if (curDistrito == null) {
            Messagebox.show("Seleccione un Distrito");
            return;
        }
        if (getSelectedContribuyente().getGene_cont() == null || getSelectedContribuyente().getGene_cont().isEmpty()) {
            Messagebox.show("Seleccione un Genero");
            return;
        }
        if ((getSelectedContribuyente().getDni_cont() + "").length() != 8) {
            Messagebox.show("El DNI debe tener 8 Digitos");
            return;
        }
        /*if (curFecha == null) {
            Messagebox.show("Seleccione una Fecha");
            return;
        }*/
        if (selectedContribuyente.getEmai_cont()==null) {
            selectedContribuyente.setEmai_cont("");
        }
        if (selectedContribuyente.getTelf_cont()==null) {
            selectedContribuyente.setTelf_cont("");
        }
        if (selectedContribuyente.getCelu_cont()==null) {
            selectedContribuyente.setCelu_cont("");
        }
        Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
        if ((getRecordMode().equals("NEW") && obj_Contribuyente_dao.verificar_nuevo_dni(selectedContribuyente.getDni_cont())
                || (getRecordMode().equals("EDIT") && obj_Contribuyente_dao.verificar_editar_dni(selectedContribuyente.getCodi_cont(), selectedContribuyente.getDni_cont())))) {
            //this.selectedContribuyente.setDfen_cont(curFecha);
            this.selectedContribuyente.setObj_dist(curDistrito);
            args.put("curContribuyente", this.selectedContribuyente);
            args.put("accion", this.getRecordMode());
            BindUtils.postGlobalCommand(null, null, "updateListContribuyente", args);
            win.detach();
        } else {
            Messagebox.show("El D.N.I. de Contribuyente Ingresado ya Existe, eliga otro",
                    "Mantenimiento de Contribuyente", Messagebox.OK, Messagebox.ERROR);
            return;
        }
        obj_Contribuyente_dao.close();

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"list_prov", "list_dist"})
    public void updateProvincia() {
        try {
            Provincia_dao objProvinciaDAO = new Provincia_dao();
            setList_prov(objProvinciaDAO.listar_provincia(curDepartamento.getCodi_depa()));
            setList_dist(null);
            objProvinciaDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("list_dist")
    public void updateDistrito() {
        try {
            Distrito_dao objDistritoDAO = new Distrito_dao();
            setList_dist(objDistritoDAO.listar_distrito(curProvincia.getCodi_prov()));
            objDistritoDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Command
    public void closeThis() {
        win.detach();
    }

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public Contribuyente getSelectedContribuyente() {
        return selectedContribuyente;
    }

    public void setSelectedContribuyente(Contribuyente selectedContribuyente) {
        this.selectedContribuyente = selectedContribuyente;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }
}
