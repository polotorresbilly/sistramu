package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.controller.win.requisito_procedimiento_win;
import com.marjuv.dao.Auditoria_mantenimiento_dao;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.dao.asunto_expediente_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.procedimiento_expediente_dao;
import com.marjuv.dao.requisito_procedimiento_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.requisito_procedimiento;
import com.marjuv.entity.usuario;
import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class procedimiento_expediente_man {

    @Wire("#procedimiento_expediente_man")
    private Window win;
    private BeanProcedimientoMan selectedProcedimiento;
    private List<BeanRequisitoMan> lst_requisito_procedimiento;
    private BeanRequisitoMan cur_requisito_procedimiento;
    private BeanRequisitoMan Selector;

    public BeanRequisitoMan getSelector() {
        return Selector;
    }

    public void setSelector(BeanRequisitoMan Selector) {
        this.Selector = Selector;
    }
    private String recordMode;
    private List<Pair<Integer, String>> listAsex, list_carg;
    private Pair<Integer, String> curAsex, cur_carg;
    private List<Pair<Integer, String>> list_Dependencia;
    private Pair<Integer, String> cur_Dependencia;

    public BeanProcedimientoMan getSelectedProcedimiento() {
        return selectedProcedimiento;
    }

    public void setSelectedProcedimiento(BeanProcedimientoMan selectedProcedimiento) {
        this.selectedProcedimiento = selectedProcedimiento;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    public List<BeanRequisitoMan> getLst_requisito_procedimiento() {
        return lst_requisito_procedimiento;
    }

    public void setLst_requisito_procedimiento(List<BeanRequisitoMan> lst_requisito_procedimiento) {
        this.lst_requisito_procedimiento = lst_requisito_procedimiento;
    }

    public BeanRequisitoMan getCur_requisito_procedimiento() {
        return cur_requisito_procedimiento;
    }

    public void setCur_requisito_procedimiento(BeanRequisitoMan cur_requisito_procedimiento) {
        this.cur_requisito_procedimiento = cur_requisito_procedimiento;
    }

    @Init
    @NotifyChange("lst_requisito_procedimiento")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sProcedimiento") BeanProcedimientoMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        try {
            Selectors.wireComponents(view, this, false);
            setRecordMode(recordMode);
            loadDataControls();

            lst_requisito_procedimiento = new ArrayList<BeanRequisitoMan>();

            //Messagebox.show(lst_requisito_procedimiento.size() + "");
            if (recordMode.equals("NEW")) {
                this.selectedProcedimiento = new BeanProcedimientoMan();
            }
            if (recordMode.equals("EDIT") || recordMode.equals("Asignar")) {
                this.selectedProcedimiento = (BeanProcedimientoMan) c1.clone();
                requisito_procedimiento_dao dao = new requisito_procedimiento_dao();
                setLst_requisito_procedimiento(dao.get_requisito_procedimiento(selectedProcedimiento.getCodi_proc()));
                dao.close();
                for (int i = 0; i < listAsex.size(); i++) {
                    if (this.selectedProcedimiento.getCodi_asex() == listAsex.get(i).getX()) {
                        this.curAsex = listAsex.get(i);
                        break;
                    }
                }
                Dependencia_dao objDependencia_dao = new Dependencia_dao();
                for (int i = 0; i < list_Dependencia.size(); i++) {
                    if (list_Dependencia.get(i).getY().equals(objDependencia_dao.get_nomb(this.selectedProcedimiento.getCodi_carg()))) {
                        cur_Dependencia = list_Dependencia.get(i);
                        cargos_dao ObjCargoDAO = new cargos_dao();
                        list_carg = ObjCargoDAO.get_select(cur_Dependencia.x);
                        ObjCargoDAO.close();
                        if (list_carg.size() > 0) {
                            cur_carg = null;
                            for (int j = 0; j < list_carg.size(); j++) {
                                if (list_carg.get(j).x == this.selectedProcedimiento.getCodi_carg()) {
                                    cur_carg = list_carg.get(j);
                                    break;
                                }
                            }
                        } else {
                            cur_carg = null;
                        }
                    }
                }
                objDependencia_dao.close();

            }
            if (recordMode.equals("Asignar")) {
                this.selectedProcedimiento = (BeanProcedimientoMan) c1.clone();
                for (int i = 0; i < listAsex.size(); i++) {
                    if (this.selectedProcedimiento.getCodi_asex() == listAsex.get(i).getX()) {
                        this.curAsex = listAsex.get(i);
                        break;
                    }
                }
                Dependencia_dao objDependencia_dao = new Dependencia_dao();
                for (int i = 0; i < list_Dependencia.size(); i++) {
                    if (list_Dependencia.get(i).getY().equals(objDependencia_dao.get_nomb(this.selectedProcedimiento.getCodi_carg()))) {
                        cur_Dependencia = list_Dependencia.get(i);
                        cargos_dao ObjCargoDAO = new cargos_dao();
                        list_carg = ObjCargoDAO.get_select(cur_Dependencia.x);
                        ObjCargoDAO.close();
                        if (list_carg.size() > 0) {
                            cur_carg = null;
                            for (int j = 0; j < list_carg.size(); j++) {
                                if (list_carg.get(j).x == this.selectedProcedimiento.getCodi_carg()) {
                                    cur_carg = list_carg.get(j);
                                    break;
                                }
                            }
                        } else {
                            cur_carg = null;
                        }
                    }
                }
                objDependencia_dao.close();
            }
        } catch (Exception ex) {
            Messagebox.show(ex.getMessage());
        }
    }

    @NotifyChange("list_carg")
    @Command
    public void updateCargo() throws SQLException {
        cargos_dao ObjCargoDAO = new cargos_dao();
        list_carg = ObjCargoDAO.get_select(cur_Dependencia.x);
        ObjCargoDAO.close();
        if (list_carg.size() > 0) {
            cur_carg = list_carg.get(0);
        } else {
            cur_carg = null;
        }
    }

    @NotifyChange("*")
    @Command
    public void quitar() {

        try {
            for (int x = 0; x < lst_requisito_procedimiento.size(); x++) {
                if (lst_requisito_procedimiento.get(x).getNomb_repr().equals(cur_requisito_procedimiento.getNomb_repr())) {
                    lst_requisito_procedimiento.remove(lst_requisito_procedimiento.get(x));
                    requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
                    obj_requisito_procedimiento_dao.delete(cur_requisito_procedimiento.getCodi_repr());
                    insert_auditoria("D", "REQUISITO_PROCEDIMIENTO");
                    break;
                }
            }
        } catch (Exception e) {
            Messagebox.show("Error Quitar: " + e.getMessage());
        }

    }

    @Command
    public void Agregar_Requisito3() {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("sRequisito", null);
        map.put("accion", "Agregar");
        Executions.createComponents("mantenimiento/man/addRequisito_Procedimiento2.zul", null, map);
    }

    @NotifyChange("lst_requisito_procedimiento")
    @GlobalCommand
    public void setRecibir_Requisito(@BindingParam("nomb_repr") String nomb_repr, @BindingParam("codi_jerarquia") int jera_repr) {
        try {
            BeanRequisitoMan objRequisito = new BeanRequisitoMan();
            if (lst_requisito_procedimiento.size() > 0) {
                boolean exist = false;
                for (int i = 0; i < lst_requisito_procedimiento.size(); i++) {
                    if (lst_requisito_procedimiento.get(i).getNomb_repr().equals(nomb_repr)) {
                        exist = true;
                        break;
                    }
                }
                if (exist) {
                    Messagebox.show("El requisito ya existe en la lista");
                } else {
                    objRequisito.setNomb_repr(nomb_repr);
                    objRequisito.setJera_repr(jera_repr);
                    this.lst_requisito_procedimiento.add(objRequisito);
                    requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
                    requisito_procedimiento req = new requisito_procedimiento();
                    req.setCodi_proc(selectedProcedimiento.getCodi_proc());
                    req.setCodi_repr(objRequisito.getCodi_repr());
                    req.setEsta_repr("1");
                    req.setJera_repr(objRequisito.getJera_repr());
                    req.setNomb_repr(objRequisito.getNomb_repr());
                    req.setPrec_repr(objRequisito.getPrec_repr());
                    
                    obj_requisito_procedimiento_dao.insert(req);
                    obj_requisito_procedimiento_dao.close();
                    insert_auditoria("I", "REQUISITO_PROCEDIMIENTO");
                    Messagebox.show("El requisito fue registrado");
                }
            } else {
                objRequisito.setNomb_repr(nomb_repr);
                objRequisito.setJera_repr(jera_repr);
                this.lst_requisito_procedimiento.add(objRequisito);
                
                requisito_procedimiento_dao obj_requisito_procedimiento_dao = new requisito_procedimiento_dao();
                    requisito_procedimiento req = new requisito_procedimiento();
                    req.setCodi_proc(selectedProcedimiento.getCodi_proc());
                    req.setCodi_repr(objRequisito.getCodi_repr());
                    req.setEsta_repr("1");
                    req.setJera_repr(objRequisito.getJera_repr());
                    req.setNomb_repr(objRequisito.getNomb_repr());
                    req.setPrec_repr(objRequisito.getPrec_repr());
                    
                    obj_requisito_procedimiento_dao.insert(req);
                    obj_requisito_procedimiento_dao.close();
                    insert_auditoria("I", "REQUISITO_PROCEDIMIENTO");
                    
                    Messagebox.show("El requisito fue registrado");
            }

        } catch (Exception e) {
            Messagebox.show("Erroradasd: " + e.getMessage());
        }
    }
    
    
    @WireVariable
    private Session _sess;
    public void insert_auditoria(String accion,String tabla) throws SQLException{
        if (_sess.getAttribute("acceso") != null && Integer.parseInt(_sess.getAttribute("acceso").toString()) == 1) {
            usuario obj_usuario = (usuario) _sess.getAttribute("usuario");
            new Auditoria_mantenimiento_dao(accion,obj_usuario.getCodi_usua()+"-"+obj_usuario.getNick_usua(), tabla);
        }
    }

    private void loadDataControls() {
        try {
            asunto_expediente_dao obj_asunto_expediente_dao = new asunto_expediente_dao();
            listAsex = obj_asunto_expediente_dao.get_select();
            obj_asunto_expediente_dao.close();
            if (listAsex.size() > 0) {
                curAsex = listAsex.get(0);
            }

            Dependencia_dao ObjDependenciaDAO = new Dependencia_dao();
            list_Dependencia = ObjDependenciaDAO.get_select2();
            if (list_Dependencia.size() > 0) {
                cur_Dependencia = list_Dependencia.get(0);
            }
            ObjDependenciaDAO.close();

            cargos_dao ObjCargoDAO = new cargos_dao();
            list_carg = ObjCargoDAO.get_select(cur_Dependencia.x);
            ObjCargoDAO.close();
            if (list_carg.size() > 0) {
                cur_carg = list_carg.get(0);
            } else {
                cur_carg = null;
            }

            /*cargos_dao obj_cargos_dao = new cargos_dao();
             list_carg = obj_cargos_dao.get_select();
             obj_cargos_dao.close();
             if (list_carg.size() > 0) {
             cur_carg = list_carg.get(0);
             }*/
        } catch (Exception ex) {
            Logger.getLogger(procedimiento_expediente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            if ((getRecordMode().equals("NEW") && obj_procedimiento_expediente_dao.verificar_nuevo_procedimiento_expediente(selectedProcedimiento.getNomb_proc()))
                    || (getRecordMode().equals("EDIT") && obj_procedimiento_expediente_dao.verificar_editar_procedimiento_expediente(selectedProcedimiento.getCodi_proc(), selectedProcedimiento.getNomb_proc()))) {
                Procedimiento_Expediente procedimiento_expediente = new Procedimiento_Expediente();
                procedimiento_expediente.setCodi_proc(selectedProcedimiento.getCodi_proc());
                procedimiento_expediente.setNomb_proc(selectedProcedimiento.getNomb_proc());
                procedimiento_expediente.setCodi_asex(curAsex.x);
                procedimiento_expediente.setCodi_carg(cur_carg.x);
                procedimiento_expediente.setDias_proc(selectedProcedimiento.getDias_proc());
                procedimiento_expediente.setCost_proc(selectedProcedimiento.getCost_proc());
                procedimiento_expediente.setEsta_proc("A");

                Map args = new HashMap();
                args.put("sProcedimiento", procedimiento_expediente);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateProcedimientoList", args);
                obj_procedimiento_expediente_dao.close();
                win.detach();

            } else {
                Messagebox.show("El Nombre de Procedimiento Ingresado ya Existe, eliga otro",
                        "Mantenimiento de Procedimientos", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Procedimientos", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save2() {
        closeThis();
//        try {
//            if (getRecordMode().equals("Asignar")) {
//                procedimiento_expediente_dao ObjProcedimiento = new procedimiento_expediente_dao();
//                int Codigo_proc = ObjProcedimiento.get_codi_proc(selectedProcedimiento.getNomb_proc());
//                ObjProcedimiento.close();
//
//                if (lst_requisito_procedimiento != null) {
//
//                    for (int i = 0; i <= lst_requisito_procedimiento.size(); i++) {
////                        Messagebox.show("Codigo: " + lst_requisito_procedimiento.get(i).getCodi_repr());
////                        Messagebox.show("Nombre: " + lst_requisito_procedimiento.get(i).getNomb_repr());
//                        requisito_procedimiento ObjRequisitoEntity = new requisito_procedimiento();
//                        Map args = new HashMap();
//                        ObjRequisitoEntity.setCodi_proc(Codigo_proc);
//                        ObjRequisitoEntity.setNomb_repr(lst_requisito_procedimiento.get(i).getNomb_repr());
//                        ObjRequisitoEntity.setPrec_repr(0.0);
//                        ObjRequisitoEntity.setJera_repr(lst_requisito_procedimiento.get(i).getJera_repr());
//                        ObjRequisitoEntity.setEsta_repr("1");
//                        args.put("sRequisito", ObjRequisitoEntity);
//                        args.put("accion", this.recordMode);
//                        BindUtils.postGlobalCommand(null, null, "updateProcedimientoList", args);
//
//                        win.detach();
//
//                    }
//
//                } else {
//                    Messagebox.show("No se agrego ningun requisito");
//                    win.detach();
//                }
//
//            }
//        } catch (Exception e) {
//        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateProcedimientoList", null);
    }

    public List<Pair<Integer, String>> getListAsex() {
        return listAsex;
    }

    public void setListAsex(List<Pair<Integer, String>> listAsex) {
        this.listAsex = listAsex;
    }

    public Pair<Integer, String> getCurAsex() {
        return curAsex;
    }

    public void setCurAsex(Pair<Integer, String> curAsex) {
        this.curAsex = curAsex;
    }

    public List<Pair<Integer, String>> getList_carg() {
        return list_carg;
    }

    public void setList_carg(List<Pair<Integer, String>> list_carg) {
        this.list_carg = list_carg;
    }

    public Pair<Integer, String> getCur_carg() {
        return cur_carg;
    }

    public void setCur_carg(Pair<Integer, String> cur_carg) {
        this.cur_carg = cur_carg;
    }

    public List<Pair<Integer, String>> getList_Dependencia() {
        return list_Dependencia;
    }

    public void setList_Dependencia(List<Pair<Integer, String>> list_Dependencia) {
        this.list_Dependencia = list_Dependencia;
    }

    public Pair<Integer, String> getCur_Dependencia() {
        return cur_Dependencia;
    }

    public void setCur_Dependencia(Pair<Integer, String> cur_Dependencia) {
        this.cur_Dependencia = cur_Dependencia;
    }

}
