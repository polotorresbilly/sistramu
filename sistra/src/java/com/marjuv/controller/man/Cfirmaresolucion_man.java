/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.Dependencia_dao;
import com.marjuv.entity.Dependencia;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.Firma_Resolucion;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

/**
 *
 * @author BILLY
 */
public class Cfirmaresolucion_man {
    @Wire("#firmaresolucion_man")
    private Window win;
    private Firma_Resolucion curFirma;
    private List<BeanDependencias> list_dependencias;
    private BeanDependencias curDependencia;
    private String recordmode;
    
    private Dependencia_dao dao_dependencia;
    
    @Init
    @NotifyChange("list_dependencias")
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
        @ExecutionArgParam("curFirma") Firma_Resolucion c1,
        @ExecutionArgParam("accion") String recordMode)
        throws CloneNotSupportedException {
        try {
            Selectors.wireComponents(view, this, false);
            dao_dependencia = new Dependencia_dao();
            setList_dependencias(dao_dependencia.getDependencias());
            dao_dependencia.close();
            setRecordmode(recordMode);
        } catch (Exception ex) {
            Logger.getLogger(Cfirmaresolucion_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (recordMode.equals("NEW")) {
            this.curFirma = new Firma_Resolucion();
        }

        if (recordMode.equals("EDIT")) {
            this.curFirma = c1;
            for (int i = 0; i < getList_dependencias().size(); i++) {
                if (curFirma.getArea_depe().getCodi_arde() == getList_dependencias().get(i).getCodi_arde()) {
                    curDependencia = getList_dependencias().get(i);
                }
            }
        }
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        Map args = new HashMap();
        Dependencia objdependencia = new Dependencia();
        objdependencia.setCodi_arde(curDependencia.getCodi_arde());
        this.curFirma.setArea_depe(objdependencia);
        if (getRecordmode().equals("NEW")) {
            this.curFirma.setEsta_fire(true);
        }
        args.put("curFirma", this.curFirma);
        args.put("accion", this.getRecordmode());
        BindUtils.postGlobalCommand(null, null, "updateFirmaResolucion", args);
        
        win.detach();
    }
    
    
    @Command
    public void closeThis() {
        win.detach();
    }
    

    public Window getWin() {
        return win;
    }

    public void setWin(Window win) {
        this.win = win;
    }

    public String getRecordmode() {
        return recordmode;
    }

    public void setRecordmode(String recordmode) {
        this.recordmode = recordmode;
    }
    
    

    public Firma_Resolucion getCurFirma() {
        return curFirma;
    }

    public void setCurFirma(Firma_Resolucion curFirma) {
        this.curFirma = curFirma;
    }

    public List<BeanDependencias> getList_dependencias() {
        return list_dependencias;
    }

    public void setList_dependencias(List<BeanDependencias> list_dependencias) {
        this.list_dependencias = list_dependencias;
    }

    public BeanDependencias getCurDependencia() {
        return curDependencia;
    }

    public void setCurDependencia(BeanDependencias curDependencia) {
        this.curDependencia = curDependencia;
    }
    
    
}
