package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanLogin;
import com.marjuv.dao.usuario_dao;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Messagebox;
import com.marjuv.entity.usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class ContrasenaControllerMan extends GenericForwardComposer{
    
    
    private Textbox codi_usua;
    private Textbox nomb_usua;
    private Textbox nomb_arde;
    private Textbox nick_usua;
    private Textbox pass_usua;
    private Textbox pass_confirm;
    private Textbox new_pass;
    @Wire("#dependencia_man")
    private Window win_actualizar;
    private BeanLogin bean;

    
   

    @NotifyChange("*")
    @Init
    public void initSetup() throws Exception {
        
    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
        super.doAfterCompose(comp);
        usuario obj_usuario = (usuario) session.getAttribute("usuario");
        codi_usua.setValue(String.valueOf(obj_usuario.getCodi_usua()));
        nomb_usua.setValue(obj_usuario.getNomb_usua() + " " + obj_usuario.getApel_usua());
        usuario_dao objusuario_dao = new usuario_dao();
        nomb_arde.setValue(objusuario_dao.get_Dependencia(obj_usuario.getCodi_arde()));
        nick_usua.setValue(obj_usuario.getNick_usua());
        
    }

    
    public void onClick$save() {
        try {
            usuario_dao objusuario_dao = new usuario_dao();
            objusuario_dao.update_password(Integer.parseInt(codi_usua.getValue()),pass_usua.getValue(),new_pass.getValue(), pass_confirm.getValue());
            new_pass.setText("");
            pass_confirm.setText("");
            pass_usua.setText("");
            objusuario_dao.close();
            Messagebox.show("Su contraseña fue actualizada");
        } catch (Exception e) {
            Messagebox.show("Error" + e.getMessage(), "Error", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public BeanLogin getBean() {
        return bean;
    }

    public void setBean(BeanLogin bean) {
        this.bean = bean;
    }

}
