package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.estado_expediente_dao;
import com.marjuv.entity.estado_expediente;
import com.marjuv.entity.usuario;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class estado_expediente_man {

    @Wire("#estado_expediente_man")
    private Window win;
    private estado_expediente selectedEstadoExpediente;
    private String recordMode;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sEstado_expediente") estado_expediente c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedEstadoExpediente = new estado_expediente();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedEstadoExpediente = (estado_expediente) c1.clone();
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            estado_expediente_dao obj_estado_expediente_dao = new estado_expediente_dao();
            if ((getRecordMode().equals("NEW") && obj_estado_expediente_dao.verificar_nuevo_estado_expediente(selectedEstadoExpediente.getNomb_esex()))
                    || (getRecordMode().equals("EDIT") && obj_estado_expediente_dao.verificar_editar_estado_expediente(selectedEstadoExpediente.getCodi_esex(), selectedEstadoExpediente.getNomb_esex()))) {
                estado_expediente estado_expediente = new estado_expediente();
                estado_expediente.setCodi_esex(selectedEstadoExpediente.getCodi_esex());
                estado_expediente.setNomb_esex(selectedEstadoExpediente.getNomb_esex());
                estado_expediente.setEsta_esex("A");
                
                Map args = new HashMap();
                args.put("sEstado_expediente", estado_expediente);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateEstadoExpedienteList", args);
                obj_estado_expediente_dao.close();
                win.detach();
            } else {
                Messagebox.show("El estado de expediente ingresado ya existe, eliga otro",
                        "Mantenimiento de Estados de Expediente", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Estados de Expediente", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateEstadoExpedienteList", null);
    }

    public estado_expediente getSelectedEstadoExpediente() {
        return selectedEstadoExpediente;
    }

    public void setSelectedEstadoExpediente(estado_expediente selectedEstadoExpediente) {
        this.selectedEstadoExpediente = selectedEstadoExpediente;
    }
}
