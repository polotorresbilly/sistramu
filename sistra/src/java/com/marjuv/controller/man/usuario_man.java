package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.permisos_usuario_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.permiso;
import com.marjuv.entity.usuario;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class usuario_man {

    @Wire("#usuario_man")
    private Window win;
    private BeanUsuarioMan selectedUsuario;
    private String recordMode;
    private List<Pair<Integer, String>> listArde;
    private Pair<Integer, String> curArde;
    private List<Pair<Integer, String>> listCargo;
    private Pair<Integer, String> curCargo;
    private List<Pair<Integer, String>> listRol;
    private Pair<Integer, String> curRol;
    private List<permiso> lst_permisos_all;
    
    private permiso cur_permisos_all;
    private List<permiso> lst_permisos_usuario;
    private permiso cur_permisos_usuario;

    @Wire
    private Textbox passusua;
    @Wire
    private Textbox vpassusua;
    @Wire
    private Listbox right;

    @Wire
    private Combobox cboCargo;

    private boolean sw;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        if (getRecordMode().equals("NEW") || getRecordMode().equals("CHANGE")) {
            this.passusua.setConstraint("no empty, /[a-zA-Z0-9]{6,12}$/ : Debe contener entre 6 y 12 carácteres. Sólo se acepta números y letras");
            this.vpassusua.setConstraint("no empty, /[a-zA-Z0-9]{6,12}$/ : Debe contener entre 6 y 12 carácteres. Sólo se acepta números y letras");
        } else if (getRecordMode().equals("EDIT")) {
            this.passusua.setDisabled(true);
            this.vpassusua.setDisabled(true);
        }
    }

    public BeanUsuarioMan getSelectedUsuario() {
        return selectedUsuario;
    }

    public void setSelectedUsuario(BeanUsuarioMan selectedUsuario) {
        this.selectedUsuario = selectedUsuario;
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @NotifyChange("*")
    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sUsuario") BeanUsuarioMan c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        loadDataControls();
        sw = false;
        if (recordMode.equals("NEW")) {
            this.selectedUsuario = new BeanUsuarioMan();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedUsuario = (BeanUsuarioMan) c1.clone();
            this.selectedUsuario.setPass_usua("");
            for (int i = 0; i < listArde.size(); i++) {
                if (this.selectedUsuario.getCodi_arde() == listArde.get(i).getX()) {
                    try {
                        this.curArde = listArde.get(i);
                        changeCargo();
                        break;
                    } catch (SQLException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
            for (int i = 0; i < listRol.size(); i++) {
                if (Integer.parseInt(this.selectedUsuario.getCodi_rous()) == listRol.get(i).getX()) {
                    this.curRol = listRol.get(i);
                    break;
                }
            }
        }
        if (recordMode.equals("CHANGE")) {
            this.selectedUsuario = (BeanUsuarioMan) c1.clone();
            this.selectedUsuario.setPass_usua("");
        }
    }

    @NotifyChange("listCargo")
    @Command
    public void changeCargo() throws SQLException {
        if (curArde != null) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            listCargo = obj_cargos_dao.get_select(curArde.x);
            if (listCargo.size() > 0) {
                curCargo = listCargo.get(0);
            } else {
                curCargo = null;
            }
            obj_cargos_dao.close();
        } else {
            curCargo = null;
        }
    }

    private void loadDataControls() {
        try {
            rol_dao obj_rol_dao = new rol_dao();
            listRol = obj_rol_dao.get_select();
            obj_rol_dao.close();
            if (listRol.size() > 0) {
                curRol = listRol.get(0);
            }

            area_dependencia_dao obj_area_dependencia_dao = new area_dependencia_dao();
            listArde = obj_area_dependencia_dao.get_select();
            obj_area_dependencia_dao.close();
            if (listArde.size() > 0) {
                curArde = listArde.get(0);
            } else {
                curArde = null;

            }

            if (curArde != null) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                listCargo = obj_cargos_dao.get_select(curArde.x);
                if (listCargo.size() > 0) {

                    curCargo = listCargo.get(0);
                } else {
                    curCargo = null;
                }
                obj_cargos_dao.close();
            } else {

                curCargo = null;

            }

        } catch (Exception ex) {
            Logger.getLogger(usuario_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @NotifyChange({"lst_permisos_all", "lst_permisos_usuario"})
    @Command
    public void onChangeRol() throws SQLException {
        sw = true;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {
        String obj_nomb_usua = selectedUsuario.getNick_usua();
        String nick = obj_nomb_usua.replaceAll(" ", "");

        String pass = "", verif_pass = "";

        if (getRecordMode().equals("NEW") || getRecordMode().equals("CHANGE")) {
            String obj_pass_usua = selectedUsuario.getPass_usua();
            pass = obj_pass_usua.replaceAll(" ", "");

            String obj_verif_pass_usua = selectedUsuario.getVerif_pass_usua();
            verif_pass = obj_verif_pass_usua.replaceAll(" ", "");
        }

        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();

        if ((getRecordMode().equals("NEW") && pass.equals(verif_pass))
                || (getRecordMode().equals("EDIT"))
                || (getRecordMode().equals("CHANGE") && pass.equals(verif_pass))) {
            try {
                usuario_dao obj_usuario_dao = new usuario_dao();
                if ((getRecordMode().equals("NEW") && obj_usuario_dao.verificar_nuevo_usuario(nick))
                        || (getRecordMode().equals("EDIT") && obj_usuario_dao.verificar_editar_usuario(selectedUsuario.getCodi_usua(), nick))
                        || getRecordMode().equals("CHANGE")) {
                    if ((getRecordMode().equals("NEW") && obj_usuario_dao.verificar_nuevo_dni(selectedUsuario.getDni_usua()))
                            || (getRecordMode().equals("EDIT") && obj_usuario_dao.verificar_editar_dni(selectedUsuario.getCodi_usua(), selectedUsuario.getDni_usua()))
                            || getRecordMode().equals("CHANGE")) {

                        usuario usuario = new usuario();
                        usuario.setCodi_usua(selectedUsuario.getCodi_usua());
                        usuario.setNomb_usua(selectedUsuario.getNomb_usua());
                        usuario.setApel_usua(selectedUsuario.getApel_usua());
                        if ((selectedUsuario.getDni_usua() + "").length() != 8) {
                            Messagebox.show("El DNI debe de tener 8 digitos");
                            return;
                        }
                        if (!getRecordMode().equals("CHANGE")) {
                            if (this.curCargo == null) {
                                Messagebox.show("Debe de seleccionar un cargo");
                                return;
                            }
                        }
                        usuario.setDni_usua(selectedUsuario.getDni_usua());
                        usuario.setNick_usua(nick);
                        if (getRecordMode().equals("NEW") || getRecordMode().equals("CHANGE")) {
                            usuario.setPass_usua(pass);
                        } else {
                            usuario.setPass_usua("");
                        }
                        if (getRecordMode().equals("NEW")) {
                            usuario.setFere_usua(new Date(currentDate.getTime()));
                        }

                        if (getRecordMode().equals("NEW") || getRecordMode().equals("EDIT")) {
                            usuario.setCodi_arde(curArde.x);
                            usuario.setCodi_carg(curCargo.x);
                            usuario.setCodi_rous(curRol.x);
                            usuario.setEsta_usua("A");
                            usuario.setNume_usua(obj_usuario_dao.nuevo_nume_usua(curRol.y, curArde.y));

                            permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                            usuario.setPermisos(obj_permisos_usuario_dao.format_list_permisos(right.getItems()));
                            obj_usuario_dao.close();
                        }

                        Map args = new HashMap();
                        args.put("sUsuario", usuario);
                        args.put("accion", this.recordMode);
                        BindUtils.postGlobalCommand(null, null, "updateUsuarioList", args);
                        win.detach();
                    } else {
                        Messagebox.show("El D.N.I. de Usuario Ingresado ya Existe, eliga otro",
                                "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
                        return;
                    }
                } else {
                    Messagebox.show("El Nombre de Usuario Ingresado ya Existe, eliga otro",
                            "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
                    return;
                }
                obj_usuario_dao.close();
            } catch (Exception ex) {
                Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                        "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
            }
        } else if (getRecordMode().equals("CHANGE") && pass.equals(verif_pass)) {
            usuario usuario = new usuario();
            usuario.setCodi_usua(selectedUsuario.getCodi_usua());
            usuario.setNomb_usua(selectedUsuario.getNomb_usua());
            usuario.setNick_usua(nick);
            usuario.setPass_usua(pass);

            Map args = new HashMap();
            args.put("sUsuario", usuario);
            args.put("accion", this.recordMode);
            BindUtils.postGlobalCommand(null, null, "updateUsuarioList", args);
            win.detach();
        } else {
            Messagebox.show("Las contraseñas no coinciden",
                    "Mantenimiento de Usuarios", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateNegocioList", null);
    }

    public List<Pair<Integer, String>> getListArde() {
        return listArde;
    }

    public void setListArde(List<Pair<Integer, String>> listArde) {
        this.listArde = listArde;
    }

    public Pair<Integer, String> getCurArde() {
        return curArde;
    }

    public void setCurArde(Pair<Integer, String> curArde) {
        this.curArde = curArde;
    }

    public List<Pair<Integer, String>> getListRol() {
        return listRol;
    }

    public void setListRol(List<Pair<Integer, String>> listRol) {
        this.listRol = listRol;
    }

    public Pair<Integer, String> getCurRol() {
        return curRol;
    }

    public void setCurRol(Pair<Integer, String> curRol) {
        this.curRol = curRol;
    }

    public void setLst_permisos_all(List<permiso> lst_permisos_all) {
        this.lst_permisos_all = lst_permisos_all;
    }

    public permiso getCur_permisos_all() {
        return cur_permisos_all;
    }

    public void setCur_permisos_all(permiso cur_permisos_all) {
        this.cur_permisos_all = cur_permisos_all;
    }

    public List<permiso> getLst_permisos_all() {
        try {
            List<permiso> temp = new ArrayList<permiso>();
            if (getRecordMode().equals("NEW")) {
                if (this.curRol.getX() != 0) {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_all = obj_permisos_usuario_dao.get_permisos_all(this.curRol.getX());
                    temp = obj_permisos_usuario_dao.get_permisos_rol(this.curRol.getX());
                    obj_permisos_usuario_dao.close();
                }
            } else if (getRecordMode().equals("EDIT")) {

                if (sw) {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_all = obj_permisos_usuario_dao.get_permisos_all(this.curRol.getX());

                   

                    temp = obj_permisos_usuario_dao.get_permisos_rol(this.curRol.getX());
                    obj_permisos_usuario_dao.close();
                } else {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_all = obj_permisos_usuario_dao.get_permisos_usuario_inverso(this.selectedUsuario.getCodi_usua());
                    temp = obj_permisos_usuario_dao.get_permisos_usuario(this.selectedUsuario.getCodi_usua());
                    obj_permisos_usuario_dao.close();
                }
            }
            List<permiso> papelera = new ArrayList<permiso>();
           
            for (int i = 0; i < lst_permisos_all.size(); i++) {
                for (int j = 0; j < temp.size(); j++) {
                    if (lst_permisos_all.get(i).getPage_perm().equals(temp.get(j).getPage_perm())) {
                        papelera.add(lst_permisos_all.get(i));
                        break;
                    }
                }
            }
            
            for (int i = 0; i < papelera.size(); i++) {
                lst_permisos_all.remove(papelera.get(i));
            }
            
            boolean sw_res_usu;
            List<permiso> lst_permisos_all_result = new ArrayList<permiso>();
            for (int i = 0; i < lst_permisos_all.size(); i++) {
                sw_res_usu = true;
                for (int j = 0; j < lst_permisos_all_result.size(); j++) {
                    if (lst_permisos_all.get(i).getPage_perm().equals(lst_permisos_all_result.get(j).getPage_perm())) {
                        sw_res_usu = false;
                        break;
                    }
                }
                if (sw_res_usu) {
                    lst_permisos_all_result.add(lst_permisos_all.get(i));
                }
            }
            lst_permisos_all = lst_permisos_all_result;
            for (int i = 0; i < papelera.size(); i++) {
            }

        } catch (Exception ex) {
            Messagebox.show(ex.getMessage());
        }
        return lst_permisos_all;
    }

    public void setLst_permisos_usuario(List<permiso> lst_permisos_usuario) {
        this.lst_permisos_usuario = lst_permisos_usuario;
    }

    public permiso getCur_permisos_usuario() {
        return cur_permisos_usuario;
    }

    public void setCur_permisos_usuario(permiso cur_permisos_usuario) {
        this.cur_permisos_usuario = cur_permisos_usuario;
    }

    public List<Pair<Integer, String>> getListCargo() {
        return listCargo;
    }

    public void setListCargo(List<Pair<Integer, String>> listCargo) {
        this.listCargo = listCargo;
    }

    public Pair<Integer, String> getCurCargo() {
        return curCargo;
    }

    public void setCurCargo(Pair<Integer, String> curCargo) {
        this.curCargo = curCargo;
    }

    public List<permiso> getLst_permisos_usuario() {
        try {
            if (getRecordMode().equals("NEW")) {
                if (this.curRol.getX() != 0) {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_usuario = obj_permisos_usuario_dao.get_permisos_rol(this.curRol.getX());
                    obj_permisos_usuario_dao.close();
                }
            } else if (getRecordMode().equals("EDIT")) {
                if (sw) {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_usuario = obj_permisos_usuario_dao.get_permisos_rol(this.curRol.getX());
                    obj_permisos_usuario_dao.close();
                } else {
                    permisos_usuario_dao obj_permisos_usuario_dao = new permisos_usuario_dao();
                    lst_permisos_usuario = obj_permisos_usuario_dao.get_permisos_usuario(this.selectedUsuario.getCodi_usua());
                    obj_permisos_usuario_dao.close();
                }
            }

        } catch (Exception ex) {
        }
        return lst_permisos_usuario;
    }

    

}
