package com.marjuv.controller.man;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.rol_dao;
import com.marjuv.dao.estado_resolucion_dao;
import com.marjuv.entity.estado_resolucion;
import com.marjuv.entity.usuario;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.text.TextBox;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class estado_resolucion_man {

    @Wire("#estado_resolucion_man")
    private Window win;
    private estado_resolucion selectedEstadoResolucion;
    private String recordMode;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }

    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sEstado_resolucion") estado_resolucion c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException {
        Selectors.wireComponents(view, this, false);
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedEstadoResolucion = new estado_resolucion();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedEstadoResolucion = (estado_resolucion) c1.clone();
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            estado_resolucion_dao obj_estado_resolucion_dao = new estado_resolucion_dao();
            if ((getRecordMode().equals("NEW") && obj_estado_resolucion_dao.verificar_nuevo_estado_resolucion(selectedEstadoResolucion.getNomb_esre()))
                    || (getRecordMode().equals("EDIT") && obj_estado_resolucion_dao.verificar_editar_estado_resolucion(selectedEstadoResolucion.getCodi_esre(), selectedEstadoResolucion.getNomb_esre()))) {
                estado_resolucion estado_resolucion = new estado_resolucion();
                estado_resolucion.setCodi_esre(selectedEstadoResolucion.getCodi_esre());
                estado_resolucion.setNomb_esre(selectedEstadoResolucion.getNomb_esre());
                estado_resolucion.setEsta_esre("1");
                
                Map args = new HashMap();
                args.put("sEstado_resolucion", estado_resolucion);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateEstadoResolucionList", args);
                obj_estado_resolucion_dao.close();
                win.detach();
            } else {
                Messagebox.show("El estado de resolucion ingresado ya existe, eliga otro",
                        "Mantenimiento de Estados de Resolucion", Messagebox.OK, Messagebox.ERROR);
                return;
            }
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Estados de Resolucion", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateEstadoResolucionList", null);
    }

    public estado_resolucion getSelectedEstadoResolucion() {
        return selectedEstadoResolucion;
    }

    public void setSelectedEstadoResolucion(estado_resolucion selectedEstadoResolucion) {
        this.selectedEstadoResolucion = selectedEstadoResolucion;
    }
}
