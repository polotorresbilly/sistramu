package com.marjuv.controller.man;

import com.marjuv.dao.Departamento_dao;
import com.marjuv.dao.Distrito_dao;
import com.marjuv.dao.Entidad_dao;
import com.marjuv.dao.Provincia_dao;
import com.marjuv.dao.area_dependencia_dao;
import com.marjuv.dao.cargos_dao;
import com.marjuv.dao.representante_dao;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Provincia;
import com.marjuv.entity.representante;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Window;

/**
 *
 * @author JACKELINE
 */
public class representante_man {

    @Wire("#repre_man")
    private Window win;
    private representante selectedRepre;
    private String recordMode;
    private List<Pair<Integer, String>> tipo;
    private List<Pair<Integer, String>> entidades;
    private List<Pair<Integer, String>> area;
    private List<Pair<Integer, String>> cargos;
    private Pair<Integer, String> curTipo;
    private Pair<Integer, String> curEntidad;
    private Pair<Integer, String> curArea;
    private Pair<Integer, String> curCargo;
    private List<Departamento> list_depa;
    private List<Provincia> list_prov;
    private List<Distrito> list_dist;
    private Departamento curDepartamento;
    private Provincia curProvincia;
    private Distrito curDistrito;
    @Wire
    private Row rEntidad;
    @Wire
    private Row rArea;
    @Wire
    private Row rCargo;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
    }
    
    public String getRecordMode() {
        return recordMode;
    }

    public void setRecordMode(String recordMode) {
        this.recordMode = recordMode;
    }

    @Init
    public void initSetup(@ContextParam(ContextType.VIEW) Component view,
            @ExecutionArgParam("sRepre") representante c1,
            @ExecutionArgParam("accion") String recordMode)
            throws CloneNotSupportedException, Exception {
        Selectors.wireComponents(view, this, false);
        try {
            Departamento_dao objDepartamentoDAO = new Departamento_dao();
            setList_depa(objDepartamentoDAO.listar_departamento());
            objDepartamentoDAO.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
        setRecordMode(recordMode);
        if (recordMode.equals("NEW")) {
            this.selectedRepre = new representante();
        }
        if (recordMode.equals("EDIT")) {
            this.selectedRepre = (representante) c1.clone();
            this.curDepartamento = c1.getObj_dist().getProvincia().getDepartamento();
            this.curProvincia = c1.getObj_dist().getProvincia();
            this.curDistrito = c1.getObj_dist();
            Departamento_dao objDepartamentoDAO = new Departamento_dao();
            Provincia_dao objProvinciaDAO = new Provincia_dao();
            Distrito_dao objDistritoDAO = new Distrito_dao();
            setList_depa(objDepartamentoDAO.listar_departamento());
            setList_prov(objProvinciaDAO.listar_provincia(curDepartamento.getCodi_depa()));
            setList_dist(objDistritoDAO.listar_distrito(curProvincia.getCodi_prov()));
            this.curDepartamento = c1.getObj_dist().getProvincia().getDepartamento();
            this.curProvincia = c1.getObj_dist().getProvincia();
            this.curDistrito = c1.getObj_dist();
            objDepartamentoDAO.close();
            objDistritoDAO.close();
            objProvinciaDAO.close();
            
        }
    }

    @NotifyChange({
        "entidades",
        "area",
        "cargos"
    })
    @Command
    public void changeTipo() throws SQLException {
        if (curTipo.x==1) {
            Entidad_dao obj_Entidad_dao = new Entidad_dao();
            entidades = obj_Entidad_dao.get_select();
            obj_Entidad_dao.close();
            curEntidad = null;
            rEntidad.setVisible(true);
            rArea.setVisible(false);
            rCargo.setVisible(false);
        } else if (curTipo.x==2) {
            area_dependencia_dao obj_area_dependencia_dao = new area_dependencia_dao();
            area = obj_area_dependencia_dao.get_select();
            obj_area_dependencia_dao.close();
            cargos = null;
            curArea = null;
            curCargo = null;
            rEntidad.setVisible(false);
            rArea.setVisible(true);
            rCargo.setVisible(true);
        }
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    public void save() {

        try {
            if (curTipo == null) {
                Messagebox.show("Seleccione Tipo");
                return;
            }
            if (curTipo.y.equals("Entidad")) {
                if (curEntidad == null) {
                    Messagebox.show("Seleccione Entidad");
                    return;
                }
            } else if (curTipo.y.equals("Cargo")) {
                if (curCargo == null) {
                    Messagebox.show("Seleccione Cargo");
                    return;
                }
            }
            if (selectedRepre.getNoms_reps() == null) {
                Messagebox.show("Ingrese Nombre(s) y Apellido(s)");
                return;
            }
            if (curDistrito == null) {
                Messagebox.show("Seleccione un Distrito");
                return;
            }
            if (selectedRepre.getCelu_reps() == null) {
                selectedRepre.setCelu_reps("");
            }
            if (selectedRepre.getDire_reps() == null) {
                selectedRepre.setDire_reps("");
            }
            if (selectedRepre.getDocs_reps() == null) {
                selectedRepre.setDocs_reps("");
            }
            if (selectedRepre.getEmai_reps() == null) {
                selectedRepre.setEmai_reps("");
            }
            if (selectedRepre.getTelf_reps() == null) {
                selectedRepre.setTelf_reps("");
            }
            representante_dao obj_representante_dao = new representante_dao();
            if ((getRecordMode().equals("NEW") && obj_representante_dao.verificar_nuevo(selectedRepre.getNoms_reps()))
                    || (getRecordMode().equals("EDIT") && obj_representante_dao.verificar_editar(selectedRepre.getCodi_reps(), selectedRepre.getNoms_reps()))) {
                representante representante = new representante();

                representante.setCodi_reps(selectedRepre.getCodi_reps());
                representante.setCelu_reps(selectedRepre.getCelu_reps());
                representante.setCodi_dist(curDistrito.getCodi_dist());
                representante.setDire_reps(selectedRepre.getDire_reps());
                representante.setDocs_reps(selectedRepre.getDocs_reps());
                representante.setEmai_reps(selectedRepre.getEmai_reps());
                representante.setEsta_reps("A");
                representante.setNoms_reps(selectedRepre.getNoms_reps());
                representante.setTelf_reps(selectedRepre.getTelf_reps());

                if (curTipo.y.equals("Entidad")) {
                    representante.setTipo_reps(1);
                    representante.setRela_reps(curEntidad.x);
                } else if (curTipo.y.equals("Cargo")) {
                    representante.setTipo_reps(2);
                    representante.setRela_reps(curCargo.x);
                }

                Map args = new HashMap();
                args.put("sRepre", representante);
                args.put("accion", this.recordMode);
                BindUtils.postGlobalCommand(null, null, "updateRepreList", args);
                win.detach();
            } else {
                Messagebox.show("El Nombre de Representante Ingresado ya Existe, eliga otro",
                        "Mantenimiento de Representantes", Messagebox.OK, Messagebox.ERROR);
                return;
            }
            obj_representante_dao.close();
        } catch (Exception ex) {
            Messagebox.show("El Proceso no fue completado por un error inesperado: " + ex.getMessage(),
                    "Mantenimiento de Representantes", Messagebox.OK, Messagebox.ERROR);
        }

    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"list_prov", "list_dist"})
    public void updateProvincia() {
        try {
            setList_prov(new Provincia_dao().listar_provincia(curDepartamento.getCodi_depa()));
            setList_dist(null);
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange({"cargos"})
    public void updateCargo() {
        try {
            cargos_dao obj_cargos = new cargos_dao();
            cargos = obj_cargos.get_select(curArea.x);
            obj_cargos.close();
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Command
    @NotifyChange("list_dist")
    public void updateDistrito() {
        try {
            setList_dist(new Distrito_dao().listar_distrito(curProvincia.getCodi_prov()));
        } catch (Exception ex) {
            Logger.getLogger(Ccontribuyente_man.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Command
    public void closeThis() {
        win.detach();
        BindUtils.postGlobalCommand(null, null, "updateRepreList", null);
    }

    public representante getSelectedRepre() {
        return selectedRepre;
    }

    public void setSelectedRepre(representante selectedRepre) {
        this.selectedRepre = selectedRepre;
    }

    public List<Departamento> getList_depa() {
        return list_depa;
    }

    public void setList_depa(List<Departamento> list_depa) {
        this.list_depa = list_depa;
    }

    public List<Provincia> getList_prov() {
        return list_prov;
    }

    public void setList_prov(List<Provincia> list_prov) {
        this.list_prov = list_prov;
    }

    public List<Distrito> getList_dist() {
        return list_dist;
    }

    public void setList_dist(List<Distrito> list_dist) {
        this.list_dist = list_dist;
    }

    public Departamento getCurDepartamento() {
        return curDepartamento;
    }

    public void setCurDepartamento(Departamento curDepartamento) {
        this.curDepartamento = curDepartamento;
    }

    public Provincia getCurProvincia() {
        return curProvincia;
    }

    public void setCurProvincia(Provincia curProvincia) {
        this.curProvincia = curProvincia;
    }

    public Distrito getCurDistrito() {
        return curDistrito;
    }

    public void setCurDistrito(Distrito curDistrito) {
        this.curDistrito = curDistrito;
    }

    public List<Pair<Integer, String>> getEntidades() {
        return entidades;
    }

    public void setEntidades(List<Pair<Integer, String>> entidades) {
        this.entidades = entidades;
    }

    public List<Pair<Integer, String>> getArea() {
        return area;
    }

    public void setArea(List<Pair<Integer, String>> area) {
        this.area = area;
    }

    public List<Pair<Integer, String>> getCargos() {
        return cargos;
    }

    public void setCargos(List<Pair<Integer, String>> cargos) {
        this.cargos = cargos;
    }

    public Pair<Integer, String> getCurEntidad() {
        return curEntidad;
    }

    public void setCurEntidad(Pair<Integer, String> curEntidad) {
        this.curEntidad = curEntidad;
    }

    public Pair<Integer, String> getCurArea() {
        return curArea;
    }

    public void setCurArea(Pair<Integer, String> curArea) {
        this.curArea = curArea;
    }

    public Pair<Integer, String> getCurCargo() {
        return curCargo;
    }

    public void setCurCargo(Pair<Integer, String> curCargo) {
        this.curCargo = curCargo;
    }

    public List<Pair<Integer, String>> getTipo() {
        tipo = new ArrayList<Pair<Integer, String>>();
        tipo.add(new Pair<Integer, String>(1, "Entidad"));
        tipo.add(new Pair<Integer, String>(2, "Cargo"));
        return tipo;
    }

    public void setTipo(List<Pair<Integer, String>> tipo) {
        this.tipo = tipo;
    }

    public Pair<Integer, String> getCurTipo() {
        return curTipo;
    }

    public void setCurTipo(Pair<Integer, String> curTipo) {
        this.curTipo = curTipo;
    }

}
