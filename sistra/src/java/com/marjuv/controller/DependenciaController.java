package com.marjuv.controller;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.dao.DependenciaDAO;
import com.marjuv.dao.usuario_dao;
import com.marjuv.entity.DependenciaTO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

public class DependenciaController {

    private BeanDependencias BeanDep;
    private List<BeanDependencias> ListaDependencia;

    Textbox txtNombre;

    @Init
    public void initSetup() {

    }

    public BeanDependencias getBeanDep() {
        return BeanDep;
    }

    public void setBeanDep(BeanDependencias BeanDep) {
        this.BeanDep = BeanDep;
    }

    

    void doOk() {
        Messagebox.show("ENTER key is pressed", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        txtNombre.focus();
    }
    
    @GlobalCommand
    @NotifyChange("allDependencias")
    public void updateUsuarioList(@BindingParam("objDependencia") DependenciaTO objDependenciaTO,
            @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            DependenciaDAO objDependenciaDAO = new DependenciaDAO();
            if (recordMode.equals("NEW")) {
                objDependenciaDAO.insertDependencia(objDependenciaTO);
                objDependenciaDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                     //Messagebox.show("Antes del Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                     objDependenciaDAO.updateDependencia(objDependenciaTO);
                     //Messagebox.show("Paso el Update", "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                    objDependenciaDAO.close();
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateUsuarioList: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }
               
            }
        }
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteDependencia() {
        String str = "¿Está seguro de que desea eliminar el usuario \""
                + BeanDep.getNomb_arde() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
            public void onEvent(Event e) {
                if (Messagebox.ON_YES.equals(e.getName())) {
                    try {
                        DependenciaDAO objDependenciaDAO = new DependenciaDAO();
                        objDependenciaDAO.deleteDependencia(BeanDep.getCodi_arde());
                        BindUtils.postNotifyChange(null, null,DependenciaController.this, "allDependencias");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }

    public List<BeanDependencias> getAllDependencias() {
        try {
            DependenciaDAO objDependencia = new DependenciaDAO();
            ListaDependencia = objDependencia.getDependencias();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        return ListaDependencia;
    }

    @Command
    public void editarDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objDependencia", this.BeanDep);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/win/editar_dependencia.zul", null, map);
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception 1: " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    @Command
    public void nuevaDependencia() {
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("objDependencia", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/win/editar_dependencia.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception 2: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }
    

}
