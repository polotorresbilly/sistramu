package com.marjuv.controller;

import com.marjuv.beans_view.BeanAsuntoResolucion;
import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.dao.AsuntoResolucionDAO;
import com.marjuv.dao.TipoResolucionDAO;
import com.marjuv.entity.AsuntoResolucionTO;
import com.marjuv.entity.TipoResolucionTO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

public class AsuntoResolucionControllerMain {

    private BeanAsuntoResolucion BeanAsuntoResolucion;
    private List<BeanAsuntoResolucion> ListAsuntoResolucion;

    
    
    @Init
    public void initSetup() {

    }

    
    
    @Command
    public void editAsuntoResolucion(){
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyAsuntoResolucion", this.getBeanAsuntoResolucion());
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_asunto_resolucion.zul", null, map);
            
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception en Edit(Asunto Resolucion): " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void newAsuntoResolucion(){
        try {
            final HashMap<String,Object> map = new HashMap<String, Object>();
            map.put("keyAsuntoResolucion", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/man/man_asunto_resolucion.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en New(Asunto Resolucion): " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @GlobalCommand
    @NotifyChange("listaAR")
    public void updateAR(@BindingParam("keyAsuntoResolucion") AsuntoResolucionTO objAsuntoResolucionTO, @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
            if (recordMode.equals("NEW")) {
                objAsuntoResolucionDAO.insert(objAsuntoResolucionTO);
                objAsuntoResolucionDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                    objAsuntoResolucionDAO.update(objAsuntoResolucionTO);
                    objAsuntoResolucionDAO.close();
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateAR: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }
               
            }
        }
    }
    
    public List<BeanAsuntoResolucion> getListaAR(){
        try {
            AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
            ListAsuntoResolucion = objAsuntoResolucionDAO.getListAsuntoResolucion();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en ListaAR " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        return ListAsuntoResolucion;
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteAsuntoResolucion(){
        String str = "¿Está seguro de que desea eliminar el asunto de Resolución \"" + BeanAsuntoResolucion.getDesc_asre() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {

            @Override
            public void onEvent(Event t) throws Exception {
                if(Messagebox.ON_YES.equals(t.getName())){
                    try {
                        AsuntoResolucionDAO objAsuntoResolucionDAO = new AsuntoResolucionDAO();
                        objAsuntoResolucionDAO.delete(BeanAsuntoResolucion.getCodi_asre());
                        BindUtils.postNotifyChange(null, null,AsuntoResolucionControllerMain.this, "listaAR");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        });
    }

    public BeanAsuntoResolucion getBeanAsuntoResolucion() {
        return BeanAsuntoResolucion;
    }

    public void setBeanAsuntoResolucion(BeanAsuntoResolucion BeanAsuntoResolucion) {
        this.BeanAsuntoResolucion = BeanAsuntoResolucion;
    }

    public List<BeanAsuntoResolucion> getListAsuntoResolucion() {
        return ListAsuntoResolucion;
    }

    public void setListAsuntoResolucion(List<BeanAsuntoResolucion> ListAsuntoResolucion) {
        this.ListAsuntoResolucion = ListAsuntoResolucion;
    }

}
