package com.marjuv.controller;

import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.dao.SubDependenciaDAO;
import com.marjuv.dao.TipoResolucionDAO;
import com.marjuv.entity.SubDependenciaTO;
import com.marjuv.entity.TipoResolucionTO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

public class TipoResolucionControllerMain {
    
    private BeanTipoResolucion BeanTipoResolucion;
    private List<BeanTipoResolucion> ListTipoResolucion;
    
    @Init
    public void initSetup() {

    }

    public BeanTipoResolucion getBeanTipoResolucion() {
        return BeanTipoResolucion;
    }

    public void setBeanTipoResolucion(BeanTipoResolucion BeanTipoResolucion) {
        this.BeanTipoResolucion = BeanTipoResolucion;
    }

    public List<BeanTipoResolucion> getListTipoResolucion() {
        return ListTipoResolucion;
    }

    public void setListTipoResolucion(List<BeanTipoResolucion> ListTipoResolucion) {
        this.ListTipoResolucion = ListTipoResolucion;
    }
    @Command
    public void editTipoResolucion(){
        try {
            final HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("keyTipoResolucion", this.BeanTipoResolucion);
            map.put("accion", "EDIT");
            Executions.createComponents("mantenimiento/man/man_tipo_resolucion.zul", null, map);
            
        } catch (Exception ex) {
            Messagebox.show("Ocurrio una Exception en Edit(Tipo Resolucion): " + ex.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @Command
    public void newTipoResolucion(){
        try {
            final HashMap<String,Object> map = new HashMap<String, Object>();
            map.put("keyTipoResolucion", null);
            map.put("accion", "NEW");
            Executions.createComponents("mantenimiento/man/man_tipo_resolucion.zul", null, map);
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en New(Tipo Resolucion): " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }
    
    @GlobalCommand
    @NotifyChange("listaTR")
    public void updateTR(@BindingParam("keyTipoResolucion") TipoResolucionTO objTipoResolucionTO, @BindingParam("accion") String recordMode) throws SQLException, Exception {
        if (recordMode != null) {
            TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
            if (recordMode.equals("NEW")) {
                objTipoResolucionDAO.insert(objTipoResolucionTO);
                objTipoResolucionDAO.close();
            } else if (recordMode.equals("EDIT")) {
                try {
                    objTipoResolucionDAO.update(objTipoResolucionTO);
                    objTipoResolucionDAO.close();
                } catch (Exception e) {
                    Messagebox.show("Ocurrio una Exception updateTR: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
                }
               
            }
        }
    }
    
    public List<BeanTipoResolucion> getListaTR(){
        try {
            TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
            ListTipoResolucion = objTipoResolucionDAO.getListTipoResolucion();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception en ListaTR " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        return ListTipoResolucion;
    }
    
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Command
    public void deleteTipoResolucion(){
        String str = "¿Está seguro de que desea eliminar el tipo de Resolución \"" + BeanTipoResolucion.getDesc_tire() + "\" ?.";
        Messagebox.show(str, "Confirm", Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {

            @Override
            public void onEvent(Event t) throws Exception {
                if(Messagebox.ON_YES.equals(t.getName())){
                    try {
                        TipoResolucionDAO objTipoResolucionDAO = new TipoResolucionDAO();
                        objTipoResolucionDAO.delete(BeanTipoResolucion.getCodi_tire());
                        BindUtils.postNotifyChange(null, null,TipoResolucionControllerMain.this, "listaTR");
                    } catch (Exception ex) {
                        Logger.getLogger(DependenciaController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        });
    }
    
    
    
}
