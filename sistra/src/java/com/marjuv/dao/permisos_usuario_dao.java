
package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.permiso;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

public class permisos_usuario_dao {
    
    protected DataTransaction dt;
    protected Connection cn;

    public permisos_usuario_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public ArrayList<permiso> get_permisos_all(int codi_rous) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from permiso where codi_rous!=?");
        ps.setInt(1, codi_rous);
        ResultSet rs = ps.executeQuery();
        ArrayList<permiso> lista = new ArrayList<permiso>();
        permiso permiso;
        while (rs.next()) {
            permiso = new permiso();
            permiso.setCodi_perm(rs.getInt(1));
            permiso.setPage_perm(rs.getString(2));
            permiso.setUrlx_perm(rs.getString(3));
            permiso.setCodi_rous(rs.getInt(4));
            permiso.setTitu_perm(rs.getString(5));
            lista.add(permiso);            
        }
        return lista;
    }
    
    public ArrayList<permiso> get_permisos_rol(int codi_rous) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from permiso where codi_rous=?");
        ps.setInt(1, codi_rous);
        ResultSet rs = ps.executeQuery();
        ArrayList<permiso> lista = new ArrayList<permiso>();
        permiso permiso;
        while (rs.next()) {
            permiso = new permiso();
            permiso.setCodi_perm(rs.getInt(1));
            permiso.setPage_perm(rs.getString(2));
            permiso.setUrlx_perm(rs.getString(3));
            permiso.setCodi_rous(rs.getInt(4));
            permiso.setTitu_perm(rs.getString(5));
            lista.add(permiso);    
        }
        return lista;
    }
    
    public ArrayList<permiso> get_permisos_usuario(int codi_usua) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select p.* from permisos_usuario pu, permiso p, usuario u where pu.codi_usua=? AND pu.codi_perm=p.codi_perm AND u.codi_usua=pu.codi_usua");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        ArrayList<permiso> lista = new ArrayList<permiso>();
        permiso permiso;
        while (rs.next()) {
            permiso = new permiso();
            permiso.setCodi_perm(rs.getInt(1));
            permiso.setPage_perm(rs.getString(2));
            permiso.setUrlx_perm(rs.getString(3));
            permiso.setCodi_rous(rs.getInt(4));
            permiso.setTitu_perm(rs.getString(5));
            lista.add(permiso);    
        }
        return lista;
    }
    
    public ArrayList<permiso> get_permisos_usuario_inverso(int codi_usua) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(""
            + "select p.* from permiso as p LEFT JOIN (select x.* from permisos_usuario pu, usuario u, permiso x where pu.codi_usua=u.codi_usua and x.codi_perm=pu.codi_perm and pu.codi_usua=?) as r ON p.codi_perm=r.codi_perm WHERE r.codi_perm is null");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        ArrayList<permiso> lista = new ArrayList<permiso>();
        permiso permiso;
        while (rs.next()) {
            permiso = new permiso();
            permiso.setCodi_perm(rs.getInt(1));
            permiso.setPage_perm(rs.getString(2));
            permiso.setUrlx_perm(rs.getString(3));
            permiso.setCodi_rous(rs.getInt(4));
            permiso.setTitu_perm(rs.getString(5));
            lista.add(permiso);    
        }
        return lista;
    }
    
    public void close() throws SQLException {
        cn.close();
    }

    public List<permiso> format_list_permisos(List<Listitem> items) {
        ArrayList<permiso> lista = new ArrayList<permiso>();
        for (int i = 0; i < items.size(); i++) {
            lista.add((permiso) items.get(i).getValue());
        }
        return lista;
    }
    
}
