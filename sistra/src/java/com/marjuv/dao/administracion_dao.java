package com.marjuv.dao;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.administracion;
import com.marjuv.entity.usuario;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.zul.Messagebox;

public class administracion_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public administracion_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(administracion administracion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO administracion(year_admi, noen_admi, dien_admi, teen_admi, enen_admi, noal_admi, "
                + "noaa_admi, noge_admi, nose_admi, vaui_admi,noci_admi)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?)");
        ps.setInt(1, administracion.getYear_admi());
        ps.setString(2, administracion.getNoen_admi());
        ps.setString(3, administracion.getDien_admi());
        ps.setString(4, administracion.getTeen_admi());
        ps.setString(5, administracion.getEnen_admi());
        ps.setString(6, administracion.getNoal_admi());
        ps.setString(7, administracion.getNoaa_admi());
        ps.setString(8, administracion.getNoge_admi());
        ps.setString(9, administracion.getNose_admi());
        ps.setString(10, administracion.getVaui_admi());
        ps.setString(11, administracion.getNoci_admi());
//        File folder = new File(administracion.getUrl_backup_admin());
//        if(folder.exists()){
//        ps.setString(12, administracion.getUrl_backup_admin());
//        }else{
//        ps.setString(12, administracion.getUrl_backup_admin());
//        Messagebox.show("Disco "+administracion.getUrl_backup_admin()+" no Existe!!!","Resultado", Messagebox.OK, Messagebox.ERROR);
//        }
     
        ps.execute();

    }
    
    public void insert_admin(administracion administracion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO administracion(year_admi, noen_admi, dien_admi, teen_admi, enen_admi, noal_admi, "
                + "noaa_admi, noge_admi, nose_admi, vaui_admi,noci_admi,url_file_admin,url_logo_admin)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?)");
        ps.setInt(1, administracion.getYear_admi());
        ps.setString(2, administracion.getNoen_admi());
        ps.setString(3, administracion.getDien_admi());
        ps.setString(4, administracion.getTeen_admi());
        ps.setString(5, administracion.getEnen_admi());
        ps.setString(6, administracion.getNoal_admi());
        ps.setString(7, administracion.getNoaa_admi());
        ps.setString(8, administracion.getNoge_admi());
        ps.setString(9, administracion.getNose_admi());
        ps.setString(10, administracion.getVaui_admi());
        ps.setString(11, administracion.getNoci_admi());
        ps.setString(12, administracion.getUrl_file_admin());
        ps.setString(13, administracion.getUrl_logo_admin());
        ps.execute();

    }

    public void update(administracion administracion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE administracion SET year_admi=?, noen_admi=?, dien_admi=?, teen_admi=?, enen_admi=?, noal_admi=?, "
                + "noaa_admi=?, noge_admi=?, nose_admi=?, vaui_admi=?, noci_admi=? WHERE codi_admi=?");
        ps.setInt(1, administracion.getYear_admi());
        ps.setString(2, administracion.getNoen_admi());
        ps.setString(3, administracion.getDien_admi());
        ps.setString(4, administracion.getTeen_admi());
        ps.setString(5, administracion.getEnen_admi());
        ps.setString(6, administracion.getNoal_admi());
        ps.setString(7, administracion.getNoaa_admi());
        ps.setString(8, administracion.getNoge_admi());
        ps.setString(9, administracion.getNose_admi());
        ps.setString(10, administracion.getVaui_admi());
        ps.setString(11, administracion.getNoci_admi());
//        File folder = new File(administracion.getUrl_backup_admin());
//        if(folder.exists()){
//        ps.setString(12, administracion.getUrl_backup_admin());
//        }else{
//        ps.setString(12, administracion.getUrl_backup_admin());
//        Messagebox.show("Disco "+administracion.getUrl_backup_admin()+" no Existe!!!","Resultado", Messagebox.OK, Messagebox.ERROR);
//        }
        ps.setInt(12, administracion.getCodi_admi());
        ps.execute();
    }
    
    public void update_admin(administracion administracion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE administracion SET year_admi=?, noen_admi=?, dien_admi=?, teen_admi=?, enen_admi=?, noal_admi=?, "
                + "noaa_admi=?, noge_admi=?, nose_admi=?, vaui_admi=?, noci_admi=?, url_file_admin=?, url_logo_admin=? WHERE codi_admi=?");
        ps.setInt(1, administracion.getYear_admi());
        ps.setString(2, administracion.getNoen_admi());
        ps.setString(3, administracion.getDien_admi());
        ps.setString(4, administracion.getTeen_admi());
        ps.setString(5, administracion.getEnen_admi());
        ps.setString(6, administracion.getNoal_admi());
        ps.setString(7, administracion.getNoaa_admi());
        ps.setString(8, administracion.getNoge_admi());
        ps.setString(9, administracion.getNose_admi());
        ps.setString(10, administracion.getVaui_admi());
        ps.setString(11, administracion.getNoci_admi());
        ps.setString(12, administracion.getUrl_file_admin());
        ps.setString(13, administracion.getUrl_logo_admin());
        ps.setInt(14, administracion.getCodi_admi());
        ps.execute();
    }

    public boolean verificar_admin(int year) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from administracion where year_admi=?");
        ps.setInt(1, year);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public administracion get_administracion(int year) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from administracion where year_admi=?");
        ps.setInt(1, year);
        ResultSet rs = ps.executeQuery();
        rs.next();
        administracion obj_administracion = new administracion();
        obj_administracion.setCodi_admi(rs.getInt(1));
        obj_administracion.setYear_admi(rs.getInt(2));
        obj_administracion.setNoen_admi(rs.getString(3));
        obj_administracion.setDien_admi(rs.getString(4));
        obj_administracion.setTeen_admi(rs.getString(5));
        obj_administracion.setEnen_admi(rs.getString(6));
        obj_administracion.setNoal_admi(rs.getString(7));
        obj_administracion.setNoaa_admi(rs.getString(8));
        obj_administracion.setNoge_admi(rs.getString(9));
        obj_administracion.setNose_admi(rs.getString(10));
        obj_administracion.setVaui_admi(rs.getString(11));
        obj_administracion.setNoci_admi(rs.getString(12));
        obj_administracion.setUrl_file_admin(rs.getString("url_file_admin"));
        obj_administracion.setUrl_logo_admin(rs.getString("url_logo_admin"));
        obj_administracion.setUrl_backup_admin(rs.getString("url_backup_admin"));
        return obj_administracion;
    }
     public administracion administracion_backup() throws Exception {
        PreparedStatement ps = cn.prepareStatement("select url_backup_admin,nom_backup from administracion");
        ResultSet rs = ps.executeQuery();
        rs.next();
        administracion obj_administracion = new administracion();
        obj_administracion.setUrl_backup_admin(rs.getString("url_backup_admin"));
        obj_administracion.setNombreBackup(rs.getString("nom_backup"));
        return obj_administracion;
    }
     
     public void updateBackup(administracion administracion) throws Exception {
          
        PreparedStatement ps = cn.prepareStatement("UPDATE administracion SET nom_backup=? WHERE codi_admi=2");
         ps.setString(1, administracion.getNombreBackup());
        ps.execute();
    }
    public void close() throws SQLException {
        cn.close();
    }
}
