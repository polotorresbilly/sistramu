/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Dependencia;
import com.marjuv.entity.Firma_Resolucion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Firma_Resolucion_dao {
    protected DataTransaction dt;
    protected Connection cn;
    public Firma_Resolucion_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public void insert_firma(Firma_Resolucion firma) throws SQLException{
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("INSERT INTO public.firma_resolucion(codi_arde, carg_fire, pofi_fire, obsv_fire, arch_fire, esta_fire) VALUES (?, ?, ?, ?, ?, ?)");
            ps.setInt(1, firma.getArea_depe().getCodi_arde());
            ps.setString(2, firma.getCarg_fire().toUpperCase());
            ps.setString(3, firma.getPofi_fire().toUpperCase());
            ps.setString(4, firma.getObsv_fire().toUpperCase());
            ps.setString(5, firma.getArch_fire().toUpperCase());
            ps.setBoolean(6, firma.getEsta_fire());
            
            ps.execute();
        } catch (Exception e) {
            Messagebox.show(e.getMessage());
        } finally{
            cn.close();
        }
    }
    
    public void update_firma(Firma_Resolucion firma) throws SQLException{
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("UPDATE public.firma_resolucion  SET codi_arde=?, carg_fire=?, pofi_fire=?, obsv_fire=?, arch_fire=?, esta_fire=? WHERE codi_fire=?;");
            ps.setInt(1, firma.getArea_depe().getCodi_arde());
            ps.setString(2, firma.getCarg_fire().toUpperCase());
            ps.setString(3, firma.getPofi_fire().toUpperCase());
            ps.setString(4, firma.getObsv_fire().toUpperCase());
            ps.setString(5, firma.getArch_fire().toUpperCase());
            ps.setBoolean(6, firma.getEsta_fire());
            ps.setInt(7, firma.getCodi_fire());
            ps.execute();
        } catch (Exception e) {
            Logger.getLogger(Firma_Resolucion_dao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            cn.close();
        }
    }
    public void delete_firma(int codigo) throws SQLException{
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("UPDATE public.firma_resolucion SET  esta_fire=? WHERE codi_fire=?;");
            ps.setBoolean(1, false);
            ps.setInt(2, codigo);
            ps.execute();
        } catch (Exception e) {
            Logger.getLogger(Firma_Resolucion_dao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            cn.close();
        }
    }
    
    public void reborn_firma(int codigo) throws SQLException{
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("UPDATE public.firma_resolucion SET  esta_fire=? WHERE codi_fire=?;");
            ps.setBoolean(1, true);
            ps.setInt(2, codigo);
            ps.execute();
        } catch (Exception e) {
            Logger.getLogger(Firma_Resolucion_dao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            cn.close();
        }
    }
    
    public String get_archivo(int codigo) throws SQLException{
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM firma_resolucion WHERE codi_fire=?;");
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString("arch_fire");
        } catch (Exception e) {
            Logger.getLogger(Firma_Resolucion_dao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            cn.close();
        }
        return "";
    }
    
    public List<Firma_Resolucion> get_all_firma_resolucion() throws Exception{
        List<Firma_Resolucion> lista_firmas = new ArrayList<Firma_Resolucion>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT codi_fire, carg_fire, pofi_fire, obsv_fire, arch_fire,esta_fire,AR.* FROM public.firma_resolucion FI INNER JOIN area_dependencia AR ON FI.codi_arde=AR.codi_arde order by codi_fire;");
            Firma_Resolucion obj;
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                obj = new Firma_Resolucion();
                obj.setCodi_fire(rs.getInt("codi_fire"));
                obj.setCarg_fire(rs.getString("carg_fire"));
                obj.setPofi_fire(rs.getString("pofi_fire"));
                obj.setObsv_fire(rs.getString("obsv_fire"));
                obj.setArch_fire(rs.getString("arch_fire"));
                obj.setEsta_fire(rs.getBoolean("esta_fire"));
                Dependencia dep = new Dependencia();
                dep.setCodi_arde(rs.getInt("codi_arde"));
                dep.setNomb_arde(rs.getString("nomb_arde"));
                dep.setEsta_arde(rs.getString("esta_arde"));
                dep.setAbre_arde(rs.getString("abre_arde"));
                dep.setJera_arde(rs.getInt("jera_arde"));
                dep.setCodi_enti(rs.getInt("codi_enti"));
                obj.setArea_depe(dep);
                lista_firmas.add(obj);
            }
        } catch (Exception e) {
            Logger.getLogger(Firma_Resolucion_dao.class.getName()).log(Level.SEVERE, null, e);
        } finally{
            cn.close();
        }
        return lista_firmas;
    }
}
