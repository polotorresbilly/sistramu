package com.marjuv.dao;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Dependencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

public class Dependencia_dao {

    protected DataTransaction dt;
    protected Connection cn;

    @WireVariable
    private Session _sess;

    public Dependencia_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public int validate_nameDependencia(String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador from area_dependencia WHERE nomb_arde=? AND esta_arde='A'");
        ps.setString(1, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 1) {
            return rs.getInt(1);
        } else {
            return -1;
        }
    }

    public int validate_abrevDependencia(String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador from area_dependencia WHERE abre_arde=? AND esta_arde='A'");
        ps.setString(1, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 1) {
            return rs.getInt(1);
        } else {
            return -1;
        }
    }

    public void insertDependencia(Dependencia objDependenciaTO) throws Exception {
        PreparedStatement ps
                = cn.prepareStatement("INSERT INTO area_dependencia(nomb_arde,abre_arde,"
                        + "jera_arde,codi_enti,esta_arde,nive_arde) VALUES(?,?,?,?,?,?)");
        ps.setString(1, objDependenciaTO.getNomb_arde().toUpperCase());
        ps.setString(2, objDependenciaTO.getAbre_arde().toUpperCase());
        ps.setInt(3, objDependenciaTO.getJera_arde());
        ps.setInt(4, objDependenciaTO.getCodi_enti());
        ps.setString(5, "A");
        ps.setInt(6, objDependenciaTO.getNive_arde());
        ps.execute();
    }

    public void updateDependencia(Dependencia objDependenciaTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE area_dependencia SET nomb_arde=?, abre_arde=? ,jera_arde=?, codi_enti=?, nive_arde=? WHERE codi_arde=?");
            ps.setString(1, objDependenciaTO.getNomb_arde().toUpperCase());
            ps.setString(2, objDependenciaTO.getAbre_arde().toUpperCase());
            ps.setInt(3, objDependenciaTO.getJera_arde());
            ps.setInt(4, objDependenciaTO.getCodi_enti());
            ps.setInt(5, objDependenciaTO.getNive_arde());
            ps.setInt(6, objDependenciaTO.getCodi_arde());
            ps.execute();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception Dao: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }

    public BeanDependencias getDependencia(int codi_arde) throws Exception {
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT ARE.*, ENTI.nomb_enti,JERA.nomb_arde as nomb_jera FROM area_dependencia ARE LEFT JOIN area_dependencia JERA ON ARE.jera_arde=JERA.codi_arde LEFT JOIN entidad ENTI ON ARE.codi_enti = ENTI.codi_enti WHERE ARE.esta_arde = 'A' and ARE.codi_arde=? ORDER BY 1");
        ps.setInt(1, codi_arde);
        BeanDependencias BeanD = null;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNive_arde(rs.getInt(7));
            BeanD.setNomb_enti(rs.getString(8));
            BeanD.setNomb_jera(rs.getString(9));
            lista.add(BeanD);
        }
        return BeanD;
    }

    public List<BeanDependencias> getDependenciasJera(int codi_arde) throws Exception {
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT ARE.*, ENTI.nomb_enti,JERA.nomb_arde as nomb_jera FROM area_dependencia ARE LEFT JOIN area_dependencia JERA ON ARE.jera_arde=JERA.codi_arde LEFT JOIN entidad ENTI ON ARE.codi_enti = ENTI.codi_enti WHERE ARE.esta_arde = 'A' and ARE.jera_arde=? ORDER BY 1");
        ps.setInt(1, codi_arde);
        BeanDependencias BeanD;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNive_arde(rs.getInt(7));
            BeanD.setNomb_enti(rs.getString(8));
            BeanD.setNomb_jera(rs.getString(9));
            lista.add(BeanD);
        }
        return lista;
    }

    public List<BeanDependencias> getDependenciasJera2() throws Exception {
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia order by 2");

        BeanDependencias BeanD;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNive_arde(rs.getInt(7));
            BeanD.setNomb_enti(rs.getString(8));
            BeanD.setNomb_jera(rs.getString(9));
            lista.add(BeanD);
        }
        return lista;
    }

    public void deleteDependencia(int codi_arde) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE area_dependencia SET esta_arde='D' WHERE codi_arde=?");
        ps.setInt(1, codi_arde);
        ps.execute();
    }

    public List<BeanDependencias> get_dependencias_reporte_estado_areas() throws Exception {
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde='A' ORDER BY 2");

        BeanDependencias BeanD;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));

            lista.add(BeanD);
        }
        return lista;
    }

    public List<BeanDependencias> getDependencias() throws Exception {
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT ARE.*, ENTI.nomb_enti,JERA.nomb_arde as nomb_jera FROM area_dependencia ARE LEFT JOIN area_dependencia JERA ON ARE.jera_arde=JERA.codi_arde LEFT JOIN entidad ENTI ON ARE.codi_enti = ENTI.codi_enti WHERE ARE.esta_arde = 'A' ORDER BY 2");

        BeanDependencias BeanD;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNive_arde(rs.getInt(7));
            BeanD.setNomb_enti(rs.getString(8));
            BeanD.setNomb_jera(rs.getString(9));
            lista.add(BeanD);
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }

    public ArrayList<Pair<Integer, String>> getEntidad() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM entidad WHERE esta_enti = 'A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public List<Pair<Integer, String>> get_select(Integer x) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where esta_arde='A' and codi_enti=? order by 2");
        ps.setInt(1, x);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    

    public String get_nomb(int codi_carg) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from cargos where codi_carg=?");
        ps.setInt(1, codi_carg);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int codi_arde = rs.getInt(4);
        ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
        ps.setInt(1, codi_arde);
        rs = ps.executeQuery();
        rs.next();
        return rs.getString(2);
    }

    public String get_nomb2(int codi_arde) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
        ps.setInt(1, codi_arde);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("nomb_arde");
    }

    public ArrayList<Pair<Integer, String>> getDependencia() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde = 'A' order by 2");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public boolean validate_nameDependencia_nuevo(String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE nomb_arde=? AND esta_arde='A'");
        ps.setString(1, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validate_nameDependencia_editar(int codigo, String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE codi_arde !=? AND nomb_arde=? AND esta_arde='A'");
        ps.setInt(1, codigo);
        ps.setString(2, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validate_abrevDependencia_nuevo(String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE abre_arde=? AND esta_arde='A'");
        ps.setString(1, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validate_abrevDependencia_editar(int codigo, String name) throws Exception {

        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE codi_arde !=? AND abre_arde=? AND esta_arde='A'");
        ps.setInt(1, codigo);
        ps.setString(2, name.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String get_Dependencia_Destinatario(String nomb) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE nomb_arde=? AND esta_arde='A' order by 2");
        ps.setString(1, nomb.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();

        return rs.getString(4);
    }

    public List<Pair<Integer, String>> get_select2() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where esta_arde='A' order by 2");

        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
}
