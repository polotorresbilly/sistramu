/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.dao;

import com.marjuv.beans_view.BeanTipoExpediente;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Provincia;
import com.marjuv.entity.Tipo_Expediente;
import com.marjuv.entity.Tipo_Expediente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.Messagebox;

/**
 *
 * @author BILLY
 */
public class Tipo_Expediente_dao {
    protected DataTransaction dt;
    protected Connection cn;

    public Tipo_Expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
     public void close() throws SQLException {
        cn.close();
    }
    public List<Tipo_Expediente> get_tipo_expedientes()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT codi_tiex, nomb_tiex, esta_tiex FROM public.tipo_expediente WHERE esta_tiex='A';");
        List<Tipo_Expediente> lista = new ArrayList<Tipo_Expediente>();
        Tipo_Expediente obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            obj = new Tipo_Expediente(
               rs.getInt("codi_tiex"),
               rs.getString("nomb_tiex"),
            rs.getString("esta_tiex"));
            lista.add(obj);
        }
        return lista;
    }
    
     public void insert(Tipo_Expediente objTipo_Expediente) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO tipo_expediente(nomb_tiex,esta_tiex) VALUES (?,?)");
            ps.setString(1, objTipo_Expediente.getNomb_tiex().toUpperCase());
            ps.setString(2, "A");
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Insert(TipoExpediente)" + e.getMessage(), "Insert Tipo Expediente", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void update(Tipo_Expediente objTipo_Expediente) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE tipo_expediente SET nomb_tiex=? WHERE codi_tiex=?");
            ps.setString(1, objTipo_Expediente.getNomb_tiex().toUpperCase());
            ps.setInt(2, objTipo_Expediente.getCodi_tiex());
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Update(TipoExpediente): " + e.getMessage(), "Update Tipo Expediente", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void delete(int codigo) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE tipo_expediente SET esta_tiex = 'D' WHERE codi_tiex = ?");
            ps.setInt(1,codigo);
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Delete(TipoExpediente)" + e.getMessage(), "Delete Tipo Expediente", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public List<BeanTipoExpediente> getListTipoExpediente() throws Exception {
        List<BeanTipoExpediente> Lista = new ArrayList<BeanTipoExpediente>();
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM tipo_expediente WHERE esta_tiex = 'A' ORDER BY 1");
        ResultSet rs = ps.executeQuery();
        BeanTipoExpediente objBeanTipoExpediente;
        while (rs.next()) {
            objBeanTipoExpediente = new BeanTipoExpediente();
            objBeanTipoExpediente.setCodi_tiex(rs.getInt(1));
            objBeanTipoExpediente.setNomb_tiex(rs.getString(2));
            objBeanTipoExpediente.setEsta_tiex(rs.getString(3));
            Lista.add(objBeanTipoExpediente);
        }
        return Lista;

    }
    
    public boolean v_nomb_tiex_new(String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM tipo_expediente WHERE nomb_tiex =? AND esta_tiex='A'");
        ps.setString(1, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean v_nomb_tiex_edit(int codigo,String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM tipo_expediente WHERE codi_tiex !=? AND nomb_tiex =? AND esta_tiex='A'");
        ps.setInt(1, codigo);
        ps.setString(2, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
}
