package com.marjuv.dao;

import com.marjuv.beans_view.BeanBusquedaExpediente;
import com.marjuv.beans_view.BeanExpediente;
import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.beans_view.BeanUbicacionExpediente;
import com.marjuv.beans_view.BeanVerDocumentos;
import com.marjuv.beans_view.Bean_Reporte_Estados_Expediente;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Entidad_Ubicacion_Expediente;
import com.marjuv.entity.EnviaHistorial;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.ExpedienteMovimientos;

import com.marjuv.entity.ExpedienteReportes;
import com.marjuv.entity.expediente;
import com.marjuv.entity.historial_expediente;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import org.zkoss.zul.Messagebox;

public class Expediente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public Expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void close() throws SQLException {
        cn.close();
    }

    public boolean validar_modificacion(String nume_expe) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select COUNT(*) as cont from historial_expediente where nume_expe=? and esta_hist > 1");
        ps.setString(1, nume_expe);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt("cont") > 0) {
            return false;
        } else {
            return true;
        }
    }

    public String get_exp_adj(int codi_reso) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from expedientes_adjuntos where codi_reso=?");
        ps.setInt(1, codi_reso);
        ResultSet rs = ps.executeQuery();
        rs.next();
        String text = rs.getString("nume_exad");
        while (rs.next()) {
            text = text + ", " + rs.getString("nume_exad");
        }
        return text;
    }

    public String nuevo_codi_expe() throws SQLException {
        Calendar fecha = new GregorianCalendar();
        int año = fecha.get(Calendar.YEAR);
        PreparedStatement ps = cn.prepareStatement("select count(*) as c from expediente where EXTRACT(YEAR FROM fech_expe)='" + año + "'");
        ResultSet rs = ps.executeQuery();
        rs.next();
        int count = rs.getInt(1) + 1;
        String code = "000000" + count;
        String code2 = code.substring(code.length() - 6);
        String codi_expe = code2 + "-" + año;
        return codi_expe;
    }

    public void insert(expediente expediente, int dias) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO expediente(nume_expe, codi_cont, codi_enti, codi_carg, fech_expe, refe_expe, nufo_expe, "
                + "nudo_expe, codi_tiex, codi_proc, codi_enex, codi_usua, espe_expe, codi_esex, codi_asex, codi_reps, valx_expe,reci_expe)"
                + "VALUES(?,?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, expediente.getNume_expe());
        ps.setInt(2, expediente.getCodi_cont());
        ps.setInt(3, expediente.getCodi_enti());
        ps.setInt(4, expediente.getCodi_carg());
        ps.setString(5, expediente.getRefe_expe());
        ps.setInt(6, expediente.getNufo_expe());
        ps.setString(7, expediente.getNudo_expe());
        ps.setInt(8, expediente.getCodi_tiex());
        ps.setInt(9, expediente.getCodi_proc());
        ps.setInt(10, expediente.getCodi_enex());
        ps.setInt(11, expediente.getCodi_usua());
        ps.setString(12, expediente.getEspe_expe());
        ps.setInt(13, expediente.getCodi_esex());
        ps.setInt(14, expediente.getCodi_asex());
        ps.setInt(15, expediente.getCodi_reps());
        ps.setString(16, expediente.getValx_expe());
        ps.setString(17, expediente.getReci_expe());
        ps.execute();

        ps = cn.prepareStatement("SELECT * FROM expediente WHERE nume_expe=?");
        ps.setString(1, expediente.getNume_expe());
        ResultSet rs = ps.executeQuery();
        rs.next();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(rs.getDate("fech_expe"));
        int contador = 0;
        if (dias == 0) {
            contador = 30;
        } else {
            if (dias > 0) {
                contador = expediente.getDias_proc();
            }
        }

        while (contador > 0) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                contador--;
            }
        }

        ps = cn.prepareStatement("UPDATE expediente SET feca_expe=? WHERE nume_expe=?");
        ps.setDate(1, new java.sql.Date(calendar.getTime().getTime()));
        ps.setString(2, expediente.getNume_expe());
        ps.execute();

        for (int i = 0; i < expediente.getHistorial_expediente().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO historial_expediente("
                    + "corr_hist, nude_hist, nume_expe, nufo_hist, fell_hist, usen_hist, esta_hist, "
                    + "caen_hist, care_hist, core_hist, enre_hist, enti_hist, reti_hist, copi_hist, obsv_hist,condicion) "
                    + "VALUES(?,?,?,?,now(),?,?,?,?,?,?,?,?, ?,?,?)");
            ps.setInt(1, expediente.getHistorial_expediente().get(i).getCorr_hist());
            ps.setInt(2, expediente.getHistorial_expediente().get(i).getNude_hist());
            ps.setString(3, expediente.getHistorial_expediente().get(i).getNume_expe());
            ps.setInt(4, expediente.getHistorial_expediente().get(i).getNufo_hist());
            ps.setInt(5, expediente.getHistorial_expediente().get(i).getUsen_hist());
            ps.setInt(6, expediente.getHistorial_expediente().get(i).getEsta_hist());
            ps.setInt(7, expediente.getHistorial_expediente().get(i).getCaen_hist());
            ps.setInt(8, expediente.getHistorial_expediente().get(i).getCare_hist());
            ps.setInt(9, expediente.getHistorial_expediente().get(i).getCore_hist());
            ps.setInt(10, expediente.getHistorial_expediente().get(i).getEnre_hist());
            ps.setInt(11, expediente.getHistorial_expediente().get(i).getEnti_hist());
            ps.setInt(12, expediente.getHistorial_expediente().get(i).getReti_hist());
            ps.setBoolean(13, expediente.getHistorial_expediente().get(i).isCopi_hist());
            ps.setString(14, expediente.getHistorial_expediente().get(i).getObsv_hist());
            ps.setInt(15, 1);
            ps.execute();
        }

//        for (int i = 0; i < expediente.getRequisitos().size(); i++) {
//            ps = cn.prepareStatement("INSERT INTO requisitos_expediente("
//                    + "codi_repr, nume_expe) VALUES(?,?)");
//            ps.setInt(1, expediente.getRequisitos().get(i).getCodi_repr());
//            ps.setString(2, expediente.getNume_expe());
//            ps.execute();
//        }
    }

    public void insert2(expediente expediente, int tipo, int dias) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO expediente(nume_expe, codi_cont, codi_enti, codi_carg, fech_expe, refe_expe, nufo_expe, "
                + "nudo_expe, codi_tiex, codi_proc, codi_enex, codi_usua, espe_expe, codi_esex, codi_asex, codi_reps, valx_expe,reci_expe)"
                + "VALUES(?,?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, expediente.getNume_expe());
        ps.setInt(2, expediente.getCodi_cont());
        ps.setInt(3, expediente.getCodi_enti());
        ps.setInt(4, expediente.getCodi_carg());
        ps.setString(5, expediente.getRefe_expe());
        ps.setInt(6, expediente.getNufo_expe());
        ps.setString(7, expediente.getNudo_expe());
        ps.setInt(8, expediente.getCodi_tiex());
        ps.setInt(9, expediente.getCodi_proc());
        ps.setInt(10, expediente.getCodi_enex());
        ps.setInt(11, expediente.getCodi_usua());
        ps.setString(12, expediente.getEspe_expe());
        ps.setInt(13, expediente.getCodi_esex());
        ps.setInt(14, expediente.getCodi_asex());
        ps.setInt(15, expediente.getCodi_reps());
        ps.setString(16, expediente.getValx_expe());
        ps.setString(17, expediente.getReci_expe());
        ps.execute();

        ps = cn.prepareStatement("SELECT * FROM expediente WHERE nume_expe=?");
        ps.setString(1, expediente.getNume_expe());
        ResultSet rs = ps.executeQuery();
        rs.next();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(rs.getDate("fech_expe"));
        int contador;
        if (tipo == 1) {
            contador = expediente.getDias_proc();
            while (contador > 0) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    contador--;
                }
            }

            ps = cn.prepareStatement("UPDATE expediente SET feca_expe=? WHERE nume_expe=?");
            ps.setDate(1, new java.sql.Date(calendar.getTime().getTime()));
            ps.setString(2, expediente.getNume_expe());
            ps.execute();

            for (int i = 0; i < expediente.getHistorial_expediente().size(); i++) {
                ps = cn.prepareStatement("INSERT INTO historial_expediente("
                        + "corr_hist, nude_hist, nume_expe, nufo_hist, fell_hist, usen_hist, esta_hist, "
                        + "caen_hist, care_hist, core_hist, enre_hist, enti_hist, reti_hist, copi_hist, obsv_hist,condicion) "
                        + "VALUES(?,?,?,?,now(),?,?,?,?,?,?,?,?, ?,?,?)");
                ps.setInt(1, expediente.getHistorial_expediente().get(i).getCorr_hist());
                ps.setInt(2, expediente.getHistorial_expediente().get(i).getNude_hist());
                ps.setString(3, expediente.getHistorial_expediente().get(i).getNume_expe());
                ps.setInt(4, expediente.getHistorial_expediente().get(i).getNufo_hist());
                ps.setInt(5, expediente.getHistorial_expediente().get(i).getUsen_hist());
                ps.setInt(6, expediente.getHistorial_expediente().get(i).getEsta_hist());
                ps.setInt(7, expediente.getHistorial_expediente().get(i).getCaen_hist());
                ps.setInt(8, expediente.getHistorial_expediente().get(i).getCare_hist());
                ps.setInt(9, expediente.getHistorial_expediente().get(i).getCore_hist());
                ps.setInt(10, expediente.getHistorial_expediente().get(i).getEnre_hist());
                ps.setInt(11, expediente.getHistorial_expediente().get(i).getEnti_hist());
                ps.setInt(12, expediente.getHistorial_expediente().get(i).getReti_hist());
                ps.setBoolean(13, expediente.getHistorial_expediente().get(i).isCopi_hist());
                ps.setString(14, expediente.getHistorial_expediente().get(i).getObsv_hist());
                ps.setInt(15, 1);
                ps.execute();
            }

        } else {
            if (tipo == 2) {
                contador = dias;
                while (contador > 0) {
                    calendar.add(Calendar.DAY_OF_YEAR, 1);
                    if (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                        contador--;
                    }
                }

                ps = cn.prepareStatement("UPDATE expediente SET feca_expe=? WHERE nume_expe=?");
                ps.setDate(1, new java.sql.Date(calendar.getTime().getTime()));
                ps.setString(2, expediente.getNume_expe());
                ps.execute();

                for (int i = 0; i < expediente.getHistorial_expediente().size(); i++) {
                    ps = cn.prepareStatement("INSERT INTO historial_expediente("
                            + "corr_hist, nude_hist, nume_expe, nufo_hist, fell_hist, usen_hist, esta_hist, "
                            + "caen_hist, care_hist, core_hist, enre_hist, enti_hist, reti_hist, copi_hist, obsv_hist,condicion) "
                            + "VALUES(?,?,?,?,now(),?,?,?,?,?,?,?,?, ?,?,?)");
                    ps.setInt(1, expediente.getHistorial_expediente().get(i).getCorr_hist());
                    ps.setInt(2, expediente.getHistorial_expediente().get(i).getNude_hist());
                    ps.setString(3, expediente.getHistorial_expediente().get(i).getNume_expe());
                    ps.setInt(4, expediente.getHistorial_expediente().get(i).getNufo_hist());
                    ps.setInt(5, expediente.getHistorial_expediente().get(i).getUsen_hist());
                    ps.setInt(6, expediente.getHistorial_expediente().get(i).getEsta_hist());
                    ps.setInt(7, expediente.getHistorial_expediente().get(i).getCaen_hist());
                    ps.setInt(8, expediente.getHistorial_expediente().get(i).getCare_hist());
                    ps.setInt(9, expediente.getHistorial_expediente().get(i).getCore_hist());
                    ps.setInt(10, expediente.getHistorial_expediente().get(i).getEnre_hist());
                    ps.setInt(11, expediente.getHistorial_expediente().get(i).getEnti_hist());
                    ps.setInt(12, expediente.getHistorial_expediente().get(i).getReti_hist());
                    ps.setBoolean(13, expediente.getHistorial_expediente().get(i).isCopi_hist());
                    ps.setString(14, expediente.getHistorial_expediente().get(i).getObsv_hist());
                    ps.setInt(15, 2);
                    ps.execute();
                }
            }
        }

//        for (int i = 0; i < expediente.getRequisitos().size(); i++) {
//            ps = cn.prepareStatement("INSERT INTO requisitos_expediente("
//                    + "codi_repr, nume_expe) VALUES(?,?)");
//            ps.setInt(1, expediente.getRequisitos().get(i).getCodi_repr());
//            ps.setString(2, expediente.getNume_expe());
//            ps.execute();
//        }
    }

    public void update(expediente expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE expediente SET codi_cont=?, codi_enti=?, codi_carg=?, refe_expe=?, nufo_expe=?, "
                + "nudo_expe=?, codi_tiex=?, codi_proc=?, codi_usua=?, espe_expe=?, codi_asex=?, codi_reps=?, valx_expe=?, reci_expe=? "
                + "WHERE nume_expe=?");
        ps.setInt(1, expediente.getCodi_cont());
        ps.setInt(2, expediente.getCodi_enti());
        ps.setInt(3, expediente.getCodi_carg());
        ps.setString(4, expediente.getRefe_expe());
        ps.setInt(5, expediente.getNufo_expe());
        ps.setString(6, expediente.getNudo_expe());
        ps.setInt(7, expediente.getCodi_tiex());
        ps.setInt(8, expediente.getCodi_proc());
        ps.setInt(9, expediente.getCodi_usua());
        ps.setString(10, expediente.getEspe_expe());
        ps.setInt(11, expediente.getCodi_asex());
        ps.setInt(12, expediente.getCodi_reps());
        ps.setString(13, expediente.getValx_expe());
        ps.setString(14, expediente.getReci_expe());
        ps.setString(15, expediente.getNume_expe());
        ps.execute();

        ps = cn.prepareStatement("DELETE FROM requisitos_expediente WHERE nume_expe=?");
        ps.setString(1, expediente.getNume_expe());
        ps.execute();

        for (int i = 0; i < expediente.getRequisitos().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO requisitos_expediente("
                    + "codi_repr, nume_expe) VALUES(?,?)");
            ps.setInt(1, expediente.getRequisitos().get(i).getCodi_repr());
            ps.setString(2, expediente.getNume_expe());
            ps.execute();
        }
    }

    public historial_expediente last_desti_hist(String nume_expe, int nude_hist) throws SQLException, Exception {
        historial_expediente obj_historial_expediente;
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM historial_expediente "
                + " WHERE nume_expe=? and nude_hist=? order by corr_hist desc ");
        ps.setString(1, nume_expe);
        ps.setInt(2, nude_hist);
        ResultSet rs2 = ps.executeQuery();
        rs2.next();
        obj_historial_expediente = new historial_expediente();
        obj_historial_expediente.setCorr_hist(rs2.getInt(1));
        obj_historial_expediente.setNude_hist(rs2.getInt(2));
        obj_historial_expediente.setNume_expe(rs2.getString(3));
        obj_historial_expediente.setNufo_hist(rs2.getInt(5));
        obj_historial_expediente.setFell_hist(new Date(rs2.getTimestamp("fell_hist").getTime()));
        if (rs2.getTimestamp("fere_hist") != null) {
            obj_historial_expediente.setFere_hist(new Date(rs2.getTimestamp("fere_hist").getTime()));
        } else {
            obj_historial_expediente.setFere_hist(null);
        }
        obj_historial_expediente.setUsen_hist(rs2.getInt(10));
        obj_historial_expediente.setCaen_hist(rs2.getInt(12));
        obj_historial_expediente.setCare_hist(rs2.getInt(15));
        obj_historial_expediente.setCore_hist(rs2.getInt(16));
        obj_historial_expediente.setEnre_hist(rs2.getInt(17));
        obj_historial_expediente.setEsta_hist(rs2.getInt("esta_hist"));
        obj_historial_expediente.setObsv_hist(rs2.getString("obsv_hist"));
        obj_historial_expediente.setCopi_hist(rs2.getBoolean("copi_hist"));
        return obj_historial_expediente;
    }

    public List<BeanExpediente> get_expediente(String nume_expe) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM expediente "
                + " WHERE nume_expe=?");
        ps.setString(1, nume_expe);
        List<BeanExpediente> lista = new ArrayList<BeanExpediente>();
        BeanExpediente obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2;
        historial_expediente obj_historial_expediente;
        List<historial_expediente> destinatarios;
        BeanRequisitoMan obj_requisito;
        List<BeanRequisitoMan> requisitos;
        while (rs.next()) {
            obj = new BeanExpediente();
            obj.setNume_expe(rs.getString("nume_expe"));
            obj.setFech_expe(new Date(rs.getTimestamp("fech_expe").getTime()));
            obj.setCodi_usua(rs.getInt("codi_usua"));
            obj.setCodi_carg(rs.getInt("codi_carg"));
            obj.setCodi_cont(rs.getInt("codi_cont"));
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setRefe_expe(rs.getString("refe_expe"));
            obj.setNufo_expe(rs.getInt("nufo_expe"));
            obj.setNudo_expe(rs.getString("nudo_expe"));
            obj.setCodi_tiex(rs.getInt("codi_tiex"));
            obj.setCodi_proc(rs.getInt("codi_proc"));
            obj.setCodi_asex(rs.getInt("codi_asex"));
            obj.setEspe_expe(rs.getString("espe_expe"));
            obj.setReci_expe(rs.getString("reci_expe"));
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            obj.setNomb_proc(obj_procedimiento_expediente_dao.get_nomb_proc(rs.getInt("codi_proc")));
            obj.setNomb_tiex(obj_procedimiento_expediente_dao.get_nomb_tiex(rs.getInt("codi_tiex")));
            obj_procedimiento_expediente_dao.close();
            if (obj.getCodi_cont() != 0) {
                Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
                obj.setNomb_expe(obj_Contribuyente_dao.get_nomb(obj.getCodi_cont()));
                obj_Contribuyente_dao.close();
            } else if (obj.getCodi_enti() != 0) {
                Entidad_dao obj_Entidad_dao = new Entidad_dao();
                obj.setNomb_expe(obj_Entidad_dao.get_nomb(obj.getCodi_enti()));
                obj_Entidad_dao.close();
            } else if (obj.getCodi_carg() != 0) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                obj.setNomb_expe(obj_cargos_dao.get_nomb(obj.getCodi_carg()));
                obj_cargos_dao.close();
            }

            ps = cn.prepareStatement("SELECT * FROM historial_expediente WHERE corr_hist=? and nume_expe=?");
            ps.setInt(1, 1);
            ps.setString(2, obj.getNume_expe());
            rs2 = ps.executeQuery();
            destinatarios = new ArrayList<historial_expediente>();
            while (rs2.next()) {
                obj_historial_expediente = new historial_expediente();
                obj_historial_expediente.setCorr_hist(rs2.getInt(1));
                obj_historial_expediente.setNude_hist(rs2.getInt(2));
                obj_historial_expediente.setNume_expe(rs2.getString(3));
                obj_historial_expediente.setNufo_hist(rs2.getInt(5));
                obj_historial_expediente.setFell_hist(rs2.getDate(21));
                obj_historial_expediente.setUsen_hist(rs2.getInt(10));
                obj_historial_expediente.setCaen_hist(rs2.getInt(12));
                obj_historial_expediente.setCare_hist(rs2.getInt(15));
                obj_historial_expediente.setCore_hist(rs2.getInt(16));
                obj_historial_expediente.setEnre_hist(rs2.getInt(17));
                obj_historial_expediente.setCopi_hist(rs2.getBoolean("copi_hist"));
                destinatarios.add(obj_historial_expediente);
            }
            obj.setHistorial_expediente(destinatarios);

            ps = cn.prepareStatement("SELECT * FROM requisitos_expediente WHERE nume_expe=?");
            ps.setString(1, obj.getNume_expe());
            rs2 = ps.executeQuery();
            requisitos = new ArrayList<BeanRequisitoMan>();
            while (rs2.next()) {
                obj_requisito = new BeanRequisitoMan();
                obj_requisito.setCodi_repr(rs2.getInt(1));
                requisitos.add(obj_requisito);
            }
            obj.setRequisitos(requisitos);

            lista.add(obj);
        }

        return lista;

    }

    public BeanExpediente cargar_expediente(String search) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM expediente "
                + " WHERE nume_expe=?");
        ps.setString(1, search);
        BeanExpediente obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2;
        historial_expediente obj_historial_expediente;
        List<historial_expediente> destinatarios;
        BeanRequisitoMan obj_requisito;
        List<BeanRequisitoMan> requisitos;
        rs.next();
        obj = new BeanExpediente();
        obj.setNume_expe(rs.getString("nume_expe"));
        obj.setFech_expe(rs.getDate("fech_expe"));
        obj.setFeca_expe(rs.getDate("feca_expe"));
        obj.setCodi_usua(rs.getInt("codi_usua"));
        obj.setCodi_carg(rs.getInt("codi_carg"));
        obj.setCodi_cont(rs.getInt("codi_cont"));
        obj.setCodi_enti(rs.getInt("codi_enti"));
        obj.setRefe_expe(rs.getString("refe_expe"));
        obj.setNufo_expe(rs.getInt("nufo_expe"));
        obj.setNudo_expe(rs.getString("nudo_expe"));
        obj.setCodi_tiex(rs.getInt("codi_tiex"));
        obj.setCodi_proc(rs.getInt("codi_proc"));
        obj.setCodi_asex(rs.getInt("codi_asex"));
        obj.setEspe_expe(rs.getString("espe_expe"));
        obj.setCodi_enex(rs.getInt("codi_enex"));
        obj.setCodi_reps(rs.getInt("codi_reps"));
        obj.setValx_expe(rs.getString("valx_expe"));
        obj.setReci_expe(rs.getString("reci_expe"));
        procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
        obj.setNomb_proc(obj_procedimiento_expediente_dao.get_nomb_proc(rs.getInt("codi_proc")));
        obj.setNomb_tiex(obj_procedimiento_expediente_dao.get_nomb_tiex(rs.getInt("codi_tiex")));
        obj_procedimiento_expediente_dao.close();
        if (obj.getCodi_cont() != 0) {
            Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
            obj.setNomb_expe(obj_Contribuyente_dao.get_nomb(obj.getCodi_cont()));
            obj_Contribuyente_dao.close();
        } else if (obj.getCodi_enti() != 0) {
            Entidad_dao obj_Entidad_dao = new Entidad_dao();
            obj.setNomb_expe(obj_Entidad_dao.get_nomb(obj.getCodi_enti()));
            obj_Entidad_dao.close();
        } else if (obj.getCodi_carg() != 0) {
            cargos_dao obj_cargos_dao = new cargos_dao();
            obj.setNomb_expe(obj_cargos_dao.get_nomb(obj.getCodi_carg()));
            obj_cargos_dao.close();
        }

        ps = cn.prepareStatement("SELECT * FROM historial_expediente WHERE corr_hist=? and nume_expe=?");
        ps.setInt(1, 1);
        ps.setString(2, obj.getNume_expe());
        rs2 = ps.executeQuery();
        destinatarios = new ArrayList<historial_expediente>();
        while (rs2.next()) {
            obj_historial_expediente = new historial_expediente();
            obj_historial_expediente.setCorr_hist(rs2.getInt(1));
            obj_historial_expediente.setNude_hist(rs2.getInt(2));
            obj_historial_expediente.setNume_expe(rs2.getString(3));
            obj_historial_expediente.setNufo_hist(rs2.getInt(5));
            obj_historial_expediente.setFell_hist(rs2.getDate(21));
            obj_historial_expediente.setUsen_hist(rs2.getInt(10));
            obj_historial_expediente.setCaen_hist(rs2.getInt(12));
            obj_historial_expediente.setCare_hist(rs2.getInt(15));
            obj_historial_expediente.setCore_hist(rs2.getInt(16));
            obj_historial_expediente.setEnre_hist(rs2.getInt(17));
            obj_historial_expediente.setCopi_hist(rs2.getBoolean("copi_hist"));
            destinatarios.add(obj_historial_expediente);

        }
        obj.setHistorial_expediente(destinatarios);

        ps = cn.prepareStatement("SELECT * FROM requisitos_expediente WHERE nume_expe=?");
        ps.setString(1, obj.getNume_expe());
        rs2 = ps.executeQuery();
        requisitos = new ArrayList<BeanRequisitoMan>();
        while (rs2.next()) {
            obj_requisito = new BeanRequisitoMan();
            obj_requisito.setCodi_repr(rs2.getInt(1));

            requisitos.add(obj_requisito);
        }
        obj.setRequisitos(requisitos);
        return obj;
    }

    public List<BeanExpediente> search_expediente(String search) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM expediente " + " WHERE nume_expe like '%" + search + "%' or CAST (fech_expe AS character varying) like '%" + search + "%'");
        List<BeanExpediente> lista = new ArrayList<BeanExpediente>();
        BeanExpediente obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2;
        historial_expediente obj_historial_expediente;
        List<historial_expediente> destinatarios;
        BeanRequisitoMan obj_requisito;
        List<BeanRequisitoMan> requisitos;
        while (rs.next()) {
            obj = new BeanExpediente();
            obj.setNume_expe(rs.getString("nume_expe"));
            obj.setFech_expe(rs.getDate("fech_expe"));
            obj.setFeca_expe(rs.getDate("feca_expe"));
            obj.setCodi_usua(rs.getInt("codi_usua"));
            obj.setCodi_carg(rs.getInt("codi_carg"));
            obj.setCodi_cont(rs.getInt("codi_cont"));
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setRefe_expe(rs.getString("refe_expe"));
            obj.setNufo_expe(rs.getInt("nufo_expe"));
            obj.setNudo_expe(rs.getString("nudo_expe"));
            obj.setCodi_tiex(rs.getInt("codi_tiex"));
            obj.setCodi_proc(rs.getInt("codi_proc"));
            obj.setCodi_asex(rs.getInt("codi_asex"));
            obj.setEspe_expe(rs.getString("espe_expe"));
            obj.setCodi_reps(rs.getInt("codi_reps"));
            obj.setReci_expe(rs.getString("reci_expe"));
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            obj.setNomb_proc(obj_procedimiento_expediente_dao.get_nomb_proc(rs.getInt("codi_proc")));
            obj.setNomb_tiex(obj_procedimiento_expediente_dao.get_nomb_tiex(rs.getInt("codi_tiex")));
            obj_procedimiento_expediente_dao.close();
            if (obj.getCodi_cont() != 0) {
                Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
                obj.setNomb_expe(obj_Contribuyente_dao.get_nomb(obj.getCodi_cont()));
                obj_Contribuyente_dao.close();
            } else if (obj.getCodi_enti() != 0) {
                Entidad_dao obj_Entidad_dao = new Entidad_dao();
                obj.setNomb_expe(obj_Entidad_dao.get_nomb(obj.getCodi_enti()));
                obj_Entidad_dao.close();
            } else if (obj.getCodi_carg() != 0) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                obj.setNomb_expe(obj_cargos_dao.get_nomb(obj.getCodi_carg()));
                obj_cargos_dao.close();
            }
            ps = cn.prepareStatement("SELECT * FROM historial_expediente WHERE corr_hist=? and nume_expe=?");
            ps.setInt(1, 1);
            ps.setString(2, obj.getNume_expe());
            rs2 = ps.executeQuery();
            destinatarios = new ArrayList<historial_expediente>();
            while (rs2.next()) {
                obj_historial_expediente = new historial_expediente();
                obj_historial_expediente.setCorr_hist(rs2.getInt(1));
                obj_historial_expediente.setNude_hist(rs2.getInt(2));
                obj_historial_expediente.setNume_expe(rs2.getString(3));
                obj_historial_expediente.setNufo_hist(rs2.getInt(5));
                obj_historial_expediente.setFell_hist(rs2.getDate(21));
                obj_historial_expediente.setUsen_hist(rs2.getInt(10));
                obj_historial_expediente.setCaen_hist(rs2.getInt(12));
                obj_historial_expediente.setCare_hist(rs2.getInt(15));
                obj_historial_expediente.setCore_hist(rs2.getInt(16));
                obj_historial_expediente.setEnre_hist(rs2.getInt(17));
                obj_historial_expediente.setCopi_hist(rs2.getBoolean("copi_hist"));
                destinatarios.add(obj_historial_expediente);
            }
            obj.setHistorial_expediente(destinatarios);

            ps = cn.prepareStatement("SELECT * FROM requisitos_expediente WHERE nume_expe=?");
            ps.setString(1, obj.getNume_expe());
            rs2 = ps.executeQuery();
            requisitos = new ArrayList<BeanRequisitoMan>();
            while (rs2.next()) {
                obj_requisito = new BeanRequisitoMan();
                obj_requisito.setCodi_repr(rs2.getInt(1));
                requisitos.add(obj_requisito);
            }
            obj.setRequisitos(requisitos);

            lista.add(obj);
        }
        return lista;
    }

    public List<BeanBusquedaExpediente> Busqueda_Expedientes(int Estado, String Busqueda) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("select * from view_busqueda_expedientes where esta_hist = " + Estado + " and nume_expe like '%" + Busqueda + "%'");
        ResultSet rs = ps.executeQuery();
        List<BeanBusquedaExpediente> list = new ArrayList<BeanBusquedaExpediente>();
        BeanBusquedaExpediente ObjBeanExpediente;
        while (rs.next()) {
            ObjBeanExpediente = new BeanBusquedaExpediente();
            ObjBeanExpediente.setNumero_expediente(rs.getString("nume_expe"));
            ObjBeanExpediente.setEstado_expediente(rs.getString("esta_hist"));
            ObjBeanExpediente.setNumero_folios_expediente(rs.getShort("nufo_hist"));
            ObjBeanExpediente.setNombre_docu_expediente(rs.getString("nomb_tiex"));
            ObjBeanExpediente.setNombre_proc_expediente(rs.getString("nomb_proc"));
            ObjBeanExpediente.setCodigo_cont_envia_expediente(rs.getInt("coen_hist"));
            ObjBeanExpediente.setCodigo_carg_envia_expedinte(rs.getInt("caen_hist"));
            ObjBeanExpediente.setCodgo_enti_envia_expediente(rs.getInt("enen_hist"));
            if (rs.getInt("coen_hist") != 0) {
                ObjBeanExpediente.setNombre_remitente_expediente(rs.getString("nomb_cont"));
            } else {
                if (rs.getInt("caen_hist") != 0) {
                    ObjBeanExpediente.setNombre_remitente_expediente(rs.getString("cargo"));
                } else {
                    if (rs.getInt("enen_hist") != 0) {
                        ObjBeanExpediente.setNombre_remitente_expediente(rs.getString("nomb_enti"));
                    }
                }
            }
            ObjBeanExpediente.setCorrelativo_expediente(rs.getInt("corr_hist"));
            list.add(ObjBeanExpediente);
        }
        return list;
    }

    public List<ExpedienteReportes> get_expedientes_reportes(String Param1, String Param2, String Param3, int Param4, int Param5, int Param6, int Param7) throws SQLException {
        List<ExpedienteReportes> lista = new ArrayList<ExpedienteReportes>();
        try {
//            Messagebox.show("Contenido del Parametro 1: " + Param1);
//            Messagebox.show("Contenido del Parametro 2: " + Param2);
//            Messagebox.show("Contenido del Parametro 3: " + Param3);
//            Messagebox.show("Contenido del Parametro 4: " + Param4);
//            Messagebox.show("Contenido del Parametro 5: " + Param5);
//            Messagebox.show("Contenido del Parametro 6: " + Param6);
//            Messagebox.show("Contenido del Parametro 7: " + Param7);
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_reportes_area3(?,?,?,?,?,?,?);");
            ps.setString(1, Param1);
            ps.setString(2, Param2);
            ps.setString(3, Param3);
            ps.setInt(4, Param4);
            ps.setInt(5, Param5);
            ps.setInt(6, Param6);
            ps.setInt(7, Param7);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteReportes obj = new ExpedienteReportes();
                obj.setEXPEDIENTE(rs.getString("EXPEDIENTE"));
                obj.setNROTRAMITE(rs.getInt("NROTRAMITE"));
                obj.setNRODESTINATARIO(rs.getInt("NRODESTINATARIO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDESTINATARIO(rs.getString("DESTINATARIO"));
                obj.setDEPENDENCIA(rs.getString("DEPENDENCIA"));
                obj.setFECHA_ENVIO(rs.getString("FECHA_ENVIO"));
                obj.setFECHA_RECEPCION(rs.getString("FECHA_RECEPCION"));
                obj.setFECHA_TRAMITE(rs.getString("FECHA_TRAMITE"));
                obj.setFECHA_INGRESO(rs.getString("FECHA_INGRESO"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));
                if (Param7 == 1) {
                    obj.setFECHA_OPCION(rs.getString("FECHA_ENVIO"));
                } else {
                    if (Param7 == 2) {
                        obj.setFECHA_OPCION(rs.getString("FECHA_RECEPCION"));
                    } else {
                        if (Param7 == 3) {
                            obj.setFECHA_OPCION(rs.getString("FECHA_TRAMITE"));
                        } else {
                            if (Param7 == 4) {
                                obj.setFECHA_OPCION(rs.getString("FECHA_TRAMITE"));
                            } else {
                                obj.setFECHA_OPCION(rs.getString("FECHA_TRAMITE"));
                            }
                        }
                    }
                }
                lista.add(obj);

            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }

        return lista;
    }

    public List<ExpedienteCliente> search_expediente_reso(String search, int codi_arde) {

        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM public.sp_buscar_expedientes_vincular(?,?,?) " + " WHERE expnum like '%" + search + "%' or documento like '%" + search + "%' or procedimiento like '%" + search + "%' or administrado like '%" + search + "%' or remitente like '%" + search + "%'");
            ps.setInt(1, codi_arde);
            ps.setInt(2, 1);
            ps.setString(3, null);

            ExpedienteCliente obj;
            ResultSet rs = ps.executeQuery(), rs2;
            int codi_carg;
            while (rs.next()) {
                obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setDESTINATARIO(rs.getString("DESTINATARIO"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                ps = cn.prepareStatement("select * from expediente where nume_expe=?");
                ps.setString(1, rs.getString("EXPNUM"));
                rs2 = ps.executeQuery();
                rs2.next();
                codi_carg = rs2.getInt("codi_carg");
                obj.setCodi_cont(rs2.getInt("codi_cont"));
                obj.setCodi_enti(rs2.getInt("codi_enti"));

                if (codi_carg != 0) {
                    ps = cn.prepareStatement("select * from cargos where codi_carg=?");
                    ps.setInt(1, codi_carg);
                    rs2 = ps.executeQuery();
                    rs2.next();
                    obj.setCodi_arde(rs2.getInt("codi_arde"));
                    ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
                    ps.setInt(1, obj.getCodi_arde());
                    rs2 = ps.executeQuery();
                    rs2.next();
                    obj.setNomb_hist(rs2.getString("nomb_arde"));
                } else if (obj.getCodi_cont() != 0) {
                    ps = cn.prepareStatement("select * from contribuyente where codi_cont=?");
                    ps.setInt(1, obj.getCodi_cont());
                    rs2 = ps.executeQuery();
                    rs2.next();
                    obj.setNomb_hist(rs2.getString("nomb_cont") + " " + rs2.getString("apel_cont"));
                } else if (obj.getCodi_enti() != 0) {
                    ps = cn.prepareStatement("select * from entidad where codi_enti=?");
                    ps.setInt(1, obj.getCodi_enti());
                    rs2 = ps.executeQuery();
                    rs2.next();
                    obj.setNomb_hist(rs2.getString("nomb_enti"));
                }
                lista.add(obj);
            }
        } catch (Exception e) {
            //Messagebox.show(e.getMessage());
        }

        return lista;
    }

    public List<BeanExpediente> search_expediente2(String search) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM expediente "
                + " WHERE codi_esex=2 AND (nume_expe like '%" + search + "%' or CAST (fech_expe AS character varying) like '%" + search + "%')");
        List<BeanExpediente> lista = new ArrayList<BeanExpediente>();
        BeanExpediente obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2;
        historial_expediente obj_historial_expediente;
        List<historial_expediente> destinatarios;
        while (rs.next()) {
            obj = new BeanExpediente();
            obj.setNume_expe(rs.getString("nume_expe"));
            obj.setFech_expe(rs.getDate("fech_expe"));
            obj.setCodi_usua(rs.getInt("codi_usua"));
            obj.setCodi_carg(rs.getInt("codi_carg"));
            obj.setCodi_cont(rs.getInt("codi_cont"));
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setRefe_expe(rs.getString("refe_expe"));
            obj.setNufo_expe(rs.getInt("nufo_expe"));
            obj.setNudo_expe(rs.getString("nudo_expe"));
            obj.setCodi_tiex(rs.getInt("codi_tiex"));
            obj.setCodi_proc(rs.getInt("codi_proc"));
            obj.setCodi_asex(rs.getInt("codi_asex"));
            obj.setEspe_expe(rs.getString("espe_expe"));
            procedimiento_expediente_dao obj_procedimiento_expediente_dao = new procedimiento_expediente_dao();
            obj.setNomb_proc(obj_procedimiento_expediente_dao.get_nomb_proc(rs.getInt("codi_proc")));
            obj.setNomb_tiex(obj_procedimiento_expediente_dao.get_nomb_tiex(rs.getInt("codi_tiex")));
            obj_procedimiento_expediente_dao.close();
            if (obj.getCodi_cont() != 0) {
                Contribuyente_dao obj_Contribuyente_dao = new Contribuyente_dao();
                obj.setNomb_expe(obj_Contribuyente_dao.get_nomb(obj.getCodi_cont()));
                obj_Contribuyente_dao.close();
            } else if (obj.getCodi_enti() != 0) {
                Entidad_dao obj_Entidad_dao = new Entidad_dao();
                obj.setNomb_expe(obj_Entidad_dao.get_nomb(obj.getCodi_enti()));
                obj_Entidad_dao.close();
            } else if (obj.getCodi_carg() != 0) {
                cargos_dao obj_cargos_dao = new cargos_dao();
                obj.setNomb_expe(obj_cargos_dao.get_nomb(obj.getCodi_carg()));
                obj_cargos_dao.close();
            }
            ps = cn.prepareStatement("SELECT * FROM historial_expediente WHERE corr_hist=? and nume_expe=?");
            ps.setInt(1, 1);
            ps.setString(2, obj.getNume_expe());
            rs2 = ps.executeQuery();
            destinatarios = new ArrayList<historial_expediente>();
            while (rs2.next()) {
                obj_historial_expediente = new historial_expediente();
                obj_historial_expediente.setCorr_hist(rs2.getInt(1));
                obj_historial_expediente.setNude_hist(rs2.getInt(2));
                obj_historial_expediente.setNume_expe(rs2.getString(3));
                obj_historial_expediente.setNufo_hist(rs2.getInt(5));
                obj_historial_expediente.setFell_hist(rs2.getDate(21));
                obj_historial_expediente.setUsen_hist(rs2.getInt(10));
                obj_historial_expediente.setCaen_hist(rs2.getInt(12));
                obj_historial_expediente.setCare_hist(rs2.getInt(15));
                obj_historial_expediente.setCoen_hist(rs2.getInt(16));
                obj_historial_expediente.setEnen_hist(rs2.getInt(17));
                obj_historial_expediente.setCopi_hist(rs2.getBoolean("copi_hist"));
                destinatarios.add(obj_historial_expediente);
            }
            obj.setHistorial_expediente(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<ExpedienteMovimientos> get_expedientes_movimientos(String expnum) throws Exception {
        List<ExpedienteMovimientos> lista = new ArrayList<ExpedienteMovimientos>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_movimientos(?);");
            ps.setString(1, expnum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteMovimientos obj = new ExpedienteMovimientos();
                obj.setMOVIMIENTO(rs.getInt("MOVIMIENTO"));
                obj.setNRODESTINO(rs.getInt("NRODESTINO"));
                obj.setESTADO(rs.getString("ESTADO"));
                obj.setLLEGO(rs.getString("LLEGO"));
                obj.setRECEPCIONO(rs.getString("RECEPCIONO"));
                obj.setTRAMITO(rs.getString("TRAMITO"));
                obj.setNROFOLIO(rs.getInt("NROFOLIO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setDESTINATARIO(rs.getString("DESTINATARIO"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDOC(rs.getString("DOC"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                lista.add(obj);
            }
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.INFO, "GET EXPEDIENTEMOVIMIENTOS");
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }

        return lista;
    }

    public List<ExpedienteMovimientos> get_expedientes_movimientos_des(String expnum, int nude_hist) throws Exception {
        List<ExpedienteMovimientos> lista = new ArrayList<ExpedienteMovimientos>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_movimientos_dest(?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, nude_hist);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteMovimientos obj = new ExpedienteMovimientos();
                obj.setMOVIMIENTO(rs.getInt("MOVIMIENTO"));
                obj.setNRODESTINO(rs.getInt("NRODESTINO"));
                obj.setESTADO(rs.getString("ESTADO"));
                obj.setLLEGO(rs.getString("LLEGO"));
                obj.setRECEPCIONO(rs.getString("RECEPCIONO"));
                obj.setTRAMITO(rs.getString("TRAMITO"));
                obj.setNROFOLIO(rs.getInt("NROFOLIO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setDESTINATARIO(rs.getString("DESTINATARIO"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDOC(rs.getString("DOC"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }

        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_consultar_unico(String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_expedientes_consultar_unico(?);");
            ps.setString(1, parametro);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setFECHA(rs.getString("FECHA"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                lista.add(obj);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_consultar_by_dni(String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_expedientes_consultar_by_dni(?);");
            ps.setString(1, parametro);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setFECHA(rs.getString("FECHA"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                lista.add(obj);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_consultar(int tipocns, int tipoadm, String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_expedientes_consultar3(?,?,?);");
            ps.setInt(1, tipocns);
            ps.setInt(2, tipoadm);
            ps.setString(3, parametro);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));
                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_consultar_in(int tipocns, int tipoadm, String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_expedientes_consultar2(?,?,?);");
            ps.setInt(1, tipocns);
            ps.setInt(2, tipoadm);
            ps.setString(3, parametro);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));
                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_recepcionar(int are_cod, int tipocns, String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_expedientes_recepcionar_area(?,?,?);");
            ps.setInt(1, are_cod);
            ps.setInt(2, tipocns);
            ps.setString(3, parametro);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));
                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_tramitar(int are_cod, int tipocns, String parametro) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM sp_consultar_expediente_new(?,?,?);");
            ps.setInt(1, are_cod);
            ps.setInt(2, tipocns);
            ps.setString(3, parametro);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));
                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public List<ExpedienteCliente> get_expedientes_restaurar(int are_cod, int tipocns, String parametro, String fechas, int estado) throws SQLException {
        List<ExpedienteCliente> lista = new ArrayList<ExpedienteCliente>();
        try {
            cn = dt.getConnection();
            String selectedfunction = "SELECT * FROM public.sp_expedientes_restaurar_otros_area(?,?,?,?,?);";
            if (estado == 6) {
                selectedfunction = "SELECT * FROM public.sp_expedientes_restaurar_tramitados_area(?,?,?,?);";
            }
            PreparedStatement ps = cn.prepareStatement(selectedfunction);
            ps.setInt(1, are_cod);
            ps.setInt(2, tipocns);
            ps.setString(3, parametro);
            if (fechas == null) {
                ps.setNull(4, java.sql.Types.VARCHAR);
            } else {
                ps.setString(4, fechas);
            }
            if (estado != 6) {
                ps.setInt(5, estado);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ExpedienteCliente obj = new ExpedienteCliente();
                obj.setEXPNUM(rs.getString("EXPNUM"));
                obj.setCORRELATIVO(rs.getInt("CORRELATIVO"));
                obj.setCONDICION(rs.getInt("CONDICION"));
                obj.setFOLIOS(rs.getInt("FOLIOS"));
                obj.setDOCUMENTO(rs.getString("DOCUMENTO"));
                obj.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
                obj.setADMINISTRADO(rs.getString("ADMINISTRADO"));
                obj.setREMITENTE(rs.getString("REMITENTE"));
                obj.setDESTINATARIO(rs.getString("DESTINATARIO"));
                obj.setPROVEIDO(rs.getString("PROVEIDO"));
                obj.setDESTINO(rs.getInt("DESTINO"));
                obj.setOBSERVACION(rs.getString("OBSERVACION"));
                obj.setFINALIZACION(rs.getString("FINALIZACION"));

                lista.add(obj);
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
        return lista;
    }

    public void expediente_recepcionar(String expnum, int correlativo, int nroDest, int usuario) throws SQLException {
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT public.usp_expedientes_recepcionar(?,?,?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, correlativo);
            ps.setInt(3, nroDest);
            ps.setInt(4, usuario);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
    }

    public void expediente_tramitar(String expnum, int correlativo, int nroDest, int usuario, int folios, int cargoRecibe, String nproveido, String resuProveido, Boolean copia, Double costo) throws SQLException {
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT public.usp_expedientes_tramitar(?,?,?,?,?,?,?,?,?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, correlativo);
            ps.setInt(3, nroDest);
            ps.setInt(4, usuario);
            ps.setInt(5, folios);
            ps.setInt(6, cargoRecibe);
            ps.setString(7, nproveido);
            ps.setString(8, resuProveido);
            ps.setBoolean(9, copia);
            ps.setDouble(10, costo);
            ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
    }

    public void expediente_observar(String expnum, int correlativo, int nroDest, int usuario, String observacion) throws SQLException {
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT public.usp_expedientes_observar(?,?,?,?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, correlativo);
            ps.setInt(3, nroDest);
            ps.setInt(4, usuario);
            ps.setString(5, observacion);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
    }

    public void expediente_finalizar(String expnum, int correlativo, int nroDest, int usuario, String observacion) throws SQLException {
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT public.usp_expedientes_finalizar(?,?,?,?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, correlativo);
            ps.setInt(3, nroDest);
            ps.setInt(4, usuario);
            ps.setString(5, observacion);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
    }

    public void expediente_restaurar(String expnum, int correlativo, int nroDest, int usuario, String observacion, int estado) throws SQLException {
        try {
            cn = dt.getConnection();
            PreparedStatement ps = cn.prepareStatement("SELECT public.usp_expedientes_restaurar(?,?,?,?,?,?);");
            ps.setString(1, expnum);
            ps.setInt(2, correlativo);
            ps.setInt(3, nroDest);
            ps.setInt(4, usuario);
            ps.setString(5, observacion);
            ps.setInt(6, estado);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Expediente_dao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cn.close();
        }
    }

    public List<EnviaHistorial> get_envia_estado_restaurar() {
        List<EnviaHistorial> list_envia = new ArrayList<EnviaHistorial>();
        list_envia.add(new EnviaHistorial(2, "Recepcionados"));
        list_envia.add(new EnviaHistorial(6, "Tramitados"));
        list_envia.add(new EnviaHistorial(4, "Finalizados"));
        list_envia.add(new EnviaHistorial(3, "Observados"));
        return list_envia;
    }

    public List<ExpedienteCliente> get_expediente_vinculados(String expnum, int correlativo, int nudedest) throws Exception {
        cn = dt.getConnection();
        List<ExpedienteCliente> list = new ArrayList<ExpedienteCliente>();
        PreparedStatement ps = cn.prepareStatement("SELECT E.*,R.nume_reso FROM public.expedientes_adjuntos E INNER JOIN resolucion R ON E.codi_reso=R.codi_reso WHERE E.codi_reso = (SELECT A.codi_reso FROM public.expedientes_adjuntos A where A.nume_exad=? and A.corr_hist=? and A.nude_hist=? LIMIT 1);");
        ps.setString(1, expnum);
        ps.setInt(2, correlativo);
        ps.setInt(3, nudedest);
        ResultSet rs = ps.executeQuery();
        ExpedienteCliente obj;
        while (rs.next()) {
            obj = new ExpedienteCliente();
            obj.setEXPNUM(rs.getString("nume_exad"));
            obj.setCORRELATIVO(rs.getInt("corr_hist"));
            obj.setDESTINO(rs.getInt("nude_hist"));
            obj.setNume_reso(rs.getString("nume_reso"));
            list.add(obj);
        }
        return list;
    }

    public List<ExpedienteCliente> get_expediente_vinculados_total(String expnum, int nudedest) throws Exception {
        cn = dt.getConnection();
        List<ExpedienteCliente> list = new ArrayList<ExpedienteCliente>();
        PreparedStatement ps = cn.prepareStatement("SELECT E.nume_exad,E.nude_hist,string_agg(CAST(e.corr_hist as TEXT),', ')as corr_hist,string_agg(R.nume_reso,', ')as nume_reso FROM public.expedientes_adjuntos E INNER JOIN resolucion R ON E.codi_reso=R.codi_reso  "
                + " WHERE E.codi_reso in (SELECT A.codi_reso FROM public.expedientes_adjuntos A where A.nume_exad=? and A.nude_hist=?)  "
                + " GROUP BY E.nume_exad,E.nude_hist "
                + " ORDER BY E.nume_exad;");
        ps.setString(1, expnum);
        ps.setInt(2, nudedest);
        ResultSet rs = ps.executeQuery();
        ExpedienteCliente obj;
        while (rs.next()) {
            obj = new ExpedienteCliente();
            obj.setEXPNUM(rs.getString("nume_exad"));
            obj.setCORRELATIVOS(rs.getString("corr_hist"));
            obj.setDESTINO(rs.getInt("nude_hist"));
            obj.setNume_reso(rs.getString("nume_reso"));
            int codi_carg = 0;
            ps = cn.prepareStatement("select * from expediente where nume_expe=?");
            ps.setString(1, rs.getString("nume_exad"));
            ResultSet rs2 = ps.executeQuery();
            rs2.next();
            codi_carg = rs2.getInt("codi_carg");
            obj.setCodi_cont(rs2.getInt("codi_cont"));
            obj.setCodi_enti(rs2.getInt("codi_enti"));

            if (codi_carg != 0) {
                ps = cn.prepareStatement("select * from cargos where codi_carg=?");
                ps.setInt(1, codi_carg);
                rs2 = ps.executeQuery();
                rs2.next();
                obj.setCodi_arde(rs2.getInt("codi_arde"));
                ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
                ps.setInt(1, obj.getCodi_arde());
                rs2 = ps.executeQuery();
                rs2.next();
                obj.setNomb_hist(rs2.getString("nomb_arde"));
            } else if (obj.getCodi_cont() != 0) {
                ps = cn.prepareStatement("select * from contribuyente where codi_cont=?");
                ps.setInt(1, obj.getCodi_cont());
                rs2 = ps.executeQuery();
                rs2.next();
                obj.setNomb_hist(rs2.getString("nomb_cont") + " " + rs2.getString("apel_cont"));
            } else if (obj.getCodi_enti() != 0) {
                ps = cn.prepareStatement("select * from entidad where codi_enti=?");
                ps.setInt(1, obj.getCodi_enti());
                rs2 = ps.executeQuery();
                rs2.next();
                obj.setNomb_hist(rs2.getString("nomb_enti"));
            }
            list.add(obj);
        }
        return list;
    }

    public String get_recibo_existe(String recibo) throws SQLException {
        cn = dt.getConnection();
        PreparedStatement ps = cn.prepareStatement("SELECT nume_expe FROM expediente WHERE reci_expe = ?");
        ps.setString(1, recibo);
        ResultSet rs = ps.executeQuery();
        String retorno = "";
        while (rs.next()) {
            retorno = rs.getString("nume_expe");
            break;
        }
        return retorno;
    }

    /*Método para consultar Ubicación Actual del Expediente*/
    public List<BeanUbicacionExpediente> Ubicacion_Actual_Expediente(String Numero_Expediente) throws SQLException {
        cn = dt.getConnection();
        List<BeanUbicacionExpediente> list = new ArrayList<BeanUbicacionExpediente>();
        BeanUbicacionExpediente objUbicacion_Expediente = new BeanUbicacionExpediente();
        PreparedStatement ps = cn.prepareStatement("SELECT \n"
                + "HE.nufo_hist AS FOLIOS,\n"
                + "HE.nume_expe AS NUMERO_EXPEDIENTE,\n"
                + "HE.nude_hist AS CORRELATIVO,\n"
                + "HE.esta_hist AS ESTADO,\n"
                + "HE.fell_hist AS FECHA_LLEGADA,\n"
                + "HE.caen_hist AS CODIGO_CARGO_ENVIA,\n"
                + "HE.coen_hist AS CODIGO_CONTRIBUYENTE_ENVIA,\n"
                + "HE.enen_hist AS CODIGO_ENTIDAD_ENVIA,\n"
                + "HE.care_hist AS CODIGO_CARGO_RECIBE,\n"
                + "HE.core_hist AS CODIGO_CONTRIBUYENTE_RECIBE,\n"
                + "HE.enre_hist AS CODIGO_ENTIDAD_RECIBE,\n"
                + "C2.nomb_carg||' - '||AD2.nomb_arde AS NOMBRE_CARGO_RECIBE_DESTINATARIO,\n"
                + "C.nomb_carg||' - '||AD.nomb_arde AS NOMBRE_CARGO_ENVIA_REMITENTE,\n"
                + "AD2.nomb_arde AS NOMBRE_AREA_RECIBE,\n"
                + "CT2.nomb_cont AS REMITENTE_CONTRIBUYENTE_EXPEDIENTE,\n"
                + "EN2.nomb_enti AS REMITENTE_ENTIDAD_EXPEDIENTE,\n"
                + "C3.nomb_carg||' - '||AD3.nomb_arde AS REMITENTE_CARGO_EXPEDIENTE,\n"
                + "E.fech_expe AS FECHA_INGRESO,\n"
                + "HE.fere_hist AS FECHA_RECEPCION,\n"
                + "PE.nomb_proc AS PROCEDIMIENTO,\n"
                + "TE.nomb_tiex AS TIPO_DOCUMENTO,\n"
                + "E.nudo_expe AS NUMERO_DOCUMENTO,\n"
                + "HE.obsv_hist AS OBSERVACION\n"
                + "FROM expediente E \n"
                + "INNER JOIN historial_expediente HE ON E.nume_expe = HE.nume_expe \n"
                + "LEFT JOIN cargos C ON C.codi_carg=HE.caen_hist\n"
                + "LEFT JOIN cargos C2 ON C2.codi_carg=HE.care_hist\n"
                + "LEFT JOIN cargos C3 ON C3.codi_carg=E.codi_carg\n"
                + "LEFT JOIN area_dependencia AD ON AD.codi_arde=C.codi_arde\n"
                + "LEFT JOIN area_dependencia AD2 ON AD2.codi_arde=c2.codi_arde\n"
                + "LEFT JOIN area_dependencia AD3 ON AD3.codi_arde=c3.codi_arde\n"
                + "LEFT JOIN contribuyente CT ON CT.codi_cont=HE.coen_hist\n"
                + "LEFT JOIN contribuyente CT2 ON CT2.codi_cont=E.codi_cont\n"
                + "LEFT JOIN entidad EN ON EN.codi_enti=HE.enen_hist\n"
                + "LEFT JOIN entidad EN2 ON en2.codi_enti=E.codi_enti\n"
                + "LEFT JOIN procedimiento_expediente PE ON E.codi_proc=PE.codi_proc\n"
                + "LEFT JOIN tipo_expediente TE ON TE.codi_tiex=e.codi_tiex\n"
                + "WHERE E.nume_expe = '" + Numero_Expediente + "' ORDER BY 3 DESC");
        ResultSet rs = ps.executeQuery();
        rs.next();
        objUbicacion_Expediente.setFOLIOS(rs.getInt("FOLIOS"));
        objUbicacion_Expediente.setNUMERO_EXPEDIENTE(rs.getString("NUMERO_EXPEDIENTE"));
        objUbicacion_Expediente.setCORRELATIVO(rs.getInt("CORRELATIVO"));
        objUbicacion_Expediente.setESTADO(rs.getInt("ESTADO"));
        objUbicacion_Expediente.setFECHA_LLEGADA(rs.getString("FECHA_LLEGADA"));
        objUbicacion_Expediente.setCODIGO_CARGO_ENVIA(rs.getInt("CODIGO_CARGO_ENVIA"));
        objUbicacion_Expediente.setCODIGO_CONTRIBUYENTE_ENVIA(rs.getInt("CODIGO_CONTRIBUYENTE_ENVIA"));
        objUbicacion_Expediente.setCODIGO_ENTIDAD_ENVIA(rs.getInt("CODIGO_ENTIDAD_ENVIA"));
        objUbicacion_Expediente.setCODIGO_CARGO_RECIBE(rs.getInt("CODIGO_CARGO_RECIBE"));
        objUbicacion_Expediente.setCODIGO_CONTRIBUYENTE_RECIBE(rs.getInt("CODIGO_CONTRIBUYENTE_RECIBE"));
        objUbicacion_Expediente.setCODIGO_ENTIDAD_RECIBE(rs.getInt("CODIGO_ENTIDAD_RECIBE"));
        objUbicacion_Expediente.setNOMBRE_CARGO_ENVIA_REMITENTE(rs.getString("NOMBRE_CARGO_ENVIA_REMITENTE"));
        objUbicacion_Expediente.setNOMBRE_AREA_RECIBE(rs.getString("NOMBRE_AREA_RECIBE"));
        objUbicacion_Expediente.setNOMBRE_CARGO_RECIBE_DESTINATARIO(rs.getString("NOMBRE_CARGO_RECIBE_DESTINATARIO"));
        objUbicacion_Expediente.setREMITENTE_CONTRIBUYENTE_EXPEDIENTE(rs.getString("REMITENTE_CONTRIBUYENTE_EXPEDIENTE"));
        objUbicacion_Expediente.setREMITENTE_ENTIDAD_EXPEDIENTE(rs.getString("REMITENTE_ENTIDAD_EXPEDIENTE"));
        objUbicacion_Expediente.setREMITENTE_CARGO_EXPEDIENTE(rs.getString("REMITENTE_CARGO_EXPEDIENTE"));
        objUbicacion_Expediente.setFECHA_INGRESO(rs.getString("FECHA_INGRESO"));
        objUbicacion_Expediente.setFECHA_RECEPCION(rs.getString("FECHA_RECEPCION"));
        objUbicacion_Expediente.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
        objUbicacion_Expediente.setTIPO_DOCUMENTO(rs.getString("TIPO_DOCUMENTO"));
        objUbicacion_Expediente.setNUMERO_DOCUMENTO(rs.getString("NUMERO_DOCUMENTO"));
        objUbicacion_Expediente.setOBSERVACION(rs.getString("OBSERVACION"));
        list.add(objUbicacion_Expediente);
        return list;
    }

    public List<BeanUbicacionExpediente> Movimientos_Expediente(String Numero_Expediente) throws SQLException {
        cn = dt.getConnection();
        List<BeanUbicacionExpediente> list = new ArrayList<BeanUbicacionExpediente>();
        BeanUbicacionExpediente objUbicacion_Expediente = new BeanUbicacionExpediente();
        PreparedStatement ps = cn.prepareStatement("SELECT \n"
                + "HE.nufo_hist AS FOLIOS,\n"
                + "HE.nume_expe AS NUMERO_EXPEDIENTE,\n"
                + "HE.nude_hist AS CORRELATIVO,\n"
                + "HE.esta_hist AS ESTADO,\n"
                + "HE.fell_hist AS FECHA_LLEGADA,\n"
                + "HE.caen_hist AS CODIGO_CARGO_ENVIA,\n"
                + "HE.coen_hist AS CODIGO_CONTRIBUYENTE_ENVIA,\n"
                + "HE.enen_hist AS CODIGO_ENTIDAD_ENVIA,\n"
                + "HE.care_hist AS CODIGO_CARGO_RECIBE,\n"
                + "HE.core_hist AS CODIGO_CONTRIBUYENTE_RECIBE,\n"
                + "HE.enre_hist AS CODIGO_ENTIDAD_RECIBE,\n"
                + "C2.nomb_carg||' - '||AD2.nomb_arde AS NOMBRE_CARGO_RECIBE_DESTINATARIO,\n"
                + "C.nomb_carg||' - '||AD.nomb_arde AS NOMBRE_CARGO_ENVIA_REMITENTE,\n"
                + "AD2.nomb_arde AS NOMBRE_AREA_RECIBE,\n"
                + "CT2.nomb_cont AS REMITENTE_CONTRIBUYENTE_EXPEDIENTE,\n"
                + "EN2.nomb_enti AS REMITENTE_ENTIDAD_EXPEDIENTE,\n"
                + "C3.nomb_carg||' - '||AD3.nomb_arde AS REMITENTE_CARGO_EXPEDIENTE,\n"
                + "E.fech_expe AS FECHA_INGRESO,\n"
                + "HE.fere_hist AS FECHA_RECEPCION,\n"
                + "PE.nomb_proc AS PROCEDIMIENTO,\n"
                + "TE.nomb_tiex AS TIPO_DOCUMENTO,\n"
                + "E.nudo_expe AS NUMERO_DOCUMENTO,\n"
                + "HE.obsv_hist AS OBSERVACION\n"
                + "FROM expediente E \n"
                + "LEFT JOIN historial_expediente HE ON E.nume_expe = HE.nume_expe \n"
                + "LEFT JOIN cargos C ON C.codi_carg=HE.caen_hist\n"
                + "LEFT JOIN cargos C2 ON C2.codi_carg=HE.care_hist\n"
                + "LEFT JOIN cargos C3 ON C3.codi_carg=E.codi_carg\n"
                + "LEFT JOIN area_dependencia AD ON AD.codi_arde=C.codi_arde\n"
                + "LEFT JOIN area_dependencia AD2 ON AD2.codi_arde=c2.codi_arde\n"
                + "LEFT JOIN area_dependencia AD3 ON AD3.codi_arde=c3.codi_arde\n"
                + "LEFT JOIN contribuyente CT ON CT.codi_cont=HE.coen_hist\n"
                + "LEFT JOIN contribuyente CT2 ON CT2.codi_cont=E.codi_cont\n"
                + "LEFT JOIN entidad EN ON EN.codi_enti=HE.enen_hist\n"
                + "LEFT JOIN entidad EN2 ON en2.codi_enti=E.codi_enti\n"
                + "LEFT JOIN procedimiento_expediente PE ON E.codi_proc=PE.codi_proc\n"
                + "LEFT JOIN tipo_expediente TE ON TE.codi_tiex=e.codi_tiex\n"
                + "WHERE E.nume_expe = '" + Numero_Expediente + "' ORDER BY 3 DESC");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            objUbicacion_Expediente.setFOLIOS(rs.getInt("FOLIOS"));
            objUbicacion_Expediente.setNUMERO_EXPEDIENTE(rs.getString("NUMERO_EXPEDIENTE"));
            objUbicacion_Expediente.setCORRELATIVO(rs.getInt("CORRELATIVO"));
            objUbicacion_Expediente.setESTADO(rs.getInt("ESTADO"));
            objUbicacion_Expediente.setFECHA_LLEGADA(rs.getString("FECHA_LLEGADA"));
            objUbicacion_Expediente.setCODIGO_CARGO_ENVIA(rs.getInt("CODIGO_CARGO_ENVIA"));
            objUbicacion_Expediente.setCODIGO_CONTRIBUYENTE_ENVIA(rs.getInt("CODIGO_CONTRIBUYENTE_ENVIA"));
            objUbicacion_Expediente.setCODIGO_ENTIDAD_ENVIA(rs.getInt("CODIGO_ENTIDAD_ENVIA"));
            objUbicacion_Expediente.setCODIGO_CARGO_RECIBE(rs.getInt("CODIGO_CARGO_RECIBE"));
            objUbicacion_Expediente.setCODIGO_CONTRIBUYENTE_RECIBE(rs.getInt("CODIGO_CONTRIBUYENTE_RECIBE"));
            objUbicacion_Expediente.setCODIGO_ENTIDAD_RECIBE(rs.getInt("CODIGO_ENTIDAD_RECIBE"));
            objUbicacion_Expediente.setNOMBRE_CARGO_ENVIA_REMITENTE(rs.getString("NOMBRE_CARGO_ENVIA_REMITENTE"));
            objUbicacion_Expediente.setNOMBRE_AREA_RECIBE(rs.getString("NOMBRE_AREA_RECIBE"));
            objUbicacion_Expediente.setNOMBRE_CARGO_RECIBE_DESTINATARIO(rs.getString("NOMBRE_CARGO_RECIBE_DESTINATARIO"));
            objUbicacion_Expediente.setREMITENTE_CONTRIBUYENTE_EXPEDIENTE(rs.getString("REMITENTE_CONTRIBUYENTE_EXPEDIENTE"));
            objUbicacion_Expediente.setREMITENTE_ENTIDAD_EXPEDIENTE(rs.getString("REMITENTE_ENTIDAD_EXPEDIENTE"));
            objUbicacion_Expediente.setREMITENTE_CARGO_EXPEDIENTE(rs.getString("REMITENTE_CARGO_EXPEDIENTE"));
            objUbicacion_Expediente.setFECHA_INGRESO(rs.getString("FECHA_INGRESO"));
            objUbicacion_Expediente.setFECHA_RECEPCION(rs.getString("FECHA_RECEPCION"));
            objUbicacion_Expediente.setPROCEDIMIENTO(rs.getString("PROCEDIMIENTO"));
            objUbicacion_Expediente.setTIPO_DOCUMENTO(rs.getString("TIPO_DOCUMENTO"));
            objUbicacion_Expediente.setNUMERO_DOCUMENTO(rs.getString("NUMERO_DOCUMENTO"));
            objUbicacion_Expediente.setOBSERVACION(rs.getString("OBSERVACION"));
            list.add(objUbicacion_Expediente);
        }
        return list;
    }

    public List<Bean_Reporte_Estados_Expediente> Buscar_Expedientes_por_Numero(String Numero_Expediente, String Fecha_inicio, String Fecha_fin) throws Exception {
        cn = dt.getConnection();
        List<Bean_Reporte_Estados_Expediente> list = new ArrayList<Bean_Reporte_Estados_Expediente>();
        Bean_Reporte_Estados_Expediente ObjExpediente;
        PreparedStatement ps = cn.prepareStatement("SELECT \n"
                + "HE.nume_expe AS NUMERO_EXPEDIENTE,\n"
                + "E.fech_expe AS FECHA_INGRESO,\n"
                + "HE.fere_hist AS FECHA_RECEPCION,\n"
                + "HE.fesa_hist AS FECHA_TRAMITE,\n"
                + "HE.corr_hist AS CORRELATIVO,\n"
                + "DOCUMENTO.nomb_tiex AS TIPO_DOCUMENTO,\n"
                + "HE.nufo_hist AS FOLIOS,\n"
                + "E.codi_proc AS CODIGO_PROCEDIMIENTO,\n"
                + "PE.nomb_proc AS NOMBRE_PROCEDIMIENTO,\n"
                + "HE.caen_hist AS CODIGO_CARGO_ENVIA,\n"
                + "HE.care_hist AS CODIGO_CARGO_RECIBE,\n"
                + "E.codi_cont AS CODIGO_ADMINISTRADO_CONTRIBUYENTE,\n"
                + "E.codi_carg AS CODIGO_ADMINISTRADO_CARGO,\n"
                + "E.codi_enti AS CODIGO_ADMINISTRADO_ENTIDAD,\n"
                + "CARGO_ADMINISTRADO.nomb_carg AS NOMBRE_ADMINISTRADO_CARGO,\n"
                + "ENTIDAD_ADMINISTRADO.nomb_enti AS NOMBRE_ADMINISTRADO_ENTIDAD,\n"
                + "CONTRIBUYENTE_ADMINISTRADO.nomb_cont AS NOMBRE_ADMINISTRADO_CONTRIBUYENTE,\n"
                + "CARGO_ENVIA.nomb_carg||' - '||DEPENDENCIA_ENVIA.nomb_arde AS NOMBRE_CARGO_ENVIA,\n"
                + "CARGO_RECIBE.nomb_carg||' - '||DEPENDENCIA_RECIBE.nomb_arde AS NOMBRE_CARGO_RECIBE,\n"
                + "PROCEDIMIENTO.nomb_proc AS PROCEDIMIENTO_UTILIZADO,\n"
                + "E.feca_expe AS FECHA_CADUCIDAD\n"
                + "FROM expediente E \n"
                + "INNER JOIN historial_expediente HE ON E.nume_expe=HE.nume_expe\n"
                + "LEFT JOIN procedimiento_expediente PE ON E.codi_proc=PE.codi_proc\n"
                + "LEFT JOIN tipo_expediente DOCUMENTO ON DOCUMENTO.codi_tiex=E.codi_tiex\n"
                + "LEFT JOIN cargos CARGO_ENVIA ON CARGO_ENVIA.codi_carg=HE.caen_hist\n"
                + "LEFT JOIN cargos CARGO_RECIBE ON CARGO_RECIBE.codi_carg=HE.care_hist\n"
                + "LEFT JOIN cargos CARGO_ADMINISTRADO ON CARGO_ADMINISTRADO.codi_carg=E.codi_carg\n"
                + "LEFT JOIN entidad ENTIDAD_ADMINISTRADO ON ENTIDAD_ADMINISTRADO.codi_enti=E.codi_enti\n"
                + "LEFT JOIN contribuyente CONTRIBUYENTE_ADMINISTRADO ON CONTRIBUYENTE_ADMINISTRADO.codi_cont=E.codi_cont\n"
                + "LEFT JOIN area_dependencia DEPENDENCIA_ENVIA ON DEPENDENCIA_ENVIA.codi_arde=CARGO_ENVIA.codi_arde \n"
                + "LEFT JOIN area_dependencia DEPENDENCIA_RECIBE ON DEPENDENCIA_RECIBE.codi_arde=CARGO_RECIBE.codi_arde\n"
                + "LEFT JOIN procedimiento_expediente PROCEDIMIENTO ON PROCEDIMIENTO.codi_proc=E.codi_proc\n"
                + "WHERE to_date(CAST((E.fech_expe) as TEXT),'yyyy-MM-dd') >= '" + Fecha_inicio + "' \n"
                + "AND to_date(CAST((E.fech_expe) as TEXT),'yyyy-MM-dd') <= '" + Fecha_fin + "' \n"
                + "AND HE.nume_expe LIKE '%" + Numero_Expediente + "%' AND HE.condicion=1;");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            ObjExpediente = new Bean_Reporte_Estados_Expediente();
            ObjExpediente.setNUMERO_EXPEDIENTE(rs.getString("NUMERO_EXPEDIENTE"));
            ObjExpediente.setFECHA_INGRESO(rs.getString("FECHA_INGRESO"));
            ObjExpediente.setFECHA_RECEPCION(rs.getString("FECHA_RECEPCION"));
            ObjExpediente.setFECHA_TRAMITE(rs.getString("FECHA_TRAMITE"));
            ObjExpediente.setCORRELATIVO(rs.getInt("CORRELATIVO"));
            ObjExpediente.setFOLIOS(rs.getInt("FOLIOS"));
            ObjExpediente.setCODIGO_PROCEDIMIENTO(rs.getInt("CODIGO_PROCEDIMIENTO"));
            ObjExpediente.setTIPO_DOCUMENTO(rs.getString("TIPO_DOCUMENTO"));
            ObjExpediente.setCODIGO_CARGO_ENVIA(rs.getInt("CODIGO_CARGO_ENVIA"));
            ObjExpediente.setCODIGO_CARGO_RECIBE(rs.getInt("CODIGO_CARGO_RECIBE"));
            ObjExpediente.setCODIGO_ADMINISTRADO_CONTRIBUYENTE(rs.getInt("CODIGO_ADMINISTRADO_CONTRIBUYENTE"));
            ObjExpediente.setCODIGO_ADMINISTRADO_CARGO(rs.getInt("CODIGO_ADMINISTRADO_CARGO"));
            ObjExpediente.setCODIGO_ADMINISTRADO_ENTIDAD(rs.getInt("CODIGO_ADMINISTRADO_ENTIDAD"));
            ObjExpediente.setNOMBRE_ADMINISTRADO_CARGO(rs.getString("NOMBRE_ADMINISTRADO_CARGO"));
            ObjExpediente.setNOMBRE_ADMINISTRADO_ENTIDAD(rs.getString("NOMBRE_ADMINISTRADO_ENTIDAD"));
            ObjExpediente.setNOMBRE_ADMINISTRADO_CONTRIBUYENTE(rs.getString("NOMBRE_ADMINISTRADO_CONTRIBUYENTE"));
            if (rs.getString("NOMBRE_ADMINISTRADO_CARGO") != null) {
                ObjExpediente.setADMINISTRADO_CORRECTO(rs.getString("NOMBRE_ADMINISTRADO_CARGO"));
            } else {
                if (rs.getString("NOMBRE_ADMINISTRADO_ENTIDAD") != null) {
                    ObjExpediente.setADMINISTRADO_CORRECTO(rs.getString("NOMBRE_ADMINISTRADO_ENTIDAD"));
                } else {
                    if (rs.getString("NOMBRE_ADMINISTRADO_CONTRIBUYENTE") != null) {
                        ObjExpediente.setADMINISTRADO_CORRECTO(rs.getString("NOMBRE_ADMINISTRADO_CONTRIBUYENTE"));
                    }
                }
            }
            ObjExpediente.setNOMBRE_CARGO_ENVIA(rs.getString("NOMBRE_CARGO_ENVIA"));
            ObjExpediente.setNOMBRE_CARGO_RECIBE(rs.getString("NOMBRE_CARGO_RECIBE"));
            ObjExpediente.setPROCEDIMIENTO_UTILIZADO(rs.getString("PROCEDIMIENTO_UTILIZADO"));
            ObjExpediente.setFECHA_CADUCIDAD(rs.getString("FECHA_CADUCIDAD"));
            Messagebox.show("Numero de expediente:" + ObjExpediente.getNUMERO_EXPEDIENTE());
            list.add(ObjExpediente);
        }
        return list;
    }

    public void Insert_Expediente_Adjunto_New(String Expediente_Existente, int Usuario) throws SQLException {
        PreparedStatement Exp = cn.prepareStatement("select substring(e.nume_expe,8)||substring(e.nume_expe,0,7) as orden, e.nume_expe from expediente e order by 1 desc limit 1");
        ResultSet rs_exp = Exp.executeQuery();
        rs_exp.next();
        PreparedStatement ps = cn.prepareStatement("INSERT INTO expediente_adjunto_tramite(numero_expe_tramite,numero_adj_tramite,estado_adj_tramite,fecha_adj_tramite,usuario_adj_tramite) \n"
                + "VALUES(?,?,?,current_date,?);");
        ps.setString(1, rs_exp.getString("nume_expe"));
        ps.setString(2, Expediente_Existente);
        ps.setString(3, "A");
        ps.setInt(4, Usuario);
        ps.execute();
    }
    
    public void Insert_Expediente_Adjunto_Edit(String Expediente_Creado, String Expediente_Existente, int Usuario) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("INSERT INTO expediente_adjunto_tramite(numero_expe_tramite,numero_adj_tramite,estado_adj_tramite,fecha_adj_tramite,usuario_adj_tramite) \n"
                + "VALUES(?,?,?,current_date,?);");
        ps.setString(1, Expediente_Creado);
        ps.setString(2, Expediente_Existente);
        ps.setString(3, "A");
        ps.setInt(4, Usuario);
        ps.execute();
    }
    
    public void Update_Expediente_Adjunto_Edit(String Expediente_Adjuntar, String Expediente_Existente) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("UPDATE expediente_adjunto_tramite SET numero_adj_tramite = '"+Expediente_Adjuntar+"' WHERE numero_expe_tramite = '"+Expediente_Existente+"'");
        ps.execute();
    }

    public List<BeanVerDocumentos> getListDocumentos(String Numero_expe) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select h.nume_expe as expediente,h.corr_hist as movimiento ,c1.nomb_carg ||' '||ad.nomb_arde as destino\n"
                + "from historial_expediente h\n"
                + "inner join cargos c1 on c1.codi_carg = h.care_hist\n"
                + "inner join area_dependencia ad on ad.codi_arde = c1.codi_arde\n"
                + "where h.nume_expe = '" + Numero_expe + "'");
        ResultSet rs = ps.executeQuery();
        List<BeanVerDocumentos> list = new ArrayList<BeanVerDocumentos>();
        BeanVerDocumentos ObjVerDoc;
        while (rs.next()) {
            ObjVerDoc = new BeanVerDocumentos();
            ObjVerDoc.setNumero_documento(rs.getString("expediente"));
            ObjVerDoc.setMovimiento(rs.getInt("movimiento"));
            ObjVerDoc.setDestino(rs.getString("destino"));
            list.add(ObjVerDoc);
        }
        return list;
    }

    public String getExpediente_Adjunto(String Numero_Expediente) throws SQLException {
        
        PreparedStatement ps = cn.prepareStatement("SELECT eat.numero_adj_tramite FROM expediente_adjunto_tramite eat WHERE eat.numero_expe_tramite = '" + Numero_Expediente + "';");
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("numero_adj_tramite");
    }

    

}
