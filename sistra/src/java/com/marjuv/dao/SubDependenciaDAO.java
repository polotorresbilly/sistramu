package com.marjuv.dao;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.beans_view.BeanSubDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.SubDependenciaTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.CollectionsX;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;

public class SubDependenciaDAO {

    protected DataTransaction dt;
    protected Connection cn;

    public SubDependenciaDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public boolean validate_desc_nuevo(String name) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM sub_dependencia WHERE desc_sude=? AND esta_sude='A'");
        ps.setString(1, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validate_desc_editar(int codigo, String name) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM sub_dependencia WHERE codi_sude !=? AND desc_sude=? AND esta_sude='A'");
        ps.setInt(1, codigo);
        ps.setString(2, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void insertSubDependencia(SubDependenciaTO objSubDependenciaTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO sub_dependencia(desc_sude,esta_sude,codi_arde) VALUES(?,?,?)");
            ps.setString(1, objSubDependenciaTO.getDesc_sude());
            ps.setString(2, "A");
            ps.setInt(3, objSubDependenciaTO.getCodi_arde());
            ps.execute();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception INSERT SUBDEPENDENCIA: " + e.getMessage(), "INSERT SUBDEPENDENCIA", Messagebox.OK, Messagebox.EXCLAMATION);
        }

    }

    public void updateSubDependencia(SubDependenciaTO objSubDependenciaTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE sub_dependencia SET desc_sude=?, codi_arde=? WHERE codi_sude=?");
            ps.setString(1, objSubDependenciaTO.getDesc_sude());
            ps.setInt(2, objSubDependenciaTO.getCodi_arde());
            ps.setInt(3, objSubDependenciaTO.getCodi_sude());
            ps.execute();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception UPDATE SUBDEPENDENCIA: " + e.getMessage(), "UPDATE SUBDEPENDENCIA", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void deleteSubDependencia(int codi_sude) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE sub_dependencia SET esta_sude='D' WHERE codi_sude=?");
        ps.setInt(1, codi_sude);
        ps.execute();

    }

    public List<BeanSubDependencias> getSubDependencias() throws SQLException {
        List<BeanSubDependencias> lista = new ArrayList<BeanSubDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT sd.codi_sude,sd.desc_sude,d.nomb_arde,sd.codi_arde FROM area_dependencia d, sub_dependencia sd WHERE esta_sude = 'A' AND d.codi_arde = sd.codi_arde");
        BeanSubDependencias BeanSubDependencia;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            BeanSubDependencia = new BeanSubDependencias();
            BeanSubDependencia.setCodi_sude(rs.getInt(1));
            BeanSubDependencia.setDesc_sude(rs.getString(2));
            BeanSubDependencia.setNomb_arde(rs.getString(3));
            BeanSubDependencia.setCodi_arde(rs.getInt(4));

            lista.add(BeanSubDependencia);
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> getDependencia() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde = 'A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }

}
