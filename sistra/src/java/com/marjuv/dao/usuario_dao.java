package com.marjuv.dao;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.usuario;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;

public class usuario_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public usuario_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public String get_Dependencia(int codi_arde) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select nomb_arde from area_dependencia where codi_arde=?");
        ps.setInt(1, codi_arde);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(1);
    }

    public int update_password(int codi_usua, String pass_usua, String password_new, String password_confirm) throws Exception {
        int var = 0;
        PreparedStatement ps1 = cn.prepareStatement("SELECT pass_usua FROM usuario WHERE codi_usua=?");
        ps1.setInt(1, codi_usua);
        ResultSet rs = ps1.executeQuery();
        rs.next();
        String password = pass_usua;

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte byteData[] = md.digest();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }

        if (rs.getString(1).equals(sb.toString())) {
            PreparedStatement ps = cn.prepareStatement("UPDATE usuario SET pass_usua=md5(?) WHERE codi_usua=?");
            if (password_confirm.equals(password_new.toUpperCase())) {
                ps.setString(1, password_new.toUpperCase());
                ps.setInt(2, codi_usua);
                ps.execute();
                var = 1;
                return var;
            } else {
                Messagebox.show("Su nueva contraseña es incorrecta", "Su nueva contraseña no coincide", Messagebox.OK, Messagebox.EXCLAMATION);
            }
        } else {
            Messagebox.show("Su contraseña actual es incorrecta", "Error de contraseña actual", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        return var;
    }

    public String nuevo_nume_usua(String nomb_rol, String nomb_arde) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as c from usuario");
        ResultSet rs = ps.executeQuery();
        rs.next();
        int count = rs.getInt(1) + 1;
        boolean sw = true;
        String nume_usua = "", code, code2, rol, arde;
        rol = nomb_rol.substring(0, 1);
        arde = nomb_arde.substring(0, 2);
        while (sw) {
            code = "000000" + count;
            code2 = code.substring(code.length() - 6);
            nume_usua = rol.toUpperCase() + arde.toUpperCase() + code2;
            ps = cn.prepareStatement("select count(*) as c from usuario where nume_usua=?");
            ps.setString(1, nume_usua);
            rs = ps.executeQuery();
            rs.next();
            if (rs.getInt(1) == 0) {
                sw = false;
            } else {
                count++;
            }
        }
        return nume_usua;
    }

    public void insert(usuario usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO usuario(codi_arde, nomb_usua, apel_usua, dni_usua, fere_usua, nick_usua, "
                + "pass_usua, esta_usua, codi_rous, nume_usua, codi_carg)"
                + "VALUES(?,?,?,?,?,?,md5(?),?,?,?,?)");
        ps.setInt(1, usuario.getCodi_arde());
        ps.setString(2, usuario.getNomb_usua().toUpperCase());
        ps.setString(3, usuario.getApel_usua().toUpperCase());
        ps.setInt(4, usuario.getDni_usua());
        ps.setDate(5, usuario.getFere_usua());
        ps.setString(6, usuario.getNick_usua().toUpperCase());
        ps.setString(7, usuario.getPass_usua().toUpperCase());
        ps.setString(8, usuario.getEsta_usua().toUpperCase());
        ps.setInt(9, usuario.getCodi_rous());
        ps.setString(10, usuario.getNume_usua().toUpperCase());
        ps.setInt(11, usuario.getCodi_carg());
        ps.execute();

        ps = cn.prepareStatement("SELECT codi_usua FROM usuario ORDER BY codi_usua DESC LIMIT 1");
        ResultSet rs = ps.executeQuery();
        rs.next();
        int codi_usua = rs.getInt(1);

        for (int i = 0; i < usuario.getPermisos().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO permisos_usuario(codi_usua, codi_perm) VALUES(?,?)");
            ps.setInt(1, codi_usua);
            ps.setInt(2, usuario.getPermisos().get(i).getCodi_perm());
            ps.execute();
        }

    }

    public void update(usuario usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE usuario SET codi_arde=?, nomb_usua=?, apel_usua=?, dni_usua=?, nick_usua=?, "
                + "codi_rous=?, nume_usua=?, codi_carg=? WHERE codi_usua=?");
        ps.setInt(1, usuario.getCodi_arde());
        ps.setString(2, usuario.getNomb_usua());
        ps.setString(3, usuario.getApel_usua());
        ps.setInt(4, usuario.getDni_usua());
        ps.setString(5, usuario.getNick_usua());
        ps.setInt(6, usuario.getCodi_rous());
        ps.setString(7, usuario.getNume_usua());
        ps.setInt(8, usuario.getCodi_carg());
        ps.setInt(9, usuario.getCodi_usua());
        ps.execute();

        ps = cn.prepareStatement("DELETE FROM permisos_usuario WHERE codi_usua=?");
        ps.setInt(1, usuario.getCodi_usua());
        ps.execute();

        for (int i = 0; i < usuario.getPermisos().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO permisos_usuario(codi_usua, codi_perm) VALUES(?,?)");
            ps.setInt(1, usuario.getCodi_usua());
            ps.setInt(2, usuario.getPermisos().get(i).getCodi_perm());
            ps.execute();
        }
    }

    public void update_pass(usuario usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE usuario SET pass_usua=md5(?) WHERE codi_usua=?");
        ps.setString(1, usuario.getPass_usua());
        ps.setInt(2, usuario.getCodi_usua());
        ps.execute();
    }

    public void delete(int codi_usua) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE usuario SET esta_usua='D' WHERE codi_usua=?");
        ps.setInt(1, codi_usua);
        ps.execute();
    }

    public int validar_usuario(usuario usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from usuario where nick_usua=? AND pass_usua=md5(?) AND esta_usua='A'");
        ps.setString(1, usuario.getNick_usua().toUpperCase());
        ps.setString(2, usuario.getPass_usua().toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt("contador") == 1) {
            ps = cn.prepareStatement("select * from usuario where nick_usua=? AND pass_usua=md5(?) AND esta_usua='A'");
            ps.setString(1, usuario.getNick_usua().toUpperCase());
            ps.setString(2, usuario.getPass_usua().toUpperCase());
            rs = ps.executeQuery();
            rs.next();
            int id = rs.getInt(1);
            ps = cn.prepareStatement("INSERT INTO public.auditoria_acceso( "
                    + " loginusuario, fecha, codi_even) "
                    + " VALUES (?, now(), 1)");
            ps.setString(1, usuario.getNick_usua().toUpperCase());
            ps.execute();

            return id;
        } else {
            ps = cn.prepareStatement("INSERT INTO public.auditoria_acceso( "
                    + " loginusuario, fecha, codi_even) "
                    + " VALUES (?, now(), 2)");
            ps.setString(1, usuario.getNick_usua().toUpperCase());
            ps.execute();
            return -1;
        }
    }

    public boolean verificar_nuevo_usuario(String nick) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from usuario where nick_usua=? AND esta_usua='A'");
        ps.setString(1, nick);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar_usuario(int codigo, String nick) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from usuario where codi_usua!=? AND nick_usua=? AND esta_usua='A'");
        ps.setInt(1, codigo);
        ps.setString(2, nick);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_nuevo_dni(int dni) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from usuario where dni_usua=? AND esta_usua='A'");
        ps.setInt(1, dni);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar_dni(int codigo, int dni) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from usuario where codi_usua!=? AND dni_usua=? AND esta_usua='A'");
        ps.setInt(1, codigo);
        ps.setInt(2, dni);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public usuario get_usuario(int codi_usua) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from usuario where codi_usua=? AND esta_usua = 'A'");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        rs.next();
        usuario obj_usuario = new usuario();
        obj_usuario.setCodi_usua(rs.getInt(1));
        obj_usuario.setCodi_arde(rs.getInt(2));
        obj_usuario.setNomb_usua(rs.getString(3));
        obj_usuario.setApel_usua(rs.getString(4));
        obj_usuario.setDni_usua(rs.getInt(5));
        obj_usuario.setFere_usua(rs.getDate(6));
        obj_usuario.setNick_usua(rs.getString(7));
        obj_usuario.setPass_usua(rs.getString(8));
        obj_usuario.setEsta_usua(rs.getString(9));
        obj_usuario.setCodi_rous(rs.getInt(10));
        obj_usuario.setNume_usua(rs.getString(11));
        obj_usuario.setCodi_carg(rs.getInt(12));
        obj_usuario.setNoms_usua(rs.getString("nomb_usua") + " " + rs.getString("apel_usua"));
        return obj_usuario;
    }

    public List<BeanUsuarioMan> get_usuarios() throws Exception {
        List<BeanUsuarioMan> lista = new ArrayList<BeanUsuarioMan>();
        PreparedStatement ps = cn.prepareStatement("select u.*, r.nomb_rol, a.nomb_arde, x.nomb_carg from usuario u, rol_usuario r, area_dependencia a, cargos x where u.codi_rous=r.codi_rous and u.codi_arde=a.codi_arde and u.esta_usua='A' and u.codi_carg=x.codi_carg ORDER BY 1");
        BeanUsuarioMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanUsuarioMan();
            bu.setCodi_usua(rs.getInt(1));
            bu.setCodi_arde(rs.getInt(2));
            bu.setNomb_usua(rs.getString(3));
            bu.setApel_usua(rs.getString(4));
            bu.setDni_usua(rs.getInt(5));
            bu.setFere_usua(rs.getString(6));
            bu.setNick_usua(rs.getString(7));
            bu.setPass_usua(rs.getString(8));
            bu.setEsta_usua(rs.getString(9));
            bu.setCodi_rous(rs.getString(10));
            bu.setCodi_carg(rs.getInt(12));
            bu.setNomb_rol(rs.getString(13));
            bu.setNomb_arde(rs.getString(14));
            bu.setNomb_carg(rs.getString(15));
            bu.setNume_usua(rs.getString(11));
            lista.add(bu);
        }
        return lista;
    }

    public List<usuario> search_usuario(String search) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM usuario "
                + " WHERE esta_usua='A' and (nomb_usua like '%" + search + "%' or apel_usua like '%" + search + "%' or nume_usua like '%" + search + "%')");
        List<usuario> lista = new ArrayList<usuario>();
        usuario obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            obj = new usuario();
            obj.setCodi_usua(rs.getInt("codi_usua"));
            obj.setNume_usua(rs.getString("nume_usua"));
            obj.setDni_usua(rs.getInt("dni_usua"));
            obj.setNoms_usua(rs.getString("nomb_usua") + " " + rs.getString("apel_usua"));

            lista.add(obj);
        }
        return lista;
    }

    public String get_noms(int codi_usua) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM usuario WHERE codi_usua=?");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("nomb_usua") + " " + rs.getString("apel_usua");
    }

    public String get_nume(int codi_usua) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM usuario WHERE codi_usua=?");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("nume_usua");
    }

    public String get_dni(int codi_usua) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM usuario WHERE codi_usua=?");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("dni_usua");
    }

    public int get_codi(String nume) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM usuario WHERE nume_usua=?");
        ps.setString(1, nume);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("codi_usua");
    }

    public List<Pair<Integer, String>> llenarComboUsuarios_Expedientes(int Codigo_Cargo) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT U.codi_usua,U.nomb_usua||' - '||U.apel_usua AS usuario FROM usuario U\n"
                + "INNER JOIN cargos C ON C.codi_carg = U.codi_carg\n"
                + "WHERE C.codi_carg = "+Codigo_Cargo+" AND U.esta_usua = 'A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt("codi_usua"), rs.getString("usuario")));
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
