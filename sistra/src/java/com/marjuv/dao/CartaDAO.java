/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.CartaTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author BILLY
 */
public class CartaDAO {
    protected DataTransaction dt;
    protected Connection cn;

    public CartaDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    public void close() throws SQLException {
        cn.close();
    }
    
    public void insert_carta(CartaTO obj)throws Exception {
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.carta(nume_expe, corr_hist, nude_hist, fech_cart, codi_usua,obsv_cart) VALUES (?, ?, ?, NOW(), ?, ?);");
        ps.setString(1, obj.getNume_expe().toUpperCase());
        ps.setInt(2, obj.getCorr_hist());
        ps.setInt(3, obj.getNude_hist());
        ps.setInt(4, obj.getCodi_usua());
        ps.setString(5, obj.getObsv_cart().toUpperCase());
        ps.execute();
    }
}
