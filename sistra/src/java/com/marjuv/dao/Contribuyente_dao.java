/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.dao;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Contribuyente;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Provincia;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BILLY
 */
public class Contribuyente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public Contribuyente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void close() throws SQLException {
        cn.close();
    }

    public boolean verificar_nuevo_dni(int dni) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from contribuyente where dni_cont=? AND esta_cont='A'");
        ps.setInt(1, dni);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar_dni(int codigo, int dni) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from contribuyente where codi_cont!=? AND dni_cont=? AND esta_cont='A'");
        ps.setInt(1, codigo);
        ps.setInt(2, dni);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String get_nomb(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from contribuyente where codi_cont=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(2) + " " + rs.getString(3);
    }

    public List<Contribuyente> get_contribuyentes() throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_cont, nomb_cont,dni_cont, gene_cont, celu_cont, telf_cont, dire_cont, emai_cont,esta_cont, DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa  FROM public.contribuyente CO INNER JOIN distrito DI ON DI.codi_dist=CO.codi_dist INNER JOIN provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN departamento DE ON DE.codi_depa=PO.codi_depa where esta_cont='A' ORDER BY 1");
        ResultSet rs = ps.executeQuery();
        List<Contribuyente> lista_contribuyente = new ArrayList<Contribuyente>();
        while (rs.next()) {
            Contribuyente obj = new Contribuyente();
            obj.setCodi_cont(rs.getInt("codi_cont"));
            obj.setNomb_cont(rs.getString("nomb_cont"));
            
            
            obj.setDni_cont(rs.getInt("dni_cont"));
            obj.setGene_cont(rs.getString("gene_cont"));
            obj.setCelu_cont(rs.getString("celu_cont"));
            obj.setTelf_cont(rs.getString("telf_cont"));
            obj.setDire_cont(rs.getString("dire_cont"));
            obj.setEmai_cont(rs.getString("emai_cont"));
            obj.setEsta_cont(rs.getString("esta_cont"));
            obj.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
            lista_contribuyente.add(obj);
        }
        return lista_contribuyente;
    }

    public Contribuyente get_contribuyente(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_cont, nomb_cont,dni_cont, gene_cont, celu_cont, telf_cont, dire_cont, emai_cont,esta_cont, DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa  FROM public.contribuyente CO INNER JOIN distrito DI ON DI.codi_dist=CO.codi_dist INNER JOIN provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN departamento DE ON DE.codi_depa=PO.codi_depa where codi_cont=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        Contribuyente obj = new Contribuyente();
        obj.setCodi_cont(rs.getInt("codi_cont"));
        obj.setNomb_cont(rs.getString("nomb_cont"));
        
        
        obj.setDni_cont(rs.getInt("dni_cont"));
        obj.setGene_cont(rs.getString("gene_cont"));
        obj.setCelu_cont(rs.getString("celu_cont"));
        obj.setTelf_cont(rs.getString("telf_cont"));
        obj.setDire_cont(rs.getString("dire_cont"));
        obj.setEmai_cont(rs.getString("emai_cont"));
        obj.setEsta_cont(rs.getString("esta_cont"));
        obj.setObj_dist(new Distrito(
                rs.getInt("codi_dist"),
                rs.getString("nomb_dist"),
                new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                        new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        return obj;
    }

    public Contribuyente get_contribuyente(Contribuyente obj2) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_cont, nomb_cont,dni_cont, gene_cont, celu_cont, telf_cont, dire_cont, emai_cont,esta_cont, DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa  FROM public.contribuyente CO INNER JOIN distrito DI ON DI.codi_dist=CO.codi_dist INNER JOIN provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN departamento DE ON DE.codi_depa=PO.codi_depa "
                + "where nomb_cont=? and apel_cont=? and dni_cont=?");
        ps.setString(1, obj2.getNomb_cont());
        ps.setString(2, obj2.getApel_cont());
        ps.setInt(3, obj2.getDni_cont());
        ResultSet rs = ps.executeQuery();
        rs.next();
        Contribuyente obj = new Contribuyente();
        obj.setCodi_cont(rs.getInt("codi_cont"));
        obj.setNomb_cont(rs.getString("nomb_cont"));
        
        
        obj.setDni_cont(rs.getInt("dni_cont"));
        obj.setGene_cont(rs.getString("gene_cont"));
        obj.setCelu_cont(rs.getString("celu_cont"));
        obj.setTelf_cont(rs.getString("telf_cont"));
        obj.setDire_cont(rs.getString("dire_cont"));
        obj.setEmai_cont(rs.getString("emai_cont"));
        obj.setEsta_cont(rs.getString("esta_cont"));
        obj.setObj_dist(new Distrito(
                rs.getInt("codi_dist"),
                rs.getString("nomb_dist"),
                new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                        new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        return obj;
    }

    public boolean insert_contribuyente(Contribuyente obj) throws Exception {
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.contribuyente(nomb_cont, apel_cont, dni_cont, gene_cont,celu_cont, telf_cont, dire_cont, emai_cont, codi_dist, esta_cont) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 'A');");
        ps.setString(1, obj.getNomb_cont().toUpperCase());
        ps.setString(2, "");
//        ps.setDate(3, new Date(obj.getDfen_cont().getTime()));
//        System.out.print(new Date(obj.getDfen_cont().getTime()));
        ps.setInt(3, obj.getDni_cont());
        ps.setString(4, obj.getGene_cont().toUpperCase());
        ps.setString(5, obj.getCelu_cont().toUpperCase());
        ps.setString(6, obj.getTelf_cont().toUpperCase());
        ps.setString(7, obj.getDire_cont().toUpperCase());
        ps.setString(8, obj.getEmai_cont());
        ps.setInt(9, obj.getObj_dist().getCodi_dist());
        ps.execute();
        return true;
    }

    public boolean update_contribuyente(Contribuyente obj) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE public.contribuyente SET nomb_cont=?, dni_cont=?, gene_cont=?, celu_cont=?, telf_cont=?, dire_cont=?, emai_cont=?, codi_dist=? WHERE codi_cont=?");
        ps.setString(1, obj.getNomb_cont().toUpperCase());
        ps.setString(2,"");
        
        ps.setInt(3, obj.getDni_cont());
        ps.setString(4, obj.getGene_cont().toUpperCase());
        ps.setString(5, obj.getCelu_cont().toUpperCase());
        ps.setString(6, obj.getTelf_cont().toUpperCase());
        ps.setString(7, obj.getDire_cont().toUpperCase());
        ps.setString(8, obj.getEmai_cont().toUpperCase());
        ps.setInt(9, obj.getObj_dist().getCodi_dist());
        ps.setInt(10, obj.getCodi_cont());
        ps.execute();
        return true;
    }

    public boolean delete_contribuyente(int codi_cont) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE public.contribuyente SET esta_cont='D' WHERE codi_cont=?");
        ps.setInt(1, codi_cont);
        ps.execute();
        return true;
    }

    public List<Contribuyente> search_contribuyente(String search) throws SQLException {
        List<Contribuyente> lista = new ArrayList<Contribuyente>();
        PreparedStatement ps = cn.prepareStatement(
                "SELECT codi_cont, nomb_cont, dni_cont, gene_cont, celu_cont, telf_cont, dire_cont, emai_cont,esta_cont, DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa  FROM public.contribuyente CO INNER JOIN distrito DI ON DI.codi_dist=CO.codi_dist INNER JOIN provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN departamento DE ON DE.codi_depa=PO.codi_depa "
                + "where esta_cont='A' AND "
                + "(nomb_cont like '%" + search.toUpperCase() + "%' or CAST (codi_cont AS character varying) like '%" + search.toUpperCase() + "%' or apel_cont like '%" + search.toUpperCase() + "%')");
        Contribuyente bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new Contribuyente();
            bu.setCodi_cont(rs.getInt("codi_cont"));
            bu.setNomb_cont(rs.getString("nomb_cont"));
            
            
            bu.setDni_cont(rs.getInt("dni_cont"));
            bu.setGene_cont(rs.getString("gene_cont"));
            bu.setCelu_cont(rs.getString("celu_cont"));
            bu.setTelf_cont(rs.getString("telf_cont"));
            bu.setDire_cont(rs.getString("dire_cont"));
            bu.setEmai_cont(rs.getString("emai_cont"));
            bu.setEsta_cont(rs.getString("esta_cont"));
            bu.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
            lista.add(bu);
        }
        return lista;
    }
}
