
package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Cargo;
import com.marjuv.entity.Departamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Cargo_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public Cargo_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    public void close() throws SQLException {
        cn.close();
    }

    public List<Cargo> get_cargo_area(int codi_arde) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_carg, esta_carg, nomb_carg, codi_arde FROM public.cargos WHERE codi_arde=?;");
        ps.setInt(1, codi_arde);
        List<Cargo> lista = new ArrayList<Cargo>();
        Cargo obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            obj = new Cargo(
                    rs.getInt("codi_carg"),
                    rs.getString("nomb_carg"),
                    rs.getInt("codi_arde"),
                    rs.getString("esta_carg"));
            lista.add(obj);
        }
        return lista;
    }
}
