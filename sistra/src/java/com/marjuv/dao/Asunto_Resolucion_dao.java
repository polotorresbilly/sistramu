/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.dao;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Asunto_Resolucion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;

/**
 *
 * @author BILLY
 */
public class Asunto_Resolucion_dao {
    protected DataTransaction dt;
    protected Connection cn;

    public Asunto_Resolucion_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public void close() throws SQLException {
        cn.close();
    }
    
    public List<Asunto_Resolucion> get_asunto_all()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT asur.codi_asre, desc_asre FROM public.asunto_resolucion ASUR WHERE ASUR.esta_asre='A';");
        ResultSet rs = ps.executeQuery();
        List<Asunto_Resolucion> lista = new ArrayList<Asunto_Resolucion>();
        while (rs.next()) {            
            Asunto_Resolucion obj = new Asunto_Resolucion();
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setDesc_asre(rs.getString("desc_asre"));
            lista.add(obj);
        }
        return lista;
    }
    
    public List<Asunto_Resolucion> get_asunto_no_dependencia(int codi_arde)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT asur.codi_asre, desc_asre,esta_asre FROM public.asunto_resolucion ASUR WHERE ASUR.esta_asre='A' and asur.codi_asre not in (select asde.codi_asre FROM asunto_destino_defecto asde WHERE asde.codi_arde=?)");
        ps.setInt(1, codi_arde);
        ResultSet rs = ps.executeQuery();
        List<Asunto_Resolucion> lista = new ArrayList<Asunto_Resolucion>();
        while (rs.next()) {            
            Asunto_Resolucion obj = new Asunto_Resolucion();
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setDesc_asre(rs.getString("desc_asre"));
            lista.add(obj);
        }
        return lista;
    }
    
    public List<Asunto_Resolucion> get_asunto_dependencia(int codi_arde)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT asur.codi_asre, desc_asre FROM public.asunto_resolucion ASUR INNER JOIN asunto_destino_defecto ASDF ON ASUR.codi_asre=ASDF.codi_asre WHERE ASDF.codi_arde=?;");
        ps.setInt(1, codi_arde);
        ResultSet rs = ps.executeQuery();
        List<Asunto_Resolucion> lista = new ArrayList<Asunto_Resolucion>();
        while (rs.next()) {            
            Asunto_Resolucion obj = new Asunto_Resolucion();
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setDesc_asre(rs.getString("desc_asre"));
            lista.add(obj);
        }
        return lista;
    }
    
    public List<BeanDependencias> get_dependencia_asunto(int codi_asre) throws Exception{
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT d.codi_arde,d.nomb_arde,d.esta_arde,d.abre_arde,d.jera_arde,d.codi_enti,e.nomb_enti FROM area_dependencia d INNER JOIN entidad e ON d.codi_enti=e.codi_enti INNER JOIN asunto_destino_defecto asd ON asd.codi_arde=d.codi_arde WHERE esta_arde = 'A' and asd.codi_asre=?");
        BeanDependencias BeanD;ps.setInt(1, codi_asre);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNomb_enti(rs.getString(7));
            lista.add(BeanD);
        }
        return lista;
    }
    
    public List<BeanDependencias> get_dependencia_no_asunto(int codi_asre) throws Exception{
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT d.codi_arde,d.nomb_arde,d.esta_arde,d.abre_arde,d.jera_arde,d.codi_enti,e.nomb_enti FROM area_dependencia d INNER JOIN entidad e ON d.codi_enti=e.codi_enti WHERE esta_arde = 'A' and d.codi_arde NOT IN (select asd.codi_arde from asunto_destino_defecto asd where asd.codi_asre=? )");
        BeanDependencias BeanD;ps.setInt(1, codi_asre);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNomb_enti(rs.getString(7));
            lista.add(BeanD);
        }
        return lista;
    }
    
    public void delete_asunto_dependencia(int codigo_dependencia,int codi_asre) throws Exception{
        PreparedStatement ps = cn.prepareStatement("DELETE FROM public.asunto_destino_defecto WHERE codi_arde=? and codi_asre=?;");
        ps.setInt(1, codigo_dependencia);
        ps.setInt(2, codi_asre);
        ps.execute();
    }
    
    public void insert_asunto_dependencia(int codigo_dependencia,int codi_asre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.asunto_destino_defecto(codi_arde, codi_asre) VALUES (?, ?);");
        ps.setInt(1, codigo_dependencia);
        ps.setInt(2, codi_asre);
        ps.execute();
    }
    public void delete_dependencia_asunto(int codigo_dependencia,int codi_asre) throws Exception{
        PreparedStatement ps = cn.prepareStatement("DELETE FROM public.asunto_destino_defecto WHERE codi_arde=? and codi_asre=?;");
        ps.setInt(1, codigo_dependencia);
        ps.setInt(2, codi_asre);
        ps.execute();
    }
    
    public void insert_dependencia_asunto(int codigo_dependencia,int codi_asre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.asunto_destino_defecto(codi_arde, codi_asre) VALUES (?, ?);");
        ps.setInt(1, codigo_dependencia);
        ps.setInt(2, codi_asre);
        ps.execute();
    }
    
    public ArrayList<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from asunto_resolucion where esta_asre='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
    
    
}