/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.Provincia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BILLY
 */
public class Distrito_dao {
    
    protected DataTransaction dt;
    protected Connection cn;

    public Distrito_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public List<Distrito> listar_distrito(int codi_prov)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT codi_dist, nomb_dist, DI.codi_prov,PR.nomb_prov,PR.codi_depa,DE.nomb_depa FROM public.distrito DI INNER JOIN provincia PR ON DI.codi_prov=PR.codi_prov INNER JOIN departamento DE ON DE.codi_depa=PR.codi_depa WHERE DI.codi_prov=?;");
        ps.setInt(1, codi_prov);
        List<Distrito> lista = new ArrayList<Distrito>();
        Distrito obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            obj = new Distrito(
               rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"), 
                                  new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))
            );
            lista.add(obj);
        }
        return lista;
    }
    
     public void close() throws SQLException {
        cn.close();
    }
}
