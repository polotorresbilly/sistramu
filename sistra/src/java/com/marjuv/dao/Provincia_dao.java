/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Provincia;
import com.marjuv.entity.Provincia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BILLY
 */
public class Provincia_dao {
    protected DataTransaction dt;
    protected Connection cn;

    public Provincia_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public List<Provincia> listar_provincia(int codi_depa)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT PR.codi_prov,PR.nomb_prov,PR.codi_depa,DE.nomb_depa FROM provincia PR INNER JOIN departamento DE ON DE.codi_depa=PR.codi_depa WHERE PR.codi_depa=?;");
        ps.setInt(1, codi_depa);
        List<Provincia> lista = new ArrayList<Provincia>();
        Provincia obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            obj = new Provincia(
               rs.getInt("codi_prov"),
               rs.getString("nomb_prov"),
               new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa"))
            );
            lista.add(obj);
        }
        return lista;
    }
     public void close() throws SQLException {
        cn.close();
    }
}
