package com.marjuv.dao;

import com.marjuv.beans_view.BeanAsuntoResolucion;
import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.AsuntoResolucionTO;
import com.marjuv.entity.TipoResolucionTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.Messagebox;

public class AsuntoResolucionDAO {
    
    protected DataTransaction dt;
    protected Connection cn;

    public AsuntoResolucionDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public void insert(AsuntoResolucionTO objAsuntoResolucionTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO asunto_resolucion(desc_asre,esta_asre) VALUES (?,?)");
            ps.setString(1, objAsuntoResolucionTO.getDesc_asre().toUpperCase());
            ps.setString(2, "A");
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Insert(Asunto Resolucion)" + e.getMessage(), "Insert Tipo Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void update(AsuntoResolucionTO objAsuntoResolucionTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE asunto_resolucion SET desc_asre=? WHERE codi_asre=?");
            ps.setString(1, objAsuntoResolucionTO.getDesc_asre().toUpperCase());
            ps.setInt(2, objAsuntoResolucionTO.getCodi_asre());
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Update(Asunto Resolución): " + e.getMessage(), "Update Asunto Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void delete(int codigo) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE asunto_resolucion SET esta_asre = 'D' WHERE codi_asre = ?");
            ps.setInt(1,codigo);
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Delete(Asunto Resolución)" + e.getMessage(), "Delete Asunto Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public List<BeanAsuntoResolucion> getListAsuntoResolucion() throws Exception {
        List<BeanAsuntoResolucion> Lista = new ArrayList<BeanAsuntoResolucion>();
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM asunto_resolucion WHERE esta_asre = 'A' ORDER BY 1");
        ResultSet rs = ps.executeQuery();
        BeanAsuntoResolucion objAsuntoResolucion;
        while (rs.next()) {
            objAsuntoResolucion = new BeanAsuntoResolucion();
            objAsuntoResolucion.setCodi_asre(rs.getInt(1));
            objAsuntoResolucion.setDesc_asre(rs.getString(2));
            objAsuntoResolucion.setEsta_asre(rs.getString(3));
            Lista.add(objAsuntoResolucion);
        }
        return Lista;

    }
    
    public boolean v_desc_asre_new(String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM asunto_resolucion WHERE desc_asre =? AND esta_asre='A'");
        ps.setString(1, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean v_desc_asre_edit(int codigo,String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM asunto_resolucion WHERE codi_asre !=? AND desc_asre =? AND esta_asre='A'");
        ps.setInt(1, codigo);
        ps.setString(2, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public void close() throws SQLException {
        cn.close();
    }
    
}
