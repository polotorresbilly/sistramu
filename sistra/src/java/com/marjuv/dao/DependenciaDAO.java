
package com.marjuv.dao;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.DependenciaTO;
import com.marjuv.entity.EntidadTO;
import com.marjuv.entity.usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;

public class DependenciaDAO {
    
    protected DataTransaction dt;
    protected Connection cn;

    public DependenciaDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public boolean validate_nameDependencia_nuevo(String name) throws Exception {
        
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE nomb_arde=? AND esta_arde='A'");
        ps.setString(1, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean validate_nameDependencia_editar(int codigo,String name) throws Exception {
        
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE codi_arde !=? AND nomb_arde=? AND esta_arde='A'");
        ps.setInt(1, codigo);
        ps.setString(2, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean validate_abrevDependencia_nuevo(String name) throws Exception {
        
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE abre_arde=? AND esta_arde='A'");
        ps.setString(1, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean validate_abrevDependencia_editar(int codigo,String name) throws Exception {
        
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS contador FROM area_dependencia WHERE codi_arde !=? AND abre_arde=? AND esta_arde='A'");
        ps.setInt(1, codigo);
        ps.setString(2, name);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public void insertDependencia(DependenciaTO objDependenciaTO)throws Exception{
        PreparedStatement ps = cn.prepareStatement("INSERT INTO area_dependencia(nomb_arde,abre_arde,jera_arde,codi_enti,esta_arde) VALUES(?,?,?,?,?)");
        ps.setString(1,objDependenciaTO.getNomb_arde());
        ps.setString(2,objDependenciaTO.getAbre_arde());
        ps.setInt(3,objDependenciaTO.getJera_arde());
        ps.setInt(4,objDependenciaTO.getCodi_enti());
        ps.setString(5,"A");
        ps.execute();
    }
    
    public void updateDependencia(DependenciaTO objDependenciaTO)throws Exception{
        try {
        PreparedStatement ps = cn.prepareStatement("UPDATE area_dependencia SET nomb_arde=?, abre_arde=? ,jera_arde=?, codi_enti=? WHERE codi_arde=?");
        ps.setString(1,objDependenciaTO.getNomb_arde());
        ps.setString(2,objDependenciaTO.getAbre_arde());
        ps.setInt(3,objDependenciaTO.getJera_arde());
        ps.setInt(4,objDependenciaTO.getCodi_enti());
        
        ps.setInt(5,objDependenciaTO.getCodi_arde());
        
        ps.execute();
        } catch (Exception e) {
            Messagebox.show("Ocurrio una Exception Dao: " + e.getMessage(), "OK", Messagebox.OK, Messagebox.EXCLAMATION);
        }
        
    }
    
    public void deleteDependencia(int codi_arde) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE area_dependencia SET esta_arde='D' WHERE codi_arde=?");
        ps.setInt(1, codi_arde);
        ps.execute();
    }
    
    public List<BeanDependencias> getDependencias() throws Exception{
        List<BeanDependencias> lista = new ArrayList<BeanDependencias>();
        PreparedStatement ps = cn.prepareStatement("SELECT d.codi_arde,d.nomb_arde,d.esta_arde,d.abre_arde,d.jera_arde,d.codi_enti,e.nomb_enti FROM area_dependencia d, entidad e WHERE esta_arde = 'A' and d.codi_enti = e.codi_enti");
        BeanDependencias BeanD;
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            BeanD = new BeanDependencias();
            BeanD.setCodi_arde(rs.getInt(1));
            BeanD.setNomb_arde(rs.getString(2));
            BeanD.setEsta_arde(rs.getString(3));
            BeanD.setAbre_arde(rs.getString(4));
            BeanD.setJera_arde(rs.getInt(5));
            BeanD.setCodi_enti(rs.getInt(6));
            BeanD.setNomb_enti(rs.getString(7));
            
            lista.add(BeanD);
        }
        return lista;
    }
    
    public void close() throws SQLException {
        cn.close();
    }
    
    public ArrayList<Pair<Integer, String>> getEntidad() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM entidad WHERE esta_enti = 'A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
    
}
