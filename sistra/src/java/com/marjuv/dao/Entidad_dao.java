package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Entidad;
import com.marjuv.entity.Provincia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;

public class Entidad_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public Entidad_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
     public void close() throws SQLException {
        cn.close();
    }
     
     public boolean verificar_nuevo(String nomb_enti) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from entidad where nomb_enti=? AND esta_enti='A'");
        ps.setString(1, nomb_enti.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar(int codi_enti, String nomb_enti) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from entidad where codi_enti!=? AND nomb_enti=? AND esta_enti='A'");
        ps.setInt(1, codi_enti);
        ps.setString(2, nomb_enti.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public String get_nomb(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from entidad where codi_enti=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(2);
    }

    public List<Entidad> get_entidades() throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_enti, nomb_enti, dire_enti, esta_enti,DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa FROM public.entidad EN INNER JOIN Distrito DI ON DI.codi_dist=EN.codi_dist INNER JOIN Provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN Departamento DE ON DE.codi_depa=PO.codi_depa WHERE esta_enti='A' ORDER BY 1;");
        List<Entidad> lista = new ArrayList<Entidad>();
        Entidad obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            obj = new Entidad();
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setNomb_enti(rs.getString("nomb_enti"));
            obj.setDire_enti(rs.getString("dire_enti"));
            obj.setEsta_enti(rs.getString("esta_enti"));
            obj.setCodi_dist(rs.getInt("codi_dist"));
            obj.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
            lista.add(obj);
        }
        return lista;
    }

    public Entidad get_entidad(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_enti, nomb_enti, dire_enti, esta_enti,DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa FROM public.entidad EN INNER JOIN Distrito DI ON DI.codi_dist=EN.codi_dist INNER JOIN Provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN Departamento DE ON DE.codi_depa=PO.codi_depa WHERE codi_enti=? ORDER BY 1");
        ps.setInt(1, codi);
        Entidad obj;
        ResultSet rs = ps.executeQuery();
        rs.next();
        obj = new Entidad();
        obj.setCodi_enti(rs.getInt("codi_enti"));
        obj.setNomb_enti(rs.getString("nomb_enti"));
        obj.setDire_enti(rs.getString("dire_enti"));
        obj.setEsta_enti(rs.getString("esta_enti"));
        obj.setCodi_dist(rs.getInt("codi_dist"));
        obj.setObj_dist(new Distrito(
                rs.getInt("codi_dist"),
                rs.getString("nomb_dist"),
                new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                        new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        return obj;
    }
    
    public Entidad get_entidad(Entidad obj2) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_enti, nomb_enti, dire_enti, esta_enti,DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa FROM public.entidad EN INNER JOIN Distrito DI ON DI.codi_dist=EN.codi_dist INNER JOIN Provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN Departamento DE ON DE.codi_depa=PO.codi_depa "
                + "WHERE nomb_enti=? and dire_enti=?");
        ps.setString(1, obj2.getNomb_enti());
        ps.setString(2, obj2.getDire_enti());
        Entidad obj;
        ResultSet rs = ps.executeQuery();
        rs.next();
        obj = new Entidad();
        obj.setCodi_enti(rs.getInt("codi_enti"));
        obj.setNomb_enti(rs.getString("nomb_enti"));
        obj.setDire_enti(rs.getString("dire_enti"));
        obj.setEsta_enti(rs.getString("esta_enti"));
        obj.setCodi_dist(rs.getInt("codi_dist"));
        obj.setObj_dist(new Distrito(
                rs.getInt("codi_dist"),
                rs.getString("nomb_dist"),
                new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                        new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        return obj;
    }

    public boolean insert_entidad(Entidad obj) throws Exception {
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.entidad(nomb_enti, dire_enti, esta_enti,codi_dist) VALUES (?, ?, ?, ?);");
        ps.setString(1, obj.getNomb_enti().toUpperCase());
        ps.setString(2, obj.getDire_enti().toUpperCase());
        ps.setString(3, "A");
        ps.setInt(4, obj.getObj_dist().getCodi_dist());
        ps.execute();
        return true;
    }

    public boolean update_entidad(Entidad obj) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE public.entidad SET nomb_enti=?,dire_enti=?,codi_dist=? WHERE codi_enti=?");
        ps.setString(1, obj.getNomb_enti().toUpperCase());
        ps.setString(2, obj.getDire_enti().toUpperCase());
        ps.setInt(3, obj.getObj_dist().getCodi_dist());
        ps.setInt(4, obj.getCodi_enti());
        ps.execute();
        return true;
    }

    public boolean delete_entidad(int codi_enti) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE entidad SET esta_enti='D' WHERE codi_enti=?");
        ps.setInt(1, codi_enti);
        return ps.execute();
    }

    public List<Entidad> search_entidad(String search) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_enti, nomb_enti, dire_enti, esta_enti,DI.codi_dist,DI.nomb_dist,PO.codi_prov,PO.nomb_prov,DE.codi_depa,DE.nomb_depa FROM public.entidad EN INNER JOIN Distrito DI ON DI.codi_dist=EN.codi_dist INNER JOIN Provincia PO ON PO.codi_prov=DI.codi_prov INNER JOIN Departamento DE ON DE.codi_depa=PO.codi_depa"
                + " WHERE esta_enti='A' AND (nomb_enti like '%" + search.toUpperCase() + "%' or CAST (codi_enti AS character varying) like '%" + search.toUpperCase() + "%' or dire_enti like '%" + search.toUpperCase() + "%' or DI.nomb_dist like '%" + search.toUpperCase() + "%')");
        List<Entidad> lista = new ArrayList<Entidad>();
        Entidad obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            obj = new Entidad();
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setNomb_enti(rs.getString("nomb_enti"));
            obj.setDire_enti(rs.getString("dire_enti"));
            obj.setEsta_enti(rs.getString("esta_enti"));
            obj.setCodi_dist(rs.getInt("codi_dist"));
            obj.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
            lista.add(obj);
        }
        return lista;
    }

    public List<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from entidad where esta_enti='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public String get_nomb_for_carg(int codi_carg) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from cargos where codi_carg=?");
        ps.setInt(1, codi_carg);
        ResultSet rs = ps.executeQuery();
        rs.next();
        int codi_arde = rs.getInt(4);
        ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
        ps.setInt(1, codi_arde);
        rs = ps.executeQuery();
        rs.next();
        int codi_enti = rs.getInt(6);
        ps = cn.prepareStatement("select * from entidad where codi_enti=?");
        ps.setInt(1, codi_enti);
        rs = ps.executeQuery();
        rs.next();
        return rs.getString(2);
    }

}
