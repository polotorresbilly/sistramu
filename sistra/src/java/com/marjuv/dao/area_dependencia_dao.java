
package com.marjuv.dao;

import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.area_dependencia;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;

public class area_dependencia_dao {
    
    protected DataTransaction dt;
    protected Connection cn;

    public area_dependencia_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public ArrayList<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where esta_arde='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
    
    public List<BeanDependencias> Llenar_Combo_Dependencias()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde='A' and nive_arde=1");
        List<BeanDependencias> list = new ArrayList<BeanDependencias>();
        BeanDependencias ObjDependencias;
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            ObjDependencias = new BeanDependencias();
            ObjDependencias.setCodi_arde(rs.getInt("codi_arde"));
            ObjDependencias.setNomb_arde(rs.getString("nomb_arde"));
            ObjDependencias.setJera_arde(rs.getInt("jera_arde"));
            ObjDependencias.setNive_arde(rs.getInt("nive_arde"));
            list.add(ObjDependencias);
        }
        return list;
    }
    
    public List<BeanDependencias> Llenar_Combo_SubDependencias(int jerarquia)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde='A' and jera_arde="+jerarquia);
        List<BeanDependencias> list = new ArrayList<BeanDependencias>();
        BeanDependencias ObjDependencias;
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            ObjDependencias = new BeanDependencias();
            ObjDependencias.setCodi_arde(rs.getInt("codi_arde"));
            ObjDependencias.setNomb_arde(rs.getString("nomb_arde"));
            ObjDependencias.setJera_arde(rs.getInt("jera_arde"));
            ObjDependencias.setNive_arde(rs.getInt("nive_arde"));
            list.add(ObjDependencias);
        }
        return list;
    }
    
    public int get_codi_enti(int codi) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt(6);
    }
    
    public List<Pair<Integer, String>> get_select_Subgerencia(Integer x) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT ARE.*,JERA.nomb_arde as nomb_jera \n"
                + "FROM area_dependencia ARE \n"
                + "LEFT JOIN area_dependencia JERA ON ARE.jera_arde=JERA.codi_arde \n"
                + "WHERE ARE.esta_arde = 'A' and ARE.jera_arde="+x+" ORDER BY 1");
        ps.setInt(1, x);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
    
    public List<BeanDependencias> get_dependencia(int codi) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        List<BeanDependencias> lista_dependencias = new ArrayList<BeanDependencias>();
        BeanDependencias ObjDependencias = new BeanDependencias();
        ObjDependencias.setCodi_arde(rs.getInt("codi_arde"));
        ObjDependencias.setNomb_arde(rs.getString("nomb_arde"));
        lista_dependencias.add(ObjDependencias);
        return lista_dependencias;
    }
    
    public String get_abreviatura_dependencia(String nomb_arde) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from area_dependencia where nomb_arde=?");
        ps.setString(1, nomb_arde);
        ResultSet rs = ps.executeQuery();
        rs.next();
        String abre = rs.getString(4);
        return abre;
    }
    
    public List<Pair<Integer, String>> Llenar_ComboBox()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM area_dependencia WHERE esta_arde='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista_ComboBox = new ArrayList<Pair<Integer, String>>();
        while(rs.next()){
            lista_ComboBox.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
            
        }
        return lista_ComboBox;
    }
    public void close() throws SQLException {
        cn.close();
    }
    
}
