package com.marjuv.dao;

import com.marjuv.beans_view.BeanDestinatarioRes;
import com.marjuv.beans_view.BeanDestinoDis;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.ExpedienteCliente;
import com.marjuv.entity.resolucion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class resolucion_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public resolucion_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void close() throws SQLException {
        cn.close();
    }

    public boolean comprobar_nuevo(String numero) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as c from resolucion where nume_reso=?");
        ps.setString(1, numero);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt(1) == 0;
    }

    public boolean comprobar_editar(int codigo, String numero) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as c from resolucion where nume_reso=? and codi_reso!=?");
        ps.setString(1, numero);
        ps.setInt(2, codigo);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt(1) == 0;
    }

    public void insert(resolucion resolucion, int usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO resolucion(nume_reso, usre_reso, codi_fire, codi_aras, codi_tire, codi_enre, hoja_reso, "
                + "feem_reso, asun_reso, fetr_reso, rubr_reso, estr_reso, esfin_reso, codi_asre, esta_reso, usem_reso)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, resolucion.getNume_reso());
        ps.setInt(2, resolucion.getUsre_reso());
        ps.setInt(3, resolucion.getCodi_fire());
        ps.setInt(4, resolucion.getCodi_aras());
        ps.setInt(5, resolucion.getCodi_tire());
        ps.setInt(6, resolucion.getCodi_enre());
        ps.setInt(7, resolucion.getHoja_reso());
        ps.setDate(8, resolucion.getFeem_reso());
        ps.setString(9, resolucion.getAsun_reso());
        ps.setDate(10, resolucion.getFetr_reso());
        ps.setString(11, resolucion.getRubr_reso());
        ps.setString(12, resolucion.getEstr_reso());
        ps.setString(13, resolucion.getEsfin_reso());
        ps.setInt(14, resolucion.getCodi_asre());
        ps.setString(15, resolucion.getEsta_reso());
        ps.setInt(16, resolucion.getUsem_reso());

        ps.execute();

        ps = cn.prepareStatement("SELECT codi_reso FROM resolucion ORDER BY codi_reso DESC");
        ResultSet rs = ps.executeQuery();
        rs.next();
        int codi_reso = rs.getInt(1);

        for (int i = 0; i < resolucion.getExpedientes().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO expedientes_adjuntos("
                    + "nume_exad, codi_reso, corr_hist, nude_hist) VALUES(?,?,?,?)");
            ps.setString(1, resolucion.getExpedientes().get(i).getEXPNUM());
            ps.setInt(2, codi_reso);
            ps.setInt(3, resolucion.getExpedientes().get(i).getCORRELATIVO());
            ps.setInt(4, resolucion.getExpedientes().get(i).getDESTINO());
            ps.execute();
            if (resolucion.getEsfin_reso().equals("1")) {
                ps = cn.prepareStatement("SELECT public.usp_expedientes_finalizar(?,?,?,?,?);");
                ps.setString(1, resolucion.getExpedientes().get(i).getEXPNUM());
                ps.setInt(2, resolucion.getExpedientes().get(i).getCORRELATIVO());
                ps.setInt(3, resolucion.getExpedientes().get(i).getDESTINO());
                ps.setInt(4, usuario);
                ps.setString(5, String.valueOf(codi_reso));
                ps.execute();
            }
        }

        for (int i = 0; i < resolucion.getDestinos().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO distribucion_resolucion("
                    + "copi_dire, codi_reso, codi_usua, codi_cont, codi_enti, "
                    + "codi_arde, nomb_dire, codi_pare) VALUES(?,?,?,?,?,?,?,?)");
            ps.setInt(1, resolucion.getDestinos().get(i).getCopias());
            ps.setInt(2, codi_reso);
            ps.setInt(3, -1);
            ps.setInt(4, resolucion.getDestinos().get(i).getCodi_cont());
            ps.setInt(5, resolucion.getDestinos().get(i).getCodi_enti());
            ps.setInt(6, resolucion.getDestinos().get(i).getCodi_arde());
            ps.setString(7, resolucion.getDestinos().get(i).getNomb_des());
            ps.setInt(8, -1);

            ps.execute();
        }

    }

    public void update(resolucion resolucion, int usuario) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE resolucion SET nume_reso=?, usre_reso=?, codi_fire=?, codi_aras=?, codi_tire=?, codi_enre=?, hoja_reso=?, "
                + "feem_reso=?, asun_reso=?, fetr_reso=?, rubr_reso=?, estr_reso=?, esfin_reso=?, codi_asre=? "
                + "WHERE codi_reso=?");
        ps.setString(1, resolucion.getNume_reso());
        ps.setInt(2, resolucion.getUsre_reso());
        ps.setInt(3, resolucion.getCodi_fire());
        ps.setInt(4, resolucion.getCodi_aras());
        ps.setInt(5, resolucion.getCodi_tire());
        ps.setInt(6, resolucion.getCodi_enre());
        ps.setInt(7, resolucion.getHoja_reso());
        ps.setDate(8, resolucion.getFeem_reso());
        ps.setString(9, resolucion.getAsun_reso());
        ps.setDate(10, resolucion.getFetr_reso());
        ps.setString(11, resolucion.getRubr_reso());
        ps.setString(12, resolucion.getEstr_reso());
        ps.setString(13, resolucion.getEsfin_reso());
        ps.setInt(14, resolucion.getCodi_asre());
        ps.setInt(15, resolucion.getCodi_reso());
        ps.execute();

        ps = cn.prepareStatement("DELETE FROM expedientes_adjuntos WHERE codi_reso=?");
        ps.setInt(1, resolucion.getCodi_reso());
        ps.execute();

        ps = cn.prepareStatement("DELETE FROM distribucion_resolucion WHERE codi_reso=?");
        ps.setInt(1, resolucion.getCodi_reso());
        ps.execute();

        for (int i = 0; i < resolucion.getExpedientes().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO expedientes_adjuntos("
                    + "nume_exad, codi_reso, corr_hist, nude_hist) VALUES(?,?,?,?)");
            ps.setString(1, resolucion.getExpedientes().get(i).getEXPNUM());
            ps.setInt(2, resolucion.getCodi_reso());
            ps.setInt(3, resolucion.getExpedientes().get(i).getCORRELATIVO());
            ps.setInt(4, resolucion.getExpedientes().get(i).getDESTINO());
            ps.execute();

            if (resolucion.getEsfin_reso().equals("1")) {
                ps = cn.prepareStatement("SELECT public.usp_expedientes_finalizar(?,?,?,?,?);");
                ps.setString(1, resolucion.getExpedientes().get(i).getEXPNUM());
                ps.setInt(2, resolucion.getExpedientes().get(i).getCORRELATIVO());
                ps.setInt(3, resolucion.getExpedientes().get(i).getDESTINO());
                ps.setInt(4, usuario);
                ps.setString(5, String.valueOf(resolucion.getCodi_reso()));
                ps.execute();
            }
        }

        for (int i = 0; i < resolucion.getDestinos().size(); i++) {
            ps = cn.prepareStatement("INSERT INTO distribucion_resolucion("
                    + "copi_dire, codi_reso, codi_usua, codi_cont, codi_enti, "
                    + "codi_arde, nomb_dire, codi_pare) VALUES(?,?,?,?,?,?,?,?)");
            ps.setInt(1, resolucion.getDestinos().get(i).getCopias());
            ps.setInt(2, resolucion.getCodi_reso());
            ps.setInt(3, -1);
            ps.setInt(4, resolucion.getDestinos().get(i).getCodi_cont());
            ps.setInt(5, resolucion.getDestinos().get(i).getCodi_enti());
            ps.setInt(6, resolucion.getDestinos().get(i).getCodi_arde());
            ps.setString(7, resolucion.getDestinos().get(i).getNomb_des());
            ps.setInt(8, -1);

            ps.execute();
        }
    }

    public List<resolucion> search_resolucion(String search, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a "
                + " WHERE r.usem_reso=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and nume_reso like '%" + search + "%'");
        ps.setInt(1, codi_arde);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setEsta_reso(rs.getString("esta_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_gen(String estado, Date date1, Date date2, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a"
                + " WHERE r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and r.esta_reso=?");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setString(4, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();

            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();

            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                System.out.println("OK");
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_res(String estado, Date date1, Date date2, int codi_tire, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a"
                + " WHERE r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and r.codi_tire=? and r.esta_reso=?");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setInt(4, codi_tire);
        ps.setString(5, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_asu(String estado, Date date1, Date date2, int codi_asre, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a"
                + " WHERE r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and r.codi_asre=? and r.esta_reso=?");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setInt(4, codi_asre);
        ps.setString(5, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_nre(String estado, Date date1, Date date2, String nume_reso, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a"
                + " WHERE r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and r.nume_reso=? and r.esta_reso=?");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setString(4, nume_reso);
        ps.setString(5, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_tas(String estado, Date date1, Date date2, String asun_reso, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a"
                + " WHERE r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and asun_reso like '%" + asun_reso + "%' and r.esta_reso=?");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setString(4, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public List<resolucion> search_resolucion_exp(String estado, Date date1, Date date2, String nume_expe, String usua, int codi_arde) throws SQLException, Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT r.* FROM resolucion r, usuario u, area_dependencia a, expedientes_adjuntos e"
                + " WHERE r.codi_reso=e.codi_reso and r." + usua + "=u.codi_usua and u.codi_arde = a.codi_arde and a.codi_arde=? and r.feem_reso >= ? and r.feem_reso <= ? and nume_exad=? and r.esta_reso=? group by r.codi_reso");
        ps.setInt(1, codi_arde);
        ps.setDate(2, date1);
        ps.setDate(3, date2);
        ps.setString(4, nume_expe);
        ps.setString(5, estado);
        List<resolucion> lista = new ArrayList<resolucion>();
        resolucion obj;
        ResultSet rs = ps.executeQuery();
        ResultSet rs2, rs3, rs4;
        ExpedienteCliente obj_BeanExpediente;
        List<ExpedienteCliente> expedientes;
        BeanDestinoDis obj_BeanDestinoDis;
        List<BeanDestinoDis> destinatarios;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setCheck(false);

            ps = cn.prepareStatement("SELECT * FROM expedientes_adjuntos WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs2 = ps.executeQuery();
            expedientes = new ArrayList<ExpedienteCliente>();
            while (rs2.next()) {
                ps = cn.prepareStatement("SELECT * FROM public.sp_expedientes_get(?,?,?)");
                ps.setString(1, rs2.getString("nume_exad"));
                ps.setInt(2, rs2.getInt("corr_hist"));
                ps.setInt(3, rs2.getInt("nude_hist"));
                rs4 = ps.executeQuery();
                rs4.next();
                obj_BeanExpediente = new ExpedienteCliente();
                obj_BeanExpediente.setEXPNUM(rs4.getString("EXPNUM"));
                obj_BeanExpediente.setCORRELATIVO(rs4.getInt("CORRELATIVO"));
                obj_BeanExpediente.setFOLIOS(rs4.getInt("FOLIOS"));
                obj_BeanExpediente.setDOCUMENTO(rs4.getString("DOCUMENTO"));
                obj_BeanExpediente.setPROCEDIMIENTO(rs4.getString("PROCEDIMIENTO"));
                obj_BeanExpediente.setADMINISTRADO(rs4.getString("ADMINISTRADO"));
                obj_BeanExpediente.setREMITENTE(rs4.getString("REMITENTE"));
                obj_BeanExpediente.setPROVEIDO(rs4.getString("PROVEIDO"));
                obj_BeanExpediente.setDESTINO(rs4.getInt("DESTINO"));
                obj_BeanExpediente.setOBSERVACION(rs4.getString("OBSERVACION"));
                expedientes.add(obj_BeanExpediente);
            }
            obj.setExpedientes(expedientes);

            ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=?");
            ps.setInt(1, obj.getCodi_reso());
            rs3 = ps.executeQuery();
            destinatarios = new ArrayList<BeanDestinoDis>();
            while (rs3.next()) {
                obj_BeanDestinoDis = new BeanDestinoDis();
                obj_BeanDestinoDis.setCodi_arde(rs3.getInt("codi_arde"));
                obj_BeanDestinoDis.setCodi_enti(rs3.getInt("codi_enti"));
                obj_BeanDestinoDis.setCodi_cont(rs3.getInt("codi_cont"));
                obj_BeanDestinoDis.setCopias(rs3.getInt("copi_dire"));
                obj_BeanDestinoDis.setNomb_des(rs3.getString("nomb_dire"));
                destinatarios.add(obj_BeanDestinoDis);
            }
            obj.setDestinos(destinatarios);
            lista.add(obj);
        }
        return lista;
    }

    public resolucion search_resolucion(int codi_reso) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT r.*,a.nomb_arde,uno.apel_usua,uno.nomb_usua,uno.dni_usua FROM resolucion r inner join usuario u ON r.usem_reso=u.codi_usua inner join area_dependencia a ON a.codi_arde=u.codi_arde left join usuario uno on r.usno_reso=uno.codi_usua WHERE r.codi_reso=? order by 1");
        ps.setInt(1, codi_reso);
        ResultSet rs = ps.executeQuery();
        resolucion obj;
        while (rs.next()) {
            obj = new resolucion();
            obj.setNume_reso(rs.getString("nume_reso"));
            obj.setUsre_reso(rs.getInt("usre_reso"));
            obj.setCodi_fire(rs.getInt("codi_fire"));
            obj.setCodi_aras(rs.getInt("codi_aras"));
            obj.setCodi_tire(rs.getInt("codi_tire"));
            obj.setCodi_enre(rs.getInt("codi_enre"));
            obj.setCodi_reso(rs.getInt("codi_reso"));
            obj.setHoja_reso(rs.getInt("hoja_reso"));
            obj.setFeem_reso(rs.getDate("feem_reso"));
            obj.setFesa_reso(rs.getDate("fesa_reso"));
            obj.setAsun_reso(rs.getString("asun_reso"));
            obj.setFetr_reso(rs.getDate("fetr_reso"));
            obj.setRubr_reso(rs.getString("rubr_reso"));
            obj.setEstr_reso(rs.getString("estr_reso"));
            obj.setEsfin_reso(rs.getString("esfin_reso"));
            obj.setCodi_asre(rs.getInt("codi_asre"));
            obj.setUsem_reso(rs.getInt("usem_reso"));
            obj.setNomb_arde(rs.getString("nomb_arde"));
            obj.setApno_usno(rs.getString("apel_usua") + " " + rs.getString("nomb_usua"));
            obj.setDni_usno(rs.getInt("dni_usua"));
            obj.setNuno_reso(rs.getString("nuno_reso"));
            obj.setCheck(false);
            return obj;
        }
        return null;
    }

    public void notificar(List<resolucion> lst_resolucion, int codi_usua) throws SQLException, Exception {
        PreparedStatement ps;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                Calendar fecha = new GregorianCalendar();
                int anio = fecha.get(Calendar.YEAR);
                ps = cn.prepareStatement("SELECT substring(nuno_reso from 1 for 4)as nuno_reso FROM resolucion WHERE nuno_reso like ? ORDER BY nuno_reso DESC LIMIT 1");
                ps.setString(1, "%-" + anio);
                ResultSet rs = ps.executeQuery();
                String num = "0001";
                while (rs.next()) {
                    int auto = Integer.parseInt(rs.getString("nuno_reso"));
                    auto++;
                    num = padLeftZeros(auto+"", 4);
                }
                num = num + "-" + anio;
                ps = cn.prepareStatement("UPDATE resolucion SET esta_reso=?, usno_reso=?, fesa_reso=?, nuno_reso=? WHERE codi_reso=?");
                ps.setString(1, "2");
                ps.setInt(2, codi_usua);
                ps.setDate(3, new java.sql.Date(new java.util.Date().getTime()));
                ps.setString(4, num);
                ps.setInt(5, lst_resolucion.get(i).getCodi_reso());
                ps.execute();
            }
        }
    }

    public static String padLeftZeros(String str, int n) {
        return String.format("%1$" + n + "s", str).replace(' ', '0');
    }

    public String nuevo_codi_noti() throws SQLException {
        Calendar fecha = new GregorianCalendar();
        int año = fecha.get(Calendar.YEAR);
        PreparedStatement ps = cn.prepareStatement("select count(*) as c from expediente where EXTRACT(YEAR FROM fech_expe)='" + año + "'");
        ResultSet rs = ps.executeQuery();
        rs.next();
        int count = rs.getInt(1) + 1;
        String code = "000000" + count;
        String code2 = code.substring(code.length() - 6);
        String codi_expe = code2 + "-" + año;
        return codi_expe;
    }

    public void restaurar(List<resolucion> lst_resolucion) throws SQLException, Exception {
        PreparedStatement ps;
        for (int i = 0; i < lst_resolucion.size(); i++) {
            if (lst_resolucion.get(i).getCheck()) {
                ps = cn.prepareStatement("UPDATE resolucion SET esta_reso=?,nuno_reso=NULL where codi_reso=?");
                ps.setString(1, "1");
                ps.setInt(2, lst_resolucion.get(i).getCodi_reso());
                ps.execute();
            }
        }
    }

    public List<BeanDestinatarioRes> search_destinatarios(int codi_reso) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM distribucion_resolucion WHERE codi_reso=? order by 1");
        ps.setInt(1, codi_reso);
        List<BeanDestinatarioRes> lista = new ArrayList<BeanDestinatarioRes>();
        BeanDestinatarioRes obj;
        ResultSet rs = ps.executeQuery();
        int count = 1;
        while (rs.next()) {
            obj = new BeanDestinatarioRes();
            obj.setCodi_reso(codi_reso);
            obj.setCodi_usua(rs.getInt("codi_usua"));
            obj.setDni_usua(rs.getString("dni_usua"));
            obj.setNoms_usua(rs.getString("noms_usua"));
            obj.setCopi_dire(rs.getInt("copi_dire"));
            obj.setCount(count);
            obj.setFeen_dire(rs.getDate("feen_dire"));
            obj.setHora_dire2(Long.parseLong(rs.getString("hora_dire")));
            obj.setNomb_dire(rs.getString("nomb_dire"));
            obj.setCodi_dire(rs.getInt("codi_dire"));
            obj.setCodi_pare(rs.getInt("codi_pare"));
            if (rs.getInt("codi_enti") != 0) {
                obj.setTipo(1);
            } else if (rs.getInt("codi_cont") != 0) {
                obj.setTipo(2);
            } else if (rs.getInt("codi_arde") != 0) {
                obj.setTipo(3);
            }
            obj.setCodi_enti(rs.getInt("codi_enti"));
            obj.setCodi_cont(rs.getInt("codi_cont"));
            obj.setCodi_arde(rs.getInt("codi_arde"));
            count++;
            lista.add(obj);
        }
        return lista;
    }

    public void update_hora_dire(long hora_dire, int codi_dire) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE distribucion_resolucion SET hora_dire=? WHERE codi_dire=?");
        ps.setString(1, String.valueOf(hora_dire));
        ps.setInt(2, codi_dire);
        ps.execute();
    }

    public void update_feen_dire(Date feen_dire, int codi_dire) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE distribucion_resolucion SET feen_dire=? WHERE codi_dire=?");
        ps.setDate(1, feen_dire);
        ps.setInt(2, codi_dire);
        ps.execute();
    }

    public void update_codi_pare(int codi_pare, int codi_dire, String nomb_pare) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE distribucion_resolucion SET codi_pare=?, nomb_pare=? WHERE codi_dire=?");
        ps.setInt(1, codi_pare);
        ps.setInt(3, codi_dire);
        ps.setString(2, nomb_pare);
        ps.execute();
    }

    public void update_usuario(int codi_usua, String noms_usua, String dni_usua, int codi_dire) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE distribucion_resolucion SET codi_usua=?,noms_usua=?,dni_usua=? WHERE codi_dire=?");
        ps.setInt(1, codi_usua);
        ps.setString(2, noms_usua);
        ps.setString(3, dni_usua);
        ps.setInt(4, codi_dire);
        ps.execute();
    }

    public void clear_usuario(int codi_dire) throws Exception {
        PreparedStatement ps = cn.prepareStatement("UPDATE distribucion_resolucion "
                + "SET hora_dire=?,feen_dire=?,codi_pare=?,codi_usua=?,noms_usua=?,dni_usua=?,nomb_pare=? WHERE codi_dire=?");
        ps.setString(1, "1453784400000");
        ps.setDate(2, null);
        ps.setInt(3, -1);
        ps.setInt(4, -1);
        ps.setString(5, "");
        ps.setString(6, "");
        ps.setInt(7, codi_dire);
        ps.setString(8, "");
        ps.execute();
    }

}
