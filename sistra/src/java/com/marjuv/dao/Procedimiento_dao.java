/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Procedimiento_Expediente;
import com.marjuv.entity.Tipo_Expediente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BILLY
 */
public class Procedimiento_dao {
    
     protected DataTransaction dt;
    protected Connection cn;
    public Procedimiento_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
     public void close() throws SQLException {
        cn.close();
    }
    
    public List<Procedimiento_Expediente> get_procedimientos()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT codi_proc, nomb_proc, esta_proc, codi_asex  FROM public.procedimiento_expediente WHERE esta_proc='A';");
        List<Procedimiento_Expediente> lista = new ArrayList<Procedimiento_Expediente>();
        Procedimiento_Expediente obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            obj = new Procedimiento_Expediente(
               rs.getInt("codi_proc"),
               rs.getString("nomb_proc"),
            rs.getString("esta_proc"),
            rs.getInt("codi_asex"));
            lista.add(obj);
        }
        return lista;
    }
}
