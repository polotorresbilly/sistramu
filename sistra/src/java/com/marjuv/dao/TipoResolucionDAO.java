package com.marjuv.dao;

import com.marjuv.beans_view.BeanTipoResolucion;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.TipoResolucionTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.Messagebox;

public class TipoResolucionDAO {

    protected DataTransaction dt;
    protected Connection cn;

    public TipoResolucionDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(TipoResolucionTO objTipoResolucionTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO tipo_resolucion(desc_tire,esta_tire) VALUES (?,?)");
            ps.setString(1, objTipoResolucionTO.getDesc_tire().toUpperCase());
            ps.setString(2, "A");
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Insert(TipoResolucion)" + e.getMessage(), "Insert Tipo Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void update(TipoResolucionTO objTipoResolucionTO) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE tipo_resolucion SET desc_tire=? WHERE codi_tire=?");
            ps.setString(1, objTipoResolucionTO.getDesc_tire().toUpperCase());
            ps.setInt(2, objTipoResolucionTO.getCodi_tire());
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Update(TipoResolucion): " + e.getMessage(), "Update Tipo Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public void delete(int codigo) throws Exception {
        try {
            PreparedStatement ps = cn.prepareStatement("UPDATE tipo_resolucion SET esta_tire = 'D' WHERE codi_tire = ?");
            ps.setInt(1,codigo);
            ps.execute();
        } catch (SQLException e) {
            Messagebox.show("Ocurrio una Exception en el Delete(TipoResolucion)" + e.getMessage(), "Delete Tipo Resolución", Messagebox.OK, Messagebox.EXCLAMATION);
        }
    }

    public List<BeanTipoResolucion> getListTipoResolucion() throws Exception {
        List<BeanTipoResolucion> Lista = new ArrayList<BeanTipoResolucion>();
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM tipo_resolucion WHERE esta_tire = 'A' ORDER BY 1");
        ResultSet rs = ps.executeQuery();
        BeanTipoResolucion objBeanTipoResolucion;
        while (rs.next()) {
            objBeanTipoResolucion = new BeanTipoResolucion();
            objBeanTipoResolucion.setCodi_tire(rs.getInt(1));
            objBeanTipoResolucion.setDesc_tire(rs.getString(2));
            objBeanTipoResolucion.setEsta_tire(rs.getString(3));
            Lista.add(objBeanTipoResolucion);
        }
        return Lista;

    }
    
    public boolean v_desc_tire_new(String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM tipo_resolucion WHERE desc_tire =? AND esta_tire='A'");
        ps.setString(1, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public boolean v_desc_tire_edit(int codigo,String nombre)throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT COUNT(*) AS resultado FROM tipo_resolucion WHERE codi_tire !=? AND desc_tire =? AND esta_tire='A'");
        ps.setInt(1, codigo);
        ps.setString(2, nombre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if(rs.getInt(1) == 0){
            return true;
        }else{
            return false;
        }
    }
    
    public void close() throws SQLException {
        cn.close();
    }

}
