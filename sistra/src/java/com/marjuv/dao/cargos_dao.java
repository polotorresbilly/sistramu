package com.marjuv.dao;

import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.cargos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.util.Pair;

public class cargos_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public cargos_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(cargos cargos) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO cargos(esta_carg, nomb_carg, codi_arde)"
                + "VALUES(?,?,?)");
        ps.setString(1, cargos.getEsta_carg().toUpperCase());
        ps.setString(2, cargos.getNomb_carg().toUpperCase());
        ps.setInt(3, cargos.getCodi_arde());
        ps.execute();
    }

    public void update(cargos cargos) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE cargos SET nomb_carg=?, codi_arde=? WHERE codi_carg=?");
        ps.setString(1, cargos.getNomb_carg().toUpperCase());
        ps.setInt(2, cargos.getCodi_arde());
        ps.setInt(3, cargos.getCodi_carg());
        ps.execute();
    }

    public void delete(int codi_carg) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE cargos SET esta_carg='0' WHERE codi_carg=?");
        ps.setInt(1, codi_carg);
        ps.execute();
    }

    public boolean verificar_nuevo_cargos(String nomb_carg,int are_cod) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from cargos where nomb_carg=? AND codi_arde=? AND esta_carg='A'");
        ps.setString(1, nomb_carg.toUpperCase());
        ps.setInt(2, are_cod);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar_cargos(int codi_carg, String nomb_carg,int codi_arde) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from cargos where codi_carg!=? AND nomb_carg=? AND codi_arde=? AND esta_carg='A'");
        ps.setInt(1, codi_carg);
        ps.setString(2, nomb_carg.toUpperCase());
        ps.setInt(3, codi_arde);
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<BeanCargoMan> get_cargos() throws Exception {
        List<BeanCargoMan> lista = new ArrayList<BeanCargoMan>();
        PreparedStatement ps = cn.prepareStatement("select c.*, a.nomb_arde, a.nomb_arde from cargos c, area_dependencia a where c.codi_arde=a.codi_arde and c.esta_carg='A' ORDER BY 1");
        BeanCargoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanCargoMan();
            bu.setCodi_carg(rs.getInt(1));
            bu.setNomb_carg(rs.getString(3));
            bu.setCodi_arde(rs.getInt(4));
            bu.setEsta_carg(rs.getString(2));
            bu.setNomb_arde(rs.getString(5));
            lista.add(bu);
        }
        return lista;
    }

    public BeanCargoMan get_cargo(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select c.*, a.nomb_arde, a.nomb_arde from cargos c, area_dependencia a where c.codi_arde=a.codi_arde and c.codi_carg=?");
        ps.setInt(1, codi);
        BeanCargoMan bu;
        ResultSet rs = ps.executeQuery();
        rs.next();
        bu = new BeanCargoMan();
        bu.setCodi_carg(rs.getInt(1));
        bu.setNomb_carg(rs.getString(3));
        bu.setCodi_arde(rs.getInt(4));
        bu.setEsta_carg(rs.getString(2));
        bu.setNomb_arde(rs.getString(5));
        return bu;
    }

    public void close() throws SQLException {
        cn.close();
    }

    public List<Pair<Integer, String>> get_select(Integer x) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from cargos where esta_carg='A' and codi_arde=?");
        ps.setInt(1, x);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(3)));
        }
        return lista;
    }
    
    public List<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from cargos where esta_carg='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(3)));
        }
        return lista;
    }

    public String get_nomb(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from cargos where codi_carg=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(3);
    }
    
    
}
