package com.marjuv.dao;

import com.marjuv.beans_view.BeanProcedimientoMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Procedimiento_Expediente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.util.Pair;
import org.zkoss.zul.Messagebox;

public class procedimiento_expediente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public procedimiento_expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(Procedimiento_Expediente procedimiento_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO procedimiento_expediente(esta_proc, nomb_proc, codi_asex, dias_proc, cost_proc, codi_carg)"
                + "VALUES(?,?,?,?,?,?)");
        ps.setString(1, procedimiento_expediente.getEsta_proc().toUpperCase());
        ps.setString(2, procedimiento_expediente.getNomb_proc().toUpperCase());
        ps.setInt(3, procedimiento_expediente.getCodi_asex());
        ps.setInt(4, procedimiento_expediente.getDias_proc());
        ps.setString(5, procedimiento_expediente.getCost_proc());
        ps.setInt(6, procedimiento_expediente.getCodi_carg());
        ps.execute();
        
    }
    
    

    public void update(Procedimiento_Expediente procedimiento_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE procedimiento_expediente SET nomb_proc=?, codi_asex=?, dias_proc=?, cost_proc=?, codi_carg=? WHERE codi_proc=?");
        ps.setString(1, procedimiento_expediente.getNomb_proc().toUpperCase());
        ps.setInt(2, procedimiento_expediente.getCodi_asex());
        ps.setInt(3, procedimiento_expediente.getDias_proc());
        ps.setString(4, procedimiento_expediente.getCost_proc());
        ps.setInt(5, procedimiento_expediente.getCodi_carg());
        ps.setInt(6, procedimiento_expediente.getCodi_proc());
        ps.execute();
    }

    public void delete(int codi_proc) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE procedimiento_expediente SET esta_proc='D' WHERE codi_proc=?");
        ps.setInt(1, codi_proc);
        ps.execute();
    }

    public boolean verificar_nuevo_procedimiento_expediente(String nomb_proc) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from procedimiento_expediente where nomb_proc=? AND esta_proc='A'");
        ps.setString(1, nomb_proc.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public double get_total(int codi_arde, String fecha01, String fecha02) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT SUM(CAST(e.valx_expe AS REAL)) AS total FROM procedimiento_expediente as p INNER JOIN expediente as e ON p.codi_proc = e.codi_proc " +
        "INNER JOIN cargos as c ON p.codi_carg = c.codi_carg INNER JOIN area_dependencia as a ON c.codi_arde = a.codi_arde "
                + "WHERE  a.codi_arde=? and e.fech_expe >= CAST(? AS timestamp) and e.fech_expe <= CAST(? AS timestamp)");
        ps.setInt(1, codi_arde);
        ps.setString(2, fecha01);
        ps.setString(3, fecha02);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getDouble("total");
    }

    public boolean verificar_editar_procedimiento_expediente(int codi_proc, String nomb_proc) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from procedimiento_expediente where codi_proc!=? AND nomb_proc=? AND esta_proc='A'");
        ps.setInt(1, codi_proc);
        ps.setString(2, nomb_proc.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<BeanProcedimientoMan> get_procedimiento_expediente() throws Exception {
        List<BeanProcedimientoMan> lista = new ArrayList<BeanProcedimientoMan>();
        PreparedStatement ps = cn.prepareStatement("select c.*, a.desc_asex, x.nomb_carg from procedimiento_expediente c, asunto_expediente a, cargos x where c.codi_asex=a.codi_asex and c.esta_proc='A' and x.codi_carg=c.codi_carg ORDER BY 1");
        BeanProcedimientoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanProcedimientoMan();
            bu.setCodi_proc(rs.getInt("codi_proc"));
            bu.setNomb_proc(rs.getString("nomb_proc"));
            bu.setCodi_asex(rs.getInt("codi_asex"));
            bu.setEsta_proc(rs.getString("esta_proc"));
            bu.setDesc_asex(rs.getString("desc_asex"));
            bu.setCodi_carg(rs.getInt("codi_carg"));
            bu.setNomb_carg(rs.getString("nomb_carg"));
            bu.setDias_proc(rs.getInt("dias_proc"));
            bu.setCost_proc(rs.getString("cost_proc"));
            lista.add(bu);
        }
        return lista;
    }
    
    public List<BeanProcedimientoMan> get_procedimiento_expediente2(int codigo) throws Exception {
        List<BeanProcedimientoMan> lista = new ArrayList<BeanProcedimientoMan>();
        PreparedStatement ps = cn.prepareStatement("select c.*, a.desc_asex, x.nomb_carg from procedimiento_expediente c, asunto_expediente a, cargos x where c.codi_asex=a.codi_asex and c.esta_proc='A' and x.codi_carg=c.codi_carg and c.codi_asex = ? ORDER BY 1");
        ps.setInt(1, codigo);
        BeanProcedimientoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanProcedimientoMan();
            bu.setCodi_proc(rs.getInt("codi_proc"));
            bu.setNomb_proc(rs.getString("nomb_proc"));
            bu.setCodi_asex(rs.getInt("codi_asex"));
            bu.setEsta_proc(rs.getString("esta_proc"));
            bu.setDesc_asex(rs.getString("desc_asex"));
            bu.setCodi_carg(rs.getInt("codi_carg"));
            bu.setNomb_carg(rs.getString("nomb_carg"));
            bu.setDias_proc(rs.getInt("dias_proc"));
            bu.setCost_proc(rs.getString("cost_proc"));
            lista.add(bu);
        }
        return lista;
    }
    
    public List<BeanProcedimientoMan> get_procedimiento_expediente3(String busqueda) throws Exception {
        List<BeanProcedimientoMan> lista = new ArrayList<BeanProcedimientoMan>();
       PreparedStatement ps = cn.prepareStatement("select c.*, a.desc_asex, x.nomb_carg from procedimiento_expediente c, asunto_expediente a, cargos x where c.codi_asex=a.codi_asex and c.esta_proc='A' and x.codi_carg=c.codi_carg and c.nomb_proc like '%"+busqueda.toUpperCase()+"%' ORDER BY 1");
        BeanProcedimientoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanProcedimientoMan();
            bu.setCodi_proc(rs.getInt("codi_proc"));
            bu.setNomb_proc(rs.getString("nomb_proc"));
            bu.setCodi_asex(rs.getInt("codi_asex"));
            bu.setEsta_proc(rs.getString("esta_proc"));
            bu.setDesc_asex(rs.getString("desc_asex"));
            bu.setCodi_carg(rs.getInt("codi_carg"));
            bu.setNomb_carg(rs.getString("nomb_carg"));
            bu.setDias_proc(rs.getInt("dias_proc"));
            bu.setCost_proc(rs.getString("cost_proc"));
            lista.add(bu);
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where esta_proc='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
    
    public List<BeanProcedimientoMan> get_procedimiento_requisito_conforme(int codi_proc)throws SQLException{
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where esta_proc='A' AND codi_proc=?");
        ps.setInt(1, codi_proc);
        ResultSet rs = ps.executeQuery();
        BeanProcedimientoMan ObjBean = new BeanProcedimientoMan();
        List<BeanProcedimientoMan> lista = new ArrayList<BeanProcedimientoMan>();
        rs.next();
        ObjBean.setCodi_proc(rs.getInt("codi_proc"));
        ObjBean.setNomb_proc(rs.getString("nomb_proc"));
        lista.add(ObjBean);
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }

    public List<BeanProcedimientoMan> search_procedimiento_expediente(String search) throws SQLException {
        List<BeanProcedimientoMan> lista = new ArrayList<BeanProcedimientoMan>();
        PreparedStatement ps = cn.prepareStatement(
                "select c.*, a.desc_asex from procedimiento_expediente c, asunto_expediente a, cargos x "
                + "where c.codi_asex=a.codi_asex and c.codi_carg=x.codi_carg and c.esta_proc='A' and "
                + "(c.nomb_proc like '%" + search.toUpperCase() + "%' or CAST (c.codi_proc AS character varying) like '%" + search.toUpperCase() + "%' or a.desc_asex like '%" + search.toUpperCase() + "%')");
        BeanProcedimientoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanProcedimientoMan();
            bu.setCodi_proc(rs.getInt("codi_proc"));
            bu.setNomb_proc(rs.getString("nomb_proc"));
            bu.setCodi_asex(rs.getInt("codi_asex"));
            bu.setEsta_proc(rs.getString("esta_proc"));
            bu.setDesc_asex(rs.getString("desc_asex"));
            bu.setCodi_carg(rs.getInt("codi_carg"));
            bu.setNomb_carg(rs.getString("nomb_carg"));
            bu.setDias_proc(rs.getInt("dias_proc"));
            bu.setCost_proc(rs.getString("cost_proc"));
            lista.add(bu);
        }
        return lista;
    }

    public List<Pair<Integer, String>> get_select(Integer x) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where esta_proc='A' and codi_asex=?");
        ps.setInt(1, x);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public String get_nomb_proc(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where codi_proc=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(2);
    }
    
    public String get_cost_proc(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where codi_proc=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("cost_proc");
    }
    
    public int get_carg_proc(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where codi_proc=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("codi_carg");
    }
    
    public int get_codi_proc(String Nombre) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select codi_proc from procedimiento_expediente where nomb_proc=?");
        ps.setString(1, Nombre);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("codi_proc");
    }
    
    public int get_dias_proc(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from procedimiento_expediente where codi_proc=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("dias_proc");
    }

    public String get_nomb_tiex(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select * from tipo_expediente where codi_tiex=?");
        ps.setInt(1, codi);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString(2);
    }
    
    
}
