package com.marjuv.dao;

import com.marjuv.beans_view.BeanExpedientes_Vinculados;
import com.marjuv.connect.DataTransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.zul.Messagebox;

public class Expediente_VinculadoDAO {

    protected DataTransaction dt;
    protected Connection cn;

    public Expediente_VinculadoDAO() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void close() throws SQLException {
        cn.close();
    }

    public List<BeanExpedientes_Vinculados> getList_Expedientes_Vinculados(String Numero_Expediene) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT\n"
                + "eat.numero_expe_tramite AS expediente_tramitado,\n"
                + "eat.numero_adj_tramite AS expediente_adjunto,\n"
                + "eat.fecha_expe_tramite AS fecha_ingreso_expediente,\n"
                + "eat.fecha_adj_tramite AS fecha_adjunto_expediente,\n"
                + "eat.estado_adj_tramite,\n"
                + "u.nomb_usua || ' ' || u.apel_usua AS usuario,\n"
                + "eat.usuario_adj_tramite\n"
                + "FROM expediente e\n"
                + "INNER JOIN historial_expediente h ON e.nume_expe = h.nume_expe\n"
                + "INNER JOIN expediente_adjunto_tramite eat ON eat.numero_expe_tramite=e.nume_expe\n"
                + "INNER JOIN usuario u ON u.codi_usua = eat.usuario_adj_tramite\n"
                + "WHERE eat.numero_adj_tramite = '"+Numero_Expediene+"' AND eat.estado_adj_tramite = 'A';");
        ResultSet rs = ps.executeQuery();
        List<BeanExpedientes_Vinculados> list = new ArrayList<BeanExpedientes_Vinculados>();
        BeanExpedientes_Vinculados ObjBeanExpedientes_Vinculados;
        while (rs.next()) {
            ObjBeanExpedientes_Vinculados = new BeanExpedientes_Vinculados();
            ObjBeanExpedientes_Vinculados.setNumero_expediente_tramitado(rs.getString("expediente_tramitado"));
            ObjBeanExpedientes_Vinculados.setNumero_expediente_adjuntado(rs.getString("expediente_adjunto"));
            ObjBeanExpedientes_Vinculados.setFecha_adjunto_expediente(rs.getString("fecha_adjunto_expediente"));
            ObjBeanExpedientes_Vinculados.setFecha_ingreso_expediente(rs.getString("fecha_ingreso_expediente"));
            ObjBeanExpedientes_Vinculados.setNombre_usuario_adjunto(rs.getString("usuario"));
            ObjBeanExpedientes_Vinculados.setCodigo_usuario_adjunto(rs.getInt("usuario_adj_tramite"));
            list.add(ObjBeanExpedientes_Vinculados);
        }
//        Messagebox.show("" + list.get(0).getNombre_usuario_adjunto());
        return list;
    }
}
