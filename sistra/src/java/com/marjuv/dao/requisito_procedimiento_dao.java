package com.marjuv.dao;

import com.marjuv.beans_view.BeanRequisitoMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.requisito_procedimiento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class requisito_procedimiento_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public requisito_procedimiento_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(requisito_procedimiento requisito_procedimiento) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO requisito_procedimiento(esta_repr, nomb_repr, codi_proc, prec_repr,jera_repr)"
                + "VALUES(?,?,?,?,?)");
        ps.setString(1, requisito_procedimiento.getEsta_repr().toUpperCase());
        ps.setString(2, requisito_procedimiento.getNomb_repr().toUpperCase());
        ps.setInt(3, requisito_procedimiento.getCodi_proc());
        ps.setDouble(4, requisito_procedimiento.getPrec_repr());
        ps.setInt(5, requisito_procedimiento.getJera_repr());
        ps.execute();
    }

    public void update(requisito_procedimiento requisito_procedimiento) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE requisito_procedimiento SET nomb_repr=?, codi_proc=?,jera_repr=?,prec_repr=? WHERE codi_repr=?");
        ps.setString(1, requisito_procedimiento.getNomb_repr().toUpperCase());
        ps.setInt(2, requisito_procedimiento.getCodi_proc());
        ps.setInt(3, requisito_procedimiento.getJera_repr());
        ps.setDouble(4, requisito_procedimiento.getPrec_repr());
        ps.setInt(5, requisito_procedimiento.getCodi_repr());

        ps.execute();
    }

    public void delete(int codi_repr) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE requisito_procedimiento SET esta_repr='0' WHERE codi_repr=?");
        ps.setInt(1, codi_repr);
        ps.execute();
    }

    public boolean verificar_nuevo_requisito_procedimiento(String nomb_repr) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from requisito_procedimiento where nomb_repr=? AND esta_repr='1'");
        ps.setString(1, nomb_repr.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar_requisito_procedimiento(int codi_repr, String nomb_repr) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from requisito_procedimiento where codi_repr!=? AND nomb_repr=? AND esta_repr='1'");
        ps.setInt(1, codi_repr);
        ps.setString(2, nomb_repr.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<BeanRequisitoMan> get_requisito_procedimiento() throws Exception {
        List<BeanRequisitoMan> lista = new ArrayList<BeanRequisitoMan>();
//        PreparedStatement ps = cn.prepareStatement("select c.*, a.nomb_proc, a.nomb_proc from requisito_procedimiento c, procedimiento_expediente a where c.codi_proc=a.codi_proc and c.esta_repr='1'");
        PreparedStatement ps = cn.prepareStatement("SELECT ARE.*,JERA.nomb_repr as nomb_jera,PROC.nomb_proc FROM requisito_procedimiento ARE LEFT JOIN requisito_procedimiento JERA ON ARE.jera_repr=JERA.codi_repr LEFT JOIN procedimiento_expediente PROC ON ARE.codi_proc=PROC.codi_proc WHERE ARE.esta_repr = '1' ORDER BY 1");
        BeanRequisitoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanRequisitoMan();
            bu.setCodi_repr(rs.getInt(1));
            bu.setNomb_repr(rs.getString(2));
            bu.setCodi_proc(rs.getInt(4));
            bu.setPrec_repr(rs.getDouble(3));
            bu.setEsta_repr(rs.getString(5));
            bu.setJera_repr(rs.getInt(6));
            bu.setNomb_proc(rs.getString(8));
            bu.setNomb_requisito_jera(rs.getString(7));
            lista.add(bu);
        }
        return lista;
    }

    public BeanRequisitoMan get_requisito_procedimiento2(int codi_repr) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select c.*, a.nomb_proc, a.nomb_proc from requisito_procedimiento c, procedimiento_expediente a where c.codi_proc=a.codi_proc and c.esta_repr='1' and codi_repr=?");
        ps.setInt(1, codi_repr);
        BeanRequisitoMan bu;
        ResultSet rs = ps.executeQuery();
        rs.next();
        bu = new BeanRequisitoMan();
        bu.setCodi_repr(rs.getInt(1));
        bu.setNomb_repr(rs.getString(2));
        bu.setCodi_proc(rs.getInt(4));
        bu.setPrec_repr(rs.getDouble(3));
        bu.setEsta_repr(rs.getString(5));
        bu.setNomb_proc(rs.getString(6));
        return bu;
    }

    public List<BeanRequisitoMan> get_requisito_Jerarquia(int codi_repr) throws Exception {
        PreparedStatement ps = cn.prepareStatement("SELECT codi_repr,nomb_repr FROM requisito_procedimiento WHERE jera_repr=? and esta_repr='1'");
        List<BeanRequisitoMan> lista = new ArrayList<BeanRequisitoMan>();
        ps.setInt(1, codi_repr);
        BeanRequisitoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanRequisitoMan();
            bu.setCodi_repr(rs.getInt(1));
            bu.setNomb_requisito_jera(rs.getString(2));
            lista.add(bu);
        }
        return lista;
    }

    public List<BeanRequisitoMan> get_requisito_procedimiento(int codi_proc) throws Exception {
        List<BeanRequisitoMan> lista = new ArrayList<BeanRequisitoMan>();
        PreparedStatement ps = cn.prepareStatement("select c.*, a.nomb_proc, a.nomb_proc from requisito_procedimiento c, procedimiento_expediente a where c.codi_proc=a.codi_proc and c.esta_repr='1' and a.codi_proc=? and c.jera_repr='0'");
        ps.setInt(1, codi_proc);
        BeanRequisitoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanRequisitoMan();
            bu.setCodi_repr(rs.getInt(1));
            bu.setNomb_repr(rs.getString(2));
            bu.setCodi_proc(rs.getInt(4));
            bu.setPrec_repr(rs.getDouble(3));
            bu.setEsta_repr(rs.getString(5));
            bu.setNomb_proc(rs.getString(6));
            bu.setCheck(true);
            lista.add(bu);
        }
        return lista;
    }

    public List<BeanRequisitoMan> listaRequisitoAgregar() throws Exception {
        List<BeanRequisitoMan> lista = new ArrayList<BeanRequisitoMan>();
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM requisito_procedimiento WHERE esta_repr='1'");

        BeanRequisitoMan bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new BeanRequisitoMan();
            bu.setCodi_repr(rs.getInt(1));
            bu.setNomb_repr(rs.getString(2));
            bu.setJera_repr(rs.getInt(6));

            lista.add(bu);
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
