/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.select.annotation.WireVariable;

/**
 *
 * @author BILLY
 */
public class Auditoria_mantenimiento_dao {
    protected DataTransaction dt;
    protected Connection cn;
    
    @WireVariable
    private Session _sess;

    public Auditoria_mantenimiento_dao(String accion,String usuario,String tabla) throws SQLException {
        dt = new DataTransaction();
        cn = dt.getConnection();
        insert_auditoria(accion, usuario, tabla);
        close();
    }
    
    public void close() throws SQLException {
        cn.close();
    }
    
    public void insert_auditoria(String accion,String usuario,String tabla) throws SQLException{
        PreparedStatement ps = cn.prepareStatement("INSERT INTO public.auditoria_mantenimiento(operacion, fecha, usuario, data_before, data_after, tabla) VALUES (?, NOW(), ?, ?, ?, ?);");
        ps.setString(1, accion);
        ps.setString(2, usuario);
        ps.setString(3, "--------");
        ps.setString(4, "--------");
        ps.setString(5, tabla);
        ps.execute();
    }
}
