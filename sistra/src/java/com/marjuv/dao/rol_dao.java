package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.zkoss.util.Pair;
import java.util.ArrayList;
import java.util.List;

public class rol_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public rol_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public ArrayList<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from rol_usuario where esta_rol='1'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(3)));
        }
        return lista;
    }
    
    public void close() throws SQLException {
        cn.close();
    }

}
