package com.marjuv.dao;

import com.marjuv.beans_view.BeanDestinoDis;
import com.marjuv.connect.DataTransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;

public class general_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public general_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public ArrayList<String> get_menu(int codi_usua) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select DISTINCT ON (p.titu_perm)  p.* from permiso p, permisos_usuario pu where p.codi_perm = pu.codi_perm and pu.codi_usua=?");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        ArrayList<String> lista = new ArrayList<String>();
        while (rs.next()) {
            lista.add(rs.getString("titu_perm"));
        }
        return lista;
    }

    public ArrayList<ArrayList<String>> get_menu_items(int codi_usua) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select p.* from permiso p, permisos_usuario pu where p.codi_perm = pu.codi_perm and pu.codi_usua=?");
        ps.setInt(1, codi_usua);
        ResultSet rs = ps.executeQuery();
        ArrayList<ArrayList<String>> lista = new ArrayList<ArrayList<String>>();
        ArrayList<String> item;
        while (rs.next()) {
            item = new ArrayList<String>();
            item.add(rs.getString("titu_perm"));
            item.add(rs.getString("page_perm"));
            item.add(rs.getString("urlx_perm"));
            lista.add(item);
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> get_select_pare(int tipo) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from parentesco where tipo_pare=?");
        ps.setInt(1, tipo);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> get_select_tipo_expediente() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from tipo_expediente where esta_tiex='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> get_select_tipo_resolucion() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from tipo_resolucion where esta_tire='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public ArrayList<Pair<Integer, String>> get_select_fire() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT U.codi_usua AS CODIGO,U.nomb_usua||' '||U.apel_usua AS NOMBRE FROM usuario U \n"
                + "LEFT JOIN cargos C ON C.codi_carg = U.codi_carg\n"
                + "WHERE C.nomb_carg = 'SUB GERENTE' \n"
                + "OR C.nomb_carg = 'GERENTE MUNICIPAL' \n"
                + "OR C.nomb_carg = 'SECRETARIO GENERAL'\n"
                + "OR C.nomb_carg = 'ALCALDE'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt("CODIGO"), rs.getString("NOMBRE")));
        }
        return lista;
    }

    public List<BeanDestinoDis> cargar_distribucion(int codi_asre) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from asunto_destino_defecto where codi_asre=?");
        ps.setInt(1, codi_asre);
        ResultSet rs = ps.executeQuery(), rs2;
        List<BeanDestinoDis> lista = new ArrayList<BeanDestinoDis>();
        BeanDestinoDis obj;
        while (rs.next()) {
            obj = new BeanDestinoDis();
            ps = cn.prepareStatement("select * from area_dependencia where codi_arde=?");
            ps.setInt(1, rs.getInt("codi_arde"));
            rs2 = ps.executeQuery();
            rs2.next();
            obj.setCodi_arde(rs2.getInt("codi_arde"));
            obj.setNomb_des(rs2.getString("nomb_arde"));
            lista.add(obj);
        }
        return lista;
    }

    public int get_codi_fire() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from firma_resolucion where esta_fire='true' order by 1 desc");
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getInt("codi_fire");
    }

    public String get_pofi_fire() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from firma_resolucion where esta_fire='true' order by 1 desc");
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("pofi_fire");
    }

    public String get_carg_fire() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from firma_resolucion where esta_fire='true' order by 1 desc");
        ResultSet rs = ps.executeQuery();
        rs.next();
        return rs.getString("carg_fire");
    }

    public void close() throws SQLException {
        cn.close();
    }
}
