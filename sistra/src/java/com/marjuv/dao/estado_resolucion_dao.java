package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.estado_resolucion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class estado_resolucion_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public estado_resolucion_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(estado_resolucion estado_resolucion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO estado_resolucion(nomb_esre, esta_esre)"
                + "VALUES(?,?)");
        ps.setString(1, estado_resolucion.getNomb_esre().toUpperCase());
        ps.setString(2, estado_resolucion.getEsta_esre().toUpperCase());
        ps.execute();
    }

    public void update(estado_resolucion estado_resolucion) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE estado_resolucion SET nomb_esre=? WHERE codi_esre=?");
        ps.setString(1, estado_resolucion.getNomb_esre().toUpperCase());
        ps.setInt(2, estado_resolucion.getCodi_esre());
        ps.execute();
    }
    
    public void delete(int codi_esre) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE estado_resolucion SET esta_esre='0' WHERE codi_esre=?");
        ps.setInt(1, codi_esre);
        ps.execute();
    }

    public boolean verificar_nuevo_estado_resolucion(String nomb_esre) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from estado_resolucion where nomb_esre=? AND esta_esre='1'");
        ps.setString(1, nomb_esre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean verificar_editar_estado_resolucion(int codi_esre, String nomb_esre) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from estado_resolucion where codi_esre!=? AND nomb_esre=? AND esta_esre='1'");
        ps.setInt(1, codi_esre);
        ps.setString(2, nomb_esre.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<estado_resolucion> get_estados_resolucion() throws Exception {
        List<estado_resolucion> lista = new ArrayList<estado_resolucion>();
        PreparedStatement ps = cn.prepareStatement("select * from estado_resolucion where esta_esre='1'");
        estado_resolucion bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new estado_resolucion();
            bu.setCodi_esre(rs.getInt(1));
            bu.setNomb_esre(rs.getString(2));
            bu.setEsta_esre(rs.getString(3));
            lista.add(bu);
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
