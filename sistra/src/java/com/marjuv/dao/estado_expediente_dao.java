package com.marjuv.dao;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.estado_expediente;
import com.marjuv.entity.usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class estado_expediente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public estado_expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(estado_expediente estado_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO estado_expediente(nomb_esex, esta_esex)"
                + "VALUES(?,?)");
        ps.setString(1, estado_expediente.getNomb_esex().toUpperCase());
        ps.setString(2, estado_expediente.getEsta_esex().toUpperCase());
        ps.execute();
    }

    public void update(estado_expediente estado_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE estado_expediente SET nomb_esex=? WHERE codi_esex=?");
        ps.setString(1, estado_expediente.getNomb_esex().toUpperCase());
        ps.setInt(2, estado_expediente.getCodi_esex());
        ps.execute();
    }
    
    public void delete(int codi_esex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE estado_expediente SET esta_esex='0' WHERE codi_esex=?");
        ps.setInt(1, codi_esex);
        ps.execute();
    }

    public boolean verificar_nuevo_estado_expediente(String nomb_esex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from estado_expediente where nomb_esex=? AND esta_esex='A'");
        ps.setString(1, nomb_esex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean verificar_editar_estado_expediente(int codi_esex, String nomb_esex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from estado_expediente where codi_esex!=? AND nomb_esex=? AND esta_esex='A'");
        ps.setInt(1, codi_esex);
        ps.setString(2, nomb_esex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<estado_expediente> get_estados_expediente() throws Exception {
        List<estado_expediente> lista = new ArrayList<estado_expediente>();
        PreparedStatement ps = cn.prepareStatement("select * from estado_expediente where esta_esex='A' ORDER BY 1");
        estado_expediente bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new estado_expediente();
            bu.setCodi_esex(rs.getInt(1));
            bu.setNomb_esex(rs.getString(2));
            bu.setEsta_esex(rs.getString(3));
            lista.add(bu);
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
