package com.marjuv.dao;

import com.marjuv.beans_view.BeanCargoMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import com.marjuv.entity.Distrito;
import com.marjuv.entity.Provincia;
import com.marjuv.entity.cargos;
import com.marjuv.entity.representante;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.util.Pair;

public class representante_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public representante_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(representante representante) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO representante(tipo_reps, rela_reps, esta_reps,"
                + "noms_reps,docs_reps,dire_reps,codi_dist,telf_reps,"
                + "celu_reps,emai_reps)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)");
        ps.setInt(1, representante.getTipo_reps());
        ps.setInt(2, representante.getRela_reps());
        ps.setString(3, representante.getEsta_reps().toUpperCase());
        ps.setString(4, representante.getNoms_reps().toUpperCase());
        ps.setString(5, representante.getDocs_reps().toUpperCase());
        ps.setString(6, representante.getDire_reps().toUpperCase());
        ps.setInt(7, representante.getCodi_dist());
        ps.setString(8, representante.getTelf_reps().toUpperCase());
        ps.setString(9, representante.getCelu_reps().toUpperCase());
        ps.setString(10, representante.getEmai_reps().toUpperCase());
        ps.execute();
    }

    public void update(representante representante) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE representante SET tipo_reps=?, rela_reps=?,"
                + "noms_reps=?,docs_reps=?,dire_reps=?,codi_dist=?"
                + ",telf_reps=?,celu_reps=?,emai_reps=?"
                + " WHERE codi_reps=?");
        ps.setInt(1, representante.getTipo_reps());
        ps.setInt(2, representante.getRela_reps());
        ps.setString(3, representante.getNoms_reps().toUpperCase());
        ps.setString(4, representante.getDocs_reps().toUpperCase());
        ps.setString(5, representante.getDire_reps().toUpperCase());
        ps.setInt(6, representante.getCodi_dist());
        ps.setString(7, representante.getTelf_reps().toUpperCase());
        ps.setString(8, representante.getCelu_reps().toUpperCase());
        ps.setString(9, representante.getEmai_reps().toUpperCase());
        ps.setInt(10, representante.getCodi_reps());
        ps.execute();
    }

    public void delete(int codi_reps) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE representante SET esta_reps='D' WHERE codi_reps=?");
        ps.setInt(1, codi_reps);
        ps.execute();
    }

    public boolean verificar_nuevo(String noms_reps) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from representante where noms_reps=? AND esta_reps='A'");
        ps.setString(1, noms_reps.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verificar_editar(int codi_reps, String noms_reps) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from representante where codi_reps!=? AND noms_reps=? AND esta_reps='A'");
        ps.setInt(1, codi_reps);
        ps.setString(2, noms_reps.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<representante> get_representantes() throws Exception {
        List<representante> lista = new ArrayList<representante>();
        PreparedStatement ps = cn.prepareStatement("select r.*, d.nomb_dist, p.codi_prov, p.nomb_prov, x.codi_depa, x.nomb_depa from representante r, distrito d, provincia p, departamento x where r.codi_dist=d.codi_dist and d.codi_prov=p.codi_prov and p.codi_depa=x.codi_depa and r.esta_reps='A' ORDER BY 1");
        representante bu;
        ResultSet rs = ps.executeQuery(), r1;
        while (rs.next()) {
            bu = new representante();
            bu.setCodi_reps(rs.getInt("codi_reps"));
            bu.setCelu_reps(rs.getString("celu_reps"));
            bu.setCodi_dist(rs.getInt("codi_dist"));
            bu.setDire_reps(rs.getString("dire_reps"));
            bu.setDocs_reps(rs.getString("docs_reps"));
            bu.setEmai_reps(rs.getString("emai_reps"));
            bu.setEsta_reps(rs.getString("esta_reps"));
            bu.setNomb_dist(rs.getString("nomb_dist"));
            bu.setNoms_reps(rs.getString("noms_reps"));
            bu.setRela_reps(rs.getInt("rela_reps"));
            bu.setTelf_reps(rs.getString("telf_reps"));
            bu.setTipo_reps(rs.getInt("tipo_reps"));
            bu.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
            if (bu.getTipo_reps() == 1) {
                bu.setNoti_reps("ENTIDAD");
                ps = cn.prepareStatement("select * from entidad where codi_enti=?");
                ps.setInt(1, bu.getRela_reps());
                r1 = ps.executeQuery();
                r1.next();
                bu.setNoxa_reps(r1.getString("nomb_enti"));
            } else if (bu.getTipo_reps() == 2) {
                bu.setNoti_reps("CARGO");
                ps = cn.prepareStatement("select * from cargos where codi_carg=?");
                ps.setInt(1, bu.getRela_reps());
                r1 = ps.executeQuery();
                r1.next();
                bu.setNoxa_reps(r1.getString("nomb_carg"));
            }
            lista.add(bu);
        }
        return lista;
    }

    public representante get_representante(int codi) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select r.*, d.nomb_dist, p.codi_prov, p.nomb_prov, x.codi_depa, x.nomb_depa from representante r, distrito d, provincia p, departamento x where r.codi_dist=d.codi_dist and d.codi_prov=p.codi_prov and p.codi_depa=x.codi_depa and r.codi_reps=?");
        ps.setInt(1, codi);
        representante bu;
        ResultSet rs = ps.executeQuery(), r1;
        rs.next();
        bu = new representante();
        bu.setCodi_reps(rs.getInt("codi_reps"));
        bu.setCelu_reps(rs.getString("celu_reps"));
        bu.setCodi_dist(rs.getInt("codi_dist"));
        bu.setDire_reps(rs.getString("dire_reps"));
        bu.setDocs_reps(rs.getString("docs_reps"));
        bu.setEmai_reps(rs.getString("emai_reps"));
        bu.setEsta_reps(rs.getString("esta_reps"));
        bu.setNomb_dist(rs.getString("nomb_dist"));
        bu.setNoms_reps(rs.getString("noms_reps"));
        bu.setRela_reps(rs.getInt("rela_reps"));
        bu.setTelf_reps(rs.getString("telf_reps"));
        bu.setTipo_reps(rs.getInt("tipo_reps"));
        bu.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        if (bu.getTipo_reps() == 1) {
            bu.setNoti_reps("ENTIDAD");
            ps = cn.prepareStatement("select * from entidad where codi_enti=?");
            ps.setInt(1, bu.getRela_reps());
            r1 = ps.executeQuery();
            r1.next();
            bu.setNoxa_reps(r1.getString("nomb_enti"));
        } else if (bu.getTipo_reps() == 2) {
            bu.setNoti_reps("CARGO");
            ps = cn.prepareStatement("select * from cargos where codi_carg=?");
            ps.setInt(1, bu.getRela_reps());
            r1 = ps.executeQuery();
            r1.next();
            bu.setNoxa_reps(r1.getString("nomb_carg"));
        }
        return bu;
    }
    
    public representante get_representante(String noms) throws Exception {
        PreparedStatement ps = cn.prepareStatement("select r.*, d.nomb_dist, p.codi_prov, p.nomb_prov, x.codi_depa, x.nomb_depa from representante r, distrito d, provincia p, departamento x where r.codi_dist=d.codi_dist and d.codi_prov=p.codi_prov and p.codi_depa=x.codi_depa and r.noms_reps=?");
        ps.setString(1, noms);
        representante bu;
        ResultSet rs = ps.executeQuery(), r1;
        rs.next();
        bu = new representante();
        bu.setCodi_reps(rs.getInt("codi_reps"));
        bu.setCelu_reps(rs.getString("celu_reps"));
        bu.setCodi_dist(rs.getInt("codi_dist"));
        bu.setDire_reps(rs.getString("dire_reps"));
        bu.setDocs_reps(rs.getString("docs_reps"));
        bu.setEmai_reps(rs.getString("emai_reps"));
        bu.setEsta_reps(rs.getString("esta_reps"));
        bu.setNomb_dist(rs.getString("nomb_dist"));
        bu.setNoms_reps(rs.getString("noms_reps"));
        bu.setRela_reps(rs.getInt("rela_reps"));
        bu.setTelf_reps(rs.getString("telf_reps"));
        bu.setTipo_reps(rs.getInt("tipo_reps"));
        bu.setObj_dist(new Distrito(
                    rs.getInt("codi_dist"),
                    rs.getString("nomb_dist"),
                    new Provincia(rs.getInt("codi_prov"), rs.getString("nomb_prov"),
                            new Departamento(rs.getInt("codi_depa"), rs.getString("nomb_depa")))));
        if (bu.getTipo_reps() == 1) {
            bu.setNoti_reps("ENTIDAD");
            ps = cn.prepareStatement("select * from entidad where codi_enti=?");
            ps.setInt(1, bu.getRela_reps());
            r1 = ps.executeQuery();
            r1.next();
            bu.setNoxa_reps(r1.getString("nomb_enti"));
        } else if (bu.getTipo_reps() == 2) {
            bu.setNoti_reps("CARGO");
            ps = cn.prepareStatement("select * from cargos where codi_carg=?");
            ps.setInt(1, bu.getRela_reps());
            r1 = ps.executeQuery();
            r1.next();
            bu.setNoxa_reps(r1.getString("nomb_carg"));
        }
        return bu;
    }

    public void close() throws SQLException {
        cn.close();
    }

    public List<Pair<Integer, String>> get_select(int x, int tipo) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from representante where esta_reps='A' and rela_reps=? and tipo_reps=?");
        ps.setInt(1, x);
        ps.setInt(2, tipo);
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt("codi_reps"), rs.getString("noms_reps")));
        }
        return lista;
    }

}
