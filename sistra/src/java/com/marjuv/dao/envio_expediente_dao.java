package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.envio_expediente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class envio_expediente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public envio_expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(envio_expediente envio_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO envio_expediente(desc_enex, esta_enex)"
                + "VALUES(?,?)");
        ps.setString(1, envio_expediente.getDesc_enex().toUpperCase());
        ps.setString(2, envio_expediente.getEsta_enex().toUpperCase());
        ps.execute();
    }

    public void update(envio_expediente envio_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE envio_expediente SET desc_enex=? WHERE codi_enex=?");
        ps.setString(1, envio_expediente.getDesc_enex().toUpperCase());
        ps.setInt(2, envio_expediente.getCodi_enex());
        ps.execute();
    }
    
    public void delete(int codi_enex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE envio_expediente SET esta_enex='0' WHERE codi_enex=?");
        ps.setInt(1, codi_enex);
        ps.execute();
    }

    public boolean verificar_nuevo_envio_expediente(String desc_enex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from envio_expediente where desc_enex=? AND esta_enex='1'");
        ps.setString(1, desc_enex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean verificar_editar_envio_expediente(int codi_enex, String desc_enex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from envio_expediente where codi_enex!=? AND desc_enex=? AND esta_enex='1'");
        ps.setInt(1, codi_enex);
        ps.setString(2, desc_enex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<envio_expediente> get_envios_expediente() throws Exception {
        List<envio_expediente> lista = new ArrayList<envio_expediente>();
        PreparedStatement ps = cn.prepareStatement("select * from envio_expediente where esta_enex='1'");
        envio_expediente bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new envio_expediente();
            bu.setCodi_enex(rs.getInt(1));
            bu.setDesc_enex(rs.getString(2));
            bu.setEsta_enex(rs.getString(3));
            lista.add(bu);
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
