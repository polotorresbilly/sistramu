package com.marjuv.dao;

import com.marjuv.beans_view.BeanAsuntoDestinoDefecto;
import com.marjuv.beans_view.BeanDependencias;
import com.marjuv.connect.DataTransaction;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.zkoss.util.Pair;

public class asunto_destino_defecto_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public asunto_destino_defecto_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
   public List<BeanAsuntoDestinoDefecto> getAsuntosDefecto(int codigo)throws Exception{
       List<BeanAsuntoDestinoDefecto> lista = new ArrayList<BeanAsuntoDestinoDefecto>();
       PreparedStatement ps = cn.prepareStatement("SELECT aread.nomb_arde,asuntor.desc_asre,aread.codi_arde,asuntor.codi_asre FROM asunto_destino_defecto asuntodd, area_dependencia aread, asunto_resolucion asuntor where asuntodd.codi_arde=aread.codi_arde and asuntodd.codi_asre=asuntor.codi_asre and asuntor.codi_asre=?");
       ps.setInt(1, codigo);
       ResultSet rs = ps.executeQuery();
       BeanAsuntoDestinoDefecto Bean;
       while (rs.next()) {
            Bean = new BeanAsuntoDestinoDefecto();
            Bean.setNomb_arde(rs.getString(1));
            Bean.setDesc_asre(rs.getString(2));
            Bean.setCodi_arde(rs.getInt(3));
            Bean.setCodi_asre(rs.getInt(4));
            lista.add(Bean);
        }
        return lista;
   }
   
   public List<Pair<Integer, String>> cboAsunto() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("SELECT * FROM asunto_resolucion WHERE esta_asre = 'A'");
        ResultSet rs = ps.executeQuery();
        List<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }
   
   public void insert(int codi_arde,int codi_asre)throws Exception{
       PreparedStatement ps = cn.prepareStatement("INSERT INTO asunto_destino_defecto(codi_arde, codi_asre) VALUES(?,?)");
        ps.setInt(1, codi_arde);
        ps.setInt(2, codi_asre);
        ps.execute();
   }
   public boolean delete(int codi_arde,int codi_asre) throws Exception {
        PreparedStatement ps = cn.prepareStatement("delete from asunto_destino_defecto where codi_arde=? and codi_asre=?");
        ps.setInt(1, codi_arde);
        ps.setInt(2, codi_asre);
        return ps.execute();
    }
   
   public void close() throws SQLException {
        cn.close();
    }
}
