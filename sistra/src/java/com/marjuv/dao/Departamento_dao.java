/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.marjuv.dao;

import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.Departamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author BILLY
 */
public class Departamento_dao {
    protected DataTransaction dt;
    protected Connection cn;

    public Departamento_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }
    
    public List<Departamento> listar_departamento()throws Exception{
        PreparedStatement ps = cn.prepareStatement("SELECT DE.codi_depa,DE.nomb_depa FROM departamento DE;");
        List<Departamento> lista = new ArrayList<Departamento>();
        Departamento obj;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {            
            obj = new Departamento(
               rs.getInt("codi_depa"),
               rs.getString("nomb_depa"));
            lista.add(obj);
        }
        return lista;
    }
    
     public void close() throws SQLException {
        cn.close();
    }
}
