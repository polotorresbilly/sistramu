package com.marjuv.dao;

import com.marjuv.beans_view.BeanUsuarioMan;
import com.marjuv.connect.DataTransaction;
import com.marjuv.entity.asunto_expediente;
import com.marjuv.entity.usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import org.zkoss.util.Pair;

public class asunto_expediente_dao {

    protected DataTransaction dt;
    protected Connection cn;

    public asunto_expediente_dao() {
        dt = new DataTransaction();
        cn = dt.getConnection();
    }

    public void insert(asunto_expediente asunto_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "INSERT INTO asunto_expediente(desc_asex, esta_asex)"
                + "VALUES(?,?)");
        ps.setString(1, asunto_expediente.getDesc_asex().toUpperCase());
        ps.setString(2, asunto_expediente.getEsta_asex().toUpperCase());
        ps.execute();
    }

    public void update(asunto_expediente asunto_expediente) throws Exception {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE asunto_expediente SET desc_asex=? WHERE codi_asex=?");
        ps.setString(1, asunto_expediente.getDesc_asex().toUpperCase());
        ps.setInt(2, asunto_expediente.getCodi_asex());
        ps.execute();
    }
    
    public void delete(int codi_asex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement(
                "UPDATE asunto_expediente SET esta_asex='D' WHERE codi_asex=?");
        ps.setInt(1, codi_asex);
        ps.execute();
    }

    public boolean verificar_nuevo_asunto_expediente(String desc_asex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from asunto_expediente where desc_asex=? AND esta_asex='A'");
        ps.setString(1, desc_asex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public boolean verificar_editar_asunto_expediente(int codi_asex, String desc_asex) throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select count(*) as contador from asunto_expediente where codi_asex!=? AND desc_asex=? AND esta_asex='A'");
        ps.setInt(1, codi_asex);
        ps.setString(2, desc_asex.toUpperCase());
        ResultSet rs = ps.executeQuery();
        rs.next();
        if (rs.getInt(1) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<asunto_expediente> get_asuntos_expediente() throws Exception {
        List<asunto_expediente> lista = new ArrayList<asunto_expediente>();
        PreparedStatement ps = cn.prepareStatement("select * from asunto_expediente where esta_asex='A' ORDER BY 1");
        asunto_expediente bu;
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            bu = new asunto_expediente();
            bu.setCodi_asex(rs.getInt(1));
            bu.setDesc_asex(rs.getString(2));
            bu.setEsta_asex(rs.getString(3));
            lista.add(bu);
        }
        return lista;
    }
    
    public ArrayList<Pair<Integer, String>> get_select() throws SQLException {
        PreparedStatement ps = cn.prepareStatement("select * from asunto_expediente where esta_asex='A'");
        ResultSet rs = ps.executeQuery();
        ArrayList<Pair<Integer, String>> lista = new ArrayList<Pair<Integer, String>>();
        while (rs.next()) {
            lista.add(new Pair<Integer, String>(rs.getInt(1), rs.getString(2)));
        }
        return lista;
    }

    public void close() throws SQLException {
        cn.close();
    }
}
