/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// fixes for newer userAgents (IE11, Edge)
var bodycls, agent = zk.agent;
if ((bodycls = agent.indexOf("edge/")) > 0) {
zk.edge = agent.substring(bodycls + 5);
zk.edge = parseFloat(zk.edge) || zk.edge;
zk.ie = 12;
bodycls = 'safari safari' + Math.floor(zk.safari);
jq(function ()

{ jq(document.body).removeClass(bodycls).addClass('ie ie12'); }
);
zk.safari_ = zk.safari = null;
zk.vendor_ = zk.vendor = 'ms';
} else if (zk.ff && agent.indexOf("trident/") > 0 && document.documentMode) {
// fix for IE 11
zk.ie = document.documentMode;
bodycls = 'gecko gecko' + Math.floor(zk.ff);
jq(function ()

{ jq(document.body).removeClass(bodycls).addClass('ie ie' + Math.floor(zk.ie)); }
);
zk.ff = zk.gecko = null;
zk.vendor_ = zk.vendor = 'ms';
}
